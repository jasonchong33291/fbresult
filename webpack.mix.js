const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
.postCss('resources/css/tms.css', 'public/css')
    .vue()
    .sass('resources/sass/app.scss', 'public/css');
mix.copyDirectory('resources/js/tmsjs', 'public/js/tmsjs');
mix.copyDirectory("resources/images", "public/assets/images");

mix.setResourceRoot("../");
 