<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//test123
Route::get('/', function () {
	return redirect('/home');
});

Route::get('api/cronresetdeposit', 'App\Http\Controllers\ApiController@cronresetdeposit');
Auth::routes(['register' => false]);
Auth::routes();
//test2
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/groups', 'App\Http\Controllers\HomeController@getGroups')->name('groups');
Route::get('/detail', 'App\Http\Controllers\HomeController@detail')->name('detail');

Route::get('api/getLeague', 'App\Http\Controllers\ApiController@getLeague');
Route::get('api/getLeagueTable', 'App\Http\Controllers\ApiController@getLeagueTable');
Route::get('api/getTeam', 'App\Http\Controllers\ApiController@getTeam');
Route::get('api/getTeamsquad', 'App\Http\Controllers\ApiController@getTeamsquad');
Route::get('api/getTeammember', 'App\Http\Controllers\ApiController@getTeammember');
Route::get('api/getUpcomingEvent', 'App\Http\Controllers\ApiController@getUpcomingEvent');
Route::get('api/getEventView', 'App\Http\Controllers\ApiController@getEventView');
Route::get('api/getEventView2', 'App\Http\Controllers\ApiController@getEventView2');
Route::get('api/updateGroup', 'App\Http\Controllers\ApiController@updateGroup');
Route::get('api/getInplayEvent', 'App\Http\Controllers\ApiController@getInplayEvent');
Route::get('api/getEndedEvent', 'App\Http\Controllers\ApiController@getEndedEvent');


Route::get('api/language', 'App\Http\Controllers\HomeController@changeLanguage');
Route::get('api/getHomeResult', 'App\Http\Controllers\HomeController@getHomeResult');
Route::get('api/getHomeResult', 'App\Http\Controllers\HomeController@getHomeResult');
Route::get('api/isstart', 'App\Http\Controllers\HomeController@isstart');
Route::get('api/isstart2', 'App\Http\Controllers\HomeController@isstart2');


Route::get('api/getFTLeague', 'App\Http\Controllers\ApiController@getFTLeague');
Route::get('api/getFTTeamsInformation', 'App\Http\Controllers\ApiController@getFTTeamsInformation');


//test

Route::get('/dashboard', 'App\Http\Controllers\HomeController@index')->name('dashboard');


Route::group(['middleware' => 'auth'], function () {
	Route::get('profile', ['as' => 'profile.edit', 'uses' => 'App\Http\Controllers\ProfileController@edit']);
	Route::put('profile', ['as' => 'profile.update', 'uses' => 'App\Http\Controllers\ProfileController@update']);
	Route::get('upgrade', function () {
		return view('pages.upgrade');
	})->name('upgrade');
	Route::get('map', function () {
		return view('pages.maps');
	})->name('map');
	Route::get('icons', function () {
		return view('pages.icons');
	})->name('icons');
	Route::get('table-list', function () {
		return view('pages.tables');
	})->name('table');
	Route::put('profile/password', ['as' => 'profile.password', 'uses' => 'App\Http\Controllers\ProfileController@password']);


	Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');


	Route::get('permissions', 'App\Http\Controllers\PermissionsController@permissionslisting')->name('permissions');
	Route::get('permissions/update', 'App\Http\Controllers\PermissionsController@update');


	Route::get('deposit/p2plisting', 'App\Http\Controllers\DepositController@listing')->name('depositp2p');
	Route::get('deposit/qrlisting', 'App\Http\Controllers\DepositController@listing')->name('depositqr');
	Route::get('deposit/view', 'App\Http\Controllers\DepositController@view');
	Route::post('deposit/filter', 'App\Http\Controllers\DepositController@filter');
	Route::get('deposit/export', 'App\Http\Controllers\DepositController@export');
	Route::post('deposit/action', 'App\Http\Controllers\DepositController@action');
	
	Route::get('bank/listing', 'App\Http\Controllers\BankController@listing')->name('bank');
	Route::post('bank/create', 'App\Http\Controllers\BankController@create');
	Route::get('bank/createForm', 'App\Http\Controllers\BankController@createForm');
	Route::get('bank/updateForm', 'App\Http\Controllers\BankController@createForm');
	Route::get('bank/deleteBank', 'App\Http\Controllers\BankController@delete');
	Route::post('bank/updatebankstatus', 'App\Http\Controllers\BankController@updatebankstatus');

	Route::get('bankmaster/listing', 'App\Http\Controllers\BankMasterController@listing')->name('bankmaster');
	Route::post('bankmaster/create', 'App\Http\Controllers\BankMasterController@create');
	Route::get('bankmaster/createForm', 'App\Http\Controllers\BankMasterController@createForm');
	Route::get('bankmaster/updateForm', 'App\Http\Controllers\BankMasterController@createForm');
	Route::get('bankmaster/deleteBankmaster', 'App\Http\Controllers\BankMasterController@delete');
	Route::post('bankmaster/updatebankmasterstatus', 'App\Http\Controllers\BankMasterController@updatebankmasterstatus');
	Route::post('bankmaster/updatebankmasterapplyto', 'App\Http\Controllers\BankMasterController@updatebankmasterapplyto');

	Route::get('bankmasterqr/listing', 'App\Http\Controllers\BankMasterQRController@listing')->name('bankmasterqr');
	Route::post('bankmasterqr/create', 'App\Http\Controllers\BankMasterQRController@create');
	Route::get('bankmasterqr/createForm', 'App\Http\Controllers\BankMasterQRController@createForm');
	Route::get('bankmasterqr/updateForm', 'App\Http\Controllers\BankMasterQRController@createForm');
	Route::get('bankmasterqr/deleteBankmasterqr', 'App\Http\Controllers\BankMasterQRController@delete');
	Route::post('bankmasterqr/updatebankmasterqrstatus', 'App\Http\Controllers\BankMasterQRController@updatebankmasterqrstatus');
	
	Route::get('merchant/listing', 'App\Http\Controllers\MerchantController@listing')->name('merchant');
	Route::post('merchant/create', 'App\Http\Controllers\MerchantController@create');
	Route::get('merchant/createForm', 'App\Http\Controllers\MerchantController@createForm');
	Route::get('merchant/updateForm', 'App\Http\Controllers\MerchantController@createForm');
	Route::get('merchant/manageBankMerchantForm', 'App\Http\Controllers\MerchantController@manageBankMerchantForm');
	Route::post('merchant/merchantbank/addnewbankline', 'App\Http\Controllers\MerchantController@addnewbankline');
	Route::post('merchant/merchantbank/create', 'App\Http\Controllers\MerchantController@merchantbankCreate');
	Route::post('merchant/merchantbank/deletebankline', 'App\Http\Controllers\MerchantController@deletebankline');
	Route::get('merchant/deleteMerchant', 'App\Http\Controllers\MerchantController@delete');
	Route::get('merchant/validateapiendpoint', 'App\Http\Controllers\MerchantController@validateapiendpoint');
	
	
	Route::get('merchant/manageSettlementBankMerchantForm', 'App\Http\Controllers\MerchantController@manageSettlementBankMerchantForm');
	Route::post('merchant/merchantsettlementbank/addnewbankline', 'App\Http\Controllers\MerchantController@addnewsettlementbankline');
	Route::post('merchant/merchantsettlementbank/create', 'App\Http\Controllers\MerchantController@merchantsettlementbankCreate');
	Route::post('merchant/merchantsettlementbank/deletebankline', 'App\Http\Controllers\MerchantController@deletesettlementbankline');
	

	Route::get('user', 'App\Http\Controllers\UserController@userlisting')->name('users');
	Route::any('user/validateusername', 'App\Http\Controllers\UserController@validateusername');
	Route::post('user/create', 'App\Http\Controllers\UserController@create');
	Route::get('user/createForm', 'App\Http\Controllers\UserController@createForm');
	Route::get('user/updateForm', 'App\Http\Controllers\UserController@createForm');
	Route::get('user/changepasswordForm', 'App\Http\Controllers\UserController@changepasswordForm');
	Route::post('user/changepassword', 'App\Http\Controllers\UserController@changepassword');

	Route::get('permissions', 'App\Http\Controllers\UserGroupController@listing')->name('permissions');
	Route::any('permissions/create', 'App\Http\Controllers\UserGroupController@create');
	Route::any('permissions/createForm', 'App\Http\Controllers\UserGroupController@createForm');
	Route::any('permissions/updateForm', 'App\Http\Controllers\UserGroupController@createForm');


	
	Route::any('report/sales', 'App\Http\Controllers\ReportController@sales')->name('salesreport');
	Route::any('report/salessearchsummary', 'App\Http\Controllers\ReportController@salessearchsummary');
	Route::any('report/salessearchdetail', 'App\Http\Controllers\ReportController@salessearchdetail');
	Route::any('report/salessearchbank', 'App\Http\Controllers\ReportController@salessearchbank');
	
	Route::get('settlement/listing', 'App\Http\Controllers\SettlementController@listing')->name('settlement');
	Route::post('settlement/create', 'App\Http\Controllers\SettlementController@create');
	Route::get('settlement/createForm', 'App\Http\Controllers\SettlementController@createForm');
	Route::post('settlement/getSettlementBank', 'App\Http\Controllers\SettlementController@getSettlementBank');
	Route::post('settlement/getSettlementBankAccount', 'App\Http\Controllers\SettlementController@getSettlementBankAccount');
	Route::post('settlement/filter', 'App\Http\Controllers\SettlementController@filter');
	Route::post('settlement/action', 'App\Http\Controllers\SettlementController@action');
	Route::post('settlement/setaction', 'App\Http\Controllers\SettlementController@setaction');
	
	Route::get('wallet/listing', 'App\Http\Controllers\WalletController@listing')->name('wallet');
	Route::post('wallet/create', 'App\Http\Controllers\WalletController@create');
	Route::get('wallet/createForm', 'App\Http\Controllers\WalletController@createForm');
	Route::get('wallet/updateForm', 'App\Http\Controllers\WalletController@createForm');
	Route::get('wallet/deleteWallet', 'App\Http\Controllers\WalletController@delete');
	Route::post('wallet/updatewalletstatus', 'App\Http\Controllers\WalletController@updatewalletstatus');


	
	


});
