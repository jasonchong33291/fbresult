
<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Deposit Detail Management">
  <meta name="author" content="">
  <title>Deposit Detail Management</title>
  <!-- Favicon -->
  <link rel="icon" href="../assets/img/brand/favicon.png" type="image/png">
  <!-- Fonts -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700">
  <!-- Icons -->
  <link rel="stylesheet" href="../assets/vendor/nucleo/css/nucleo.css" type="text/css">
  <link rel="stylesheet" href="../assets/vendor/@fortawesome/fontawesome-free/css/all.min.css" type="text/css">
  <link rel="stylesheet" href="../assets/vendor/datatables.net-bs4/css/dataTables.bootstrap4.min.css" type="text/css">
  <link rel="stylesheet" href="../assets/vendor/select2/dist/css/select2.min.css" type="text/css">
  <link rel="stylesheet" href="../assets/vendor/sweetalert2/dist/sweetalert2.min.css" type="text/css">
  <!-- Argon CSS -->
  <link rel="stylesheet" href="../assets/css/argon.css?v=1.2.0" type="text/css">
  <link rel="stylesheet" href="../css/tms.css" type="text/css">
  <style>
    .deposit_title{
      font-size:16px !important;
      font-weight:700 !important;
    }
    .deposit_detail_table .left_title,.deposit_detail_table .right_title {
      text-align:right;
      font-weight: 700;
    }
    .table th, .table td{
      padding: 0.5rem;
    }
  </style>
</head>

<body>
  <!-- Sidenav -->
  {{-- @include('layouts.navbars.sidebar')  --}}
  <!-- Main content -->
  <div class="main-content" id="panel">
    <!-- Topnav -->
    {{-- @include('layouts.navbars.topheader')  --}}
    <!-- Header -->
    <!-- Header -->
    <div class="header bg-primary pb-6  py-4">
      <div class="container-fluid">
        <div class="header-body">
          <div class="row align-items-center py-4 hide">
            <div class="col-lg-6 col-7">
              <!-- <h6 class="h2 text-white d-inline-block mb-0">TMS</h6> -->
              <nav aria-label="breadcrumb" class="d-none d-md-inline-block ">
                <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                  <li class="breadcrumb-item"><a href="dashboard"><i class="fas fa-home"></i></a></li>
                  <li class="breadcrumb-item"><a href="/deposit">Deposit Listing</a></li>
                  <li class="breadcrumb-item"><a href="/deposit/view">Deposit Detail</a></li>
                </ol>
              </nav>
            </div>

            <div class="col-lg-6 col-5 text-right">
             
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--6">
      <div class="row">
        <div class="col">



        <div class="card">

            <div class="card-body">
              <table class = 'table deposit_detail_table'>
                <thead>
                  <tr>
                    <th class = 'deposit_title' colspan = '4'>Deposit Detail</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td class = 'left_title'>DEPOSIT NO</td>
                    <td class = 'left_value'>{{$data['deposit']->deposit_no}}</td>
                    <td class = 'right_title'>DEPOSIT DATE/TIME</td>
                    <td class = 'right_value'>{{$data['deposit']->deposit_date}}</td>
                  </tr>
                  <tr>
                    <td class = 'left_title'>DEPOSIT MODE</td>
                    <td class = 'left_value'>{{strtoupper($data['deposit']->deposit_mode)}}</td>
                    <td class = 'right_title'>PROCESS TIME</td>
                    <td class = 'right_value'>
                      <?php
                      if(($data['deposit']->deposit_date != null) || ($data['deposit']->deposit_enddate != null)){
                        $expiry_time = new DateTime($data['deposit']->deposit_enddate);
                        $current_date = new DateTime($data['deposit']->deposit_date);
                        $diff = $expiry_time->diff($current_date);
                        echo $diff->format('%i min and %S sec'); 
                      }
                      ?>
                    </td>
                  </tr>
                  <tr>
                    <td class = 'left_title'>MERCHANT CODE</td>
                    <td class = 'left_value'>{{strtoupper($data['deposit']->merchant_code)}}</td>
                    <td class = 'right_title'>STATUS</td>
                    <td class = 'right_value'>
                      <?php
                      if ($data['deposit']->deposit_status == 'Success') {
                          echo  '<span class="badge badge-success ">Success</span>';
                      } elseif ($data['deposit']->deposit_status == 'Registered') {
                          echo  '<span class="badge badge-warning ">Registered</span>';
                      } elseif (($data['deposit']->deposit_status == 'Failled') || ($data['deposit']->deposit_status == 'Cancelled')) {
                          echo '<span class="badge badge-danger ">'.$data['deposit']->deposit_status.'</span>';
                      } elseif ($data['deposit']->deposit_status == 'Processing') {
                          echo  '<span class="badge badge-violet ">Processing</span>';
                      } else {
                          echo  '<span class="badge badge-black ">Unknown</span>';
                      }
                      if($data['deposit']->deposit_updatestatus_json != null){
                        $json = json_decode($data['deposit']->deposit_updatestatus_json,true);
                        if($json){
                          if (($data['deposit']->deposit_status == 'Failled') || ($data['deposit']->deposit_status == 'Cancelled')) {
                            echo '<br><span class="badge badge-danger ">'.$json['msg'].'</span>';
                          }
                        }
                      }

                      ?>
                      
                      </td>
                  </tr>
                  <tr>
                    <td class = 'left_title'>MERCHANT REF.NO.</td>
                    <td class = 'left_value'>{{($data['deposit']->deposit_merchant_refno1)}}</td>
                    <td class = 'right_title'>ERROR MESSAGE</td>
                    <td class = 'right_value'></td>
                  </tr>
                  <tr>
                    <td class = 'left_title'>BANK REFERENCE</td>
                    <td class = 'left_value'></td>
                    <td class = 'right_title'>TRANSACTION PROVIDER MESSAGE</td>
                    <td class = 'right_value'></td>
                  </tr>
                  <tr>
                    <td class = 'left_title'>FROM/TO</td>
                    <td class = 'left_value'>
                      <?php
                      if($data['deposit']->deposit_mode == 'qr'){
                        echo "/" . $data['deposit']->bankmasterqr_name;
                      }else{
                        echo strtoupper($data['deposit']->from_bank) . "/" . strtoupper($data['deposit']->to_bank);
                      }
                      
                      ?>
                      {{-- {{strtoupper($data['deposit']->from_bank)}}/{{strtoupper($data['deposit']->to_bank)}} --}}
                    </td>
                    <td class = 'right_title'>REMARKS</td>
                    <td class = 'right_value'></td>
                  </tr>
                  <tr>
                    <td class = 'left_title'>ACCOUNT NO.</td>
                    <td class = 'left_value'>{{$data['deposit']->deposit_bankto_account_number}}</td>
                    <td class = 'right_title'>CUSTOMER ID/NAME</td>
                    <td class = 'right_value'>{{($data['deposit']->deposit_merchant_refno2)}}</td>
                  </tr>
                  <tr>
                    <td class = 'left_title'>DEPOSIT AMOUNT</td>
                    <td class = 'left_value' style = 'color:green;font-weight:700;'>{{num_format($data['deposit']->deposit_amount)}}</td>
                    <td class = 'right_title'>SET ACTION DETAIL</td>
                    <td class = 'right_value'>
                      <?php
                      if($data['deposit']->deposit_bankresponse != null){
                        $json = json_decode($data['deposit']->deposit_bankresponse,true);
                        if(($json) && (isset($json['updatedbyname']))){
                          echo "Update By : " . $json['updatedbyname'] . "<br>";
                        }
                        if(($json) && (isset($json['updateddatetime']))){
                          echo "Update DateTime : " . $json['updateddatetime'] . "<br>";
                        }
                      }
                      ?>
                    </td>
                  </tr>
                  <tr>
                    <td class = 'left_title'>MERCHANT FEE</td>
                    <td class = 'left_value' style = 'color:green;font-weight:700;'>
                      <?php
                      if($data['deposit']->deposit_internalstatuscode == '80001'){
                        echo num_format($data['deposit']->deposit_merchant_fee);
                      }
                      ?>
                    </td>
                    <td class = 'right_title'></td>
                    <td class = 'right_value'></td>
                  </tr>
                  <tr>
                    <td class = 'left_title'>NETT AMOUNT</td>
                    <td class = 'left_value' style = 'color:green;font-weight:700;'>
                      <?php
                      if($data['deposit']->deposit_internalstatuscode == '80001'){
                        echo num_format($data['deposit']->deposit_nett_amount);
                      }
                      ?>
                      </td>
                    <td class = 'right_title'></td>
                    <td class = 'right_value'></td>
                  </tr>
                  <tr>
                    <td class = 'left_title'>DEPOSIT POSTBACK (JSON) RESULT</td>
                    <td class = 'left_value'>
                      <?php
                      if($data['deposit']->deposit_postdata_status == '1'){
                        echo "Success";
                      }else{
                        echo "Failed";
                      }
                      ?>
                      <br>{{strtoupper($data['deposit']->deposit_postdata_count)}} times</td>
                    <td class = 'right_title'></td>
                    <td class = 'right_value'></td>
                  </tr>
                  <tr>
                    <td class = 'left_title'>Screenshots</td>
                    <td class = 'left_value'>
                      <?php
						//dd($data['deposit']->deposit_process_ip);
                      if($data['screenshots']){
                        foreach($data['screenshots'] as $i => $s){
                          echo ($i+1) . " : " . "<a href = 'http://".$data['deposit']->deposit_process_ip."/".$s."' target = '_blank'>http://".$data['deposit']->deposit_process_ip."/".$s."</a><br>";
                        }
                      }
                      ?>
                    <td class = 'right_title'></td>
                    <td class = 'right_value'></td>
                  </tr>
                </tbody>
              </table>
                  


              
            </div>
          </div>
        </div>

      </div>

      <!-- Footer -->
      @include('pages.footer') 
    </div>
  </div>


  <!-- Argon Scripts -->
  <!-- Core -->
  <script src="../assets/vendor/jquery/dist/jquery.min.js"></script>
  <script src="../assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
  <script src="../assets/vendor/js-cookie/js.cookie.js"></script>
  <script src="../assets/vendor/jquery.scrollbar/jquery.scrollbar.min.js"></script>
  <script src="../assets/vendor/jquery-scroll-lock/dist/jquery-scrollLock.min.js"></script>
  <script src="../assets/vendor/datatables.net/js/jquery.dataTables.min.js"></script>
  <script src="../assets/vendor/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
  <script src="../assets/vendor/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
  <script src="../assets/vendor/select2/dist/js/select2.min.js"></script>
  <script src="../assets/vendor/sweetalert2/dist/sweetalert2.min.js"></script>
  <script src="../js/tmsjs/jqueryvalidation/jquery.validate.1.8.js"></script>
  <script src="../js/tmsjs/loadingoverlay/loadingoverlay.min.js"></script>
  <!-- Argon JS -->
  <script src="../assets/js/argon.js?v=1.2.0"></script>

  <script>
    $(document).ready( function () {
      

    });

  </script>
</body>

</html>