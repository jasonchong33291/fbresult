<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Deposit Listing Management">
    <meta name="author" content="">
    <title>Deposit Listing Management</title>
    <!-- Favicon -->
    <link rel="icon" href="../assets/img/brand/favicon.png" type="image/png">
    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700">
    <!-- Icons -->
    <link rel="stylesheet" href="../assets/vendor/nucleo/css/nucleo.css" type="text/css">
    <link rel="stylesheet" href="../assets/vendor/@fortawesome/fontawesome-free/css/all.min.css" type="text/css">
    <link rel="stylesheet" href="../assets/vendor/datatables.net-bs4/css/dataTables.bootstrap4.min.css" type="text/css">
    <link rel="stylesheet" href="../assets/vendor/select2/dist/css/select2.min.css" type="text/css">
    <link rel="stylesheet" href="../assets/vendor/sweetalert2/dist/sweetalert2.min.css" type="text/css">
    <!-- Argon CSS -->
    <link rel="stylesheet" href="../assets/css/argon.css?v=1.2.0" type="text/css">
    <link rel="stylesheet" href="../css/tms.css" type="text/css">
    <style>
        body {
        overflow-y: hidden; /* Hide vertical scrollbar */
        overflow-x: hidden; /* Hide horizontal scrollbar */
        }
        #form_table tr:hover{
            background:#c6c8c6c4;
            color:black;
            cursor:pointer;
        }
        .text-light{
            color: black !important;
        }
        a.text-light:hover, a.text-light:focus {
            color: black !important;
        }
        .bottom_success_table{
            width:100%;
            margin-top:20px;
        }
        .bottom_success_table tr th{
            background:black;
            color:white;
            padding: 10px 25px;
            font-size: 13px;
            text-align:center;
        }
        .bottom_success_table tr td{
            color:Green;
            padding: 10px 25px;
            font-size: 13px;
            font-weight:700;
            text-align:center;
        }
        .form-group{
            margin-bottom: 0.5rem;
        }
        .header{
            padding-top: 10px;
        }
        .hide{
            display:none;
        }
        .search_div_mobile{
            display:none;
            margin-left:20px;
        }

        @media only screen and (max-width: 600px) {
            .search_div_mobile{
                display:block;
            }
            #search_div{
                display:none;
            }
        }
    </style>
</head>

<body>
    <!-- Sidenav -->
    @include('layouts.navbars.sidebar')
    <!-- Main content -->
    <div class="main-content" id="panel">
        <!-- Topnav -->
        {{-- @include('layouts.navbars.topheader') --}}
        <!-- Header -->
        <!-- Header -->
        <div class="header bg-primary pb-6">
            <div class="container-fluid">
                <div class="header-body">
                    <div class="row align-items-center ">
                        <div class="col-lg-6 col-7">
                            <!-- <h6 class="h2 text-white d-inline-block mb-0">TMS</h6> -->
                            {{-- <nav aria-label="breadcrumb" class="d-none d-md-inline-block ">
                                <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                                    <li class="breadcrumb-item"><a href="dashboard"><i class="fas fa-home"></i></a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="/deposit/listing">Deposit Listing</a></li>
                                </ol>
                            </nav> --}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Page content -->
        <div class="container-fluid mt--6">
            <div class="row">
                <div class="col">
                    <div class="card">
                        <!-- Card header -->
                        <div class="card-header border-0" style = 'display:flex'>
                            <div class="col-lg-6 col-5 text-left">
                                <h3 class="mb-0">Deposit Listing</h3>
                            </div>
                            <div class="col-lg-6 col-5 text-right">
                                <a href="#" class="btn btn-sm btn-neutral" id = 'deposit_export_btn'>Export Excel</a>
                            </div>
                        </div>
                        <div class = 'search_div_mobile'>
                            <a href="javascript:void(0)" class="btn btn-sm btn-info" id = 'search_div_btn_mobile'  >Search</a>
                        </div>
                        <div id = 'search_div'  >
                            <div class="row col-md-12">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="form-control-label filter-name">Date From</label><br>
                                        <div class="input-group input-group-alternative">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="ni ni-calendar-grid-58"></i>
                                                </span>
                                            </div>
                                            <input class="form-control datepicker" placeholder="Select date" type="text"
                                                name="filter_date_from" id="filter_date_from"
                                                value="{{ $data['filter_date_from'] != null ? $data['filter_date_from'] : '' }}"
                                                onkeydown="return false">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="form-control-label filter-name">Date To</label><br>
                                        <div class="input-group input-group-alternative">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="ni ni-calendar-grid-58"></i>
                                                </span>
                                            </div>
                                            <input class="form-control datepicker" placeholder="Select date" type="text"
                                                name="filter_date_to" id="filter_date_to"
                                                value="{{ $data['filter_date_to'] != null ? $data['filter_date_to'] : '' }}"
                                                onkeydown="return false">
                                        </div>
                                    </div>
                                </div>
                                <?php
                                if((Auth::user()->user_group_id == 1) || (Auth::user()->user_group_id == 2)){//super admin & admin
                                ?>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="form-control-label filter-name" for = 'filter_merchant'>Merchant</label><br>
                                        <select name="filter_merchant" class="form-control select2" id="filter_merchant" >
                                            <option value="">All</option>
                                            <?php
                                            foreach($data['merchant'] as $m){
                                            ?>
                                            <option value = '{{$m->merchant_id}}' >{{$m->merchant_code}}</option>
                                            <?php
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <?php }?>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="form-control-label filter-name" for = 'filter_status'>Status</label><br>
                                        <select name="filter_status" class="form-control " id="filter_status" >
                                            <option value="">All</option>
                                            <option value="Registered">Registered</option>
                                            <option value="Processing">Processing</option>
                                            {{-- <option value="Cancel">Cancel</option> --}}
                                            <option value="Failled">Failled</option>
                                            <option value="Success">Success</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row col-md-12">
                                <?php
                                if(Route::currentRouteName() == "depositqr"){
                                ?>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="form-control-label filter-name" for = 'filter_banktype'>From</label><br>
                                        <select name="filter_banktype" class="form-control select2" id="filter_banktype" >
                                            <option value="">All</option>
                                            <?php
                                            foreach($data['walletlist'] as $m){
                                            ?>
                                            <option value = '{{$m->wallet_code}}' >{{$m->wallet_name}}</option>
                                            <?php
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <?php
                                if((Auth::user()->user_group_id == 1) || (Auth::user()->user_group_id == 2) || (Auth::user()->user_group_id == 7) || (Auth::user()->user_group_id == 9)){  
                                ?>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="form-control-label filter-name" for = 'filter_qrto'>QR Code</label><br>
                                        <select name="filter_qrto" class="form-control select2" id="filter_qrto" >
                                            <option value="">All</option>
                                            <?php
                                            foreach($data['bankMasterQR'] as $m){
                                            ?>
                                            <option value = '{{$m->bankmasterqr_id}}' >{{$m->bankmasterqr_name}}</option>
                                            <?php
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <?php }?>
                                <?php
                                }else{
                                ?>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="form-control-label filter-name" for = 'filter_bank_from'>From Bank</label><br>
                                        <select name="filter_bank_from" class="form-control select2" id="filter_bank_from" >
                                            <option value="">All</option>
                                            <?php
                                            foreach($data['bank'] as $m){
                                            ?>
                                            <option value = '{{$m->bank_id}}' >{{$m->bank_name}}</option>
                                            <?php
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3 hide">
                                    <div class="form-group">
                                        <label class="form-control-label filter-name" for = 'filter_bank_to'>Bank To</label><br>
                                        <select name="filter_bank_to" class="form-control select2" id="filter_bank_to" >
                                            <option value="">All</option>
                                            <?php
                                            foreach($data['bank'] as $m){
                                            ?>
                                            <option value = '{{$m->bank_id}}' >{{$m->bank_name}}</option>
                                            <?php
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <?php }?>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="form-control-label filter-name" for = 'filter_bank_to'>Search</label><br>
                                        <input class="form-control " type="text" name="filter_text" id="filter_text">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Light table -->
                        <div class="table-responsive table-bottom-scrollbarbar" >
                            <div id = 'deposit_listing_div' style = 'height:400px;overflow-y:scroll'>
                                <table class="table align-items-center table-flush" id='form_table'>
                                    <thead class="thead-light">
                                        <tr>
                                            <th scope="col" class="sort">DEPOSIT <br>DATE/TIME</th>
                                            <th scope="col" class="sort">DEPOSIT <br>ID </th>
                                            <th scope="col" class="sort">MERCHANT <br>CODE</th>
                                            <th scope="col" class="sort">MERCHANT <br>REF. NO.</th>
                                            <th scope="col" class="sort">CUSTOMER ID<br>/NAME</th>
                                            <?php
                                            if(Route::currentRouteName() == "depositp2p"){
                                            ?>
                                            <th scope="col" class="sort">FROM <br>BANK</th>
                                            <?php }?>
                                            <th scope="col" class="sort">TO <br>BANK</th>
                                            <th scope="col" class="sort">DEPOSIT <br>AMOUNT</th>
                                            <th scope="col" class="sort">MERCHANT <br>FEE</th>
                                            <th scope="col" class="sort">NETT <br>AMOUNT</th>
                                            <th scope="col" class="sort">STATUS</th>
                                            <th scope="col" class="sort"></th>
                                        </tr>
                                    </thead>
                                    <tbody class="list">
                                        

                                    </tbody>
                                </table>
                            </div>
                            <hr style = 'margin-top:5px;margin-bottom:5px;'>

                            <table class = 'bottom_success_table'>
                                <tr>
                                    <th>TOTAL SUCCESS COUNT</th>
                                    <th>SUM DEPOSIT AMT</th>
                                    <th>SUM MERCHANT FEE</th>
                                    <th>SUM NETT AMT</th>
                                </tr>
                                <tr>
                                    <td id = 'bottom_success_count'></td>
                                    <td id = 'bottom_success_deposit'></td>
                                    <td id = 'bottom_success_fee'></td>
                                    <td id = 'bottom_success_nett'></td>
                                </tr>
                            </table>
                        </div>

                    </div>
                </div>
            </div>

            <!-- Footer -->
            @include('pages.footer')
        </div>
    </div>
    <!-- Argon Scripts -->
    <!-- Core -->
    <script src="../assets/vendor/jquery/dist/jquery.min.js"></script>
    <script src="../assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <script src="../assets/vendor/js-cookie/js.cookie.js"></script>
    <script src="../assets/vendor/jquery.scrollbar/jquery.scrollbar.min.js"></script>
    <script src="../assets/vendor/jquery-scroll-lock/dist/jquery-scrollLock.min.js"></script>
    <script src="../assets/vendor/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../assets/vendor/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
    <script src="../assets/vendor/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
    <script src="../assets/vendor/select2/dist/js/select2.min.js"></script>
    <script src="../assets/vendor/sweetalert2/dist/sweetalert2.min.js"></script>
    <script src="../js/tmsjs/loadingoverlay/loadingoverlay.min.js"></script>
    <!-- Argon JS -->
    <script src="../assets/js/argon.js?v=1.2.0"></script>

    <script>
        var orderTable = $("#form_table").DataTable({});
        $(window).resize(function() {
            resize();
        });
        $(document).ready(function() {

            filterList();
            $('.select2').select2();

            $("#filter_date_from,#filter_date_to,#filter_merchant,#filter_status,#filter_bank_from,#filter_bank_to,#filter_qrto,#filter_banktype").on("change", function() {
                filterList();
            });
            $("#filter_text").on("keyup", function() {
                filterList();
            });
            resize();

            $('#search_div_btn_mobile').click(function(){
                
                    if($('#search_div').is(':visible')){
                        $('#search_div').css('display','none');
                    }else{
                        $('#search_div').css('display','block');
                    }
            });

            $('#deposit_export_btn').click(function(){
                var parameter = 'filter_bank_from='+$('#filter_bank_from').val()+"&filter_bank_to="+$('#filter_bank_to').val();
                    parameter += '&filter_date_from='+$('#filter_date_from').val()+"&filter_date_to="+$('#filter_date_to').val();
                    parameter += '&filter_status='+$('#filter_status').val()+"&filter_deposit_mode=" + @json($data['deposit_mode']);


                window.location.href  = '/deposit/export?'+parameter;
            });
            $(document).on("click", '.click_link', function(){
               
                window.open('/deposit/view?deposit_id='+$(this).parent().find("td:first").find('div').attr('deposit_id'));
            });

            <?php
            if(canAccess('action_deposit')){
            ?>
                $(document).on("click", '.set_action_class', function(){
                    if($(this).attr('pid') == 'success'){
                        var msg = "Confirm set Success?";
                    }else{
                        var msg = "Confirm set Fail?";
                    }

                    var deposit_id = $(this).attr('deposit_id');
                    var action = $(this).attr('pid');
                    var line = $(this).attr('line');

                    Swal({  
                        title: msg,  
                        type: "question",  
                        html:
                            "<input type = 'checkbox' id = 'sendpostback' value = '1'/> Please tick the checkbox if you want system postback.",
                        showCloseButton: true,
                        showCancelButton: true,
                        focusConfirm: true,
                    }).then(function(r) {
                 
                        if((r.dismiss =='cancel') || (r.dismiss =='close')){
                            return false;
                        }
                        if(!r.value){
                            return false;
                        }
                        if($('#sendpostback').is(":checked")){
                            var ispostback = 1;
                        }else{
                            var ispostback = 0;
                        }

                        $.ajax({
                                url: '/deposit/action',
                                type: 'POST',
                                data: "_token={{ csrf_token() }}&deposit_id="+deposit_id+"&action="+action+"&ispostback="+ispostback,
                                cache: false,
                                beforeSend: function() {
                                    $.LoadingOverlay("show");
                                },
                                error: function(xhr) {
                                    Swal({
                                        type: 'error',
                                        title: "Something went wrong.",
                                        showConfirmButton: false,
                                        allowOutsideClick: false,
                                        timer: 1500
                                    })
                                    $.LoadingOverlay("hide");
                                },
                                success: function(jsonObj) {
                                    $.LoadingOverlay("hide");
                                    if (jsonObj.status == 1) {
                                        if (jsonObj.msg != "") {
                                            Swal({
                                                type: 'success',
                                                title: jsonObj.msg,
                                                showConfirmButton: false,
                                                allowOutsideClick: false,
                                                timer: 1500
                                            }).then(function() {
                                                $('.action_status' + line).removeClass('badge-warning');
                                                $('.action_status' + line).removeClass('badge-danger');
                                                $('.action_status' + line).removeClass('badge-violet');
                                                $('.action_status' + line).removeClass('badge-black');
                                                $('.action_status' + line).removeClass('badge-success');
                                                if(action == 'success'){
                                                    $('.action_status' + line).addClass('badge-success');
                                                    $('.action_status' + line).text('SUCCESS');
                                                }else{
                                                    $('.action_status' + line).addClass('badge-danger');
                                                    $('.action_status' + line).text('FAILLED');
                                                }

                                                filterList();
                                            });
                                        }

                                    } else {
                                        Swal({
                                            type: 'error',
                                            title: jsonObj.msg,
                                            showConfirmButton: false,
                                            allowOutsideClick: false,
                                            timer: 1500
                                        })
                                    }
                                }
                        });


                    }); 

                });
            <?php
            }
            ?>
        });
        function resize(){
            var summary_table = $(".bottom_success_table").height();
            let search_div = document.getElementById('search_div');

            document.getElementById("deposit_listing_div").style.height = ($(window).height() - summary_table - (search_div.offsetHeight + 20) - 80) + "px"; 
        }
        function filterList() {
            orderTable.destroy();

            var from = $('#filter_date_from').val();
            var to = $('#filter_date_to').val();
            var sales = $('#filter_sales_person').val();
            var filter_merchant = $('#filter_merchant').val();
            var filter_status = $('#filter_status').val();
            var filter_bank_from = $('#filter_bank_from').val();
            var filter_bank_to = $('#filter_bank_to').val();
            var filter_text = $('#filter_text').val();
            var filter_banktype = $('#filter_banktype').val();
            var filter_qrto = $('#filter_qrto').val();
            var deposit_mode = @json($data['deposit_mode']);
            
            var data = {
                "_token": "{{ csrf_token() }}",
                "from": from,
                "to": to,
                "sales": sales,
                "filter_merchant": filter_merchant,
                "filter_status": filter_status,
                "filter_bank_from": filter_bank_from,
                "filter_bank_to": filter_bank_to,
                "filter_text": filter_text,
                "filter_banktype": filter_banktype,
                "filter_qrto": filter_qrto,
                "deposit_mode": deposit_mode,
                "route": "<?php echo Route::currentRouteName();?>",
            };
            refreshData(data);
            runScroll();
        }




        function refreshData(data) {
            orderTable = $("#form_table").DataTable({
                "serverSide": true,
                "processing": true,
                "searching": false,
                "ajax": {
                    "url": "filter",
                    "type": "post",
                    "dataType": "json",
                    "data": data,
                },
                "responsive": true,
                initComplete: function (k,json) {
                
                    $('#bottom_success_count').html(json.total_success_count);
                    $('#bottom_success_deposit').html(json.total_deposit_amount);
                    $('#bottom_success_fee').html(json.deposit_merchant_fee);
                    $('#bottom_success_nett').html(json.deposit_nett_amount);
                    resize();
                },
                createdRow: function( row, data, dataIndex ) {
                
                    // Set the data-status attribute, and add a class
                   // if(row)

                    for(var i=0;i<11;i++){
                        $( row ).find('td:eq('+i+')').addClass('click_link');
                    }
                    
                },
                language: {
                    paginate: {
                        next: '<i class="fas fa-chevron-right"></i>',
                        previous: '<i class="fas fa-chevron-left"></i>'
                    }
                },
                "bLengthChange" : false, //thought this line could hide the LengthMenu
                "bInfo":false,  
                dom: 'lpftrip',
                "iDisplayLength": 10,
                "order": [
                    [0, "desc"]
                ]

            });
            //orderTable.column(0).visible(false);

        }
        function runScroll() {
            var tableContainer = $(".table-bottom-scrollbarbar");
            var table = $(".table-bottom-scrollbarbar table");
            var fakeContainer = $(".table-top-scrollbar");
            var fakeDiv = $(".table-top-scrollbar div");

            var tableWidth = table.width();


            fakeDiv.width(tableWidth + tableWidth / 2);


            fakeContainer.scroll(function() {
                tableContainer.scrollLeft(fakeContainer.scrollLeft());
            });
            tableContainer.scroll(function() {
                fakeContainer.scrollLeft(tableContainer.scrollLeft());
            });
        }
    </script>
</body>

</html>
