<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Settlement Management">
    <meta name="author" content="">
    <title>Settlement Management</title>
    <!-- Favicon -->
    <link rel="icon" href="../assets/img/brand/favicon.png" type="image/png">
    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700">
    <!-- Icons -->
    <link rel="stylesheet" href="../assets/vendor/nucleo/css/nucleo.css" type="text/css">
    <link rel="stylesheet" href="../assets/vendor/@fortawesome/fontawesome-free/css/all.min.css" type="text/css">
    <link rel="stylesheet" href="../assets/vendor/datatables.net-bs4/css/dataTables.bootstrap4.min.css" type="text/css">
    <link rel="stylesheet" href="../assets/vendor/select2/dist/css/select2.min.css" type="text/css">
    <!-- Argon CSS -->
    <link rel="stylesheet" href="../assets/css/argon.css?v=1.2.0" type="text/css">
    <link rel="stylesheet" href="../css/tms.css" type="text/css">
    <style>
        body {
        overflow-y: hidden; /* Hide vertical scrollbar */
        overflow-x: hidden; /* Hide horizontal scrollbar */
        }
        #form_table tr:hover{
            background:#5ee47fc4;
            color:white;
        }
        .text-light{
            color: black !important;
        }
        a.text-light:hover, a.text-light:focus {
            color: black !important;
        }

        .bottom_success_table{
            width:100%;
            margin-top:20px;
        }
        .bottom_success_table tr th{
            background:black;
            color:white;
            padding: 10px 25px;
            font-size: 13px;
            text-align:center;
        }
        .bottom_success_table tr td{
            color:Green;
            padding: 10px 25px;
            font-size: 13px;
            font-weight:700;
            text-align:center;
        }
        .form-group{
            margin-bottom: 0.5rem;
        }
        .header{
            padding-top: 10px;
        }
    </style>
</head>

<body>
    <!-- Sidenav -->
    @include('layouts.navbars.sidebar')
    <!-- Main content -->
    <div class="main-content" id="panel">
        <!-- Topnav -->
        {{-- @include('layouts.navbars.topheader') --}}
        <!-- Header -->
        <!-- Header -->
        <div class="header bg-primary pb-6">
            <div class="container-fluid">
                <div class="header-body">
                    {{-- <div class="row align-items-center py-4">
                        <div class="col-lg-6 col-7">
                            <!-- <h6 class="h2 text-white d-inline-block mb-0">TMS</h6> -->
                            <nav aria-label="breadcrumb" class="d-none d-md-inline-block ">
                                <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                                    <li class="breadcrumb-item"><a href="dashboard"><i class="fas fa-home"></i></a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="/settlement/listing">Settlement Listing</a></li>
                                </ol>
                            </nav>
                        </div>
                        <div class="col-lg-6 col-5 text-right">
                            <?php
                            if(canAccess('create_settlement')){
                            ?>
                            <a href="/settlement/createForm" class="btn btn-sm btn-neutral">Create New</a>
                            <?php }?>
                        </div>
                    </div> --}}
                </div>
            </div>
        </div>
        <!-- Page content -->
        <div class="container-fluid mt--6">
            <div class="row">
                <div class="col">
                    <div class="card">
                        <!-- Card header -->
                        <div class="card-header border-0" style = 'display:flex'>
                            <div class="col-lg-6 col-5 text-left">
                                <h3 class="mb-0">Settlement Listing</h3>
                            </div>
                            <div class="col-lg-6 col-5 text-right">
                                <a href="/settlement/createForm" class="btn btn-sm btn-neutral">Create New</a>
                            </div>
                        </div>
                        <div id = 'search_div'>
                            <div class="row col-md-12">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="form-control-label filter-name">Date From</label><br>
                                        <div class="input-group input-group-alternative">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="ni ni-calendar-grid-58"></i>
                                                </span>
                                            </div>
                                            <input class="form-control datepicker" placeholder="Select date" type="text"
                                                name="filter_date_from" id="filter_date_from"
                                                value="{{ $data['filter_date_from'] != null ? $data['filter_date_from'] : '' }}"
                                                onkeydown="return false">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="form-control-label filter-name">Date To</label><br>
                                        <div class="input-group input-group-alternative">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="ni ni-calendar-grid-58"></i>
                                                </span>
                                            </div>
                                            <input class="form-control datepicker" placeholder="Select date" type="text"
                                                name="filter_date_to" id="filter_date_to"
                                                value="{{ $data['filter_date_to'] != null ? $data['filter_date_to'] : '' }}"
                                                onkeydown="return false">
                                        </div>
                                    </div>
                                </div>
                                <?php
                                if((Auth::user()->user_group_id == 1) || (Auth::user()->user_group_id == 2)){//super admin & admin
                                ?>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="form-control-label filter-name" for = 'filter_merchant'>Merchant</label><br>
                                        <select name="filter_merchant" class="form-control select2" id="filter_merchant" >
                                            <option value="">All</option>
                                            <?php
                                            foreach($data['merchant'] as $m){
                                            ?>
                                            <option value = '{{$m->merchant_id}}' >{{$m->merchant_code}}</option>
                                            <?php
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <?php }?>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="form-control-label filter-name" for = 'filter_status'>Status</label><br>
                                        <select name="filter_status" class="form-control select2" id="filter_status" >
                                            <option value="">All</option>
                                            <option value="Approved">Approved</option>
                                            <option value="Pending">Pending</option>
                                            <option value="Rejected">Rejected</option>
                                            <option value="Draft">Draft</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {{-- <div class="table-top-scrollbar mb-3">
                            <div>&nbsp;</div>
                        </div> --}}
                        <!-- Light table -->
                        <div class="table-responsive table-bottom-scrollbarbar">
                            <div id = 'settlement_listing_div' style = 'height:480px;overflow-y:scroll'>
                                <table class="table align-items-center table-flush" id='form_table'>
                                    <thead class="thead-light">
                                        <tr>
                                            <th scope="col" class="sort">REQUESTED <br>DATE/TIME</th>
                                            <th scope="col" class="sort">ACTION <br>DATE/TIME</th>
                                            <th scope="col" class="sort">SETTLEMENT<br>ID </th>
                                            <th scope="col" class="sort">MERCHANT <br>CODE</th>
                                            <th scope="col" class="sort">MERCHANT <br>REF. NO</th>
                                            <th scope="col" class="sort">BANK <br>NAME</th>
                                            <th scope="col" class="sort">ACCOUNT <br>NAME</th>
                                            <th scope="col" class="sort">ACCOUNT <br>NO.</th>
                                            <th scope="col" class="sort">FROM <br>AMOUNT</th>
                                            <th scope="col" class="sort">TO <br>AMOUNT</th>
                                            <th scope="col" class="sort">STATUS</th>
                                            <th scope="col" class="sort">REMARKS</th>
                                            <th scope="col" class="sort"></th>
                                        </tr>
                                    </thead>
                                    <tbody class="list">
                                        

                                    </tbody>
                                </table>
                            </div>


                            <hr style = 'margin-top:10px;margin-bottom:10px;'>

                            <table class = 'bottom_success_table'>
                                <tr>
                                    <th>TOTAL APPROVED COUNT</th>
                                    <th>SUM PENDING AMOUNT</th>
                                    <th>SUM SETTLEMENT AMOUNT</th>
                                </tr>
                                <tr>
                                    <td id = 'bottom_success_count'></td>
                                    <td id = 'bottom_success_pending'></td>
                                    <td id = 'bottom_success_settlement'></td>
                                </tr>
                            </table>

                        </div>

                    </div>
                </div>
            </div>

            <!-- Footer -->
            @include('pages.footer')
        </div>
    </div>
    <!-- Argon Scripts -->
    <!-- Core -->
    <script src="../assets/vendor/jquery/dist/jquery.min.js"></script>
    <script src="../assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <script src="../assets/vendor/js-cookie/js.cookie.js"></script>
    <script src="../assets/vendor/jquery.scrollbar/jquery.scrollbar.min.js"></script>
    <script src="../assets/vendor/jquery-scroll-lock/dist/jquery-scrollLock.min.js"></script>
    <script src="../assets/vendor/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../assets/vendor/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
    <script src="../assets/vendor/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
    <script src="../assets/vendor/select2/dist/js/select2.min.js"></script>
    <!-- Argon JS -->
    <script src="../assets/js/argon.js?v=1.2.0"></script>

    <script>
        var orderTable = $("#form_table").DataTable({});
        $(window).resize(function() {
            resize();
        });
        $(document).ready(function() {

            filterList();
            $('.select2').select2();

            $("#filter_date_from,#filter_date_to,#filter_merchant,#filter_status").on("change", function() {
                filterList();
            });
            resize();
            
        });
        function filterList() {
            orderTable.destroy();

            var from = $('#filter_date_from').val();
            var to = $('#filter_date_to').val();
            var sales = $('#filter_sales_person').val();
            var filter_merchant = $('#filter_merchant').val();
            var filter_status = $('#filter_status').val();
            var deposit_mode = @json($data['deposit_mode']);
            
            var data = {
                "_token": "{{ csrf_token() }}",
                "from": from,
                "to": to,
                "sales": sales,
                "filter_merchant": filter_merchant,
                "filter_status": filter_status,
                "deposit_mode": deposit_mode,
            };
            refreshData(data);
            runScroll();
        }

        function resize(){
            var summary_table = $(".bottom_success_table").height();
            let search_div = document.getElementById('search_div');

            document.getElementById("settlement_listing_div").style.height = ($(window).height() - summary_table - (search_div.offsetHeight + 20) - 80) + "px"; 
        }
        function refreshData(data) {
            orderTable = $("#form_table").DataTable({
                "serverSide": true,
                "processing": true,
                "ajax": {
                    "url": "filter",
                    "type": "post",
                    "dataType": "json",
                    "data": data,
                },
                "responsive": true,
                initComplete: function (k,json) {
                    
                    $('#bottom_success_count').html(json.settlement_success_count);
                    $('#bottom_success_settlement').html(json.settlement_nett_amount);
                    $('#bottom_success_pending').html(json.settlement_pending_amount);
                    resize();
                },
                language: {
                    paginate: {
                        next: '<i class="fas fa-chevron-right"></i>',
                        previous: '<i class="fas fa-chevron-left"></i>'
                    }
                },
                "bLengthChange" : false, //thought this line could hide the LengthMenu
                "bInfo":false,
                "iDisplayLength": 50,
                "order": [
                    [0, "desc"]
                ]

            });
            //orderTable.column(0).visible(false);

        }
        function runScroll() {
            var tableContainer = $(".table-bottom-scrollbarbar");
            var table = $(".table-bottom-scrollbarbar table");
            var fakeContainer = $(".table-top-scrollbar");
            var fakeDiv = $(".table-top-scrollbar div");

            var tableWidth = table.width();


            fakeDiv.width(tableWidth + tableWidth / 2);


            fakeContainer.scroll(function() {
                tableContainer.scrollLeft(fakeContainer.scrollLeft());
            });
            tableContainer.scroll(function() {
                fakeContainer.scrollLeft(tableContainer.scrollLeft());
            });
        }
    </script>
</body>

</html>
