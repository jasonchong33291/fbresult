<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Settlement Management">
    <meta name="author" content="">
    <title>Settlement Management</title>
    <!-- Favicon -->
    <link rel="icon" href="../assets/img/brand/favicon.png" type="image/png">
    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700">
    <!-- Icons -->
    <link rel="stylesheet" href="../assets/vendor/nucleo/css/nucleo.css" type="text/css">
    <link rel="stylesheet" href="../assets/vendor/@fortawesome/fontawesome-free/css/all.min.css" type="text/css">
    <link rel="stylesheet" href="../assets/vendor/datatables.net-bs4/css/dataTables.bootstrap4.min.css" type="text/css">
    <link rel="stylesheet" href="../assets/vendor/select2/dist/css/select2.min.css" type="text/css">
    <link rel="stylesheet" href="../assets/vendor/sweetalert2/dist/sweetalert2.min.css" type="text/css">
    <!-- Argon CSS -->
    <link rel="stylesheet" href="../assets/css/argon.css?v=1.2.0" type="text/css">
    <link rel="stylesheet" href="../css/tms.css" type="text/css">
</head>

<body>
    <!-- Sidenav -->
    @include('layouts.navbars.sidebar')
    <!-- Main content -->
    <div class="main-content" id="panel">
        <!-- Topnav -->
        @include('layouts.navbars.topheader')
        <!-- Header -->
        <!-- Header -->
        <div class="header bg-primary pb-6">
            <div class="container-fluid">
                <div class="header-body">
                    <div class="row align-items-center py-4">
                        <div class="col-lg-6 col-7">
                            <!-- <h6 class="h2 text-white d-inline-block mb-0">TMS</h6> -->
                            <nav aria-label="breadcrumb" class="d-none d-md-inline-block ">
                                <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                                    <li class="breadcrumb-item"><a href="dashboard"><i class="fas fa-home"></i></a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="/settlement/listing">Settlement Management</a>
                                    </li>
                                </ol>
                            </nav>
                        </div>

                        <div class="col-lg-6 col-5 text-right">

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Page content -->
        <div class="container-fluid mt--6">
            <div class="row">
                <div class="col">

                    <div class="card">

                        <div class="card-body">

                            <form id='settlementform_id'>
                                <h6 class="heading-small text-muted mb-4">Settlement Information</h6>
                                <div class="row">
                                   <?php
                                        if(($data['settlement']->settlement_status == 'Pending') || 
                                           ($data['settlement']->settlement_status == 'Approved')){   
                                            $disabled = " disabled ";
                                            $readonly = " READONLY";

                                        }else{
                                            $disabled = "";
                                            $readonly = "";
                                        }
                                   ?>
                                    <div class="col-md-9" style="border-right: 1px solid #e6e6e6;">
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label class="form-control-label" for="">Settlement No. : </label>
                                                 
                                                    <input readonly type="text" style="text-transform:uppercase" class="form-control" value="<?php if($data['settlement']->settlement_no == null){ echo "-- system generate --";}else{ echo $data['settlement']->settlement_no;}?>" />

                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label class="form-control-label" for="settlement_from_amount">Available Balance : </label>
                                                    <input <?php echo $readonly;?> type="text"  readonly style="text-align:right" name = "settlement_from_amount" id = "settlement_from_amount"
                                                    class="form-control"
                                                    value="{{ ($data['available_balance']) }}" />
                                                </div>
                                            </div>
                                        </div>

                                            <div class="row">
                                                <?php
                                                if((Auth::user()->user_group_id == 1) || (Auth::user()->user_group_id == 2) || (Auth::user()->user_group_id == 7) || (Auth::user()->user_group_id == 9)){
                                                ?>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label class="form-control-label" for="">Merchant: </label>
                                                        <select <?php echo $disabled;?> name="settlement_merchant_id" class="form-control select2" id="settlement_merchant_id" >
                                                            <option value="">All</option>
                                                            <?php
                                                            foreach($data['merchant'] as $m){
                                                                if($data['settlement']->settlement_merchant_id == $m->merchant_id){
                                                                    $selected = " SELECTED";
                                                                }else{
                                                                    $selected = " ";
                                                                }
                                                            ?>
                                                            <option value = '{{Crypt::encryptString($m->merchant_id)}}' {{$selected}}>{{$m->merchant_code}}</option>
                                                            <?php
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <?php }?>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label class="form-control-label" for="settlement_requested_amount">Requested Amount : </label>
                                                        <input <?php echo $readonly;?> type="text"  style="text-align:right" name = "settlement_requested_amount" id = "settlement_requested_amount"
                                                        class="form-control"
                                                        value="{{ ($data['settlement']->settlement_requested_amount) }}" />
                                                    </div>
                                                </div>
                                            </div>
                                        

                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label class="form-control-label" for="">Bank Name: </label>
                                                 
                                                    <select <?php echo $disabled;?> name="settlement_merchantsettlementbank_id" class="form-control select2" id="settlement_merchantsettlementbank_id" >
                                                       
                                                        <?php
                                                        echo $data['merchantsettlement_bank'];
                                                      
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label class="form-control-label" for="">Merchant Reference No. : </label>
                                                
                                                    <input <?php echo $readonly;?> type="text" style="text-transform:uppercase" name = 'settlement_ref' class="form-control" value="{{  $data['settlement']->settlement_ref}}" />

                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label class="form-control-label" for="">Account Holder.: <?php if($data['settlement']->settlement_id > 0){?> <button class="btn btn-sm btn-warning" type="button"   onclick="copyToClipboard('settlement_bank_account_holder')">Copy</button><?php }?></label>
                                                 
                                                    <input readonly type="text" style="text-transform:uppercase" name = 'settlement_bank_account_holder' id = 'settlement_bank_account_holder' class="form-control" value="{{  $data['settlement']->settlement_bank_account_holder}}" />
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label class="form-control-label" for="">Account No.: <?php if($data['settlement']->settlement_id > 0){?> <button class="btn btn-sm btn-warning" type="button"   onclick="copyToClipboard('settlement_bank_account')">Copy</button><?php }?></label>
                                                 
                                                    <input readonly type="text" style="text-transform:uppercase" name = 'settlement_bank_account' id = 'settlement_bank_account' class="form-control" value="{{  $data['settlement']->settlement_bank_account}}" />
                                                </div>
                                            </div>
                                        </div>
                                        

                                        {{-- <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label class="form-control-label" for="trainer_salutation">Remarks:</label>
                                                        <textarea class="form-control" name = "settlement_remarks" id = "settlement_remarks">
                                                            {{$data['settlement']->settlement_remarks }}
                                                        </textarea>
                                                </div>
                                            </div>
                                        </div> --}}
                                        <hr>

                                        @csrf
                                        
                                        

                                     
                                        <input type="hidden" name='settlement_id' value="{{ Crypt::encryptString($data['settlement']->settlement_id)}}" />

                                        <button onclick="history.go(-1);return false;"
                                            class="btn btn-outline-default">Back</button>

                                        <?php 
                                        if(($data['settlement']->settlement_status == 'Rejected') 
                                        || ($data['settlement']->settlement_status == 'Draft')
                                        || ($data['settlement']->settlement_status == null)){
                                        ?>
                                                <button class="btn btn-primary float-right" type="button"
                                                    id='submit_btn'>Submit</button>
                                                    <button style = 'margin-right:10px;' class="btn btn-warning float-right" type="button"
                                                        id='submit_draft_btn'>Submit as draft </button>
                                        <?php }?>
                                        <input type = "hidden" value = '' id = 'submit_button_status' name = "submit_button_status">
                                    </div>
                                    <div class="col-md-3">
                                        <?php 
                                       if(($data['settlement']->settlement_status == 'Pending') || 
                                           ($data['settlement']->settlement_status == 'Approved')|| 
                                           ($data['settlement']->settlement_status == 'Rejected')|| 
                                           ($data['settlement']->settlement_status == 'Cancelled')){   

                                            if($data['settlement']->settlement_status == 'Pending'){
                                                $readonly = '';
                                            }
                                            
                                                if(((Auth::user()->user_group_id == 1) || (Auth::user()->user_group_id == 2) || (Auth::user()->user_group_id == 7) || (Auth::user()->user_group_id == 9)) && (canAccess('approval_settlement'))){
                                                    $readonly2 = '';
                                                }else{
                                                    $readonly2 = ' READONLY';
                                                }
                                               // if($data['settlement']->settlement_status != 'Pending'){
                                        ?>
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label class="form-control-label" for="settlement_to_amount">To Settlement Amount : </label>
                                                    <input <?php echo $readonly2;?> type="text" style="text-align:right" name = "settlement_to_amount" id = "settlement_to_amount"
                                                    class="form-control"
                                                    value="{{ num_format($data['settlement']->settlement_to_amount) }}" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label class="form-control-label" for="settlement_remarks">Remarks : </label>
                                                    <textarea <?php echo $readonly2;?> class="form-control" name = "settlement_remarks" id = "settlement_remarks">{{$data['settlement']->settlement_remarks}}</textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <?php 
                                                //}
                                        
                                        ?>

                                        <?php
                                        if($data['settlement']->settlement_status == 'Pending'){
                                            if((Auth::user()->user_group_id == 1) || (Auth::user()->user_group_id == 2) || (Auth::user()->user_group_id == 7) || (Auth::user()->user_group_id == 9)){
                                                if(canAccess('approval_settlement')){
                                        ?>
                                        <div style = 'display:flex;justify-content: center;'>
                                            <button class="btn btn-success action_btn" type="button" id='approve_btn'>Approve</button>
                                            <button class="btn btn-danger action_btn" type="button" id='reject_btn'>Reject</button>
                                        </div>
                                        <?php 
                                                }
                                            }else{
                                            ?>
                                        <div style = 'text-align:center;'>
                                            <span class="badge badge-warning">Pending</span>
                                            <p></p>
                                            <p>{{$data['settlement']->settlement_submission}}</p>
                                            <p></p>
                                        </div>
                                            <?php
                                            }
                                        }else if($data['settlement']->settlement_status == 'Approved'){
                                        ?>
                                        <div style = 'text-align:center;'>
                                            <span class="badge badge-success">Approved</span>
                                            <p></p>
                                            <p>{{$data['settlement']->settlement_actiondatetime}}<br>{{$data['settlement']->user_name}}</p>
                                            <p></p>
                                        </div>
                                        <?php
                                        if(((Auth::user()->user_group_id == 1) || (Auth::user()->user_group_id == 2) || (Auth::user()->user_group_id == 7) || (Auth::user()->user_group_id == 9)) && (canAccess('approval_settlement'))){
                                        ?>
                                        <div style = "display:flex">
                                            <a href="javascript:void(0)" class="btn btn-sm btn-danger set_action_class" pid = "reject"  >Set Reject</a>
                                            {{-- <a href="javascript:void(0)" class="btn btn-sm btn-warning set_action_class" pid = "cancel"  >Set Cancel</a> --}}
                                        </div>

                                        <?php
                                        }
                                        }else if(($data['settlement']->settlement_status == 'Rejected') || ($data['settlement']->settlement_status == 'Cancel')){
                                        ?>
                                        <div style = 'text-align:center;'>
                                            <?php
                                            if($data['settlement']->settlement_status == 'Rejected'){
                                            ?>
                                            <span class="badge badge-danger"><?php echo $data['settlement']->settlement_status;?></span>
                                            <?php
                                            }else{
                                            ?>
                                            <span class="badge badge-warning"><?php echo $data['settlement']->settlement_status;?></span>
                                            <?php
                                            }
                                            ?>
                                            
                                            <p></p>
                                            <p>{{$data['settlement']->settlement_actiondatetime}}<br>{{$data['settlement']->user_name}}</p>
                                            <p></p>
                                        </div>
                                            <?php
                                            if(((Auth::user()->user_group_id == 1) || (Auth::user()->user_group_id == 2) || (Auth::user()->user_group_id == 7) || (Auth::user()->user_group_id == 9)) && (canAccess('approval_settlement'))){
                                            ?>
                                        <div style = "display:flex">
                                            <a href="javascript:void(0)" class="btn btn-sm btn-success set_action_class" pid = "approve"  >Set Approve</a>
                                        </div>

                                        <?php
                                            }
                                        }
                                    }?>
                                    </div>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>

        </div>

        <!-- Footer -->
        @include('pages.footer')
    </div>
    </div>


    <!-- Argon Scripts -->
    <!-- Core -->
    <script src="../assets/vendor/jquery/dist/jquery.min.js"></script>
    <script src="../assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <script src="../assets/vendor/js-cookie/js.cookie.js"></script>
    <script src="../assets/vendor/jquery.scrollbar/jquery.scrollbar.min.js"></script>
    <script src="../assets/vendor/jquery-scroll-lock/dist/jquery-scrollLock.min.js"></script>
    <script src="../assets/vendor/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../assets/vendor/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
    <script src="../assets/vendor/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
    <script src="../assets/vendor/select2/dist/js/select2.min.js"></script>
    <script src="../assets/vendor/sweetalert2/dist/sweetalert2.min.js"></script>
    <script src="../js/tmsjs/jqueryvalidation/jquery.validate.1.8.js"></script>
    <script src="../js/tmsjs/loadingoverlay/loadingoverlay.min.js"></script>
    <!-- Argon JS -->
    <script src="../assets/js/argon.js?v=1.2.0"></script>

    <script>
        jQuery.validator.addMethod("dollarsscents", function(value, element) {
            return this.optional(element) || /^\d{0,4}(\.\d{0,2})?$/i.test(value);
        }, "You must include two decimal places");
        $(document).ready(function() {
           

            $('#settlement_merchant_id').change(function() {
                var merchant_id = $(this).val();
                
                $.ajax({
                        url: '/settlement/getSettlementBank',
                        type: 'POST',
                        data: 'merchant_id='+merchant_id+'&_token={{csrf_token()}}',
                        cache: false,
                        beforeSend: function() {
                            $.LoadingOverlay("show");
                        },
                        error: function(xhr) {
                            Swal({
                                type: 'error',
                                title: "Something went wrong.",
                                showConfirmButton: false,
                                allowOutsideClick: false,
                                timer: 1500
                            })
                            $.LoadingOverlay("hide");
                        },
                        success: function(jsonObj) {
                            $.LoadingOverlay("hide");
                            $('#settlement_merchantsettlementbank_id').html(jsonObj.html);
                            $('#settlement_from_amount').val(jsonObj.available_balance);
                        }
                });
            });

            $('#settlement_merchantsettlementbank_id').change(function() {
                var merchantsettlementbank_id = $(this).val();
                
                $.ajax({
                        url: '/settlement/getSettlementBankAccount',
                        type: 'POST',
                        data: 'merchantsettlementbank_id='+merchantsettlementbank_id+'&_token={{csrf_token()}}',
                        cache: false,
                        beforeSend: function() {
                            $.LoadingOverlay("show");
                        },
                        error: function(xhr) {
                            Swal({
                                type: 'error',
                                title: "Something went wrong.",
                                showConfirmButton: false,
                                allowOutsideClick: false,
                                timer: 1500
                            })
                            $.LoadingOverlay("hide");
                        },
                        success: function(jsonObj) {
                            $.LoadingOverlay("hide");
                            $('#settlement_bank_account').val(jsonObj.merchantsettlementbank_account_number);
                            $('#settlement_bank_account_holder').val(jsonObj.merchantsettlementbank_account_holder);
                        }
                });
            });

            <?php
            if(canAccess('approval_settlement')){
            ?>
            $('.action_btn').click(function(){
                if($(this).attr('id') == 'approve_btn'){
                    var msg = "Approve?";

                    if(parseFloat($('#settlement_requested_amount').val()) != parseFloat($('#settlement_to_amount').val())){
                       
                       alert('Settlement Amount not match!');
                       return false;
                    }
                }else{
                    var msg = "Reject?";
                }
                
                Swal({  
                    title: 'Confirm ' + msg,  
                    type: "question",  
                    showCloseButton: true,
                    showCancelButton: true,
                    focusConfirm: true,
                }).then(function(r) {

                    if(r.value ==true){
                        $.ajax({
                                url: '/settlement/action',
                                type: 'POST',
                                data: $('#settlementform_id').serialize() + "&action="+msg,
                                cache: false,
                                beforeSend: function() {
                                    $.LoadingOverlay("show");
                                },
                                error: function(xhr) {
                                    Swal({
                                        type: 'error',
                                        title: "Something went wrong.",
                                        showConfirmButton: false,
                                        allowOutsideClick: false,
                                        timer: 1500
                                    })
                                    $.LoadingOverlay("hide");
                                },
                                success: function(jsonObj) {
                                    $.LoadingOverlay("hide");
                                    if (jsonObj.status == 1) {
                                        if (jsonObj.msg != "") {
                                            Swal({
                                                type: 'success',
                                                title: jsonObj.msg,
                                                showConfirmButton: false,
                                                allowOutsideClick: false,
                                                timer: 1500
                                            }).then(function() {
                                                if(msg == 'Approve?'){
                                                    location.href = '/settlement/listing';
                                                }else{
                                                    location.reload();
                                                }
                                            
                                            });
                                        }

                                    } else {
                                        Swal({
                                            type: 'error',
                                            title: jsonObj.msg,
                                            showConfirmButton: false,
                                            allowOutsideClick: false,
                                            timer: 1500
                                        })
                                    }
                                }
                        });
                    }else{
                        return false;
                    }


                });
               

            });
            <?php }?>
            $('.select2').select2();

            $('#submit_draft_btn').click(function(){
                $('#submit_button_status').val('draft');
                $("#settlementform_id").submit();
            });
            $('#submit_btn').click(function(){
                $('#submit_button_status').val('confirm');
                $("#settlementform_id").submit();
            });

            
            $("#settlementform_id").validate({
                rules: {
                    settlement_to_amount: {
                        number: true,
                    },
                    settlement_requested_amount: {
                        required: true,
                        number: true,
                        min: 100
                    }
                },
                showErrors: function(errorMap, errorList) {
                    this.defaultShowErrors();
                },
                submitHandler: function(form) {
                    
                    $.ajax({
                        url: '/settlement/create',
                        type: 'POST',
                        data: $('#settlementform_id').serialize() + "&submit_button_status="+$('#submit_button_status').val(),
                        cache: false,
                        beforeSend: function() {
                            $('#submit_btn').text("loading...");
                            $('#submit_btn').attr("disabled", true);
                            $('#submit_draft_btn').text("loading...");
                            $('#submit_draft_btn').attr("disabled", true);
                            $.LoadingOverlay("show");
                        },
                        error: function(xhr) {
                            Swal({
                                type: 'error',
                                title: "Something went wrong.",
                                showConfirmButton: false,
                                allowOutsideClick: false,
                                timer: 1500
                            })
                            $('#submit_btn').attr("disabled", false);
                            $('#submit_btn').text("Submit");
                            $('#submit_draft_btn').attr("disabled", false);
                            $('#submit_draft_btn').text("Submit as draft");
                            $.LoadingOverlay("hide");
                        },
                        success: function(jsonObj) {
                            $('#submit_btn').attr("disabled", false);
                            $('#submit_btn').text("Submit");
                            $('#submit_draft_btn').attr("disabled", false);
                            $('#submit_draft_btn').text("Submit as draft");
                            $.LoadingOverlay("hide");
                            if (jsonObj.status == 1) {
                                if (jsonObj.msg != "") {
                                    Swal({
                                        type: 'success',
                                        title: jsonObj.msg,
                                        showConfirmButton: false,
                                        allowOutsideClick: false,
                                        timer: 1500
                                    }).then(function() {
                                        // window.location.href =
                                        //     '/settlement/listing?msg2=' +
                                        //     'Settlement successfully submitted.'
                                        // location.href = '/settlement/createForm?settlement_id='+jsonObj.settlement_id;
                                        location.href = '/settlement/listing';
                                    });
                                }

                            } else {
                                Swal({
                                    type: 'error',
                                    title: jsonObj.msg,
                                    showConfirmButton: false,
                                    allowOutsideClick: false,
                                    timer: 1500
                                })
                            }
                        }
                    });
                },
            });


            $(".remove_file").click(function() {
                console.log('Removing: ' + this.id);
                $("#ordersubmission_url_" + this.id).val(null);
                $("#ordersubmission_link_" + this.id).attr("href", '');
                $("#ordersubmission_link_" + this.id).html('');
                $("#ordersubmission_link_" + this.id).change(function() {
                    window.location.assign('');
                });
                $(".remove_btn_" + this.id).addClass('hide');
            });

            <?php
            if(canAccess('setaction_settlement')){
            ?>
                $(document).on("click", '.set_action_class', function(){
                    if($(this).attr('pid') == 'approve'){
                        var msg = "Confirm set Approve?";
                    }else if($(this).attr('pid') == 'reject'){
                        var msg = "Confirm set Reject?";
                    }else{
                        var msg = "Confirm set Cancel?";
                    }

                    var deposit_id = $(this).attr('deposit_id');
                    var action = $(this).attr('pid');
                    var line = $(this).attr('line');

                    Swal({  
                        title: msg,  
                        type: "question",  
                        showCloseButton: true,
                        showCancelButton: true,
                        focusConfirm: true,
                    }).then(function(r) {
                        if(r.value ==true){
                            $.ajax({
                                    url: '/settlement/setaction',
                                    type: 'POST',
                                    data: "_token={{ csrf_token() }}&settlement_id={{ Crypt::encryptString($data['settlement']->settlement_id)}}&action="+action,
                                    cache: false,
                                    beforeSend: function() {
                                        $.LoadingOverlay("show");
                                    },
                                    error: function(xhr) {
                                        Swal({
                                            type: 'error',
                                            title: "Something went wrong.",
                                            showConfirmButton: false,
                                            allowOutsideClick: false,
                                            timer: 1500
                                        })
                                        $.LoadingOverlay("hide");
                                    },
                                    success: function(jsonObj) {
                                        $.LoadingOverlay("hide");
                                        if (jsonObj.status == 1) {
                                            if (jsonObj.msg != "") {
                                                Swal({
                                                    type: 'success',
                                                    title: jsonObj.msg,
                                                    showConfirmButton: false,
                                                    allowOutsideClick: false,
                                                    timer: 1500
                                                }).then(function() {
                                                    location.href = '/settlement/listing';
                                                });
                                            }

                                        } else {
                                            Swal({
                                                type: 'error',
                                                title: jsonObj.msg,
                                                showConfirmButton: false,
                                                allowOutsideClick: false,
                                                timer: 1500
                                            })
                                        }
                                    }
                            });
                        }else{
                            return false;
                        }


                    }); 

                });
            <?php
            }
            ?>


        });
        function copyToClipboard(id) {
            document.getElementById(id).select();
            document.execCommand('copy');
            Swal({
                type: 'success',
                title: 'Copied',
                showConfirmButton: false,
                allowOutsideClick:false,
                timer: 1500
            }).then(function() {
        
            });
        }
     
    </script>
</body>

</html>
