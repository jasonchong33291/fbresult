<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  {{-- <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"> --}}
  <meta name="description" content="i1bnScore">
  <meta name="author" content="Jason Chong">
  <title>2022 FIFA World Cup|2022 FIFA World Cup Qualifiers| FIFA World Cup Qatar Qualifiers|2022 FIFA World Cup Qatar Qualifiers|2022 FIFA World Cup Match Fixtures|2022 FIFA World Cup Matches</title>
  <!-- Favicon -->
  <link rel="icon" href="../assets/img/brand/favicon.png" type="image/png">
  <!-- Fonts -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Orbitron:300,400,600,700">
  <!-- Icons -->
  <link rel="stylesheet" href="../assets/vendor/nucleo/css/nucleo.css" type="text/css">
  <link rel="stylesheet" href="../assets/vendor/@fortawesome/fontawesome-free/css/all.min.css" type="text/css">
  <link rel="stylesheet" href="../assets/vendor/datatables.net-bs4/css/dataTables.bootstrap4.min.css" type="text/css">
  <link rel="stylesheet" href="../assets/vendor/select2/dist/css/select2.min.css" type="text/css">
  <link rel="stylesheet" href="../assets/vendor/sweetalert2/dist/sweetalert2.min.css" type="text/css">
  <!-- Argon CSS -->
  <link rel="stylesheet" href="../assets/css/argon.css?v=1.2.0" type="text/css">
  <link rel="stylesheet" href="../css/tms.css" type="text/css">
  <link rel="stylesheet" href="https://2022.7m.com.cn/static/css/items/match_base.css" type="text/css">
  <link rel="stylesheet" href="  https://2022.7m.com.cn/static/css/items/goaldata.css" type="text/css">

  <style>
  .hdb_tab {
      height: 50px;
      background: #9a1032;
      position: relative;
  }
  .hdb_tab ul li {
      display: inline-block;
      vertical-align: top;
      height: 50px;
      margin-left: 46px;
      position: relative;
  }
  .hdb_tab ul li a {
      font-size: 16px;
      display: block;
      color: #fff;
      line-height: 50px;
      text-decoration: none;
      height: 47px;
  }
  .b_match .bm_tab {
      height: 48px;
      border-bottom: 2px solid #9a1032;
      background: #f8f8f8;
      position: relative;
      margin-bottom: 10px;
  }
  .b_match .bm_tab .bmt_tab .on {
      background: #9a1032;
      color: #fff;
  }
  .b_match .bm_tab .bmt_tab li {
      cursor: pointer;
      display: inline-block;
      vertical-align: top;
      width: 100px;
      height: 48px;
      line-height: 48px;
      text-align: center;
      color: #353535;
      font-size: 16px;
  }
  .b_match .bm_title {
      width: 100%;
  }
  .b_match .bm_title ul {
      background: #e5e5e5;
      font-size: 0;
      text-align: left;
  }
  .b_match .bm_title .col0 {
      width: 74px;
  }
  .b_match .bm_title .col1 {
      width: 50px;
  }
  .b_match .bm_title .col2 {
      width: 50px;
  }
  .b_match .bm_title .col3 {
      width: 426px;
  }
  .b_match .bm_title .col4 {
      width: 30px;
  }
  .b_match .bm_title .col5 {
      width: 28px;
      white-space: nowrap;
  }
  .b_match .bm_title .col6 {
      width: 140px;
  }
  .b_match .bm_title .col7 {
      width: 80px;
  }
  .b_match .bm_title li {
      display: inline-block;
      vertical-align: middle;
      color: #323232;
      font-size: 12px;
      text-align: center;
      line-height: 14px;
      padding: 8px 0;
  }
  .b_match .bm_table .bmt_title {
      border-bottom: 1px solid #ebebeb;
      border-left: none;
  }
  .b_match .bm_table .bmt_title li {
      text-align: left;
      border: none;
      padding-left: 10px;
      line-height: 30px;
      margin-left: 0px;
  }
  .b_match .bm_table li em {
      position: absolute;
      width: 4px;
      height: 12px;
      background: #9a1032;
      top: 50%;
      left: 0;
      margin-top: -6px;
  }
  .b_match .bm_table .row {
      width: 100%;
      display: block;
      background: #fff;
      padding: 0;
  }
  .b_match{
    margin-top: 30px;
  }
  ol, ul {
      list-style: none;
  }
  ul{
    margin: 0px;
    padding: 0px;
  }
  .b_match .bm_table ul {
      position: relative;
      font-size: 0;
      text-align: left;
      border-left: 1px solid #ebebeb;
  }

  .info_wrap {
      width: 100%;
      height: 197px;
      position: relative;
      margin: 0 auto;
      overflow: hidden;
      /* background: url(//static.sportsdt.com/images/pc/2018fifa/base/menu_bg.gif) repeat-x; */
      min-width: 1200px;
  }
  .info_top {
    width: 1200px;
    margin: 38px auto 0;
}
.info {
    clear: both;
    overflow: hidden;
    width: 100%;
    height: 112px;
    background: url('https://2022.7m.com.cn/static/images/index/th_bg.jpg') no-repeat center 0;
    position: relative;
}
.nav_m {
    width: 100%;
    height: 46px;
    line-height: 46px;
    color: #bfec95;
    text-align: center;
    background: #4e9315;
}
.score {
    width: 100%;
}
.info_league {
    display: block;
    padding: 5px 0 1px;
    text-align: center;
    position: relative;
}
.info_league span, .info_league a:link, .info_league a:visited {
    color: #444;
}
.score_t {
    width: 1200px;
    margin: 0 auto;
}
.score_l, .score_c, .score_r {
    float: left;
    /* width: 500px; */
}
.score_c {
    width: 200px;
    text-align: center;
    padding-top: 5px;
}
.body {
    width: 1200px;
    margin: 0 auto;
    position: relative;
}
.score_r {
    width: 500px;
}
.overtime {
    clear: both;
    padding: 5px 0;
    width: 100%;
    text-align: center;
    color: #fff;
    position: relative;
    line-height: 13px;
}
.team_name1 a {
    float: right;
}
.team_name1 a, .team_name2 a {
    color: #444;
    font-family: "微软雅黑";
    font-size: 24px;
    font-weight: bold;
}
.team_record1, .team_record2 {
    clear: both;
    float: right;
    width: 100px;
    height: 3px;
    overflow: hidden;
}
.wr {
    clear: both;
    overflow: hidden;
    width: 100px;
    margin: 0;
    height: 5px;
}
.team_logo {
    float: left;
    width: 64px;
    height: 56px;
    padding-top: 8px;
    text-align: center;
    border: 1px solid #d5d5d5;
    background: #fff;
    position: relative;
}
.team_logo img {
    max-width: 100%;
    max-height: 100%;
}
.score_c span.vs_score {
    line-height: inherit;
    font-size: 20px;
    font-weight: bold;
    position: relative;
}
#pk {
    display: block;
    height: 22px;
    position: relative;
}
#pk a {
    color: #ce0000;
    font-size: 14px;
}
.team2 {
    margin-left: 10px;
    width: 423px;
}
.team1, .team2 {
    float: left;
    width: 423px;
    padding-top: 10px;
}
.team_record2 {
    float: none;
}
.wrap{
  margin-top:-30px;
}
.g_listcon li{
    font-weight: 700;
}
.score_num{
    font-size:30px;
}
#mn{
    color: #258D2D;
    font-family: Orbitron;
    font-weight:700;
    font-size: 21px;
    font-style: normal;
    letter-spacing: 5px;
}
  </style>
</head>

<body>

  <!-- Main content -->
  <div class="main-content" id="panel">
    <!-- Topnav -->
    @include('layouts.navbars.topheader') 
    <!-- Header -->
    <!-- Header -->
    <div class="header  pb-6 " style = 'background-color:white;'>
      <div class=" ">
        <div class="header-body">
            <div class=" align-items-center">
              <div class="hdb_tab">
                <div id="js_head_tab" class="container flex jc-sb">
                  <ul>
                    <li>
                      <a data-tab="live" href="/home"  class = "<?php if(Route::currentRouteName() == "home"){ echo " on";}?>">Live Scores</a>
                    </li>
                    <li>
                      <a data-tab="match" href="/groups" class = "<?php if(Route::currentRouteName() == "groups"){ echo " on";}?>">Groups</a>
                    </li>
                    {{-- <li>
                      <a data-tab="rank" href="javascript:void(0);" class = "<?php if(Route::currentRouteName() == "standing"){ echo " on";}?>" >Standings</a>
                    </li> --}}
                  </ul>
                </div>
        
              </div>
            </div>

            <div class="col-lg-6 col-5 text-right">
             
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Page content -->
    {{-- mt--6 --}}
    <div class="container-fluid ">
      <div class="body" >

        <div class="wrap">
            <div class="info_wrap">
              <div class="info_top">
                  <div class="info">
                      <div class="score">
                        <?php
                        $upcomingevent_api_league = json_decode($data['Upcomingevent']['upcomingevent_api_league'],true);
                        $upcomingevent_api_json = json_decode($data['Upcomingevent']['upcomingevent_api_json'],true);
                        
                        if(isset($upcomingevent_api_json) && (isset($upcomingevent_api_json['timer']))){
                        ?>
                          <div class="info_league" id="mn" start = '{{date('H:i:00',strtotime($data['Upcomingevent']['upcomingevent_api_date']))}}' tm = '<?php echo $upcomingevent_api_json['timer']['tm']?>' ts = '<?php echo $upcomingevent_api_json['timer']['ts']?>' tt = '<?php echo $upcomingevent_api_json['timer']['tt']?>' ta = '<?php echo $upcomingevent_api_json['timer']['ta']?>'>
                            
                            
                          </div>
                          <?php
                        }else{
                        ?>
                          <div class="info_league"  >
                            <?php echo $data['Upcomingevent']['upcomingevent_api_date'];?>
                            <span> <?php echo $upcomingevent_api_league['name'];?>
                            </span>
                          </div>
                        <?php
                        }
                            $upcomingevent_api_home = json_decode($data['Upcomingevent']['upcomingevent_api_home'],true);
                            $upcomingevent_api_away = json_decode($data['Upcomingevent']['upcomingevent_api_away'],true);
                            
                            $score = explode('-',$upcomingevent_api_json['ss']);
                            
                          ?>
                          <div class="score_t">
                              <div class="score_l">
                                  <div class="team1">
                                      <div class="team_name1" id="ta"><a target="_blank" href="#"><?php echo $upcomingevent_api_home['name']?></a></div>
                                      <div class="team_record1" id="taRecord"><div class="wr" title="共統計過去20場比賽，45%勝、40%平、15%負"><span class="wr_w" style="width:45%" title="共統計過去20場比賽，45%勝、40%平、15%負"></span><span class="wr_d" style="width:40%" title="共統計過去20場比賽，45%勝、40%平、15%負"></span><span class="wr_l" style="width:15%" title="共統計過去20場比賽，45%勝、40%平、15%負"></span></div></div>
                                  </div>
                                  <div class="team_logo" id="th_logo"><img id="e_la" width="48" height="52" src="https://assets.b365api.com/images/team/m/<?php echo $upcomingevent_api_home['image_id']?>.png" onerror="onLogoError('e_la')"></div>
                              </div>
                              <div class="score_c">
                                  <span class="vs_score" id="vs_score"><span class = 'score_num' style = 'margin-right: 20px;' ><?php echo $score[0]?></span> VS <span class = 'score_num' style = 'margin-left: 20px;'><?php echo $score[1]?></span></span>
                                  
                              </div>
                              <div class="score_r">
                                  <div class="team_logo" id="ta_logo"><img id="e_lb" width="48" height="52" src="https://assets.b365api.com/images/team/m/<?php echo $upcomingevent_api_away['image_id']?>.png" onerror="onLogoError('e_lb')"></div>
                                  <div class="team2">
                                      <div class="team_name2" id="tb"><a target="_blank" href="#"><?php echo $upcomingevent_api_away['name']?></a></div>
                                      <div class="team_record2" id="tbRecord"> <div class="wr" title="共統計過去20場比賽，35%勝、50%平、15%負"><span class="wr_w" style="width:35%" title="共統計過去20場比賽，35%勝、50%平、15%負"></span><span class="wr_d" style="width:50%" title="共統計過去20場比賽，35%勝、50%平、15%負"></span><span class="wr_l" style="width:15%" title="共統計過去20場比賽，35%勝、50%平、15%負"></span></div></div>
                                  </div>
                              </div>
                              <div class="overtime" name="e_resume" style="display: none;"></div>
                          </div>
                      </div>

                  </div>
                  <div class="nav_m" id="nav_m">
                    <a href="#" class="nav_m_on"><?php if($data['lang'] == "2"){ echo "現場數據";}else{ echo "Field Data";}?></a>   
                  </div>
              </div>
            </div>

            <div class="contact_wrap">
              <div class="goal_wrap">
                  <div class="goal_history">
                      <div class="line_up">
                          <div class="ad1" id="L1" style=""></div>
                          <div class="line_up_list">
                              <div class="title">
                                <span class="number"><?php if($data['lang'] == "2"){ echo "號碼";}else{ echo "Number";}?></span>
                                <span class="name"><?php if($data['lang'] == "2"){ echo "姓名";}else{ echo "Name";}?></span>
                                <span class="location"><?php if($data['lang'] == "2"){ echo "位置";}else{ echo "Position";}?></span>
                              </div>
                              <ul class="list pre_lineup_big" id="lineUpa">
  
                              <li class="remarks"><?php if($data['lang'] == "2"){ echo "以下顯示為:";}else{ }?><span><?php if($data['lang'] == "2"){ echo "預測陣容";}else{ echo "Lineup Forecas";}?></span></li>
                              <?php 
                              
                              $data['Teamsquad'] = collect($data['Teamsquad']);
                              $Teamsquad = $data['Teamsquad']->where('teamsquad_team_id',$upcomingevent_api_home['id'])->sortBy('teamsquad_api_position');
                              foreach($Teamsquad as $t){
                              ?>
                              <li>
                                <span class="number">{{$t['teamsquad_api_shirtnumber']}}</span>
                                <span class="name_red">
                                    <a href="#"> {{$t['teamsquad_api_name']}}</a>
                                </span>
                                <span class="location">{{$t['teamsquad_api_position']}}</span>
                              </li>
                              <?php
                              }
                              ?>

                            </ul>
                          </div>
                      </div>
                      <div class="history">
                          <div class="ad3" name="word_ad"></div>
                          <div class="goals_list">
                              <ul class="g_listcon" id="goal_list">
                                <?php
                                   
                                    if(isset($upcomingevent_api_json['events'])){
                                        foreach($upcomingevent_api_json['events'] as $e){
                                        ?>
                                        <li>{{$e['text']}}</li>
                                        <?php
                                        }
                                    }
                                ?>
                                
                              </ul>
                          </div>
                          <div class="ad4" name="top_ad"></div>
                          
                          
                          <table border="0" cellpadding="0" cellspacing="0" class="weather" id="e_meno" style="">
                                <tbody>
                                    <tr><td valign="top" align="left"><?php if($data['lang'] == "2"){ echo "球場名稱";}else{ echo "Stadium";}?>:&nbsp;</td>
                                        <td valign="top"><a href="#"><?php if(isset($upcomingevent_api_json['extra']['stadium_data'])){ echo $upcomingevent_api_json['extra']['stadium_data']['name'];}?></a></td>
                                    </tr>
                                </tbody>
                            </table>
  
                          <div class="clear"></div>
                      </div>
                      <div class="line_up">
                          <div class="ad1" id="R1" style=""></div>
                          <div class="line_up_list">
                              <div class="title">
                                <span class="number"><?php if($data['lang'] == "2"){ echo "號碼";}else{ echo "Number";}?></span>
                                <span class="name"><?php if($data['lang'] == "2"){ echo "姓名";}else{ echo "Name";}?></span>
                                <span class="location"><?php if($data['lang'] == "2"){ echo "位置";}else{ echo "Position";}?></span>
                              </div>
                              <ul class="list pre_lineup_big" id="lineUpb">
  
                                <li class="remarks"><?php if($data['lang'] == "2"){ echo "以下顯示為:";}else{ }?><span><?php if($data['lang'] == "2"){ echo "預測陣容";}else{ echo "Lineup Forecas";}?></span></li>
                              <?php 
                              
                             
                              $Teamsquad = $data['Teamsquad']->where('teamsquad_team_id',$upcomingevent_api_away['id'])->sortBy('teamsquad_api_position');
                            
                              foreach($Teamsquad as $t){
                              ?>
                              <li>
                                <span class="number">{{$t['teamsquad_api_shirtnumber']}}</span>
                                <span class="name_blue">
                                    <a href="#"> {{$t['teamsquad_api_name']}}</a>
                                </span>
                                <span class="location">{{$t['teamsquad_api_position']}}</span>
                              </li>
                              <?php
                              }
                              ?>
                            </ul>
                          </div>
                      </div>
                      <div class="clear"></div>
                  </div>
              </div>
          </div>

        </div>
        
      </div>

      <!-- Footer -->
      @include('pages.footer') 
    </div>
  </div>
  <input type = "hidden" id = 'isstart' value = '0'/>

  <!-- Argon Scripts -->
  <!-- Core -->
  <script src="../assets/vendor/jquery/dist/jquery.min.js"></script>
  <script src="../assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
  <script src="../assets/vendor/js-cookie/js.cookie.js"></script>
  <script src="../assets/vendor/jquery.scrollbar/jquery.scrollbar.min.js"></script>
  <script src="../assets/vendor/jquery-scroll-lock/dist/jquery-scrollLock.min.js"></script>
  <script src="../assets/vendor/datatables.net/js/jquery.dataTables.min.js"></script>
  <script src="../assets/vendor/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
  <script src="../assets/vendor/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
  <script src="../assets/vendor/select2/dist/js/select2.min.js"></script>
  <script src="../assets/vendor/sweetalert2/dist/sweetalert2.min.js"></script>
  <script src="../js/tmsjs/jqueryvalidation/jquery.validate.1.8.js"></script>
  <script src="../js/tmsjs/loadingoverlay/loadingoverlay.min.js"></script>
  <script src="../assets/vendor/moment.min.js"></script>
  <!-- Argon JS -->
  <script src="../assets/js/argon.js?v=1.2.0"></script>

  <script>
    var myInterval = null;
    $(document).ready( function () {
      
      $('.bmt_sound').click(function(){
          if($(this).hasClass('bmt_sound_on')){
            $(this).removeClass('bmt_sound_on');
            $(this).attr('title','Sound On')
          }else{
            $(this).addClass('bmt_sound_on');
            $(this).attr('title','Sound Off') 
          }
      });
      $('.bmt_refresh').click(function(){
          getHomeResult();
      });

      $('.league_title').click(function(){
          $('.league_detail').removeClass('on');
          $('.league_title').removeClass('on');
          $('.league_detail').addClass('hide');
          $('#league_detail_'+$(this).attr('pid')).addClass('on');
          $('#league_detail_'+$(this).attr('pid')).removeClass('hide');
          $(this).addClass('on');

      });

      $('.league_title2').click(function(){
          $('.league_detail2').removeClass('on');
          $('.league_title2').removeClass('on');
          $('.league_detail2').addClass('hide');
          $('#league_detail_'+$(this).attr('pid')).addClass('on');
          $('#league_detail_'+$(this).attr('pid')).removeClass('hide');
          $(this).addClass('on');

      });
      myInterval = setInterval(getIsStart, 3000);
      
    });
    var coundownProcess = null;
    function startCount(){
        display = $('#mn');
               // start time and end time
               var startTime = moment($('#mn').attr('start'), 'HH:mm:ss');
            //    var startTime = moment('00:00:00', 'HH:mm:ss');
                var endTime = moment(moment().format("HH:mm:ss"), 'HH:mm:ss');
                // var endTime = moment('01:25:00','HH:mm:ss');
                // var endTime = moment(moment().format("HH:mm:ss a"), 'HH:mm:ss a');
                console.log(startTime);
                console.log(endTime);
                // calculate total duration
                var duration = moment.duration(endTime.diff(startTime));
                var minutes = moment.utc(moment(endTime, "HH:mm:ss").diff(moment(startTime, "HH:mm:ss"))).format("mm")
                var HH = moment.utc(moment(endTime, "HH:mm:ss").diff(moment(startTime, "HH:mm:ss"))).format("h")
                console.log(HH);
                console.log(moment(endTime, 'HH').format("H"));
                console.log(moment(startTime, 'HH').format("H"));
                // && (moment(endTime, 'HH').format("H") == moment(startTime, 'HH').format("H"))
                if((HH == 1) ){
                    var pp = moment(endTime, 'HH').format("H");
                    var ll = moment(startTime, 'HH').format("H");
                    if((parseInt(pp) - parseInt(ll)) > 1){
                        display.text('END');
                    }else{
                        var duration = (minutes * 60) + (HH * 60 * 60);
                    }
                    
                }else{
                    var pp = moment(endTime, 'HH').format("H");
                    var ll = moment(startTime, 'HH').format("H");
                    if((parseInt(pp) - parseInt(ll)) > 1){
                        display.text('END');
                    }else{
                        var duration = (minutes * 60);
                    }
                    
                }
                 
                 console.log(duration);
                // var minutes = 300;
                // var seconds = 19;
                var timer = duration,minutes, seconds;
        if(coundownProcess == null){
            
            let coundownProcess = setInterval(function() {
                    minutes = parseInt(timer / 60, 10)
                    seconds = parseInt(timer % 60, 10);

                    minutes = minutes < 10 ? "0" + minutes : minutes;
                    seconds = seconds < 10 ? "0" + seconds : seconds;
                    // console.log(minutes);
                    display.text(minutes + ":" + seconds);
                    ++timer;
                    if (minutes >= 100 ) {
                        clearInterval(coundownProcess); 
                        display.text('END');
                        
                    }
                    }, 1000);
        }
    }
    function getIsStart(){
        $.ajax({
                url:'api/isstart?upcomingevent_id={{$data['Upcomingevent']['upcomingevent_id']}}',
                type:'GET',
                cache:false,
                success:function(jsonObj){

                    if(jsonObj.status == 1){
                        startCount();
                        clearInterval(myInterval); 
                        
                    }
                }
        });
    }

  </script>
</body>

</html>