
<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  {{-- <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"> --}}
  <meta name="description" content="i1bnScore">
  <meta name="author" content="Jason Chong">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>2022 FIFA World Cup|2022 FIFA World Cup Qualifiers| FIFA World Cup Qatar Qualifiers|2022 FIFA World Cup Qatar Qualifiers|2022 FIFA World Cup Match Fixtures|2022 FIFA World Cup Matches</title>
  <!-- Favicon -->
  <link rel="icon" href="../assets/img/brand/favicon.png" type="image/png">
  <!-- Fonts -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700">
  <!-- Icons -->
  <link rel="stylesheet" href="../assets/vendor/nucleo/css/nucleo.css" type="text/css">
  <link rel="stylesheet" href="../assets/vendor/@fortawesome/fontawesome-free/css/all.min.css" type="text/css">
  <link rel="stylesheet" href="../assets/vendor/datatables.net-bs4/css/dataTables.bootstrap4.min.css" type="text/css">
  <link rel="stylesheet" href="../assets/vendor/select2/dist/css/select2.min.css" type="text/css">
  <link rel="stylesheet" href="../assets/vendor/sweetalert2/dist/sweetalert2.min.css" type="text/css">
  <!-- Argon CSS -->
  <link rel="stylesheet" href="../assets/css/argon.css?v=1.2.0" type="text/css">
  <link rel="stylesheet" href="../css/tms.css" type="text/css">

  {{-- <link rel="stylesheet" href="https://2022.7m.com.cn/static/css/items/live.css" type="text/css"> --}}


  <style>
  .hdb_tab {
      height: 50px;
      background: #9a1032;
      position: relative;
  }
  .hdb_tab ul li {
      display: inline-block;
      vertical-align: top;
      height: 50px;
      margin-left: 46px;
      position: relative;
  }
  .hdb_tab ul li a {
      font-size: 16px;
      display: block;
      color: #fff;
      line-height: 50px;
      text-decoration: none;
      height: 47px;
  }
  .b_match .bm_tab {
      height: 48px;
      border-bottom: 2px solid #9a1032;
      background: #f8f8f8;
      position: relative;
      margin-bottom: 10px;
  }
  .b_match .bm_tab .bmt_tab .on {
      background: #9a1032;
      color: #fff;
  }
  .b_match .bm_tab .bmt_tab li {
      cursor: pointer;
      display: inline-block;
      vertical-align: top;
      width: 100px;
      height: 48px;
      line-height: 48px;
      text-align: center;
      color: #353535;
      font-size: 16px;
  }
  .b_match .bm_title {
      width: 100%;
  }
  .b_match .bm_title ul {
      background: #e5e5e5;
      font-size: 0;
      text-align: left;
  }
  .b_match .bm_title .col0 {
      width: 74px;
  }
  .b_match .bm_title .col1 {
      width: 50px;
  }
  .b_match .bm_title .col2 {
      width: 50px;
  }
  .b_match .bm_title .col3 {
      width: 366px;
  }
  .b_match .bm_title .col4 {
      width: 30px;
  }
  .b_match .bm_title .col5 {
      width: 58px;
      white-space: nowrap;
  }
  .b_match .bm_title .col6 {
      width: 140px;
  }
  .b_match .bm_title .col7 {
      width: 30px;
  }
  .b_match .bm_title li {
      display: inline-block;
      vertical-align: middle;
      color: #323232;
      font-size: 12px;
      text-align: center;
      line-height: 14px;
      padding: 8px 0;
  }
  .b_match .bm_table .bmt_title {
      border-bottom: 1px solid #ebebeb;
      border-left: none;
  }
  .b_match .bm_table .bmt_title li {
      text-align: left;
      border: none;
      padding-left: 10px;
      line-height: 30px;
      margin-left: 0px;
  }
  .b_match .bm_table li em {
      position: absolute;
      width: 4px;
      height: 12px;
      background: #9a1032;
      top: 50%;
      left: 0;
      margin-top: -6px;
  }
  .b_match .bm_table .row {
      width: 100%;
      display: block;
      background: #fff;
      padding: 0;
  }
  .b_match{
    margin-top: 30px;
  }
  ol, ul {
      list-style: none;
  }
  ul{
    margin: 0px;
    padding: 0px;
  }
  .b_match .bm_table ul {
      position: relative;
      font-size: 0;
      text-align: left;
      border-left: 1px solid #ebebeb;
  }
  .b_match .bm_table .col0 {
      width: 72px;
  }
  .b_match .bm_table .col1 {
      width: 50px;
  }
  .b_match .bm_table .col2 {
      width: 50px;
  }
  .b_match .bm_table .col3 {
      width: 474px;
  }
  .b_match .bm_table .col4 {
      width: 30px;
      color: #d40000;
  }
  .b_match .bm_table .col5 {
      width: 26px;
  } 
  .b_match .bm_table .col6 {
      width: 138px;
  }
  .b_match .bm_table .col7 {
      width: 80px;
  }
  .b_match .bm_table .grey {
      color: #666;
  }
  .b_match .bm_table li {
      color: #323232;
      display: table-cell;
      vertical-align: middle;
      font-size: 12px;
      border-right: 1px solid #ebebeb;
      border-bottom: 1px solid #ebebeb;
      line-height: 18px;
      text-align: center;
      position: relative;
      padding: 10px 0;
  }
  .b_match .bm_table .vs {
      vertical-align: middle;
  }
  .b_match .bm_table .btn {
      text-align: center;
      padding-left: 4px;
  }
  .b_match .bm_table .vs .v_vs {
      width: 50px;
      display: inline-block;
      *display: inline;
      *zoom: 1;
      vertical-align: middle;
      font-weight: bold;
      color: #323232;
      font-size: 14px;
  }
  .b_match .bm_table .vs .v_l {
      width: 96px;
      text-align: left;
  }
  .b_match .bm_table .vs span {
      /* font-size: 14px; */
      font-weight: bold;
      color: #323232;
      width: 50px;
  }
  .b_match .bm_table .vs img, .b_match .bm_table .vs span {
      display: inline-block;
      /* *display: inline;
      *zoom: 1; */
      vertical-align: middle;
  }
  .b_match .bm_table .vs .v_l a, .b_match .bm_table .vs .v_r a {
      line-height: 18px;
  }
  .b_match .bm_table .vs img {
      width: 27px;
      height: 26px;
      border: 1px solid #ECEEEB;
      margin: 0 4px;
  }
  .b_match .bm_table .vs .v_r {
      width: 96px;
      text-align: right;
  }
  a {
      text-decoration: none;
      color: #000;
  }
  .b_match .bm_tab .bmt_btn {
      position: absolute;
      top: 0;
      right: 8px;
  }
  .b_match .bm_tab .bmt_btn .bmt_sound {
      background-position-x: -78px;
  }
  .b_match .bm_tab .bmt_btn .bmt_sound_on {
      background-position-x: -52px;
  }
  .b_match .bm_tab .bmt_btn li {
      cursor: pointer;
      margin-right: 8px;
      display: inline-block;
      *display: inline;
      *zoom: 1;
      vertical-align: top;
      width: 26px;
      height: 48px;
      background: url(//static.7m.com.cn/images/pc/2018fifa/live/icon_list.png) no-repeat 0 center;
  }
  .b_match .bm_tab .bmt_btn .bmt_refresh {
      background-position-x: -104px;
  }
  .r_scoreBoard .rs_tab {
      border-bottom: 1px solid #9a1032;
      font-size: 0;
  }
  #lang_en .r_scoreBoard .rs_tab li {
      padding: 0 10px;
  }
  .r_scoreBoard .rs_tab li.on {
      background: #9a1032;
      color: #fff;
  }
  .r_scoreBoard .rs_tab li {
      cursor: pointer;
      text-align: center;
      line-height: 30px;
      display: inline-block;
      vertical-align: top;
      font-size: 14px;
      border-right: 1px solid #fff;
      color: #353535;
      background: #f8f8f8;
      height: 30px;
      padding: 0 10px;
  }
  .r_scoreBoard .rs_tabC li {
      display: none;
  }
  .r_scoreBoard .rs_tabC li.on {
      display: block;
  }
  .r_scoreBoard .rs_title em {
      position: absolute;
      width: 4px;
      height: 16px;
      background: #9a1032;
      left: 0;
      top: 8px;
  }
  .r_scoreBoard .rs_title p {
      line-height: 30px;
      padding-left: 14px;
      font-size: 18px;
      color: #1f1f1f;
      font-weight: bold;
  }
  .r_scoreBoard .rs_title a {
      position: absolute;
      color: #1f1f1f;
      font-size: 14px;
      right: 4px;
      top: 8px;
  }
  .r_scoreBoard .rs_tabC td {
      height: 40px;
      line-height: 40px;
      vertical-align: middle;
      white-space: nowrap;
  }
  .r_scoreBoard .rs_tabC tr.first {
      font-size: 12px;
      color: #afb8bd;
  }
  .r_scoreBoard .rs_tabC tr {
      background: #fff;
      height: 40px;
      font-size: 14px;
      color: #353535;
      text-align: center;
      border-bottom: 1px solid #fff;
  }
  .r_scoreBoard .rs_tabC td.red {
      color: #be070d;
      font-weight: bold;
  }
  .r_scoreBoard .rs_tabC td.left {
      text-align: left;
      padding-left: 20px;
  }
  .b_match .bm_table .vs .v_l .red_card, .b_match .bm_table .vs .v_l .yellow_card {
      margin-left: 2px;
  }
  .b_match .bm_table .vs .v_l .red_card, .b_match .bm_table .vs .v_r .red_card, .b_match .bm_table .vs .v_l .yellow_card, .b_match .bm_table .vs .v_r .yellow_card {
      display: inline-block;
      width: 11px;
      height: 13px;
      line-height: 15px;
      color: #fff;
      font-family: 'Arial';
      font-size: 12px;
      font-weight: normal;
      text-align: center;
      background: #f04848;
      border-radius: 2px;
      margin-right: 2px;
  }
  .b_match .bm_table .vs .v_l .yellow_card, .b_match .bm_table .vs .v_r .yellow_card {
      background: #fcb103;
      margin-right: 2px;
  }
  .r_scoreBoard .rs_tabC td img {
      width: 27px;
      height: 16px;
      margin-right: 6px;
      border: 1px solid #ebebeb;
  }
  .r_scoreBoard .rs_tabC table {
      width: 100%;
      border-right: 1px solid #ebebeb;
      border-left: 1px solid #ebebeb;
      border-bottom: 1px solid #ebebeb;
  }
  .hdb_tab li a.on {
    border-bottom: 3px solid #d5c89e;
}
.jc-c {
    justify-content: center;
}
.flex {
    display: flex;
    flex-direction: row;
}
.body {
    width: 1200px;
    font-size: 0;
    position: relative;
    padding-bottom: 10px;
}
@media only screen   
and (min-device-width : 768px)   
and (max-device-width : 1024px)  
{
  
}  
  </style>
</head>

<body>

  <!-- Main content -->
  <div class="main-content" id="panel">
    <!-- Topnav -->
    @include('layouts.navbars.topheader') 
    <!-- Header -->
    <!-- Header -->
    <div class="header  pb-6" style = 'background-color:white'>
      <div class=" ">
        <div class="header-body">
            <div class=" align-items-center">
              <div class="hdb_tab">
                <div id="js_head_tab" class="container flex jc-sb">
                  <ul>
                    <li>
                      <a data-tab="live" href="/home"  class="on">
                        <?php
                        if($data['lang'] == 1){
                            echo "Live Scores";
                        }else{
                            echo "即時比分";
                        }
                        ?>
                      </a>
                    </li>
                    <li>
                      <a data-tab="match" href="/groups" >
                        <?php
                        if($data['lang'] == 1){
                            echo "Groups";
                        }else{
                            echo "賽程";
                        }
                        ?>
                      </a>
                    </li>
                    {{-- <li>
                      <a data-tab="rank" href="javascript:void(0);" >Standings</a>
                    </li> --}}
                  </ul>
                </div>
        
              </div>
            </div>

            <div class="col-lg-6 col-5 text-right">
             
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Page content -->
    <div class=" mt--6">
      <div class="flex jc-c">
        <div class = 'body ' style = 'display:flex'>
          <div class="b_match col-lg-9" id="js_list">
            <div class="bm_tab">
                <ul class="bmt_tab" id="js_matchTab">
                    <li class="on" id="data-allMatch">Results</li>
                </ul>
                <ul class="bmt_btn even">
                    <li id="js_soundSwitch" class="bmt_sound " title="Sound Off"></li>
                    <li class="bmt_refresh"  title="Refresh"></li>
                </ul>
            </div>
            <div class="bm_title" id="data-tableHead">
                <ul class="bmt_head">
                    <li class="col0">
                      <?php
                      if($data['lang'] == 1){
                          echo "Stage";
                      }else{
                          echo "輪次";
                      }
                      ?>
                    </li>
                    <li class="col1">
                      <?php
                      if($data['lang'] == 1){
                          echo "Kick-off";
                      }else{
                          echo "時間";
                      }
                      ?>
                      
                    </li>
                    <li class="col2">
                      <?php
                      if($data['lang'] == 1){
                          echo "Status";
                      }else{
                          echo "狀態";
                      }
                      ?>
                      
                    </li>
                    <li class="col3">
                      <?php
                      if($data['lang'] == 1){
                          echo "Game";
                      }else{
                          echo "比賽";
                      }
                      ?>
                      
                    </li>
                    <li class="col5" style="white-space: nowrap;">
                      <?php
                      if($data['lang'] == 1){
                          echo "In-play";
                      }else{
                          echo "进行中";
                      }
                      ?>
                      
                    </li>
                    <li class="col7">
                      <?php
                      if($data['lang'] == 1){
                          echo "Action";
                      }else{
                          echo "操作";
                      }
                      ?>
                      
                    </li>
                </ul>
            </div>
            <div class="bm_table bm_mt10" id="js_liveList">
              <?php
              $date = "";
            
              foreach($data['Upcomingevent'] as $d){
              
                if($date != date('d/m/Y',strtotime($d->upcomingevent_api_date))){
                ?>
                <ul class="bmt_title" id="date1"><li class="row"><em></em><?php echo date('d/m/Y',strtotime($d->upcomingevent_api_date)) . " " . date('l',strtotime($d->upcomingevent_api_date));?></li></ul>
                <?php
                  $date = date('d/m/Y',strtotime($d->upcomingevent_api_date));
                }
              
                $upcomingevent_api_home = json_decode($d->upcomingevent_api_home,true);
                $upcomingevent_api_away = json_decode($d->upcomingevent_api_away,true);
              ?>
              
              <ul  class="">    
                <li class="col0"><?php echo $d->upcomingevent_api_group ; ?></li>    
                <li class="col1 grey"><?php if($d->upcomingevent_api_date != null){ echo date('h:i',strtotime($d->upcomingevent_api_date));}?></li>    
                <li class="col2 grey" data-status="17" data-timing="1" data-time="2022-11-20 23:59:00">NS</li>    
                <li class="col3 vs">        
                  <img  src="https://assets.b365api.com/images/team/m/<?php echo $upcomingevent_api_home['image_id'];?>.png">        
                  <span class="v_l"><a target="_blank" href="#"><?php echo $upcomingevent_api_home['name']?></a>
                    <i class="red_card" style="display:none">0</i><i class="yellow_card" style="display:none">0</i>
                  </span>        

                  <a href = '/detail?q={{$d->upcomingevent_id }}' class="v_vs" data-h-y="0" data-a-y="0" data-h-r="0" data-a-r="0" data-h="0" data-a="0" href="" target="_blank">0 - 0</a>        
                  
                  <span class="v_r"><i class="yellow_card" style="display:none">0</i><i class="red_card" style="display:none">0</i>
                    <a target="_blank" href="#"><?php echo $upcomingevent_api_away['name']?></a></span>        
                    <img  src="https://assets.b365api.com/images/team/m/<?php echo $upcomingevent_api_away['image_id'];?>.png">
                </li>    
                  <li class="col5">	 
                  </li>    
                  <li class="col7 odds">        
                    <a target="_blank" href="#"><img src="/assets/images/icon1.png" class="btn" title="Tips"></a>
                  </li>
                    
              </ul>
              <?php }?>
            </div>
          </div>

          <div class="b_match col-lg-3" id="js_list" style = 'width:400px;'>
            <div class="b_r">

                <div id="js_scoreBoard">
                  <div class="r_scoreBoard">
                    <div class="rs_title">
                      <em></em><p>
                        <?php
                        if($data['lang'] == 1){
                            echo "Standings";
                        }else{
                            echo "積分榜";
                        }
                        ?>
                        
                      </p>                
                    </div>                    
                    <ul class="rs_tab">
                      <?php
                      foreach($data['Leaguetable'] as $l){
                        $dd = json_decode($l['leaguetable_overall'],true);
                        $dd = collect($dd['tables']);
                        $dd = $dd->sortBy('groupname');
                        
                        foreach($dd as $i => $d){
                        
                          if(($d['groupname'] == 'A') || ($d['groupname'] == 'B') || ($d['groupname'] == 'C') || ($d['groupname'] == 'D')){
                              if($d['groupname'] == 'A'){
                                echo "<li class='league_title on' pid = '{$d['groupname']}'>".$d['name']."</li>";
                              }else{
                                echo "<li class='league_title ' pid = '{$d['groupname']}'>".$d['name']."</li>";
                              }
                          }
                        }
                      }
                      ?>                  
                    </ul>                    
                    <ul class="rs_tabC" id="js_rScoreRank01">
                      <?php
                      foreach($data['Leaguetable'] as $l){      
                        $dd = json_decode($l['leaguetable_overall'],true);
                        $dd = collect($dd['tables']);
                        $dd = $dd->sortBy('groupname');
                        foreach($dd as $i => $d){    
                          if(($d['groupname'] == 'A') || ($d['groupname'] == 'B') || ($d['groupname'] == 'C') || ($d['groupname'] == 'D')){
                          ?>
                          <li class="league_detail <?php if($d['groupname'] == 'A'){ echo " on";}else{ echo " hide";}?>" id = 'league_detail_<?php echo $d['groupname'];?>' >    
                            <table>
                              <tbody>
                                <tr class="first">
                                  <td width="60">
                                    <?php
                                    if($data['lang'] == 1){
                                        echo "Ranking";
                                    }else{
                                        echo "排名";
                                    }
                                    ?>
                                    </td>
                                  <td>
                                    <?php
                                    if($data['lang'] == 1){
                                        echo "Team";
                                    }else{
                                        echo "球隊";
                                    }
                                    ?>
                                    </td>
                                  <td width="50">
                                    <?php
                                    if($data['lang'] == 1){
                                        echo "Points";
                                    }else{
                                        echo "分数";
                                    }
                                    ?>
                                  </td>
                                  <td width="90">
                                    <?php
                                    if($data['lang'] == 1){
                                        echo "W/D/L";
                                    }else{
                                        echo "勝/平/負";
                                    }
                                    ?>
                                    
                                  </td>
                                </tr>
                          <?php
                            $d['rows'] = collect($d['rows']);
                            $d['rows'] = $d['rows']->sortBy('sort_pos');

                            
                            foreach($d['rows'] as $i => $r){
                            ?>
                                <tr class="">
                                  <td class="<?php if(($i == 0) || ($i == 1)){ echo "red ";}?> pr"><?php echo $r['sort_pos']?></td>
                                  <td class="left">
                                    <a target="_blank" href="#">
                                    <img src="https://assets.b365api.com/images/team/m/<?php echo $r['team']['image_id'];?>.png"><span><?php echo $r['team']['name']?></span>
                                    </a>
                                  </td>
                                  <td><?php echo $r['points']?></td>
                                  <td><?php echo $r['win'] . "/" . $r['draw'] . "/" . $r['loss'];?></td>
                                </tr>
                            <?php
                            }
                            ?>
                                  </tbody>
                                </table>
                              </li>
                            <?php
                          }
                        }
                      ?>
                          
                      <?php
                      }
                      ?>
                    </ul>

                    <ul class="rs_tab" style = 'margin-top:20px;'>
                      <?php
                      foreach($data['Leaguetable'] as $l){
                        $dd = json_decode($l['leaguetable_overall'],true);
                        $dd = collect($dd['tables']);
                        $dd = $dd->sortBy('groupname');
                        
                        foreach($dd as $i => $d){
                        
                          if(($d['groupname'] == 'E') || ($d['groupname'] == 'F') || ($d['groupname'] == 'G') || ($d['groupname'] == 'H')){
                              if($d['groupname'] == 'E'){
                                echo "<li class='league_title2 on' pid = '{$d['groupname']}'>".$d['name']."</li>";
                              }else{
                                echo "<li class='league_title2 ' pid = '{$d['groupname']}'>".$d['name']."</li>";
                              }
                          }
                        }
                      }
                      ?>                  
                    </ul>                    
                    <ul class="rs_tabC" id="js_rScoreRank01">
                      <?php
                      foreach($data['Leaguetable'] as $l){      
                        $dd = json_decode($l['leaguetable_overall'],true);
                        $dd = collect($dd['tables']);
                        $dd = $dd->sortBy('groupname');
                        foreach($dd as $i => $d){    
                          if(($d['groupname'] == 'E') || ($d['groupname'] == 'F') || ($d['groupname'] == 'G') || ($d['groupname'] == 'H')){
                          ?>
                          <li class="league_detail2 <?php if($d['groupname'] == 'E'){ echo " on";}else{ echo " hide";}?>" id = 'league_detail_<?php echo $d['groupname'];?>' >    
                            <table>
                              <tbody>
                                <tr class="first">
                                  <td width="60">
                                    <?php
                                    if($data['lang'] == 1){
                                        echo "Ranking";
                                    }else{
                                        echo "排名";
                                    }
                                    ?>
                                  </td>
                                  <td>
                                    <?php
                                    if($data['lang'] == 1){
                                        echo "Team";
                                    }else{
                                        echo "球隊";
                                    }
                                    ?>
                                  </td>
                                  <td width="50">
                                    <?php
                                    if($data['lang'] == 1){
                                        echo "Points";
                                    }else{
                                        echo "分数";
                                    }
                                    ?>
                                  </td>
                                  <td width="90">
                                    <?php
                                    if($data['lang'] == 1){
                                        echo "W/D/L";
                                    }else{
                                        echo "勝/平/負";
                                    }
                                    ?>
                                  </td>
                                </tr>
                          <?php
                            $d['rows'] = collect($d['rows']);
                            $d['rows'] = $d['rows']->sortBy('sort_pos');

                          
                            foreach($d['rows'] as $i => $r){
                              
                            ?>
                                <tr class="">
                                  <td class="<?php if(($i == 0) || ($i == 1)){ echo "red ";}?> pr"><?php echo $r['sort_pos']?></td>
                                  <td class="left">
                                    <a target="_blank" href="#">
                                    <img src="https://assets.b365api.com/images/team/m/<?php echo $r['team']['image_id'];?>.png"><span><?php echo $r['team']['name']?></span>
                                    </a>
                                  </td>
                                  <td><?php echo $r['points']?></td>
                                  <td><?php echo $r['win'] . "/" . $r['draw'] . "/" . $r['loss'];?></td>
                                </tr>
                            <?php
                            }
                            ?>
                                  </tbody>
                                </table>
                              </li>
                            <?php
                          }
                        }
                      ?>
                          
                      <?php
                      }
                      ?>
                    </ul>



                  </div>
                </div>

            </div>
          </div>
        </div>
        
      </div>


    </div>
  </div>
  <input type = "hidden" id = 'sound' value = ''/>
{{-- <button id = 'play' >&nbsp;</button> --}}

  <!-- Argon Scripts -->
  <!-- Core -->
  <script src="../assets/vendor/jquery/dist/jquery.min.js"></script>
  <script src="../assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
  <script src="../assets/vendor/js-cookie/js.cookie.js"></script>
  <script src="../assets/vendor/jquery.scrollbar/jquery.scrollbar.min.js"></script>
  <script src="../assets/vendor/jquery-scroll-lock/dist/jquery-scrollLock.min.js"></script>
  <script src="../assets/vendor/datatables.net/js/jquery.dataTables.min.js"></script>
  <script src="../assets/vendor/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
  <script src="../assets/vendor/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
  <script src="../assets/vendor/select2/dist/js/select2.min.js"></script>
  <script src="../assets/vendor/sweetalert2/dist/sweetalert2.min.js"></script>
  <script src="../js/tmsjs/jqueryvalidation/jquery.validate.1.8.js"></script>
  <script src="../js/tmsjs/loadingoverlay/loadingoverlay.min.js"></script>
  <script src="../js/ion.sound-3.0.7/ion.sound.js"></script>
  <!-- Argon JS -->
  <script src="../assets/js/argon.js?v=1.2.0"></script>

  <script>
    var myInterval = null;
    $(document).ready( function () {

      ion.sound({
          sounds: [
              {
                  name: "door_bell"
              }
          ],
          volume: 1.0,
          path: "../js/ion.sound-3.0.7/sounds/",
          // preload: true
      }); 
      
      $('.bmt_sound').click(function(){
          if($('#sound').val() != ""){
            ion.sound.play("door_bell");
            return true;
          }
          if($(this).hasClass('bmt_sound_on')){
            $(this).removeClass('bmt_sound_on');
            $(this).attr('title','Sound On')
          }else{
            $(this).addClass('bmt_sound_on');
            $(this).attr('title','Sound Off'); 
            ion.sound.play("door_bell");
          }
      });
      $('.bmt_refresh').click(function(){
          getHomeResult();
      });

      $('.league_title').click(function(){
          $('.league_detail').removeClass('on');
          $('.league_title').removeClass('on');
          $('.league_detail').addClass('hide');
          $('#league_detail_'+$(this).attr('pid')).addClass('on');
          $('#league_detail_'+$(this).attr('pid')).removeClass('hide');
          $(this).addClass('on');

      });

      $('.league_title2').click(function(){
          $('.league_detail2').removeClass('on');
          $('.league_title2').removeClass('on');
          $('.league_detail2').addClass('hide');
          $('#league_detail_'+$(this).attr('pid')).addClass('on');
          $('#league_detail_'+$(this).attr('pid')).removeClass('hide');
          $(this).addClass('on');

      });
      
      // $('#play').click(function(){
      //   ion.sound.play("door_bell");
      // });
      myInterval = setInterval(getHomeResult, 3000);
      
      // getHomeResult();
    });
   
    function getHomeResult(){
          $.ajax({
            <?php
              if($data['lang'] == 1){
                echo "url:'/uploads/json/sources_en.json',";
              }else{
                echo "url:'/uploads/json/sources_cn.json',";
              }
            ?>
                
                type:'GET',
                cache:false,
                success:function(jsonObj){
                  var html = "";
                  var date = "";
                  var istrue = true;
                  // $('#sound').val('');
                  $(jsonObj).each(function(e,i){
                    var a = $.parseJSON(i.upcomingevent_api_away)
                    var h = $.parseJSON(i.upcomingevent_api_home)
                    var l = $.parseJSON(i.upcomingevent_api_league)
                    var j = $.parseJSON(i.upcomingevent_api_json)
                   
                    if((i.upcomingevent_api_alert == '1') && (istrue == true)){
                      if($('#js_soundSwitch').hasClass('bmt_sound_on')){
                      
                        if(i.upcomingevent_api_score != $('#sound').val()){
                          console.log(i.upcomingevent_api_score +"/" + $('#sound').val());
                          $('#sound').val(i.upcomingevent_api_score);
                          $('#js_soundSwitch').trigger('click');
                        }
                      }
                        istrue = false;
                    }
                   
                    if(date != i.date){
                      html += '<ul class="bmt_title" id="date1"><li class="row"><em></em>'+i.display_date+'</li></ul>';
                      date = i.date;
                    }
                   
                    html += '<ul data-dateid="date1" class="">';
                      html += '<li class="col0">'+ i.upcomingevent_api_group + '</li>';
                      html += '<li class="col1 grey">'+i.time+'</li>';
                      if((j) && (j.time_status)){
                        if(j.time_status == 1){
                          html += '<li class="col2 grey" title = "InPlay" style = "color:#9a1032;font-size: 15px;">'+j.timer.tm + "'";

                          if(j.timer.ta > 0){
                            html += "<br><span style = 'font-size:9px;'>+"+j.timer.ta+ '</span></li>';
                          }else{
                            html += '</li>';
                          }
                          
                        }else if(j.time_status == 2){
                          html += '<li class="col2 grey" title = "TO BE FIXED">TBE</li>';
                        }else if(j.time_status == 3){
                          html += '<li class="col2 grey"  title = "Ended">END</li>';
                        }else if(j.time_status == 4){
                          html += '<li class="col2 grey"  title = "Postponed">PP</li>';
                        }else if(j.time_status == 5){
                          html += '<li class="col2 grey"  title = "Cancelled">CL</li>';
                        }else if(j.time_status == 6){
                          html += '<li class="col2 grey"  title = "Walkover">WR</li>';
                        }else if(j.time_status == 7){
                          html += '<li class="col2 grey"  title = "Interrupted">IT</li>';
                        }else if(j.time_status == 8){
                          html += '<li class="col2 grey"  title = "Abandoned">AB</li>';
                        }else if(j.time_status == 9){
                          html += '<li class="col2 grey"  title = "Retired">RT</li>';
                        }else if(j.time_status == 10){
                          html += '<li class="col2 grey"  title = "Suspended">SP</li>';
                        }else if(j.time_status == 11){
                          html += '<li class="col2 grey"  title = "Decided by FA">DBF</li>';
                        }else if(j.time_status == 99){
                          html += '<li class="col2 grey"  title = "Removed">RM</li>';
                        }else{
                          <?php
                          if($data['lang'] == 1){
                          ?>
                          html += '<li class="col2 grey"  title = "Unknown">UN</li>';
                          <?php
                          }else{
                          ?>
                          html += '<li class="col2 grey"  title = "未">未</li>';
                          <?php
                          }
                          ?>
                          
                        }
                      }else{
                        html += '<li class="col2 grey" >NS</li>';
                      }
                      
                      html += '<li class="col3 vs">';
                        html += '<img src="https://assets.b365api.com/images/team/m/'+h.image_id+'.png">';
                        html += '<span class="v_l"><a href="#">'+h.name+'</a>';
                          if((j) && (j.stats)){
                            
                            if(parseInt(j.stats.redcards[0]) > 0){
                              html += '<i class="red_card" style="">'+j.stats.redcards[0]+'</i>';
                            }else{
                              html += '<i class="red_card" style="display:none">0</i>';
                            }
                            if(j.stats.yellowcards[0] > 0){
                              html += '<i class="yellow_card" >'+j.stats.yellowcards[0]+'</i>';
                            }else{
                              html += '<i class="yellow_card" style="display:none">0</i>';
                            }
                          }
                        html += '</span>';

                      html += '<a class="v_vs" href="/detail?q='+i.upcomingevent_id+'" target="_blank">'+i.upcomingevent_api_score+'</a>';    

                      
                      html += '<span class="v_r">';
                          if((j) && (j.stats)){
                            if(parseInt(j.stats.yellowcards[1]) > 0){
                              console.log(a.name + parseInt(j.stats.redcards[0]))
                              html += '<i class="yellow_card" style="">'+j.stats.yellowcards[1]+'</i>';
                            }else{
                              html += '<i class="yellow_card" style="display:none">0</i>';
                            }
                            if(j.stats.redcards[1] > 0){
                              html += '<i class="red_card" >'+j.stats.redcards[1]+'</i>';
                            }else{
                              html += '<i class="red_card" style="display:none">0</i>';
                            }
                          }

                      html += '<a  href="#">'+a.name+'</a></span>';      
                      html += '<img src="https://assets.b365api.com/images/team/m/'+a.image_id+'.png"></li>';

                      if((j) && (j.time_status == 1)){
                        html += '<li class="col5"><a href="javascript:void(0);" ><img src="/assets/images/birs.gif"></a></li>'; 
                      }else{
                        html += '<li class="col5"><a href="javascript:void(0);" ></a></li>'; 
                      }
                           
                      html += '<li class="col7 odds"><a target="_blank" href="/detail?q='+i.upcomingevent_id+'"><img src="/assets/images/icon1.png" class="btn" title="Tips"></a></li></ul>';     

                      

                    
                  });
                  $('#js_liveList').html(html);
                  // js_liveList
                }
            });

        
           
    }

  </script>
</body>

</html>