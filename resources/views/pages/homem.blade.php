
<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,width=device-width, user-scalable=no, minimal-ui">
  <meta name="description" content="i1bnScore">
  <meta name="author" content="Jason Chong">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>England Championship Matches | England Premier League Matches | England League 1 Matches | England League 2 Matches | France Ligue 1 Matches | France Ligue 2 Matches | Germany Bundesliga I Matches | Germany Bundesliga II Matches | UEFA Champions League Matches | UEFA Europa League League Matches</title>
  <!-- Favicon -->
  <link rel="icon" href="../assets/img/brand/favicon.png" type="image/png">
  <!-- Fonts -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700">
  <!-- Icons -->
  <link rel="stylesheet" href="../assets/vendor/nucleo/css/nucleo.css" type="text/css">
  <link rel="stylesheet" href="../assets/vendor/@fortawesome/fontawesome-free/css/all.min.css" type="text/css">
  <link rel="stylesheet" href="../assets/vendor/datatables.net-bs4/css/dataTables.bootstrap4.min.css" type="text/css">
  <link rel="stylesheet" href="../assets/vendor/select2/dist/css/select2.min.css" type="text/css">
  <link rel="stylesheet" href="../assets/vendor/sweetalert2/dist/sweetalert2.min.css" type="text/css">
  <!-- Argon CSS -->
  {{-- <link rel="stylesheet" href="../assets/css/argon.css?v=1.2.0" type="text/css"> --}}
  {{-- <link rel="stylesheet" href="../css/tms.css" type="text/css"> --}}


  <link rel="stylesheet" href="../assets/css/base1.css" type="text/css">
  <link rel="stylesheet" href="../assets/css/font.css" type="text/css">
  <link rel="stylesheet" href="../assets/css/skin.css" type="text/css">
  <link rel="stylesheet" href="../assets/css/live.css?v20221202" type="text/css">
  <link rel="stylesheet" href="../assets/css/m_urs.css" type="text/css">
  <style>

    .hdb_toMobile {
        display: inline-block;

        position: absolute;
        top: 10px;
        right: 83px;
        background: #ec2958;
        color: white;
        border-radius: 10px;
        padding: 10px;
        font-size: 11px;
    }
    .hdb_toMobile2 {
      display: inline-block;
      position: absolute;
      top: 10px;
      right: 23px;
      background: #ec2958;
      color: white;
      border-radius: 10px;
      padding: 9px;
      font-size: 11px;
    }
    .hdb_toMobile2.on,.hdb_toMobile.on{
      background:#9f062b;
      border-bottom: 1px solid red;
    }
  </style>

<!-- Google tag (gtag.js) -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-WBYJ4RHC6F"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-WBYJ4RHC6F');
</script>
</head>

<body>

  <!-- Main content -->
<header class="headTab">
    <ul id="js_tab">
        <li class="on" id="data-live" pid = 'js_live'>
         
          <?php
          if($data['lang'] == 1){
              echo "In-play";
          }else{
              echo "即時比分";
          }
          ?>
        </li>
        <li id="data-upcoming" pid = 'js_upcoming'>
          <?php
          if($data['lang'] == 1){
              echo "Upcoming";
          }else{
              echo "赛事";
          }
          ?>
          
        </li>
        <li id="data-end" pid = 'js_end'>
          <?php
          if($data['lang'] == 1){
              echo "Results";
          }else{
              echo "成绩";
          }
          ?>
          
        </li>
        {{-- <li id="data-gMatch" onclick="app.gMatchShow();">Groups</li> --}}
    </ul>
    <a href="/api/language?lang=en" class="hdb_toMobile changelanguage <?php if($data['lang'] == 1){ echo " on";}?>">English</a>
    <a href="/api/language?lang=cn"  class="hdb_toMobile2 changelanguage <?php if($data['lang'] == 2){ echo " on";}?>">中文</a>
    
</header>
<section class="bodyBox" id="js_tabC">
  <div class="tabC" style="display: block;" id="js_live">
    <div class="tab_m" id="js_ge_a">
      <?php
      foreach($data['league'] as $i => $lea){
        $lea_json = json_decode($lea['leaguetable_season'],true);
      ?>
      <a href="javascript:void(0);" class = 'search_list <?php if($i == 0){ echo " on";}?>' league_id = '{{$lea['leaguetable_league_id']}}' style="display: inline-block;">{{str_replace('22/23','',$lea_json['name'])}}</a>
      <?php
      }
      ?>
    </div>

    <?php
    if(count($data['Upcomingevent']) == 0){
    ?>
    <div id="js_none_live" class="g_loadingBox"><img src="//static.7m.com.cn/images/webapp/m/fifa2018/base/no_data.png"><p>No Data</p></div>
    <?php
    }
    foreach($data['Upcomingevent'] as $d){

      $j = json_decode($d->upcomingevent_api_json,true);
      $a = json_decode($d->upcomingevent_api_away,true);
      $h = json_decode($d->upcomingevent_api_home,true);

      if($j != null){
        
        if((array_key_exists('scores',$j)) && ($j['scores']) ){
          if(isset($j['scores'][1])){
            $ht = $j['scores'][1]['home'] . ":" . $j['scores'][1]['away'];
          }else{
            $ht = '';
          }
        }else{
          $ht = '';
        }
      }else{
        $ht = '';
      }
    ?>
    <div class="t_list t_list1 t_list1_{{$d->upcomingevent_league_id}}" style = 'display:none'>
      <p class="tl_date"><span><?php echo date('d/m',strtotime($d->upcomingevent_api_date)) . " " . date('l',strtotime($d->upcomingevent_api_date));?></span></p>
      <div data-mtype="" class="tl_box tl_on"  data-time="{{$d->upcomingevent_api_date}}" data-status="3" id="event_{{$d->upcomingevent_id }}">
         <a class="tlb_box" href="/detail?q={{$d->upcomingevent_id}}">
            <p class="tlbb_title">
              <span class="t1">{{date('h:i',strtotime($d->upcomingevent_api_date))}}</span>
              <span>{{$d->upcomingevent_api_group}}</span>
              <span class="t2" <?php if($ht == ""){ echo "style = 'border:0px solid'";}?>> <?php if($ht != ""){ echo "HT " . $ht;}?></span>
              <span data-timing="49" class="time ">
                <?php
                if(($j) && (isset($j['time_status'])) && ($j['time_status'] == 1)){
                  if($j['timer']['ta'] > 0){
                      echo $j['timer']['tm'] . "<br><span style = 'font-size:9px;color:#9a1032'>+".$j['timer']['ta']."</span>";
                  }else{
                      echo $j['timer']['tm'];
                  }
                }else if(($j) && (isset($j['time_status'])) && ($j['time_status'] == 3)){
                  if($data['lang'] == 1){
                    echo "End";
                  }else{
                    echo "完";
                  }
                }else{
                  if($data['lang'] == 1){
                    echo "Not Started";
                  }else{
                    echo "未";
                  }
                }
                ?>
              </span>
            </p>
            <div class="tlbb_box">
               <div class="info r home">
                  <span class="i1">&nbsp;</span>                
                  <p class="i2"><em class="yellow_card yellow_l" style="display:none">0</em><em class="red_card red_l" style="display:none">0</em><span>{{$h['name']}}</span></p>
               </div>
               <div class="img"><img class = 'home_img' src="https://assets.b365api.com/images/team/m/{{$h['image_id']}}.png"></div>
               <div class="vs">
                <?php
                 $dd = explode(" - ",$d->upcomingevent_api_score);
                 if($dd[0] > $dd[1]){
                  echo '<span kk><b style="color:#cc1300;">'.$dd[0].'</b> - '.$dd[1].'</span>';
                 }else if($dd[1] > $dd[0]){
                  echo '<span tt>'.$dd[0].' - <b style="color:#cc1300;">'.$dd[1].'</b></span>';
                 }else if($dd[1] == $dd[0]){
                  echo '<span tt>'.$dd[0].' - '.$dd[1].'</span>';
                 }else{
                  echo '<span>0 - 0</span>';
                 }
                ?>
              </div>
              <div class="img"><img class = 'away_img' src="https://assets.b365api.com/images/team/m/{{$a['image_id']}}.png"></div>
               <div class="info away">
                  <span class="i1">&nbsp;</span>                
                  <p class="i2"><span>{{$a['name']}}</span><em class="yellow_card yellow_r" style="display:none">0</em><em class="red_card red_r" style="display:none">0</em></p>
               </div>
            </div>
         </a>
         <div class="bayesBoxNoLogin"> 
            <p><i></i><span id = 'prediction_{{$d->upcomingevent_id}}'></span></p>
         </div>
         <p style="display:none" class="tlbb_remark"></p>
      </div>
    </div>
   <?php }?>
  </div>

  <div class="tabC" style="display: none;" id="js_upcoming">
    <div class="tab_m" id="js_ge_a">
      <?php
      foreach($data['league'] as $i => $lea){
        $lea_json = json_decode($lea['leaguetable_season'],true);
      ?>
      <a href="javascript:void(0);" class = 'search_list3 <?php if($i == 0){ echo " on";}?>' league_id = '{{$lea['leaguetable_league_id']}}' style="display: inline-block;">{{str_replace('22/23','',$lea_json['name'])}}</a>
      <?php
      }
      ?>
    </div>

    <?php
    if(count($data['Upcomingevent2']) == 0){
    ?>
    <div id="js_none_live" class="g_loadingBox"><img src="//static.7m.com.cn/images/webapp/m/fifa2018/base/no_data.png"><p>No Data</p></div>
    <?php
    }
    foreach($data['Upcomingevent2'] as $d){

      $j = json_decode($d->upcomingevent_api_json,true);
      $a = json_decode($d->upcomingevent_api_away,true);
      $h = json_decode($d->upcomingevent_api_home,true);

      if($j != null){
        
        if((array_key_exists('scores',$j)) && ($j['scores']) ){
          if(isset($j['scores'][1])){
            $ht = $j['scores'][1]['home'] . ":" . $j['scores'][1]['away'];
          }else{
            $ht = '';
          }
        }else{
          $ht = '';
        }
      }else{
        $ht = '';
      }
    ?>
    <div class="t_list t_list3 t_list3_{{$d->upcomingevent_league_id}}" style = 'display:none'>
      <p class="tl_date"><span><?php echo date('d/m',strtotime($d->upcomingevent_api_date)) . " " . date('l',strtotime($d->upcomingevent_api_date));?></span></p>
      <div data-mtype="" class="tl_box tl_on"  data-time="{{$d->upcomingevent_api_date}}" data-status="3" id="event_{{$d->upcomingevent_id }}">
         <a class="tlb_box" href="/detail?q={{$d->upcomingevent_id}}">
            <p class="tlbb_title">
              <span class="t1">{{date('h:i',strtotime($d->upcomingevent_api_date))}}</span>
              <span>{{$d->upcomingevent_api_group}}</span>
              <span class="t2" <?php if($ht == ""){ echo "style = 'border:0px solid'";}?>> <?php if($ht != ""){ echo "HT " . $ht;}?></span>
              <span data-timing="49" class="time ">
                <?php
                if(($j) && (isset($j['time_status'])) && ($j['time_status'] == 1)){
                  if($j['timer']['ta'] > 0){
                      echo $j['timer']['tm'] . "<br><span style = 'font-size:9px;color:#9a1032'>+".$j['timer']['ta']."</span>";
                  }else{
                      echo $j['timer']['tm'];
                  }
                }else if(($j) && (isset($j['time_status'])) && ($j['time_status'] == 3)){
                  if($data['lang'] == 1){
                    echo "End";
                  }else{
                    echo "完";
                  }
                }else{
                  if($data['lang'] == 1){
                    echo "Not Started";
                  }else{
                    echo "未";
                  }
                }
                ?>
              </span>
            </p>
            <div class="tlbb_box">
               <div class="info r home">
                  <span class="i1">&nbsp;</span>                
                  <p class="i2"><em class="yellow_card yellow_l" style="display:none">0</em><em class="red_card red_l" style="display:none">0</em><span>{{$h['name']}}</span></p>
               </div>
               <div class="img"><img class = 'home_img' src="https://assets.b365api.com/images/team/m/{{$h['image_id']}}.png"></div>
               <div class="vs">
                <?php
                 $dd = explode(" - ",$d->upcomingevent_api_score);
                 if($dd[0] > $dd[1]){
                  echo '<span kk><b style="color:#cc1300;">'.$dd[0].'</b> - '.$dd[1].'</span>';
                 }else if($dd[1] > $dd[0]){
                  echo '<span tt>'.$dd[0].' - <b style="color:#cc1300;">'.$dd[1].'</b></span>';
                 }else if($dd[1] == $dd[0]){
                  echo '<span tt>'.$dd[0].' - '.$dd[1].'</span>';
                 }else{
                  echo '<span>0 - 0</span>';
                 }
                ?>
              </div>
              <div class="img"><img class = 'away_img' src="https://assets.b365api.com/images/team/m/{{$a['image_id']}}.png"></div>
               <div class="info away">
                  <span class="i1">&nbsp;</span>                
                  <p class="i2"><span>{{$a['name']}}</span><em class="yellow_card yellow_r" style="display:none">0</em><em class="red_card red_r" style="display:none">0</em></p>
               </div>
            </div>
         </a>
         <div class="bayesBoxNoLogin"> 
            <p><i></i><span id = 'prediction_{{$d->upcomingevent_id}}'></span></p>
         </div>
         <p style="display:none" class="tlbb_remark"></p>
      </div>
    </div>
   <?php }?>
  </div>

  <div class="tabC" style="display: none;" id="js_end">
    <div class="tab_m" id="js_ge_a">
      <?php
      foreach($data['league2'] as $i => $lea){
        $lea_json = json_decode($lea['leaguetable_season'],true);
      ?>
      <a href="javascript:void(0);" class = 'search_list2 <?php if($i == 0){ echo " on";}?>' league_id = '{{$lea['leaguetable_league_id']}}' style="display: inline-block;">{{str_replace('22/23','',$lea_json['name'])}}</a>
      <?php
      }
      ?>
    </div>
    <?php
    
    foreach($data['Endedevent'] as $d){

      $j = json_decode($d->upcomingevent_api_json,true);
      $a = json_decode($d->upcomingevent_api_away,true);
      $h = json_decode($d->upcomingevent_api_home,true);
      
      if($j != null){
        
        if((array_key_exists('scores',$j)) && ($j['scores']) ){
          if(isset($j['scores'][1])){
            $ht = $j['scores'][1]['home'] . ":" . $j['scores'][1]['away'];
          }else{
            $ht = '';
          }
        }else{
          $ht = '';
        }
      }else{
        $ht = '';
      }
    ?>
    <div class="t_list t_list2 t_list2_{{$d->upcomingevent_league_id}}" style = 'display:none'>
      <p class="tl_date"><span><?php echo date('d/m',strtotime($d->upcomingevent_api_date)) . " " . date('l',strtotime($d->upcomingevent_api_date));?></span></p>
      <div data-mtype="" class="tl_box tl_on"  data-time="{{$d->upcomingevent_api_date}}" data-status="3" id="event_{{$d->upcomingevent_id }}">
         <a class="tlb_box" href="/detail?q={{$d->upcomingevent_id}}">
            <p class="tlbb_title">
              <span class="t1">{{date('h:i',strtotime($d->upcomingevent_api_date))}}</span>
              <span>{{$d->upcomingevent_api_group}}</span>
              <span class="t2">HT <?php echo $ht;?></span>
              <span data-timing="49" class="time "><?php if($data['lang'] == 1){ echo "FINISHED";}else{ echo "完";}?></span>
            </p>
            <div class="tlbb_box">
               <div class="info r home">
                  <span class="i1">&nbsp;</span>                
                  <p class="i2">
                    <?php 
                    if(($j) && isset($j['stats']) && ($j['stats']['yellowcards'][0])){
                    ?>
                    <em class="yellow_card yellow_l">{{$j['stats']['yellowcards'][0]}}</em>
                    <?php
                    }
                    ?>
                    <?php 
                    if(($j) && isset($j['stats']) && ($j['stats']['redcards'][0])){
                    ?>
                    <em class="red_card red_l">{{$j['stats']['redcards'][0]}}</em>
                    <?php
                    }
                    ?>
                    <span>{{$h['name']}}</span>
                  </p>
               </div>
               <div class="img"><img class = 'home_img' src="https://assets.b365api.com/images/team/m/{{$h['image_id']}}.png"></div>
               <div class="vs">
                <?php
                 $dd = explode(" - ",$d->upcomingevent_api_score);
                 if($dd[0] > $dd[1]){
                  echo '<span><b style="color:#cc1300;">'.$dd[0].'</b> - '.$dd[1].'</span>';
                 }else if($dd[1] > $dd[0]){
                  echo '<span>'.$dd[1].' - <b style="color:#cc1300;">'.$dd[0].'</b></span>';
                 }else if($dd[1] == $dd[0]){
                  echo '<span>'.$dd[0].' - '.$dd[1].'</span>';
                 }else{
                  echo '<span>0 - 0</span>';
                 }
                ?>
              </div>
               <div class="img"><img class = 'away_img' src="https://assets.b365api.com/images/team/m/{{$a['image_id']}}.png"></div>
               <div class="info away">
                  <span class="i1">&nbsp;</span>                
                  <p class="i2"><span>{{$a['name']}}</span>
                    <?php 
                    if(($j) && isset($j['stats']) && ($j['stats']['yellowcards'][1])){
                    ?>
                    <em class="yellow_card yellow_r">{{$j['stats']['yellowcards'][1]}}</em>
                    <?php
                    }
                    ?>
                    <?php 
                    if(($j) && isset($j['stats']) && ($j['stats']['redcards'][1])){
                    ?>
                    <em class="red_card red_r">{{$j['stats']['redcards'][1]}}</em>
                    <?php
                    }
                    ?>
                  </p>
               </div>
            </div>
         </a>
         <div class="bayesBoxNoLogin">
            <p><i></i><span id = 'prediction_{{$d->upcomingevent_id}}'>{{$d->upcoming_prediction}}</span></p>
         </div>
         <p style="<?php if(isset($j['scores'][4])){ echo "display:block";}else{ echo "display:none";}?>" class="tlbb_remark">
          <?php
              if(($j) && ($j['time_status'] == 3)){
                  if(isset($j['scores'][4])){
                      $html = "90 minutes[".$j['scores'][2]['home']."-".$j['scores'][2]['away']."], 120 minutes[".$j['scores'][3]['home']."-".$j['scores'][3]['away']."], Penalty Kicks[".$j['scores'][4]['home']."-".$j['scores'][4]['away']."]";
                  }else if(isset($j['scores'][3])){
                      $html = "90 minutes[".$j['scores'][2]['home']."-".$j['scores'][2]['away']."], 120 minutes[".$j['scores'][3]['home']."-".$j['scores'][3]['away']."]";
                  }else{
                      $html = "90 minutes[".$j['scores'][2]['home']."-".$j['scores'][2]['away']."]";
                  }
                  echo $html;
              }
          ?>
         </p>
      </div>
    </div>
   <?php }?>
  </div>

</section>

  <input type = "hidden" id = 'sound' value = ''/>
{{-- <button id = 'play' >&nbsp;</button> --}}

  <!-- Argon Scripts -->
  <!-- Core -->
  <script src="../assets/vendor/jquery/dist/jquery.min.js"></script>
  <script src="../assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
  <script src="../assets/vendor/js-cookie/js.cookie.js"></script>
  <script src="../assets/vendor/jquery.scrollbar/jquery.scrollbar.min.js"></script>
  <script src="../assets/vendor/jquery-scroll-lock/dist/jquery-scrollLock.min.js"></script>
  <script src="../assets/vendor/datatables.net/js/jquery.dataTables.min.js"></script>
  <script src="../assets/vendor/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
  <script src="../assets/vendor/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
  <script src="../assets/vendor/select2/dist/js/select2.min.js"></script>
  <script src="../assets/vendor/sweetalert2/dist/sweetalert2.min.js"></script>
  <script src="../js/tmsjs/jqueryvalidation/jquery.validate.1.8.js"></script>
  <script src="../js/tmsjs/loadingoverlay/loadingoverlay.min.js"></script>
  <script src="../js/ion.sound-3.0.7/ion.sound.js"></script>
  <!-- Argon JS -->
  <script src="../assets/js/argon.js?v=1.2.0"></script>

  <script>
    var myInterval = null;
    $(document).ready( function () {
      $('.changelanguage').click(function(){
        $('.changelanguage').removeClass('on');
        $(this).addClass('on');
      });
      $('#js_tab li').click(function(){
        $('#js_tab li').removeClass('on');
        $(this).addClass('on');
        $('.tabC').css('display','none');
        $('#'+$(this).attr('pid')).css('display','block');
        

      });

      ion.sound({
          sounds: [
              {
                  name: "door_bell"
              }
          ],
          volume: 1.0,
          path: "../js/ion.sound-3.0.7/sounds/",
          // preload: true
      }); 
      
      $('.bmt_sound').click(function(){
          if($('#sound').val() != ""){
            ion.sound.play("door_bell");
            return true;
          }
          if($(this).hasClass('bmt_sound_on')){
            $(this).removeClass('bmt_sound_on');
            $(this).attr('title','Sound On')
          }else{
            $(this).addClass('bmt_sound_on');
            $(this).attr('title','Sound Off'); 
            ion.sound.play("door_bell");
          }
      });
      $('.bmt_refresh').click(function(){
          getHomeResult();
      });

      $('.league_title').click(function(){
          $('.league_detail').removeClass('on');
          $('.league_title').removeClass('on');
          $('.league_detail').addClass('hide');
          $('#league_detail_'+$(this).attr('pid')).addClass('on');
          $('#league_detail_'+$(this).attr('pid')).removeClass('hide');
          $(this).addClass('on');

      });

      $('.league_title2').click(function(){
          $('.league_detail2').removeClass('on');
          $('.league_title2').removeClass('on');
          $('.league_detail2').addClass('hide');
          $('#league_detail_'+$(this).attr('pid')).addClass('on');
          $('#league_detail_'+$(this).attr('pid')).removeClass('hide');
          $(this).addClass('on');

      });
      $('.search_list').click(function(){
        search($(this).attr('league_id'));
        $('.search_list').removeClass('on');
        $(this).addClass('on');
      });
      search();

      $('.search_list2').click(function(){
        search2($(this).attr('league_id'));
        $('.search_list2').removeClass('on');
        $(this).addClass('on');
      });
      search2();

      $('.search_list3').click(function(){
        search3($(this).attr('league_id'));
        $('.search_list3').removeClass('on');
        $(this).addClass('on');
      });
      search3();


      // $('#play').click(function(){
      //   ion.sound.play("door_bell");
      // });
      myInterval = setInterval(getHomeResult, 3000);
      
      // getHomeResult();
    });
    function search(league_id){
      if(!league_id){
        $('.t_list1:eq(0)').css('display','block');
      }else{
        $('.t_list1').css('display','none');
        $('.t_list1_'+league_id).css('display','block');
      }
    }
    function search2(league_id){
      if(!league_id){
        $('.t_list2:eq(0)').css('display','block');
      }else{
        $('.t_list2').css('display','none');
        $('.t_list2_'+league_id).css('display','block');
      }
    }
    function search3(league_id){
      if(!league_id){
        $('.t_list3:eq(0)').css('display','block');
      }else{
        $('.t_list3').css('display','none');
        $('.t_list3_'+league_id).css('display','block');
      }
    }
    function getHomeResult(){
          $.ajax({
            <?php
              if($data['lang'] == 1){
                echo "url:'/uploads/json/sources_en.json',";
              }else{
                echo "url:'/uploads/json/sources_cn.json',";
              }
            ?>
                
                type:'GET',
                cache:false,
                success:function(jsonObj){
                  var html = "";
                  var date = "";
                  var istrue = true;
                  // $('#sound').val('');
                  $(jsonObj).each(function(e,i){
                    var a = $.parseJSON(i.upcomingevent_api_away)
                    var h = $.parseJSON(i.upcomingevent_api_home)
                    var l = $.parseJSON(i.upcomingevent_api_league)
                    var j = $.parseJSON(i.upcomingevent_api_json)
                    if(j != null){
                      if(j.scores){
                        if(j.scores[1]){
                          
                          var ht = j.scores[1]['home'] + ":" + j.scores[1]['away'];
                        }else{
                          var ht = '';
                        }
                      }
                    }else{
                      var ht = '';
                    }
                    
                    
                    
                   
                    if((i.upcomingevent_api_alert == '1') && (istrue == true)){
                      if($('#js_soundSwitch').hasClass('bmt_sound_on')){
                      
                        if(i.upcomingevent_api_score != $('#sound').val()){
                          console.log(i.upcomingevent_api_score +"/" + $('#sound').val());
                          $('#sound').val(i.upcomingevent_api_score);
                          $('#js_soundSwitch').trigger('click');
                        }
                      }
                        istrue = false;
                    }
                      var upcomingevent_api_score = i.upcomingevent_api_score.split(" - ");
                      var text = "<span>";
                      if(upcomingevent_api_score[0] > upcomingevent_api_score[1]){
                        text += '<b style="color:#cc1300;">'+upcomingevent_api_score[0]+'</b> : ' + upcomingevent_api_score[1];
                      }else if(upcomingevent_api_score[1] > upcomingevent_api_score[0]){
                        text += upcomingevent_api_score[0] + ' : <b style="color:#cc1300;">'+upcomingevent_api_score[1]+'</b>'
                      }else{
                        text += i.upcomingevent_api_score;
                      }

                      text += "</span>";
                      if((ht != "") && (ht != undefined)){
                        $('#event_'+i.upcomingevent_id + ' .t2').css('border-left','1px solid #666666');
                        $('#event_'+i.upcomingevent_id + ' .t2').html('HT ' + ht);
                      }else{
                        $('#event_'+i.upcomingevent_id + ' .t2').css('border-left','0px');
                        $('#event_'+i.upcomingevent_id + ' .t2').html('');
                      }
                      
                        
                      $('#event_'+i.upcomingevent_id + ' .vs').html(text);
                      
                      $('#event_'+i.upcomingevent_id + ' .home .i2 span').text(h.name);
                      $('#event_'+i.upcomingevent_id + ' .away .i2 span').text(a.name);

                      if((j) && (j.stats)){
                        if(parseInt(j.stats.yellowcards[0]) > 0){
                          $('#event_'+i.upcomingevent_id + ' .home .yellow_r').text(j.stats.yellowcards[0]);
                          $('#event_'+i.upcomingevent_id + ' .home .yellow_r').css('display','');
                        }else{
                          $('#event_'+i.upcomingevent_id + ' .home .yellow_r').css('display','none');
                        }
                        if(j.stats.redcards[0] > 0){
                          $('#event_'+i.upcomingevent_id + ' .home .red_l').text(j.stats.redcards[0]);
                          $('#event_'+i.upcomingevent_id + ' .home .red_l').css('display','');
                        }else{
                          $('#event_'+i.upcomingevent_id + ' .home .red_l').css('display','none');
                        }
                      }
                      
                      
                      if((j) && (j.stats)){
                        if(parseInt(j.stats.yellowcards[1]) > 0){
                          $('#event_'+i.upcomingevent_id + ' .away .yellow_r').text(j.stats.yellowcards[1]);
                          $('#event_'+i.upcomingevent_id + ' .away .yellow_r').css('display','');
                        }else{
                          $('#event_'+i.upcomingevent_id + ' .away .yellow_r').css('display','none');
                        }
                        if(j.stats.redcards[1] > 0){
                          $('#event_'+i.upcomingevent_id + ' .away .red_l').text(j.stats.redcards[1]);
                          $('#event_'+i.upcomingevent_id + ' .away .red_l').css('display','');
                        }else{
                          $('#event_'+i.upcomingevent_id + ' .away .red_l').css('display','none');
                        }
                      }
                      
                      if(i.upcoming_prediction == null){
                        $('#prediction_'+i.upcomingevent_id).text('Coming Soon');
                      }else{
                        $('#prediction_'+i.upcomingevent_id).text(i.upcoming_prediction);
                      }
                      
                      $('#event_'+i.upcomingevent_id + ' .home_img').attr('src','https://assets.b365api.com/images/team/m/'+h.image_id+'.png');
                      $('#event_'+i.upcomingevent_id + ' .away_img').attr('src','https://assets.b365api.com/images/team/m/'+a.image_id+'.png');

                      
                      if((j) && (j.time_status == 1)){
                          
                          
                          if(j.timer.ta > 0){
                            var kkk = "<br><span style = 'font-size:9px;color:#9a1032'>+"+j.timer.ta+ '</span>';
                          }else{
                            var kkk = '';
                          }
                          $('#event_'+i.upcomingevent_id + ' .time').html(j.timer.tm +"'" + kkk);
                      }else if((j) && (j.time_status == 3)){
                          <?php
                          if($data['lang'] == 1){
                          ?>
                          $('#event_'+i.upcomingevent_id + ' .time').text('End');
                          <?php
                          }else{
                          ?>
                          $('#event_'+i.upcomingevent_id + ' .time').text('完');
                          <?php
                          }
                          ?>
                      }else{
                        <?php
                          if($data['lang'] == 1){
                          ?>
                          $('#event_'+i.upcomingevent_id + ' .time').text('Not Started');
                          <?php
                          }else{
                          ?>
                          $('#event_'+i.upcomingevent_id + ' .time').text('未');
                          <?php
                          }
                          ?>
                         
                      }

                      if((j) && (j.time_status == 1)){
                        html += '<li class="col5"><a href="javascript:void(0);" ><img src="/assets/images/birs.gif"></a></li>'; 
                      }else{
                        html += '<li class="col5"><a href="javascript:void(0);" ></a></li>'; 
                      }
                           
                      html += '<li class="col7 odds"><a target="_blank" href="/detail?q='+i.upcomingevent_id+'"><img src="/assets/images/icon1.png" class="btn" title="Tips"></a></li></ul>';     

                      

                    
                  });
                  $('#js_liveList').html(html);
                  // js_liveList
                }
            });

        
           
    }

  </script>
</body>

</html>