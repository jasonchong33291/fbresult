<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  {{-- <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"> --}}
  <meta name="description" content="i1bnScore">
  <meta name="author" content="Jason Chong">
  <title>2022 FIFA World Cup|2022 FIFA World Cup Qualifiers| FIFA World Cup Qatar Qualifiers|2022 FIFA World Cup Qatar Qualifiers|2022 FIFA World Cup Match Fixtures|2022 FIFA World Cup Matches</title>
  <!-- Favicon -->
  <link rel="icon" href="../assets/img/brand/favicon.png" type="image/png">
  <!-- Fonts -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700">
  <!-- Icons -->
  <link rel="stylesheet" href="../assets/vendor/nucleo/css/nucleo.css" type="text/css">
  <link rel="stylesheet" href="../assets/vendor/@fortawesome/fontawesome-free/css/all.min.css" type="text/css">
  <link rel="stylesheet" href="../assets/vendor/datatables.net-bs4/css/dataTables.bootstrap4.min.css" type="text/css">
  <link rel="stylesheet" href="../assets/vendor/select2/dist/css/select2.min.css" type="text/css">
  <link rel="stylesheet" href="../assets/vendor/sweetalert2/dist/sweetalert2.min.css" type="text/css">
  <!-- Argon CSS -->
  <link rel="stylesheet" href="../assets/css/argon.css?v=1.2.0" type="text/css">
  <link rel="stylesheet" href="../css/tms.css" type="text/css">
  <link rel="stylesheet" href="https://2022.7m.com.cn/static/css/items/match.css" type="text/css">
  <style>
  .main-content{
    width: 100%;
    min-width: 1200px;
  }
  .hdb_tab {
      height: 50px;
      background: #9a1032;
      position: relative;
  }
  .hdb_tab ul li {
      display: inline-block;
      vertical-align: top;
      height: 50px;
      margin-left: 46px;
      position: relative;
  }
  .hdb_tab ul li a {
      font-size: 16px;
      display: block;
      color: #fff;
      line-height: 50px;
      text-decoration: none;
      height: 47px;
  }
  .b_match .bm_tab {
      height: 48px;
      border-bottom: 2px solid #9a1032;
      background: #f8f8f8;
      position: relative;
      margin-bottom: 10px;
  }
  .b_match .bm_tab .bmt_tab .on {
      background: #9a1032;
      color: #fff;
  }
  .b_match .bm_tab .bmt_tab li {
      cursor: pointer;
      display: inline-block;
      vertical-align: top;
      width: 100px;
      height: 48px;
      line-height: 48px;
      text-align: center;
      color: #353535;
      font-size: 16px;
  }
  .b_match .bm_title {
      width: 100%;
  }
  .b_match .bm_title ul {
      background: #e5e5e5;
      font-size: 0;
      text-align: left;
  }
  .b_match .bm_title .col0 {
      width: 74px;
  }
  .b_match .bm_title .col1 {
      width: 50px;
  }
  .b_match .bm_title .col2 {
      width: 50px;
  }
  .b_match .bm_title .col3 {
      width: 426px;
  }
  .b_match .bm_title .col4 {
      width: 30px;
  }
  .b_match .bm_title .col5 {
      width: 28px;
      white-space: nowrap;
  }
  .b_match .bm_title .col6 {
      width: 140px;
  }
  .b_match .bm_title .col7 {
      width: 80px;
  }
  .b_match .bm_title li {
      display: inline-block;
      vertical-align: middle;
      color: #323232;
      font-size: 12px;
      text-align: center;
      line-height: 14px;
      padding: 8px 0;
  }
  .b_match .bm_table .bmt_title {
      border-bottom: 1px solid #ebebeb;
      border-left: none;
  }
  .b_match .bm_table .bmt_title li {
      text-align: left;
      border: none;
      padding-left: 10px;
      line-height: 30px;
      margin-left: 0px;
  }
  .b_match .bm_table li em {
      position: absolute;
      width: 4px;
      height: 12px;
      background: #9a1032;
      top: 50%;
      left: 0;
      margin-top: -6px;
  }
  .b_match .bm_table .row {
      width: 100%;
      display: block;
      background: #fff;
      padding: 0;
  }
  .b_match{
    margin-top: 30px;
  }
  ol, ul {
      list-style: none;
  }
  ul{
    margin: 0px;
    padding: 0px;
  }
  .b_match .bm_table ul {
      position: relative;
      font-size: 0;
      text-align: left;
      border-left: 1px solid #ebebeb;
  }
  .glist {
      font-size: 0;
      border: 1px solid #ececec;
      padding: 20px;
      margin-top: 20px;
      margin-bottom: 20px;
      display:flex;
  }
  .glist .g_l {
      display: inline-block;
      *display: inline;
      *zoom: 1;
      vertical-align: top;
      width: 816px;
  }
  .glist .g_l .gl_title {
      padding-left: 12px;
      font-size: 24px;
      color: #9a1032;
      font-weight: bold;
      height: 40px;
      line-height: 20px;
      padding-top:10px;
      *line-height: 30px;
  }
  .glist .g_l .gl_table {
      width: 800px;
  }
  .glist .g_l .gl_table tr.even {
      background: #f8f8f8;
  }
  .glist .g_l .gl_table tr.title {
      background: #e5e5e5;
      color: #323232;
  }
  .glist .g_l .gl_table tr {
      font-size: 14px;
      color: #666;
      height: 44px;
      line-height: 40px;
  }
  .glist .g_r {
      display: inline-block;
      *display: inline;
      *zoom: 1;
      vertical-align: top;
      width: 340px;
  }
  .glist .g_r .gr_title {
      margin-bottom: 14px;
      height: 40px;
      line-height: 28px;
      font-size: 18px;
      color: #323232;
      border-bottom: 1px solid #e6e6e6;
  }
  .glist .g_r .gr_table {
      width: 100%;
      text-align: center;
      border: 1px solid #ebebeb;
      /* display:flex; */
  }
  .glist .g_r .gr_table tr.width {
      color: #afb8bd;
  }
  .glist .g_r .gr_table tr {
      font-size: 14px;
      color: #666;
      height: 43px;
      line-height: 43px;
      border-bottom: 1px solid #fff;
  }
  .glist .g_l .gl_table td img {
      width: 27px;
      height: 16px;
      border: 1px solid #ECEEEB;
  }
  .glist .g_l .gl_table td a, .glist .g_l .gl_table td img {
      display: inline-block;
      *display: inline;
      *zoom: 1;
      vertical-align: middle;
  }
  .glist .g_l .gl_table td .r_name {
      text-align: right;
      padding-right: 6px;
  }
  .glist .g_l .gl_table td {
    border: 1px solid #ebebeb;
    text-align: center;
    vertical-align: middle;
}
  .glist .g_l .gl_table td .l_name, .glist .g_l .gl_table td .r_name {
      display: inline-block;
      *display: inline;
      *zoom: 1;
      vertical-align: middle;
      font-weight: bold;
      color: #323232;
      width: 110px;
      overflow: hidden;
      text-overflow: ellipsis;
      white-space: nowrap;
  }
  .glist .g_l .gl_table td .l_name {
    text-align: left;
    padding-left: 6px;
}
.glist .g_l .gl_table td .vs {
    font-weight: bold;
    color: #323232;
    width: 50px;
}
a {
    text-decoration: none;
    color: #000;
}
.glist .g_l .gl_table td img.btn {
    width: auto;
    height: auto;
    border: none;
}
.glist .g_r .gr_table {
    width: 100%;
    text-align: center;
    border: 1px solid #ebebeb;
}
.glist .g_r .gr_table tr.width {
    color: #afb8bd;
}
.glist .g_r .gr_table tr.width td {
    color: #323232;
    font-size: 14px;
}
.cafb6bc {
    color: #afb6bc!important;
}
.glist .g_r .gr_table tr {
    font-size: 14px;
    color: #666;
    height: 43px;
    line-height: 43px;
    border-bottom: 1px solid #fff;
}
.glist .g_r .gr_table td.red {
    color: #9a1032;
    font-weight: bold;
}
.glist .g_r .gr_table td.info {
    text-align: left;
    white-space: nowrap;
}
.glist .g_r .gr_table td img {
    width: 27px;
    height: 16px;
    border: 1px solid #ECEEEB;
    margin: 0 4px;
}
.hdb_tab li a.on {
    border-bottom: 3px solid #d5c89e;
}
.glist .g_l .gl_table tr:hover, .glist2 .g_l .gl_table tr:hover {
    background-color: #FFFDDA;
}
.body {
    width: 1200px;
    margin: 0 auto;
    position: relative;
}
.jc-c {
    justify-content: center;
}
.flex {
    display: flex;
    flex-direction: row;
}
.glist .g_l .gl_table td .red {
    font-weight: bold;
    color: #be070e;
}

.timeclassstart{
    color: #258D2D;
    font-family: Orbitron;
    font-weight:700;
    font-size: 21px;
    font-style: normal;
    letter-spacing: 5px;
}
  </style>
</head>

<body>

  <!-- Main content -->
  <div class="main-content" id="panel">
    <!-- Topnav -->
    @include('layouts.navbars.topheader') 
    <!-- Header -->
    <!-- Header -->
    <div class="header  pb-6 " style = 'background-color:white;'>
      <div class=" ">
        <div class="header-body">
            <div class=" align-items-center">
              <div class="hdb_tab">
                <div id="js_head_tab" class="container flex jc-sb">
                  <ul>
                    <li>
                      <a data-tab="live" href="/home"  >
                        <?php
                        if($data['lang'] == 1){
                            echo "Live Scores";
                        }else{
                            echo "即時比分";
                        }
                        ?>
                      </a>
                    </li>
                    <li>
                      <a data-tab="match" href="/groups" class="on">
                        <?php
                        if($data['lang'] == 1){
                            echo "Groups";
                        }else{
                            echo "賽程";
                        }
                        ?>
                      </a>
                    </li>
                    {{-- <li>
                      <a data-tab="rank" href="javascript:void(0);" class = "<?php if(Route::currentRouteName() == "standing"){ echo " on";}?>" >Standings</a>
                    </li> --}}
                  </ul>
                </div>
        
              </div>
            </div>

            <div class="col-lg-6 col-5 text-right">
             
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Page content -->
    {{-- mt--6 --}}
    
      <div class="body " >

        <div class="tabC  mt--6 " id="js_groupList" style="display: block;">
          <?php
         
          foreach($data['data'] as $d){  
          ?>  
            <div class="glist">

              <div class="g_l ">
                  <?php
                  echo '<p class="gl_title">'.$d['name'].'</p>';
                  ?>
                  <table class="gl_table" id="js_groupA">
                    <tbody>
                      <tr class="title even">
                        <td width="210">
                          <?php
                          if($data['lang'] == 1){
                              echo "Kick-off Time";
                          }else{
                              echo "時間";
                          }
                          ?>
                          
                        </td>
                        <td width="">
                          <?php
                          if($data['lang'] == 1){
                              echo "Status";
                          }else{
                              echo "狀態";
                          }
                          ?>
                        </td>
                        <td width="366">
                          <?php
                          if($data['lang'] == 1){
                              echo "Game";
                          }else{
                              echo "比賽";
                          }
                          ?>
                        </td>
                        <td width="110"></td>
                      </tr>
                      <?php 
                      $d['hh'] = collect($d['hh']);
                      $d['hh'] = $d['hh']->sortBy('upcomingevent_api_date');
                     
                      foreach($d['hh'] as $h){
                        
                        $upcomingevent_api_home = json_decode($h['upcomingevent_api_home'],true);
                        $upcomingevent_api_away = json_decode($h['upcomingevent_api_away'],true);
                        $upcomingevent_api_json = json_decode($h['upcomingevent_api_json'],true);
                       
                        if(isset($upcomingevent_api_json) && (isset($upcomingevent_api_json['timer']))){
                          
                          $start = date('H:i:00',strtotime($h['upcomingevent_api_date']));
                          $ts = $upcomingevent_api_json['timer']['ts'];
                          $tm = $upcomingevent_api_json['timer']['tm'];
                          $tt = $upcomingevent_api_json['timer']['tt'];
                          $ta = $upcomingevent_api_json['timer']['ta'];
                        }else{
                          $start = '';
                          $ts = '';
                          $tm = '';
                          $tt = '';
                          $ta = '';
                        }
                      ?>
                        <tr>    
                          <td><?php echo date('d/m/Y h:i',strtotime($h['upcomingevent_api_date'])) . " " . date('l',strtotime($h['upcomingevent_api_date']));?></td>    
                          <td class = 'timeclass' pid = '{{$h['upcomingevent_id']}}' id = 'time_id_{{$h['upcomingevent_id']}}' start = '{{$start}}' tm = '<?php echo $tm?>' ts = '<?php echo $ts?>' tt = '<?php echo $tt?>' ta = '<?php echo $ta?>'>
                            <?php
                            if(($upcomingevent_api_json) && ($upcomingevent_api_json['time_status'])){
                              if($upcomingevent_api_json['time_status'] == 1){
                               
                                if(($upcomingevent_api_json['timer']) && ($upcomingevent_api_json['timer']['tm'])){
                                  echo "<span style = 'color:#9a1032;font-size: 15px;'>".$upcomingevent_api_json['timer']['tm']."'</span>";
                                }

                                if(($upcomingevent_api_json['timer']) && ($upcomingevent_api_json['timer']['ta']) && ($upcomingevent_api_json['timer']['ta'] > 0)){
                                  echo "<span style = 'font-size:9px;'>".$upcomingevent_api_json['timer']['ta']."</span>";
                                }
                                
                              }else if($upcomingevent_api_json['time_status'] == 2){
                                echo 'TBE';
                              }else if($upcomingevent_api_json['time_status'] == 3){
                                echo 'END';
                              }else if($upcomingevent_api_json['time_status'] == 4){
                                echo 'PP';
                              }else if($upcomingevent_api_json['time_status'] == 5){
                                echo 'CL';
                              }else if($upcomingevent_api_json['time_status'] == 6){
                                echo 'WR';
                              }else if($upcomingevent_api_json['time_status'] == 7){
                                echo 'IT';
                              }else if($upcomingevent_api_json['time_status'] == 8){
                                echo 'AB';
                              }else if($upcomingevent_api_json['time_status'] == 9){
                                echo 'RT';
                              }else if($upcomingevent_api_json['time_status'] == 10){
                                echo 'SP';
                              }else if($upcomingevent_api_json['time_status'] == 11){
                                echo 'DBF';
                              }else if($upcomingevent_api_json['time_status'] == 99){
                                echo 'RM';
                              }else{
                               
                                if($data['lang'] == 1){
                               
                                  echo 'UN';
                               
                                }else{
                                
                                  echo "未";
                                }
                                
                              }
                            }else{
                              if($data['lang'] == 1){
                                
                                echo 'UN';
                              
                              }else{
                              
                                echo "未";
                              }
                            }
                            ?>
                          </td>    
                          <td><img src="https://assets.b365api.com/images/team/m/{{$upcomingevent_api_home['image_id']}}.png">
                            <span class="l_name">
                              <a target="_blank" href="#">{{$upcomingevent_api_home['name']}}</a></span>
                              <?php 
                              if(($upcomingevent_api_json) && ($upcomingevent_api_json['time_status'])){
                                if($upcomingevent_api_json['time_status'] != 0){
                              ?>
                              <a href="/detail?q={{$h['upcomingevent_id']}}" target="_blank" class="vs red">{{$h['upcomingevent_api_score']}}</a>
                                <?php
                                }else{
                                ?>
                                <a href="/detail?q={{$h['upcomingevent_id']}}" target="_blank" class="vs">VS</a>
                                <?php
                                }
                              }else{  
                              ?>
                              <a href="/detail?q={{$h['upcomingevent_id']}}" target="_blank" class="vs">VS</a>
                              <?php
                              }
                                ?>
                              
                              
                              
                              <span class="r_name">        	
                              <a target="_blank" href="#">{{$upcomingevent_api_away['name']}}</a>		 </span>        
                              <img src="https://assets.b365api.com/images/team/m/{{$upcomingevent_api_away['image_id']}}.png">
                          </td> 
                          <td><a target="_blank" href="/detail?q={{$h['upcomingevent_id']}}"><img src="/assets/images/icon1.png" class="btn" title="Tips"></a></td>
                        </tr> 
                      <?php
                      }
                      ?>

                    </tbody>
                  </table>
              </div>

              <div class="g_r">
                <p class="gr_title">
                  <?php
                  if($data['lang'] == 1){
                      echo "Standings";
                  }else{
                      echo "積分榜";
                  }
                  ?>
                </p>
                <table class="gr_table">
                  <tbody>
                    <tr class="first">
                      <td width="60">
                        <?php
                        if($data['lang'] == 1){
                            echo "Ranking";
                        }else{
                            echo "排名";
                        }
                        ?>
                      </td>
                      <td>
                        <?php
                        if($data['lang'] == 1){
                            echo "Team";
                        }else{
                            echo "球隊";
                        }
                        ?>
                      </td>
                      <td width="50">
                        <?php
                        if($data['lang'] == 1){
                            echo "Points";
                        }else{
                            echo "分数";
                        }
                        ?>
                      </td>
                      <td width="90">
                        <?php
                        if($data['lang'] == 1){
                            echo "W/D/L";
                        }else{
                            echo "勝/平/負";
                        }
                        ?>
                      </td>
                    </tr>
                    <?php
                   
                      $dd = json_decode($data['Leaguetable'][0]['leaguetable_overall'],true);
                      $dd = collect($dd['tables']);
                      $dd = $dd->sortBy('groupname');
                      foreach($dd as $i => $k){    
                       
                        if($k['groupname'] == $d['groupname']){
                        ?>
                        <?php
                          $k['rows'] = collect($k['rows']);
                          $k['rows'] = $k['rows']->sortBy('sort_pos');

                          
                          foreach($k['rows'] as $i => $r){
                          ?>
                              <tr class="">
                                <td class="<?php if(($i == 0) || ($i == 1)){ echo "red ";}?> pr"><?php echo $r['sort_pos']?></td>
                                <td class="left">
                                  <a target="_blank" href="#">
                                  <img src="https://assets.b365api.com/images/team/m/<?php echo $r['team']['image_id'];?>.png"><span><?php echo $r['team']['name']?></span>
                                  </a>
                                </td>
                                <td><?php echo $r['points']?></td>
                                <td><?php echo $r['win'] . "/" . $r['draw'] . "/" . $r['loss'];?></td>
                              </tr>
                          <?php
                          }
                        }
                      }
                    ?>
                  </tbody>
                </table>
              </div>
        
            </div>
          <?php  
            }
          ?>
        </div>
        
      </div>


   
  </div>


  <!-- Argon Scripts -->
  <!-- Core -->
  <script src="../assets/vendor/jquery/dist/jquery.min.js"></script>
  <script src="../assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
  <script src="../assets/vendor/js-cookie/js.cookie.js"></script>
  <script src="../assets/vendor/jquery.scrollbar/jquery.scrollbar.min.js"></script>
  <script src="../assets/vendor/jquery-scroll-lock/dist/jquery-scrollLock.min.js"></script>
  <script src="../assets/vendor/datatables.net/js/jquery.dataTables.min.js"></script>
  <script src="../assets/vendor/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
  <script src="../assets/vendor/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
  <script src="../assets/vendor/select2/dist/js/select2.min.js"></script>
  <script src="../assets/vendor/sweetalert2/dist/sweetalert2.min.js"></script>
  <script src="../js/tmsjs/jqueryvalidation/jquery.validate.1.8.js"></script>
  <script src="../js/tmsjs/loadingoverlay/loadingoverlay.min.js"></script>
  <script src="../assets/vendor/moment.min.js"></script>
  <!-- Argon JS -->
  <script src="../assets/js/argon.js?v=1.2.0"></script>

  <script>
    var myInterval = null;
    window.setTimeout( function() {
      window.location.reload();
    }, 30000);
    $(document).ready( function () {
      
      $('.bmt_sound').click(function(){
          if($(this).hasClass('bmt_sound_on')){
            $(this).removeClass('bmt_sound_on');
            $(this).attr('title','Sound On')
          }else{
            $(this).addClass('bmt_sound_on');
            $(this).attr('title','Sound Off') 
          }
      });
      $('.bmt_refresh').click(function(){
          getHomeResult();
      });

      $('.league_title').click(function(){
          $('.league_detail').removeClass('on');
          $('.league_title').removeClass('on');
          $('.league_detail').addClass('hide');
          $('#league_detail_'+$(this).attr('pid')).addClass('on');
          $('#league_detail_'+$(this).attr('pid')).removeClass('hide');
          $(this).addClass('on');

      });

      $('.league_title2').click(function(){
          $('.league_detail2').removeClass('on');
          $('.league_title2').removeClass('on');
          $('.league_detail2').addClass('hide');
          $('#league_detail_'+$(this).attr('pid')).addClass('on');
          $('#league_detail_'+$(this).attr('pid')).removeClass('hide');
          $(this).addClass('on');

      });

      
      
      
      myInterval = setInterval(getIsStart, 3000);
      
    });
    var coundownProcess = null;
    function startCount(upcomingevent_id){
        display = $('#time_id_'+upcomingevent_id);
        $('#time_id_'+upcomingevent_id).addClass('timeclassstart');
        
               // start time and end time
               var startTime = moment($('#time_id_'+upcomingevent_id).attr('start'), 'HH:mm:ss');
                var endTime = moment(moment().format("HH:mm:ss"), 'HH:mm:ss');
                // var endTime = moment(moment().format("HH:mm:ss a"), 'HH:mm:ss a');
                console.log(startTime);
                console.log(endTime);
                // calculate total duration
                var duration = moment.duration(endTime.diff(startTime));
                var minutes = moment.utc(moment(endTime, "HH:mm:ss").diff(moment(startTime, "HH:mm:ss"))).format("mm")
                var HH = moment.utc(moment(endTime, "HH:mm:ss").diff(moment(startTime, "HH:mm:ss"))).format("h")
            
                if((HH == 1) ){
                    var pp = moment(endTime, 'HH').format("H");
                    var ll = moment(startTime, 'HH').format("H");
                    if((parseInt(pp) - parseInt(ll)) > 1){
                        display.text('END');
                    }else{
                        var duration = (minutes * 60) + (HH * 60 * 60);
                    }
                    
                }else{
                    var pp = moment(endTime, 'HH').format("H");
                    var ll = moment(startTime, 'HH').format("H");
                    if((parseInt(pp) - parseInt(ll)) > 1){
                        display.text('END');
                    }else{
                        var duration = (minutes * 60);
                    }
                    
                }
                // var minutes = 300;
                // var seconds = 19;
                var timer = duration,minutes, seconds;
        if(coundownProcess == null){
            
            let coundownProcess = setInterval(function() {
                    minutes = parseInt(timer / 60, 10)
                    seconds = parseInt(timer % 60, 10);

                    minutes = minutes < 10 ? "0" + minutes : minutes;
                    seconds = seconds < 10 ? "0" + seconds : seconds;
                    
                    display.text(minutes + ":" + seconds);
                    ++timer;
                    if (minutes >= 100 ) {
                        clearInterval(coundownProcess); 
                        $('#time_id_'+upcomingevent_id).removeClass('timeclassstart');
                        display.text('END');
                        
                        
                        
                    }
                    }, 1000);
        }
    }
    function getIsStart(){
        $.ajax({
                url:'api/isstart2?q=1',
                type:'GET',
                cache:false,
                success:function(jsonObj){

                    if(jsonObj.status == 1){

                        startCount(jsonObj.upcomingevent_id);
                        clearInterval(myInterval); 
                        
                    }
                }
        });
    }

  </script>
</body>

</html>