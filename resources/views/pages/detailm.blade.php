<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,width=device-width, user-scalable=no, minimal-ui">
  <meta name="description" content="i1bnScore">
  <meta name="author" content="Jason Chong">
  <title>England Championship Matches | England Premier League Matches | England League 1 Matches | England League 2 Matches | France Ligue 1 Matches | France Ligue 2 Matches | Germany Bundesliga I Matches | Germany Bundesliga II Matches | UEFA Champions League Matches | UEFA Europa League League Matches</title>
  <!-- Favicon -->
  <link rel="icon" href="../assets/img/brand/favicon.png" type="image/png">
  <!-- Fonts -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Orbitron:300,400,600,700">
  <!-- Icons -->
  <link rel="stylesheet" href="../assets/vendor/nucleo/css/nucleo.css" type="text/css">
  <link rel="stylesheet" href="../assets/vendor/@fortawesome/fontawesome-free/css/all.min.css" type="text/css">
  <link rel="stylesheet" href="../assets/vendor/datatables.net-bs4/css/dataTables.bootstrap4.min.css" type="text/css">
  <link rel="stylesheet" href="../assets/vendor/select2/dist/css/select2.min.css" type="text/css">
  <link rel="stylesheet" href="../assets/vendor/sweetalert2/dist/sweetalert2.min.css" type="text/css">
  <!-- Argon CSS -->
  {{-- <link rel="stylesheet" href="../assets/css/argon.css?v=1.2.0" type="text/css">
  <link rel="stylesheet" href="../css/tms.css" type="text/css"> --}}
  <link rel="stylesheet" href="../assets/css/base.css" type="text/css">
  <link rel="stylesheet" href="../assets/css/style1.css" type="text/css">
  <link rel="stylesheet" href="../assets/css/A2002_style.css" type="text/css">
  <link rel="stylesheet" href="../assets/css/A2001_style.css" type="text/css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">  
  <style>
    .event_img {
        background: url('../assets/css/event_icon.png?2016071302') no-repeat;
        background-size: 60px 48px;
        width: 12px;
        height: 12px;
        position: relative;
        top: 4px;
        left: 4px;
    }
    .live_right{
        -webkit-transform: inherit;
    }
    .goal_situation_list li div.live_right{
        transform:inherit;
        -webkit-transform:inherit;
    }
    .goal_situation_list li{
        border-right:0px;
        width:100%;
    }
    .live_right_content_text{
        width:100%;
    }
    .live_right_content_text_team_name{
        float:left;
        width:100%;
    }
    @media screen and (min-width: 374px) and (max-width: 479px){
        .game_over {
            margin-top: 10px;
        }
    }
    .r_scoreBoard .rs_tab {
      border-bottom: 1px solid #9a1032;
      font-size: 0;
  }
  #lang_en .r_scoreBoard .rs_tab li {
      padding: 0 10px;
  }
  .r_scoreBoard .rs_tab li.on {
      background: #9a1032;
      color: #fff;
  }
  .r_scoreBoard .rs_tab li {
      cursor: pointer;
      text-align: center;
      line-height: 30px;
      display: inline-block;
      vertical-align: top;
      font-size: 14px;
      border-right: 1px solid #fff;
      color: #353535;
      background: #f8f8f8;
      height: 30px;
      padding: 0 10px;
  }
  .r_scoreBoard .rs_tabC li {
      display: none;
  }
  .r_scoreBoard .rs_tabC li.on {
      display: block;
  }
  .r_scoreBoard .rs_title em {
      position: absolute;
      width: 4px;
      height: 16px;
      background: #9a1032;
      left: 0;
      top: 8px;
  }
  .r_scoreBoard .rs_title p {
      line-height: 30px;
      padding-left: 14px;
      font-size: 18px;
      color: #1f1f1f;
      font-weight: bold;
  }
  .r_scoreBoard .rs_title a {
      position: absolute;
      color: #1f1f1f;
      font-size: 14px;
      right: 4px;
      top: 8px;
  }
  .r_scoreBoard .rs_tabC td {
      height: 40px;
      line-height: 40px;
      vertical-align: middle;
      white-space: nowrap;
  }
  .r_scoreBoard .rs_tabC tr.first {
      font-size: 12px;
      color: #afb8bd;
  }
  .r_scoreBoard .rs_tabC tr {
      background: #fff;
      height: 40px;
      font-size: 14px;
      color: #353535;
      text-align: center;
      border-bottom: 1px solid #fff;
  }
  .r_scoreBoard .rs_tabC td.red {
      color: #be070d;
      font-weight: bold;
  }
  .r_scoreBoard .rs_tabC td.left {
      text-align: left;
      padding-left: 20px;
  }
  .b_match .bm_table .vs .v_l .red_card, .b_match .bm_table .vs .v_l .yellow_card {
      margin-left: 2px;
  }
  .b_match .bm_table .vs .v_l .red_card, .b_match .bm_table .vs .v_r .red_card, .b_match .bm_table .vs .v_l .yellow_card, .b_match .bm_table .vs .v_r .yellow_card {
      display: inline-block;
      width: 11px;
      height: 13px;
      line-height: 15px;
      color: #fff;
      font-family: 'Arial';
      font-size: 12px;
      font-weight: normal;
      text-align: center;
      background: #f04848;
      border-radius: 2px;
      margin-right: 2px;
  }
  .b_match .bm_table .vs .v_l .yellow_card, .b_match .bm_table .vs .v_r .yellow_card {
      background: #fcb103;
      margin-right: 2px;
  }
  .r_scoreBoard .rs_tabC td img {
      width: 27px;
      height: 16px;
      margin-right: 6px;
      border: 1px solid #ebebeb;
  }
  .r_scoreBoard .rs_tabC table {
      width: 100%;
      border-right: 1px solid #ebebeb;
      border-left: 1px solid #ebebeb;
      border-bottom: 1px solid #ebebeb;
  }
  .r_scoreBoard .rs_tabC tr.outgoingLine {
        background: #ebfaf0;
    }
    .league_detail .first td,.league_detail2 .first td{
        font-size: 14px;
        font-weight: 700;
        color: black; 
    }
  </style>

<!-- Google tag (gtag.js) -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-WBYJ4RHC6F"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-WBYJ4RHC6F');
</script>

</head>

<body>
<?php
    $upcomingevent_api_league = json_decode($data['Upcomingevent']['upcomingevent_api_league'],true);
    $upcomingevent_api_json = json_decode($data['Upcomingevent']['upcomingevent_api_json'],true);
    $upcomingevent_api_home = json_decode($data['Upcomingevent']['upcomingevent_api_home'],true);
    $upcomingevent_api_away = json_decode($data['Upcomingevent']['upcomingevent_api_away'],true);

    $dd = explode(" - ",$data['Upcomingevent']['upcomingevent_api_score']);
    if($upcomingevent_api_json != null){
        
        if((array_key_exists('scores',$upcomingevent_api_json)) && ($upcomingevent_api_json['scores']) ){
          if(isset($upcomingevent_api_json['scores'][1])){
            $ht = "(" . $upcomingevent_api_json['scores'][1]['home'] . ":" . $upcomingevent_api_json['scores'][1]['away'] . ")";
          }else{
            $ht = '';
          }
        }else{
          $ht = '';
        }
      }else{
        $ht = '';
      }
?>
    <div class="header" style="">
        <div class="top_part">
            <ul class="top_nav">
                <li class="top_nav_left"><span id="goBack"></span></li>
                <li class="top_nav_center"><div><span id="cup_name">{{$upcomingevent_api_league['name']}}</span></div></li>
                {{-- <li class="top_nav_right"><span></span></li> --}}
            </ul>
        </div>
    
        <div class="center_part">
            <ul class="center_content">
                <li class="team_home">
                    <div class="team_img" id="teamA">
                        <span>
                            <img src="https://assets.b365api.com/images/team/m/<?php echo $upcomingevent_api_home['image_id'];?>.png"  style="height: 100%; width: 100%;">
                        </span>
                    </div>
                    <div class="team_name"><span id="teamNameA">{{$upcomingevent_api_home['name']}}</span></div>
                </li>
                <li class="time_content">
                    <!-- 比赛开始前 -->
                    <div class="time" style="display: none;">00 : 00 : 00</div>

                    <!-- 比赛开始后 -->
                    <div class="center_time" style="display: none;">
                        <?php 
                        if($data['lang'] == 1){
                            echo "Extra time";
                        }else{
                            echo "额外的时间";
                        }
                        ?>
                        
                    </div>
                    <div class="game_score1" style="">{{$dd[0]}} - {{$dd[1]}}</div>
                    <div class="game_over" style="">
                        <span id="goTime">ROUND  <?php if(isset($upcomingevent_api_json['extra']['round'])){ echo $upcomingevent_api_json['extra']['round'];}else{ echo " - ";}?></span>
                        <span id="status"></span>
                    </div>
                    <div class="game_score2" style="display: block;">{{$ht}}</div>
    
                </li>
                <li class="team_away">
                    <div class="team_img" id="teamB"><span>
                        <img src="https://assets.b365api.com/images/team/m/{{$upcomingevent_api_away['image_id']}}.png" onload="resize(this)" style="height: 100%; width: 100%;"></span>
                    </div>
                    <div class="team_name">
                        <span id="teamNameB">{{$upcomingevent_api_away['name']}}</span>
                    </div>
                </li>
            </ul>
            <div class="game_replay">
                <div class="play_button" style="display:none;"></div>
                <div class="game_tip" style="display:none;"></div>
            </div>
        </div>
        <div class="score_show" style="">
            <?php
                if(($upcomingevent_api_json != null) && $upcomingevent_api_json['time_status'] == 3){
                    if(isset($upcomingevent_api_json['scores'][4])){
                        $html = "90 minutes[".$upcomingevent_api_json['scores'][2]['home']."-".$upcomingevent_api_json['scores'][2]['away']."], 120 minutes[".$upcomingevent_api_json['scores'][3]['home']."-".$upcomingevent_api_json['scores'][3]['away']."], Penalty Kicks[".$upcomingevent_api_json['scores'][4]['home']."-".$upcomingevent_api_json['scores'][4]['away']."]";
                    }else if(isset($upcomingevent_api_json['scores'][3])){
                        $html = "90 minutes[".$upcomingevent_api_json['scores'][2]['home']."-".$upcomingevent_api_json['scores'][2]['away']."], 120 minutes[".$upcomingevent_api_json['scores'][3]['home']."-".$upcomingevent_api_json['scores'][3]['away']."]";
                    }else{
                        $html = "90 minutes[".$upcomingevent_api_json['scores'][2]['home']."-".$upcomingevent_api_json['scores'][2]['away']."]";
                    }
                    echo $html;
                }
            ?>
        </div>
    </div>
    <div class="bottom_part_nav" id="nav">
        <div class="bottom_nav_content nav_swiper-container swiper-container swiper-container-horizontal">
            <ul class="bottom_nav_list swiper-wrapper" style="transform: translate3d(0px, 0px, 0px); transition: all 0.5s ease 0s;">
                <li class="swiper-slide swiper-slide-active on" data-index="0" pid = 'detail'>
                    <a href="#">
                        <?php 
                        if($data['lang'] == 1){
                            echo "Details";
                        }else{
                            echo "细节";
                        }
                        ?>
                    </a>
                </li>
                <li class="swiper-slide swiper-slide-next " data-index="1" pid = 'statistics'>
                    <a href="#">
                        <?php 
                        if($data['lang'] == 1){
                            echo "Statistics";
                        }else{
                            echo "统计数据";
                        }
                        ?>
                    </a>
                </li>
                {{-- <li class="swiper-slide" data-index="2"><a href="#">Analysis</a></li> --}}
                <!-- <li class="swiper-slide" data-index = '9'><a href="#"></a></li> -->
                
                {{-- <li class="swiper-slide" data-index="4"><a href="#">Odds</a></li> --}}
                
                
                <li class="swiper-slide" data-index="3" pid = 'league_table'>
                    <a href="#">
                        <?php 
                        if($data['lang'] == 1){
                            echo "Standings";
                        }else{
                            echo "積分榜";
                        }
                        ?>
                    </a>
                </li>
                
            </ul>
            <div class="swiper_prev" style="display:none;"></div>
            <div class="swiper_next" style="display: block;"></div>
        </div>
    </div>
    <div class="bottom_part" style="height: 496px;">
        <div class="tabBox" style="display: block;" id = 'detail'>
            <div class="live">
                <div class="goal_situation" style="">
                    <ul class="goal_situation_list">

                        <?php
                        
                        if(isset($upcomingevent_api_json['events'])){
                            foreach($upcomingevent_api_json['events'] as $e){
                                
                            ?>
                                <li>
                                    <div class="live_right">
                                        <div class="live_right_content_text">
                                            <p>
                                                {{-- <span class="live_right_content_text_time">80'</span> --}}
                                                <span class="live_right_content_text_team_name">
                                                    <a href="#" >{{$e['text']}}</a>
                                                </span>
                                            </p>
                                        </div>
                                    </div>
                                </li>
                            <?php
                            }
                        }
                        ?>


                    </ul>
                </div>
                
                <div class="field_condition" style="">
                    <div class="field_condition_title"><?php if($data['lang'] == "2"){ echo "球场状况";}else{ echo "Stadium Condition";}?></div>
                    <div class="field_condition_contain">
        
                        <table border="0" cellpadding="0" cellspacing="0">
                            <tbody>
                                <tr>
                                    <td valign="top" align="right"><?php if($data['lang'] == "2"){ echo "球場名稱";}else{ echo "Stadium";}?>:&nbsp;</td>
                                    <td valign="top"><?php if(isset($upcomingevent_api_json['extra']['stadium_data'])){ echo $upcomingevent_api_json['extra']['stadium_data']['name'];}else{ echo " - ";}?></td>
                                </tr>
                                <tr>
                                    <td valign="top" align="right"><?php if($data['lang'] == "2"){ echo "城市";}else{ echo "City";}?>:&nbsp;</td>
                                    <td valign="top"><?php if(isset($upcomingevent_api_json['extra']['stadium_data'])){ echo $upcomingevent_api_json['extra']['stadium_data']['city'];}else{ echo " - ";}?></td>
                                </tr>
                                <tr><td valign="top" align="right"><?php if($data['lang'] == "2"){ echo "国家";}else{ echo "Country";}?>:&nbsp;</td>
                                    <td valign="top"><a href="javascript:Stadium_en(103)"><?php if(isset($upcomingevent_api_json['extra']['stadium_data'])){ echo $upcomingevent_api_json['extra']['stadium_data']['country'];}else{ echo " - ";}?></a></td>
                                </tr>
                                <tr>
                                    <td valign="top" align="right"><?php if($data['lang'] == "2"){ echo "容纳";}else{ echo "Capacity";}?>:&nbsp;</td>
                                    <td valign="top"><?php if(isset($upcomingevent_api_json['extra']['stadium_data'])){ echo $upcomingevent_api_json['extra']['stadium_data']['capacity'];}else{ echo " - ";}?></td>
                                </tr>
                                <tr><td valign="top" align="right"><?php if($data['lang'] == "2"){ echo "坐标";}else{ echo "Coordinate";}?>:&nbsp;</td>
                                    <td valign="top"><?php if(isset($upcomingevent_api_json['extra']['stadium_data'])){ echo $upcomingevent_api_json['extra']['stadium_data']['googlecoords'];}else{ echo " - ";}?></td>
                                </tr>
                                <tr>
                                    <td valign="top" align="right"><?php if($data['lang'] == "2"){ echo "裁判";}else{ echo "Referee";}?>:&nbsp;</td>
                                    <td valign="top"><a href="#"><?php if(isset($upcomingevent_api_json['extra']['stadium_data'])){  if(isset($upcomingevent_api_json['extra']['referee'])){ echo $upcomingevent_api_json['extra']['referee']['name'];}}else{ echo " - ";}?></a></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="field_condition" style="">
                    <div class="field_condition_title"><?php if($data['lang'] == "2"){ echo "经理";}else{ echo "Manager";}?></div>
                    <div class="field_condition_contain">
        
                        <table border="0" cellpadding="0" cellspacing="0">
                            <tbody>
                                <tr>
                                    <td valign="top" align="right"><?php if($data['lang'] == "2"){ echo "主队经理";}else{ echo "Home Manager";}?>:&nbsp;</td>
                                    <td valign="top"><?php if(isset($upcomingevent_api_json['extra']['home_manager'])){ echo $upcomingevent_api_json['extra']['home_manager']['name'];}else{ echo " - ";}?></td>
                                </tr>
                                <tr>
                                    <td valign="top" align="right"><?php if($data['lang'] == "2"){ echo "客队经理";}else{ echo "Away Manager";}?>:&nbsp;</td>
                                    <td valign="top"><?php if(isset($upcomingevent_api_json['extra']['away_manager'])){ echo $upcomingevent_api_json['extra']['away_manager']['name'];}else{ echo " - ";}?></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                
            </div>
        </div>

        <div class="tabBox" style="display: none;" id = 'statistics'>
			<div class="load" style="display: none;">
				<div class="load_ball">
					<div class="ball"></div>
					<div class="shadow"></div>
				</div>
				<span class="loadName">Loading…</span>
			</div>
            <div class="cout_warp">
                <div class="event_statistics" style="">
                    <div class="event_statistics_contain">
                        <div class="event_statistics_contain_title">
                            <ul>
                                <li class="event_statistics_contain_title_left"><div id="teamA">{{$upcomingevent_api_home['name']}}</div></li>
                                <li class="event_statistics_contain_title_center"><?php if($data['lang'] == "2"){ echo "比赛统计";}else{ echo "Match Stat";}?></li>
                                <li class="event_statistics_contain_title_right" style=""><div id="teamB">{{$upcomingevent_api_away['name']}}</div></li>
                            </ul>
                        </div>
                        <div class="event_statistics_contain_content"></div>
                    </div>
                </div>
                <div class="goal_statistics" style="">
                    <div class="goal_statistics_contain">
                        <div class="goal_statistics_title">Goal Stat</div>
                        <div class="goal_statistics_content">
                            <div class="total_goal_number">
                                <div class="goal_part_title" id="total_goal">Goal Tally</div>
                                <?php
                                if(isset($upcomingevent_api_json['stats'])){
                                    foreach($upcomingevent_api_json['stats'] as $st => $tt){
                                        
                                    ?>
                                    <div class="time_count">
                                        <ul>
                                            <p>{{$st}}</p>
                                            <li>
                                                <i class="right">
                                                <i class="left">
                                                    <div class="left_goal_number">{{$tt[0]}}</div>
                                                </i>
                                                    <div class="right_goal_number">{{$tt[1]}}</div>
                                                </i>
                                            </li>
                                        </ul>
                                    </div>
                                    <?php
                                    }
                                }else{
                                    echo 'coming soon';
                                }
                                ?>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
		</div>

        <div class="tabBox r_scoreBoard" style="display: none;" id = 'league_table'>
            <ul class="rs_tab">
                <?php
                foreach($data['Leaguetable'] as $l){
                  $dd = json_decode($l['leaguetable_overall'],true);
                  $dd = collect($dd['tables']);
                  $dd = $dd->sortBy('groupname');
                  
                  foreach($dd as $i => $d){
                  
                    if(($d['groupname'] == 'A') || ($d['groupname'] == 'B') || ($d['groupname'] == 'C') || ($d['groupname'] == 'D')){
                        if($d['groupname'] == 'A'){
                          echo "<li class='league_title on' pid = '{$d['groupname']}'>".$d['name']."</li>";
                        }else{
                          echo "<li class='league_title ' pid = '{$d['groupname']}'>".$d['name']."</li>";
                        }
                    }else if($d['groupname'] == null){
                        echo "<li class='league_title on' pid = '{$d['groupname']}'>".$d['name']."</li>";
                    }
                  }
                }
                ?>                  
              </ul>                    
              <ul class="rs_tabC" id="js_rScoreRank01">
                <?php
                foreach($data['Leaguetable'] as $l){      
                  $dd = json_decode($l['leaguetable_overall'],true);
                  $dd = collect($dd['tables']);
                  $dd = $dd->sortBy('groupname');
                  foreach($dd as $i => $d){    
                    if(($d['groupname'] == 'A') || ($d['groupname'] == 'B') || ($d['groupname'] == 'C') || ($d['groupname'] == 'D') || ($d['groupname'] == null)){
                    ?>
                    <li class="league_detail <?php if(($d['groupname'] == 'A') || ($d['groupname'] == null)){ echo " on";}else{ echo " hide";}?>" id = 'league_detail_<?php echo $d['groupname'];?>' >    
                      <table>
                        <tbody>
                          <tr class="first">
                            <td width="60">
                              <?php
                              if($data['lang'] == 1){
                                  echo "Ranking";
                              }else{
                                  echo "排名";
                              }
                              ?>
                              </td>
                            <td>
                              <?php
                              if($data['lang'] == 1){
                                  echo "Team";
                              }else{
                                  echo "球隊";
                              }
                              ?>
                              </td>
                            <td width="50">
                              <?php
                              if($data['lang'] == 1){
                                  echo "Points";
                              }else{
                                  echo "分数";
                              }
                              ?>
                            </td>
                            <td width="90">
                              <?php
                              if($data['lang'] == 1){
                                  echo "W/D/L";
                              }else{
                                  echo "勝/平/負";
                              }
                              ?>
                              
                            </td>
                          </tr>
                    <?php
                      $d['rows'] = collect($d['rows']);
                      $d['rows'] = $d['rows']->sortBy('sort_pos');

                      
                      foreach($d['rows'] as $i => $r){
                      ?>
                          <tr class="<?php if((($i == 0) || ($i == 1)) && ($d['groupname'] != null) ){ echo "outgoingLine ";}?>">
                            <td class=" pr"><?php echo $r['sort_pos']?>
                            <?php
                            if($r['change'] >=1){
                            ?>
                            <i class="fa fa-arrow-up" style = 'color: #25765C !important;' aria-hidden="true"></i>
                            <?php
                            }else if($r['change'] < 0){
                            ?>
                            <i class="fa fa-arrow-down" style = 'color: #f04124 !important;' aria-hidden="true"></i>
                            <?php
                            }else{
                            ?>
                            <i class="fa fa-arrows-h" aria-hidden="true"></i>
                            <?php    
                            }
                            ?>
                            </td>
                            <td class="left">
                              <a target="_blank" href="#">
                              <img src="https://assets.b365api.com/images/team/m/<?php echo $r['team']['image_id'];?>.png"><span><?php echo $r['team']['name']?></span>
                              </a>
                            </td>
                            <td><?php echo $r['points']?></td>
                            <td><?php echo $r['win'] . "/" . $r['draw'] . "/" . $r['loss'];?></td>
                          </tr>
                      <?php
                      }
                      ?>
                            </tbody>
                          </table>
                        </li>
                      <?php
                    }
                  }
                ?>
                    
                <?php
                }
                ?>
              </ul>
        
              <ul class="rs_tab" style = 'margin-top:20px;'>
                <?php
                foreach($data['Leaguetable'] as $l){
                  $dd = json_decode($l['leaguetable_overall'],true);
                  $dd = collect($dd['tables']);
                  $dd = $dd->sortBy('groupname');
                  
                  foreach($dd as $i => $d){
                  
                    if(($d['groupname'] == 'E') || ($d['groupname'] == 'F') || ($d['groupname'] == 'G') || ($d['groupname'] == 'H')){
                        if($d['groupname'] == 'E'){
                          echo "<li class='league_title2 on' pid = '{$d['groupname']}'>".$d['name']."</li>";
                        }else{
                          echo "<li class='league_title2 ' pid = '{$d['groupname']}'>".$d['name']."</li>";
                        }
                    }
                  }
                }
                ?>                  
              </ul>                    
              <ul class="rs_tabC" id="js_rScoreRank01">
                <?php
                foreach($data['Leaguetable'] as $l){      
                  $dd = json_decode($l['leaguetable_overall'],true);
                  $dd = collect($dd['tables']);
                  $dd = $dd->sortBy('groupname');
                  foreach($dd as $i => $d){    
                    if(($d['groupname'] == 'E') || ($d['groupname'] == 'F') || ($d['groupname'] == 'G') || ($d['groupname'] == 'H')){
                    ?>
                    <li class="league_detail2 <?php if($d['groupname'] == 'E'){ echo " on";}else{ echo " hide";}?>" id = 'league_detail_<?php echo $d['groupname'];?>' >    
                      <table>
                        <tbody>
                          <tr class="first">
                            <td width="60">
                              <?php
                              if($data['lang'] == 1){
                                  echo "Ranking";
                              }else{
                                  echo "排名";
                              }
                              ?>
                            </td>
                            <td>
                              <?php
                              if($data['lang'] == 1){
                                  echo "Team";
                              }else{
                                  echo "球隊";
                              }
                              ?>
                            </td>
                            <td width="50">
                              <?php
                              if($data['lang'] == 1){
                                  echo "Points";
                              }else{
                                  echo "分数";
                              }
                              ?>
                            </td>
                            <td width="90">
                              <?php
                              if($data['lang'] == 1){
                                  echo "W/D/L";
                              }else{
                                  echo "勝/平/負";
                              }
                              ?>
                            </td>
                          </tr>
                    <?php
                      $d['rows'] = collect($d['rows']);
                      $d['rows'] = $d['rows']->sortBy('sort_pos');

                    
                      foreach($d['rows'] as $i => $r){
                        
                      ?>
                          <tr class="<?php if(($i == 0) || ($i == 1)){ echo "outgoingLine ";}?>">
                            <td class=" pr"><?php echo $r['sort_pos']?></td>
                            <td class="left">
                              <a target="_blank" href="#">
                              <img src="https://assets.b365api.com/images/team/m/<?php echo $r['team']['image_id'];?>.png"><span><?php echo $r['team']['name']?></span>
                              </a>
                            </td>
                            <td><?php echo $r['points']?></td>
                            <td><?php echo $r['win'] . "/" . $r['draw'] . "/" . $r['loss'];?></td>
                          </tr>
                      <?php
                      }
                      ?>
                            </tbody>
                          </table>
                        </li>
                      <?php
                    }
                  }
                ?>
                    
                <?php
                }
                ?>
              </ul>
       </div>

    </div>
  <input type = "hidden" id = 'isstart' value = '0'/>

  <!-- Argon Scripts -->
  <!-- Core -->
  <script src="../assets/vendor/jquery/dist/jquery.min.js"></script>
  <script src="../assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
  <script src="../assets/vendor/js-cookie/js.cookie.js"></script>
  <script src="../assets/vendor/jquery.scrollbar/jquery.scrollbar.min.js"></script>
  <script src="../assets/vendor/jquery-scroll-lock/dist/jquery-scrollLock.min.js"></script>
  <script src="../assets/vendor/datatables.net/js/jquery.dataTables.min.js"></script>
  <script src="../assets/vendor/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
  <script src="../assets/vendor/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
  <script src="../assets/vendor/select2/dist/js/select2.min.js"></script>
  <script src="../assets/vendor/sweetalert2/dist/sweetalert2.min.js"></script>
  <script src="../js/tmsjs/jqueryvalidation/jquery.validate.1.8.js"></script>
  <script src="../js/tmsjs/loadingoverlay/loadingoverlay.min.js"></script>
  <script src="../assets/vendor/moment.min.js"></script>
  <!-- Argon JS -->
  <script src="../assets/js/argon.js?v=1.2.0"></script>

  <script>
    var myInterval = null;
    $(document).ready( function () {
        
      $('#goBack').click(function(){
        location.href = '/home';
     });
      $('.bmt_sound').click(function(){
          if($(this).hasClass('bmt_sound_on')){
            $(this).removeClass('bmt_sound_on');
            $(this).attr('title','Sound On')
          }else{
            $(this).addClass('bmt_sound_on');
            $(this).attr('title','Sound Off') 
          }
      });
      $('.bmt_refresh').click(function(){
          getHomeResult();
      });

      $('.swiper-slide').click(function(){
          $('.swiper-slide').removeClass('on');
          $(this).addClass('on');
          

          $('.tabBox').css('display','none');
          $('#'+$(this).attr('pid')).css('display','block');
          

      });

    //   myInterval = setInterval(getIsStart, 3000);
      
        $('.league_title').click(function(){
          $('.league_detail').removeClass('on');
          $('.league_title').removeClass('on');
          $('.league_detail').addClass('hide');
          $('#league_detail_'+$(this).attr('pid')).addClass('on');
          $('#league_detail_'+$(this).attr('pid')).removeClass('hide');
          $(this).addClass('on');

        });

        $('.league_title2').click(function(){
            $('.league_detail2').removeClass('on');
            $('.league_title2').removeClass('on');
            $('.league_detail2').addClass('hide');
            $('#league_detail_'+$(this).attr('pid')).addClass('on');
            $('#league_detail_'+$(this).attr('pid')).removeClass('hide');
            $(this).addClass('on');

        });


    });
    var coundownProcess = null;
    function startCount(){
        display = $('#mn');
               // start time and end time
               var startTime = moment($('#mn').attr('start'), 'HH:mm:ss');
            //    var startTime = moment('00:00:00', 'HH:mm:ss');
                var endTime = moment(moment().format("HH:mm:ss"), 'HH:mm:ss');
                // var endTime = moment('01:25:00','HH:mm:ss');
                // var endTime = moment(moment().format("HH:mm:ss a"), 'HH:mm:ss a');
                console.log(startTime);
                console.log(endTime);
                // calculate total duration
                var duration = moment.duration(endTime.diff(startTime));
                var minutes = moment.utc(moment(endTime, "HH:mm:ss").diff(moment(startTime, "HH:mm:ss"))).format("mm")
                var HH = moment.utc(moment(endTime, "HH:mm:ss").diff(moment(startTime, "HH:mm:ss"))).format("h")
                console.log(HH);
                console.log(moment(endTime, 'HH').format("H"));
                console.log(moment(startTime, 'HH').format("H"));
                // && (moment(endTime, 'HH').format("H") == moment(startTime, 'HH').format("H"))
                if((HH == 1) ){
                    var pp = moment(endTime, 'HH').format("H");
                    var ll = moment(startTime, 'HH').format("H");
                    if((parseInt(pp) - parseInt(ll)) > 1){
                        display.text('END');
                    }else{
                        var duration = (minutes * 60) + (HH * 60 * 60);
                    }
                    
                }else{
                    var pp = moment(endTime, 'HH').format("H");
                    var ll = moment(startTime, 'HH').format("H");
                    if((parseInt(pp) - parseInt(ll)) > 1){
                        display.text('END');
                    }else{
                        var duration = (minutes * 60);
                    }
                    
                }
                 
                 console.log(duration);
                // var minutes = 300;
                // var seconds = 19;
                var timer = duration,minutes, seconds;
        if(coundownProcess == null){
            
            let coundownProcess = setInterval(function() {
                    minutes = parseInt(timer / 60, 10)
                    seconds = parseInt(timer % 60, 10);

                    minutes = minutes < 10 ? "0" + minutes : minutes;
                    seconds = seconds < 10 ? "0" + seconds : seconds;
                    // console.log(minutes);
                    display.text(minutes + ":" + seconds);
                    ++timer;
                    if (minutes >= 100 ) {
                        clearInterval(coundownProcess); 
                        display.text('END');
                        
                    }
                    }, 1000);
        }
    }
    function getIsStart(){
        $.ajax({
                url:'api/isstart?upcomingevent_id=',
                type:'GET',
                cache:false,
                success:function(jsonObj){

                    if(jsonObj.status == 1){
                        startCount();
                        clearInterval(myInterval); 
                        
                    }
                }
        });
    }

  </script>
</body>

</html>