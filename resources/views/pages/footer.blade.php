<footer class="footer pt-0">
        <div class="row align-items-center justify-content-lg-between">
          <div class="col-lg-6">
            <div class="copyright text-center text-xl-left text-muted">
                &copy; {{ now()->year }}
                <a href="#" class="font-weight-bold ml-1" >i1bnscore.com</a>
            </div>
          </div>
          <div class="col-xl-6">
              <ul class="nav nav-footer justify-content-center justify-content-xl-end">
                  <li class="nav-item">
                      <a href="#" class="nav-link" target="_blank">Version 1.0.0</a>
                  </li>
              </ul>
          </div>
        </div>
</footer>