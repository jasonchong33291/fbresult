
<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Wallet Management">
  <meta name="author" content="">
  <title>Wallet Management</title>
  <!-- Favicon -->
  <link rel="icon" href="../assets/img/brand/favicon.png" type="image/png">
  <!-- Fonts -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700">
  <!-- Icons -->
  <link rel="stylesheet" href="../assets/vendor/nucleo/css/nucleo.css" type="text/css">
  <link rel="stylesheet" href="../assets/vendor/@fortawesome/fontawesome-free/css/all.min.css" type="text/css">
  <link rel="stylesheet" href="../assets/vendor/datatables.net-bs4/css/dataTables.bootstrap4.min.css" type="text/css">
  <link rel="stylesheet" href="../assets/vendor/select2/dist/css/select2.min.css" type="text/css">
  <link rel="stylesheet" href="../assets/vendor/sweetalert2/dist/sweetalert2.min.css" type="text/css">
  <!-- Argon CSS -->
  <link rel="stylesheet" href="../assets/css/argon.css?v=1.2.0" type="text/css">
  <link rel="stylesheet" href="../css/tms.css" type="text/css">
</head>

<body>
  <!-- Sidenav -->
  @include('layouts.navbars.sidebar') 
  <!-- Main content -->
  <div class="main-content" id="panel">
    <!-- Topnav -->
    @include('layouts.navbars.topheader') 
    <!-- Header -->
    <!-- Header -->
    <div class="header bg-primary pb-6">
      <div class="container-fluid">
        <div class="header-body">
          <div class="row align-items-center py-4">
            <div class="col-lg-6 col-7">
              <!-- <h6 class="h2 text-white d-inline-block mb-0">TMS</h6> -->
              <nav aria-label="breadcrumb" class="d-none d-md-inline-block ">
                <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                  <li class="breadcrumb-item"><a href="dashboard"><i class="fas fa-home"></i></a></li>
                  <li class="breadcrumb-item"><a href="/wallet/listing">Wallet Listing</a></li>
                </ol>
              </nav>
            </div>

            <div class="col-lg-6 col-5 text-right">
             
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--6">
      <div class="row">
        <div class="col">



        <div class="card">

            <div class="card-body">

                  <form id = 'walletform_id' >
                      <h6 class="heading-small text-muted mb-4">Wallet Information</h6>
                      <div class="pl-lg-4">
                        <div class="row">
                          <div class="col-lg-6">
                            <div class="form-group">
                              <label class="form-control-label" for="wallet_code">Wallet Code {!!getMandatory()!!}</label> 
                              <input type="text" id="wallet_code" name = "wallet_code" class="form-control" placeholder="Wallet Code" value="{{$data['wallet']->wallet_code}}">
                            </div>
                          </div>
                          <div class="col-lg-6">
                            <div class="form-group">
                              <label class="form-control-label" for="wallet_name">Bank Name {!!getMandatory()!!}</label> 
                              <input type="text" id="wallet_name" name = "wallet_name" class="form-control" placeholder="Wallet Name" value="{{$data['wallet']->wallet_name}}">
                            </div>
                          </div>
                        </div>
                        {{-- <div class="row">
                          <div class="col-lg-6">
                            <div class="form-group">
                              <label class="form-control-label" for="wallet_seq">Seq No. </label> 
                              <input type="text" id="wallet_seq" name = "wallet_seq" class="form-control" placeholder="Seq No." value="{{$data['wallet']->wallet_seq}}">
                            </div>
                          </div>
                        </div> --}}
                      <div class="row">
                        <div class="col-lg-6">
                          <div class="form-group">
                              <label class="form-control-label" for="wallet_status">Status</label>
                              <select name="wallet_status" class="form-control select2" id="wallet_status" >
                                  <option value="1"
                                      {{ $data['wallet']->wallet_status == 1 ? 'selected' : '' }}>
                                      Active
                                  </option>
                                  <option value="2"
                                      {{ $data['wallet']->wallet_status == 2 ? 'selected' : '' }}>
                                      In-active
                                  </option>
                              </select>
                          </div>
                      </div>
                      </div>
                        
                        @csrf
                        <input type = "hidden" name = 'wallet_id' value = "{{Crypt::encryptString($data['wallet']->wallet_id)}}"/>
                        <button class="btn btn-primary" type="submit" id = 'submit_btn'  >Submit</button>
                      </div>

                  </form>


              
            </div>
          </div>
        </div>

      </div>

      <!-- Footer -->
      @include('pages.footer') 
    </div>
  </div>


  <!-- Argon Scripts -->
  <!-- Core -->
  <script src="../assets/vendor/jquery/dist/jquery.min.js"></script>
  <script src="../assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
  <script src="../assets/vendor/js-cookie/js.cookie.js"></script>
  <script src="../assets/vendor/jquery.scrollbar/jquery.scrollbar.min.js"></script>
  <script src="../assets/vendor/jquery-scroll-lock/dist/jquery-scrollLock.min.js"></script>
  <script src="../assets/vendor/datatables.net/js/jquery.dataTables.min.js"></script>
  <script src="../assets/vendor/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
  <script src="../assets/vendor/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
  <script src="../assets/vendor/select2/dist/js/select2.min.js"></script>
  <script src="../assets/vendor/sweetalert2/dist/sweetalert2.min.js"></script>
  <script src="../js/tmsjs/jqueryvalidation/jquery.validate.1.8.js"></script>
  <script src="../js/tmsjs/loadingoverlay/loadingoverlay.min.js"></script>
  <!-- Argon JS -->
  <script src="../assets/js/argon.js?v=1.2.0"></script>

  <script>
    $(document).ready( function () {
      $('.select2').select2();
      $("#walletform_id").validate({
              rules: 
              {
                  wallet_code:
                  {
                      required: true
                  },
                  wallet_name:
                  {
                      required: true
                  },
              },
            showErrors: function(errorMap, errorList) {
                this.defaultShowErrors();

             },
             submitHandler: function (form) {
              //  console.log(form)
              //  alert('123');return false;
             
                  $.ajax({
                      url:'/wallet/create',
                      type:'POST',
                      data:$('#walletform_id').serialize(),
                      cache:false,
                      beforeSend: function() {
                          $('#submit_btn').text("loading...");
                          $('#submit_btn').attr("disabled",true);
                          $.LoadingOverlay("show");
                      },
                      error: function(xhr) {
                          Swal({
                              type: 'error',
                              title: "Something went wrong.",
                              showConfirmButton: false,
                              allowOutsideClick:false,
                              timer: 1500
                          })
                          $('#submit_btn').attr("disabled",false);
                          $('#submit_btn').text("Submit");
                          $.LoadingOverlay("hide");
                      },
                      success:function(jsonObj){
                          $('#submit_btn').attr("disabled",false);
                          $('#submit_btn').text("Submit");
                          $.LoadingOverlay("hide");
                          if(jsonObj.status == 1){
                              if(jsonObj.msg != ""){
                                  Swal({
                                      type: 'success',
                                      title: jsonObj.msg,
                                      showConfirmButton: false,
                                      allowOutsideClick:false,
                                      timer: 1500
                                  }).then(function() {
                                      window.location.href = '/wallet/listing'
                                  });
                              }

                          }else{
                              Swal({
                                  type: 'error',
                                  title: jsonObj.msg,
                                  showConfirmButton: false,
                                  allowOutsideClick:false,
                                  timer: 1500
                              })
                          }
                      }
                  });
              },
      });
      
      


    });

  </script>
</body>

</html>