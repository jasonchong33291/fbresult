<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Wallet Management">
    <meta name="author" content="">
    <title>Wallet Listing Management</title>
    <!-- Favicon -->
    <link rel="icon" href="../assets/img/brand/favicon.png" type="image/png">
    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700">
    <!-- Icons -->
    <link rel="stylesheet" href="../assets/vendor/nucleo/css/nucleo.css" type="text/css">
    <link rel="stylesheet" href="../assets/vendor/@fortawesome/fontawesome-free/css/all.min.css" type="text/css">
    <link rel="stylesheet" href="../assets/vendor/datatables.net-bs4/css/dataTables.bootstrap4.min.css" type="text/css">
    <link rel="stylesheet" href="../assets/vendor/select2/dist/css/select2.min.css" type="text/css">
    <link rel="stylesheet" href="../assets/vendor/sweetalert2/dist/sweetalert2.min.css" type="text/css">
    <!-- Argon CSS -->
    <link rel="stylesheet" href="../assets/css/argon.css?v=1.2.0" type="text/css">
    <link rel="stylesheet" href="../css/tms.css" type="text/css">
</head>

<body>
    <!-- Sidenav -->
    @include('layouts.navbars.sidebar')
    <!-- Main content -->
    <div class="main-content" id="panel">
        <!-- Topnav -->
        @include('layouts.navbars.topheader')
        <!-- Header -->
        <!-- Header -->
        <div class="header bg-primary pb-6">
            <div class="container-fluid">
                <div class="header-body">
                    <div class="row align-items-center py-4">
                        <div class="col-lg-6 col-7">
                            <!-- <h6 class="h2 text-white d-inline-block mb-0">TMS</h6> -->
                            <nav aria-label="breadcrumb" class="d-none d-md-inline-block ">
                                <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                                    <li class="breadcrumb-item"><a href="dashboard"><i class="fas fa-home"></i></a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="/wallet/listing">Wallet Listing</a></li>
                                </ol>
                            </nav>
                        </div>

                        <div class="col-lg-6 col-5 text-right">
                            <?php
                            if(canAccess('create_wallet')){
                            ?>
                            <a href="/wallet/createForm" class="btn btn-sm btn-neutral">Create New</a>
                            <?php }?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Page content -->
        <div class="container-fluid mt--6">
            <div class="row">
                <div class="col">
                    <div class="card">
                        <!-- Card header -->
                        <div class="card-header border-0">
                            <h3 class="mb-0">Wallet Listing</h3>

                            <?php
                            if ($data['failled_msg']) {
                                echo '<span class="badge badge-danger">' . $data['failled_msg'] . '</span>';
                            }
                            if ($data['success_msg']) {
                                echo '<span class="badge badge-success">' . $data['success_msg'] . '</span>';
                            }
                            ?>
                        </div>
                        <!-- Light table -->
                        <div class="table-responsive" style='padding-bottom:20px;'>
                            <table class="table align-items-center table-flush" id='form_table'>
                                <thead class="thead-light">
                                    <tr>
                                        <th scope="col" class="sort">Code</th>
                                        <th scope="col" class="sort">Name</th>
                                        <th scope="col" class="sort">Wallet Status</th>
                                        <th scope="col" class="sort"></th>
                                    </tr>
                                </thead>
                                <tbody class="list">
                                    @foreach ($data['wallet'] as $c)
                                        <tr>
                                            <td>
                                                
                                                {{ $c->wallet_code }}
                                              
                                            </td>
                                            <td>
                                                
                                                {{ $c->wallet_name }}
                                               
                                            </td>
                                            <td>
                                                <select class="form-control select2 wallet_status_class" pid = '{{ Crypt::encryptString($c->wallet_id) }}'>
                                                    <option value = '1' <?php if($c->wallet_status == 1){ echo " SELECTED";}?>>Active</option>
                                                    <option value = '2' <?php if($c->wallet_status == 2){ echo " SELECTED";}?>>In-active</option>
                                                </select>
                                            </td>
                                            <td
                                                class="text-right ">
                                                <div class="dropdown">
                                                    <a class="btn btn-sm btn-icon-only text-light" href="#"
                                                        role="button" data-toggle="dropdown" aria-haspopup="true"
                                                        aria-expanded="false">
                                                        <i class="fas fa-ellipsis-v"></i>
                                                    </a>
                                                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow"
                                                        style="">
                                                    <?php
                                                     if(canAccess('edit_wallet')){   
                                                    ?>
                                                        <a class="dropdown-item {{ canAccess('edit_wallet') }}"
                                                            href="/wallet/updateForm?wallet_id={{ Crypt::encryptString($c->wallet_id) }}">Edit</a>
                                                    <?php }?>       

                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach

                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>

            <!-- Footer -->
            @include('pages.footer')
        </div>
    </div>
    <!-- Argon Scripts -->
    <!-- Core -->
    <script src="../assets/vendor/jquery/dist/jquery.min.js"></script>
    <script src="../assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <script src="../assets/vendor/js-cookie/js.cookie.js"></script>
    <script src="../assets/vendor/jquery.scrollbar/jquery.scrollbar.min.js"></script>
    <script src="../assets/vendor/jquery-scroll-lock/dist/jquery-scrollLock.min.js"></script>
    <script src="../assets/vendor/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../assets/vendor/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
    <script src="../assets/vendor/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
    <script src="../assets/vendor/select2/dist/js/select2.min.js"></script>
    <script src="../assets/vendor/sweetalert2/dist/sweetalert2.min.js"></script>
    <script src="../js/tmsjs/loadingoverlay/loadingoverlay.min.js"></script>
    <!-- Argon JS -->
    <script src="../assets/js/argon.js?v=1.2.0"></script>

    <script>
        $(document).ready(function() {
            $('#form_table').DataTable({
                "iDisplayLength": 100,
            });

            $('#courserun_modeoftraining').select2();
            $('#courserun_trainer,.wallet_status_class').select2();

            $(document).on("change", '.wallet_status_class', function() {
                    var obj = $(this);
                    
                    $.ajax({
                        url:'/wallet/updatewalletstatus',
                        type:'POST',
                        data:'_token={{csrf_token()}}&wallet_id='+obj.attr('pid')+"&wallet_status="+obj.val(),
                        cache:false,
                        beforeSend: function() {
                            $.LoadingOverlay("show");
                        },
                        error: function(xhr) {
                            Swal({
                                type: 'error',
                                title: "Something went wrong.",
                                showConfirmButton: false,
                                allowOutsideClick:false,
                                timer: 1500
                            })
                            $.LoadingOverlay("hide");
                        },
                        success:function(jsonObj){
                            $.LoadingOverlay("hide");
                            if(jsonObj.status == 1){
                                if(jsonObj.msg != ""){
                                    Swal({
                                        type: 'success',
                                        title: jsonObj.msg,
                                        showConfirmButton: false,
                                        allowOutsideClick:false,
                                        timer: 1500
                                    }).then(function() {
                                      
                                    });
                                }

                            }else{
                                Swal({
                                    type: 'error',
                                    title: jsonObj.msg,
                                    showConfirmButton: false,
                                    allowOutsideClick:false,
                                    timer: 1500
                                })
                            }
                            
                        }
                    });

                });

            
        });
    </script>
</body>

</html>
