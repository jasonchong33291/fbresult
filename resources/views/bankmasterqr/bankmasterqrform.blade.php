
<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Bank Master QR Management">
  <meta name="author" content="">
  <title>Bank Master QR Management</title>
  <!-- Favicon -->
  <link rel="icon" href="../assets/img/brand/favicon.png" type="image/png">
  <!-- Fonts -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700">
  <!-- Icons -->
  <link rel="stylesheet" href="../assets/vendor/nucleo/css/nucleo.css" type="text/css">
  <link rel="stylesheet" href="../assets/vendor/@fortawesome/fontawesome-free/css/all.min.css" type="text/css">
  <link rel="stylesheet" href="../assets/vendor/datatables.net-bs4/css/dataTables.bootstrap4.min.css" type="text/css">
  <link rel="stylesheet" href="../assets/vendor/select2/dist/css/select2.min.css" type="text/css">
  <link rel="stylesheet" href="../assets/vendor/sweetalert2/dist/sweetalert2.min.css" type="text/css">
  <!-- Argon CSS -->
  <link rel="stylesheet" href="../assets/css/argon.css?v=1.2.0" type="text/css">
  <link rel="stylesheet" href="../css/tms.css" type="text/css">
</head>

<body>
  <!-- Sidenav -->
  @include('layouts.navbars.sidebar') 
  <!-- Main content -->
  <div class="main-content" id="panel">
    <!-- Topnav -->
    @include('layouts.navbars.topheader') 
    <!-- Header -->
    <!-- Header -->
    <div class="header bg-primary pb-6">
      <div class="container-fluid">
        <div class="header-body">
          <div class="row align-items-center py-4">
            <div class="col-lg-6 col-7">
              <!-- <h6 class="h2 text-white d-inline-block mb-0">TMS</h6> -->
              <nav aria-label="breadcrumb" class="d-none d-md-inline-block ">
                <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                  <li class="breadcrumb-item"><a href="dashboard"><i class="fas fa-home"></i></a></li>
                  <li class="breadcrumb-item"><a href="/bankmasterqr/listing">Bank Master QR Listing</a></li>
                </ol>
              </nav>
            </div>

            <div class="col-lg-6 col-5 text-right">
             
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--6">
      <div class="row">
        <div class="col">



        <div class="card">

            <div class="card-body">
              
                  <form id = 'bankform_id' >
                      <h6 class="heading-small text-muted mb-4">Bank Master QR Information</h6>
                      <div class="row">
                        <div class="col-lg-8">
                          <div class="row">
                            <div class="col-lg-6">
                              <div class="form-group">
                                <label class="form-control-label" for="bankmasterqr_bank_id">Bank {!!getMandatory()!!}</label> 
                                <select class = 'form-control select2' name = 'bankmasterqr_bank_id'>
                                  <option value = ''>Select One</option>
                                  <?php
                                  $banklist = "";
                                  foreach($data['bank_list'] as $c){
                                      if($c->bank_id == $data['bankmasterqr']->bankmasterqr_bank_id){
                                        $banklist .= "<option value = '{$c->bank_id}' SELECTED>{$c->bank_name}</option>";  
                                      }else{
                                        $banklist .= "<option value = '{$c->bank_id}'>{$c->bank_name}</option>";  
                                      }
                                      
                                  }
                                  echo $banklist;
                                  ?>
                                </select>
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-lg-6">
                              <div class="form-group">
                                <label class="form-control-label" for="bankmasterqr_name">Name {!!getMandatory()!!}</label> 
                                <input type="text" id="bankmasterqr_name" name = "bankmasterqr_name" class="form-control" placeholder="Name" value="{{$data['bankmasterqr']->bankmasterqr_name}}">
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-lg-6">
                              <div class="form-group">
                                <label class="form-control-label" for="bankmasterqr_file">File {!!getMandatory()!!}</label> 
                                <input type="file" id="bankmasterqr_file" name = "bankmasterqr_file" class="form-control"  value="{{$data['bankmasterqr']->bankmasterqr_file}}">
                                <?php 
                                if($data['bankmasterqr']->bankmasterqr_file != null){
                                ?>
                                <p><a href = '{{$data['bankmasterqr']->bankmasterqr_file}}' download>View</a></p>
                                  <?php }?>
                              </div>
                            </div>
                          </div>

                          <div class="row">
                            <div class="col-lg-6">
                              <div class="form-group">
                                  <label class="form-control-label" for="bankmasterqr_status">Status</label>
                                  <select name="bankmasterqr_status" class="form-control select2" id="bankmasterqr_status" class="bankmasterqr_status">
                                      <option value="1"
                                          {{ $data['bankmasterqr']->bankmasterqr_status == 1 ? 'selected' : '' }}>
                                          Active
                                      </option>
                                      <option value="2"
                                          {{ $data['bankmasterqr']->bankmasterqr_status == 2 ? 'selected' : '' }}>
                                          In-active
                                      </option>
                                  </select>
                              </div>
                            </div>
                          </div>

                      </div>  
                        <div class="col-lg-4">
                          <?php
                          $bankmasterqrmerchant = collect($data['bankmasterqrmerchant']);
                          foreach($data['merchant'] as $i => $m){
                        //  dd($bankmasterqrmerchant);
                      
                          ?>
                          <div class="form-check">
                            <input class="form-check-input" <?php if($bankmasterqrmerchant->where('bankmasterqrmerchant_merchant_id',$m->merchant_id)->count() > 0){ echo " CHECKED";}?> type="checkbox" value="{{ Crypt::encryptString($m->merchant_id) }}" name = 'bankmasterqr_merchant[]' id="checkbox{{$i}}">
                            <label class="form-check-label" for="checkbox{{$i}}">
                              <?php echo $m->merchant_name;?>
                            </label>
                          </div>
                          <?php
                          }
                          ?>
                        </div>
                     
                      </div>
                      <hr>
                      <h4>Login Credentails</h4>
                      <br>
                      
                      <div class="row">
                          <div class="col-lg-6">
                              <div class="form-group">
                                  <label class="form-control-label" for="bankmasterqr_username">Username
                                      {!! getMandatory() !!}</label>
                                  <input type="text" id="bankmasterqr_username" name="bankmasterqr_username"
                                      class="form-control" placeholder="Username"
                                      value="{{ $data['bankmasterqr']->bankmasterqr_username }}">
                              </div>
                          </div>
                      </div> 
                      <div class="row">
                          <div class="col-lg-6">
                              <div class="form-group">
                                  <label class="form-control-label" for="bankmasterqr_password">Password
                                      {!! getMandatory() !!}</label>
                                  <input type="text" id="bankmasterqr_password" name="bankmasterqr_password"
                                      class="form-control" placeholder="Password" value="">
                              </div>
                          </div>
                      </div>
                     
                        @csrf
                        <input type = "hidden" name = 'bankmasterqr_id' value = "{{Crypt::encryptString($data['bankmasterqr']->bankmasterqr_id)}}"/>
                        <button class="btn btn-primary" type="submit" id = 'submit_btn'  >Submit</button>
                  </form>


              
            </div>
          </div>
        </div>

      </div>

      <!-- Footer -->
      @include('pages.footer') 
    </div>
  </div>


  <!-- Argon Scripts -->
  <!-- Core -->
  <script src="../assets/vendor/jquery/dist/jquery.min.js"></script>
  <script src="../assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
  <script src="../assets/vendor/js-cookie/js.cookie.js"></script>
  <script src="../assets/vendor/jquery.scrollbar/jquery.scrollbar.min.js"></script>
  <script src="../assets/vendor/jquery-scroll-lock/dist/jquery-scrollLock.min.js"></script>
  <script src="../assets/vendor/datatables.net/js/jquery.dataTables.min.js"></script>
  <script src="../assets/vendor/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
  <script src="../assets/vendor/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
  <script src="../assets/vendor/select2/dist/js/select2.min.js"></script>
  <script src="../assets/vendor/sweetalert2/dist/sweetalert2.min.js"></script>
  <script src="../js/tmsjs/jqueryvalidation/jquery.validate.1.8.js"></script>
  <script src="../js/tmsjs/loadingoverlay/loadingoverlay.min.js"></script>
  <!-- Argon JS -->
  <script src="../assets/js/argon.js?v=1.2.0"></script>

  <script>
    $(document).ready( function () {
      $('.select2').select2();
      $("#bankform_id").validate({
              rules: 
              {
                  bankmasterqr_name:
                  {
                      required: true
                  },
                  bankmasterqr_bank_id:
                  {
                      required: true
                  },
                  bankmasterqr_username:
                  {
                      required: true
                  },
                  bankmasterqr_password:
                  {
                      required: @json($data['edit']) ? false : true,
                  }
              },
            showErrors: function(errorMap, errorList) {
                this.defaultShowErrors();

             },
             submitHandler: function (form) {
              var formData = new FormData(document.getElementById('bankform_id'));
                  formData.append('_token', "{{ csrf_token() }}");
             
                  $.ajax({
                      url:'/bankmasterqr/create',
                      type:'POST',
                      data:formData,
                      processData: false,
                      contentType: false,
                      enctype: 'multipart/form-data',
                      beforeSend: function() {
                          $('#submit_btn').text("loading...");
                          $('#submit_btn').attr("disabled",true);
                          $.LoadingOverlay("show");
                      },
                      error: function(xhr) {
                          Swal({
                              type: 'error',
                              title: "Something went wrong.",
                              showConfirmButton: false,
                              allowOutsideClick:false,
                              timer: 1500
                          })
                          $('#submit_btn').attr("disabled",false);
                          $('#submit_btn').text("Submit");
                          $.LoadingOverlay("hide");
                      },
                      success:function(jsonObj){
                          $('#submit_btn').attr("disabled",false);
                          $('#submit_btn').text("Submit");
                          $.LoadingOverlay("hide");
                          if(jsonObj.status == 1){
                              if(jsonObj.msg != ""){
                                  Swal({
                                      type: 'success',
                                      title: jsonObj.msg,
                                      showConfirmButton: false,
                                      allowOutsideClick:false,
                                      timer: 1500
                                  }).then(function() {
                                      window.location.href = '/bankmasterqr/listing'
                                  });
                              }

                          }else{
                              Swal({
                                  type: 'error',
                                  title: jsonObj.msg,
                                  showConfirmButton: false,
                                  allowOutsideClick:false,
                                  timer: 1500
                              })
                          }
                      }
                  });
              },
      });
      

    });

  </script>
</body>

</html>