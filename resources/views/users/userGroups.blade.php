
<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>User Group Permissions Management</title>
  <!-- Favicon -->
  <link rel="icon" href="../assets/img/brand/favicon.png" type="image/png">
  <!-- Fonts -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700">
  <!-- Icons -->
  <link rel="stylesheet" href="../assets/vendor/nucleo/css/nucleo.css" type="text/css">
  <link rel="stylesheet" href="../assets/vendor/@fortawesome/fontawesome-free/css/all.min.css" type="text/css">
  <link rel="stylesheet" href="../assets/vendor/datatables.net-bs4/css/dataTables.bootstrap4.min.css" type="text/css">
  <link rel="stylesheet" href="../assets/vendor/select2/dist/css/select2.min.css" type="text/css">
  <!-- Argon CSS -->
  <link rel="stylesheet" href="../assets/css/argon.css?v=1.2.0" type="text/css">

  <link rel="stylesheet" href="../css/tms.css" type="text/css">
</head>

<body>
  <!-- Sidenav -->
  @include('layouts.navbars.sidebar') 
  <!-- Main content -->
  <div class="main-content" id="panel">
    <!-- Topnav -->
    @include('layouts.navbars.topheader') 
    <!-- Header -->
    <!-- Header -->
    <div class="header bg-primary pb-6  {{Auth::user()->user_group_id == 1?'':'hide'}}">
      <div class="container-fluid">
        <div class="header-body">
          <div class="row align-items-center py-4">
            <div class="col-lg-6 col-md-7">
              <!-- <h6 class="h2 text-white d-inline-block mb-0">TMS</h6> -->
              <nav aria-label="breadcrumb" class="d-none d-md-inline-block ">
                <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                  <li class="breadcrumb-item"><a href="dashboard"><i class="fas fa-home"></i></a></li>
                  <li class="breadcrumb-item"><a href="/user/listing">User Group Permissions</a></li>
                </ol>
              </nav>
            </div>

            <div class="col-lg-6 col-md-5 text-right">
              <a href="/permissions/createForm" class="btn btn-sm btn-neutral">Create New</a>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--6  {{Auth::user()->user_group_id == 1?'':'hide'}}">
      <div class="row">
        <div class="col">
          <div class="card">
            <!-- Card header -->
            <div class="card-header border-0">
              <h3 class="mb-0"></h3>
            </div>
            <!-- Light table -->
            <div class="table-responsive" style = 'padding-bottom:20px;'>
              <table class="table align-items-center table-flush" id = 'form_table'>
                <thead class="thead-light">
                  <tr>
                    <th scope="col" class="sort" >User Group ID</th>
                    <th scope="col" class="sort" >User Group Name</th>
                    <th scope="col"></th>
                  </tr>
                </thead>
                <tbody class="list">
                  <?php
                  
                  foreach($role as $c){
                  ?>
                  <tr>
                    <td> <a  href="/permissions/updateForm?id={{$c->id}}">{{$c->id}}</a></td>
                    <td>{{$c->name}}</td>
                    <td>
                        <a href='/permissions/updateForm?id={{$c->id}}'>
                            <i class="fa fa-edit" style='color:cornflowerblue;font-size:20px;'></i>
                        </a>
                    </td>
                  </tr>
                  <?php  
                  }
                  ?>
                  
                </tbody>
              </table>
            </div>

          </div>
        </div>
      </div>

      <!-- Footer -->
      @include('pages.footer') 
    </div>
  </div>
  <!-- Argon Scripts -->
  <!-- Core -->
  <script src="../assets/vendor/jquery/dist/jquery.min.js"></script>
  <script src="../assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
  <script src="../assets/vendor/js-cookie/js.cookie.js"></script>
  <script src="../assets/vendor/jquery.scrollbar/jquery.scrollbar.min.js"></script>
  <script src="../assets/vendor/jquery-scroll-lock/dist/jquery-scrollLock.min.js"></script>
  <script src="../assets/vendor/datatables.net/js/jquery.dataTables.min.js"></script>
  <script src="../assets/vendor/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
  <script src="../assets/vendor/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
  <script src="../assets/vendor/select2/dist/js/select2.min.js"></script>
  <!-- Argon JS -->
  <script src="../assets/js/argon.js?v=1.2.0"></script>

  <script>
    $(document).ready( function () {
      $('#form_table').DataTable({
        "iDisplayLength": 100,
      });

    });
  </script>
</body>

</html>