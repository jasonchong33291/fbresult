<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Users Management</title>
    <!-- Favicon -->
    <link rel="icon" href="../assets/img/brand/favicon.png" type="image/png">
    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700">
    <!-- Icons -->
    <link rel="stylesheet" href="../assets/vendor/nucleo/css/nucleo.css" type="text/css">
    <link rel="stylesheet" href="../assets/vendor/@fortawesome/fontawesome-free/css/all.min.css" type="text/css">
    <link rel="stylesheet" href="../assets/vendor/datatables.net-bs4/css/dataTables.bootstrap4.min.css" type="text/css">
    <link rel="stylesheet" href="../assets/vendor/select2/dist/css/select2.min.css" type="text/css">
    <link rel="stylesheet" href="../assets/vendor/sweetalert2/dist/sweetalert2.min.css" type="text/css">
    <!-- Argon CSS -->
    <link rel="stylesheet" href="../assets/css/argon.css?v=1.2.0" type="text/css">
    <link rel="stylesheet" href="../css/tms.css" type="text/css">
</head>

<body>
    <!-- Sidenav -->
    @include('layouts.navbars.sidebar')
    <!-- Main content -->
    <div class="main-content" id="panel">
        <!-- Topnav -->
        @include('layouts.navbars.topheader')
        <!-- Header -->
        <!-- Header -->
        <div class="header bg-primary pb-6  {{Auth::user()->user_group_id == 1?'':'hide'}}">
            <div class="container-fluid">
                <div class="header-body">
                    <div class="row align-items-center py-4">
                        <div class="col-lg-6 col-7">
                            <!-- <h6 class="h2 text-white d-inline-block mb-0">TMS</h6> -->
                            <nav aria-label="breadcrumb" class="d-none d-md-inline-block ">
                                <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                                    <li class="breadcrumb-item"><a href="dashboard"><i class="fas fa-home"></i></a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="/permissions">User Group Listing</a></li>
                                </ol>
                            </nav>
                        </div>

                        <div class="col-lg-6 col-5 text-right">

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Page content -->
        <div class="container-fluid mt--6  {{Auth::user()->user_group_id == 1?'':'hide'}}">
            <div class="row">
                <div class="col">
                    <div class="card">
                        <div class="card-header border-0">
                            <h3 class="mb-0">{{ $data['edit'] ? 'Edit' : 'Add' }} User Group</h3>
                        </div>
                        <div class="card-body">
                            <form id="userGroupForm" method="POST" action="" enctype="multipart/form-data">
                                @csrf
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-control-label">User Group Name</label>
                                        <input type="text" name="name" value="{{ $data['role_info']->name }}"
                                            class="form-control" {{ $data['edit'] ? 'readonly' : '' }}
                                            placeholder="User Group Name">
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="table-responsive py-4 purple-table">
                                        <table class="table table-flush dataTable-table permission-list"
                                            id="permission-list">
                                            <thead class="thead-light-purple">
                                                <tr>
                                                    <th class="no-sort">Module Permission</th>
                                                    <th class="no-sort">
                                                        <div class="custom-control custom-checkbox">
                                                            <input class="custom-control-input" id="table-check-all"
                                                                onclick="$('input[name*=\'permission\']').prop('checked', this.checked);"
                                                                type="checkbox">
                                                            <label class="custom-control-label"
                                                                for="table-check-all"></label>Select All
                                                        </div>
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($data['all_permissions'] as $key => $permissions)
                                                    <tr>

                                                        <td class="font-weight-600">
                                                            {{ ucwords(str_replace('_', ' ', $key)) }}</td>
                                                        <td>
                                                            @foreach ($permissions as $permission)
                                                                <div class="custom-control custom-checkbox">
                                                                    <input type="checkbox" name="permission[][name]"
                                                                        {{ isset($permission->checked) && $permission->checked !== false ? 'checked' : '' }}
                                                                        {{ $data['edit'] ? '' : 'checked' }}
                                                                        value="{{ $permission->name }}"
                                                                        class="custom-control-input"
                                                                        id="permission_{{ $permission->name }}">
                                                                    <label class="custom-control-label"
                                                                        for="permission_{{ $permission->name }}">{{ $permission->display_name }}</label>
                                                                </div>
                                                            @endforeach
                                                        </td>

                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                                <input type="hidden" id="role_id" name="role_id"
                                    value="{{ $data['edit'] ? $data['role_info']->id : '' }}">

                                <button onclick="history.go(-1);return false;"
                                    class="btn btn-outline-default">Back</button>
                                <button type="submit" class="btn btn-primary float-right"
                                    id="btn-submit">Submit</button>
                            </form>


                        </div>
                    </div>
                </div>

            </div>

            <!-- Footer -->
            @include('pages.footer')
        </div>
    </div>


    <!-- Argon Scripts -->
    <!-- Core -->
    <script src="../assets/vendor/jquery/dist/jquery.min.js"></script>
    <script src="../assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <script src="../assets/vendor/js-cookie/js.cookie.js"></script>
    <script src="../assets/vendor/jquery.scrollbar/jquery.scrollbar.min.js"></script>
    <script src="../assets/vendor/jquery-scroll-lock/dist/jquery-scrollLock.min.js"></script>
    <script src="../assets/vendor/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../assets/vendor/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
    <script src="../assets/vendor/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
    <script src="../assets/vendor/select2/dist/js/select2.min.js"></script>
    <script src="../assets/vendor/sweetalert2/dist/sweetalert2.min.js"></script>
    <script src="../js/tmsjs/jqueryvalidation/jquery.validate.1.8.js"></script>
    <script src="../js/tmsjs/loadingoverlay/loadingoverlay.min.js"></script>
    <script src="../assets/vendor/moment.min.js"></script>
    <!-- Argon JS -->
    <script src="../assets/js/argon.js?v=1.2.0"></script>

    <script>
        $(document).ready(function() {

            $("#userGroupForm").validate({
                ignore: "",
                focusInvalid: false,
                invalidHandler: function(form, validator) {

                    if (!validator.numberOfInvalids())
                        return;

                    $('html, body').animate({
                        scrollTop: $(validator.errorList[0].element).offset().top
                    }, 500);

                },
                rules: {
                    name: {
                        required: true,
                        minlength: 5,
                        maxlength: 32,
                        number: false
                    },
                },
                errorPlacement: function(error, element) {
                    // console.log(error);
                    if (element.is(":checkbox")) {
                        error.insertBefore(element); // custom placement example
                    } else {
                        error.insertAfter(element); // default placement
                    }
                }

            });

            $("#btn-submit").click(function(e) {

                e.preventDefault();
                var formStatus = $('#userGroupForm').validate().form();

                //   console.log(formStatus)
                if (true == formStatus) {
                    var form = $('#userGroupForm')[0];

                    $.ajax({
                        url: '/permissions/create',
                        dataType: 'json',
                        data: new FormData(form),
                        processData: false,
                        contentType: false,
                        type: 'post',
                        beforeSend: function() {
                            $('#btn-submit').prop('disabled', true);
                            $('#btn-submit').html('loading');
                        },
                        complete: function() {
                            $('#btn-submit').prop('disabled', false);
                            $('#btn-submit').html('<i class="far fa-save pr-1"></i> Save');
                        },
                        success: function(jsonObj) {
                            console.log(jsonObj)
                            if (jsonObj.status == 1) {
                                swal({
                                    icon: "success",
                                    text: jsonObj.msg,
                                }).then(function() {
                                    window.location.href =
                                        '/permissions?msg2=User Group Form Submitted Successfully.'
                                });
                            } else {
                                if (jsonObj.status == 0) {
                                    swal({
                                        icon: "warning",
                                        text: jsonObj.msg,
                                    })
                                } else {
                                    swal({
                                        icon: "warning",
                                        text: "System Error. Please contact adminstrator",
                                    })
                                }
                            }
                        }
                    });
                }

            });
        });
    </script>
</body>

</html>
