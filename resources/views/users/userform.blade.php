<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Users Management</title>
    <!-- Favicon -->
    <link rel="icon" href="../assets/img/brand/favicon.png" type="image/png">
    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700">
    <!-- Icons -->
    <link rel="stylesheet" href="../assets/vendor/nucleo/css/nucleo.css" type="text/css">
    <link rel="stylesheet" href="../assets/vendor/@fortawesome/fontawesome-free/css/all.min.css" type="text/css">
    <link rel="stylesheet" href="../assets/vendor/datatables.net-bs4/css/dataTables.bootstrap4.min.css" type="text/css">
    <link rel="stylesheet" href="../assets/vendor/select2/dist/css/select2.min.css" type="text/css">
    <link rel="stylesheet" href="../assets/vendor/sweetalert2/dist/sweetalert2.min.css" type="text/css">
    <!-- Argon CSS -->
    <link rel="stylesheet" href="../assets/css/argon.css?v=1.2.0" type="text/css">
    <link rel="stylesheet" href="../css/tms.css" type="text/css">
</head>

<body>
    <!-- Sidenav -->
    @include('layouts.navbars.sidebar')
    <!-- Main content -->
    <div class="main-content" id="panel">
        <!-- Topnav -->
        @include('layouts.navbars.topheader')
        <!-- Header -->
        <!-- Header -->
        <div class="header bg-primary pb-6">
            <div class="container-fluid">
                <div class="header-body">
                    <div class="row align-items-center py-4">
                        <div class="col-lg-6 col-7">
                            <!-- <h6 class="h2 text-white d-inline-block mb-0">TMS</h6> -->
                            <nav aria-label="breadcrumb" class="d-none d-md-inline-block ">
                                <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                                    <li class="breadcrumb-item"><a href="dashboard"><i class="fas fa-home"></i></a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="/user">Users Listing</a></li>
                                </ol>
                            </nav>
                        </div>

                        <div class="col-lg-6 col-5 text-right">

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Page content -->
        <div class="container-fluid mt--6">
            <div class="row">
                <div class="col">



                    <div class="card">

                        <div class="card-body">

                            <form id='userform_id'>
                                <h6 class="heading-small text-muted mb-4">Users Information</h6>
                                <div class="pl-lg-4">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label class="form-control-label" for="user_name">Name
                                                    {!! getMandatory() !!}</label>
                                                <input type="text" id="user_name" name="user_name"
                                                    class="form-control" placeholder="Name"
                                                    value="{{ $data['user']->user_name }}">
                                            </div>
                                        </div>
                                        {{-- <div class="col-lg-6">
                                            <div class="form-group">
                                                <label class="form-control-label" for="user_email">Email</label>
                                                <input type="text" id="user_email" name="user_email"
                                                    class="form-control" placeholder="Email"
                                                    value="{{ $data['user']->user_email }}">
                                            </div>
                                        </div> --}}
                                    </div>
                                    {{-- <div class="row">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label class="form-control-label" for="user_mobile">Mobile
                                                    {!! getMandatory() !!}</label>
                                                <input type="text" id="user_mobile" name="user_mobile"
                                                    class="form-control" placeholder="Mobile"
                                                    value="{{ $data['user']->user_mobile }}">
                                            </div>
                                        </div>
                                    </div> --}}
                                    <?php
                                    if((Auth::user()->user_group_id == 1) || (Auth::user()->user_group_id == 2) || (Auth::user()->user_group_id == 4)){//super admin & admin & merchant
                                    ?>
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="form-group"><label class="form-control-label"
                                                    for="user_group_id">Role {!! getMandatory() !!}</label>
                                                <select name="user_group_id" class="form-control" id="user_group_id"
                                                    class="user_group_id">
                                                    <option value="0">
                                                        --- Please Select ---
                                                    </option>
                                                    @foreach ($data['roles'] as $role)
                                                        <option value="{{ $role->id }}"
                                                            {{ $data['user']->user_group_id == $role->id ? 'selected' : '' }}>
                                                            {{ $role->name }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        {{-- <div class="col-lg-6">
                                            <div class="form-group">
                                                <label class="form-control-label" for="user_parent">Parent</label>
                                                <select name="user_parent" class="form-control"
                                                    id="user_parent" class="user_parent">
                                                    <option value="0">
                                                        --- Please Select ---
                                                    </option>
                                                    @foreach ($data['managers'] as $m)
                                                        <option value="{{ $m->id }}"
                                                            {{ $data['user']->user_parent == $m->id ? 'selected' : '' }}>
                                                            {{ $m->user_name }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div> --}}
                                    </div>
                                    <?php }?>
                                    <div class="row">

                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label class="form-control-label" for="user_status">Status</label>
                                                <select name="user_status" class="form-control" id="user_status" class="user_status">
                                                    <option value="1"
                                                        {{ $data['user']->user_status == 1 ? 'selected' : '' }}>
                                                        Active
                                                    </option>
                                                    <option value="2"
                                                        {{ $data['user']->user_status == 2 ? 'selected' : '' }}>
                                                        In-active
                                                    </option>
                                                </select>
                                            </div>
                                        </div>

                                    </div>
                                    {{-- <hr>
                                    <h4>Rate Settings</h4>
                                    <br>
                                    <div class="row">

                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label class="form-control-label" for="user_p2p_comm">P2P Rate (%)
                                                    {!! getMandatory() !!}</label>
                                                <input type="text" id="user_p2p_comm" name="user_p2p_comm"
                                                    class="form-control" placeholder="P2P Rate (%)" onkeypress="return isNumberKey(event)"
                                                    value="{{ $data['user']->user_p2p_comm }}">
                                            </div>
                                        </div>

                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label class="form-control-label" for="user_qr_comm">QR Rate (%)
                                                    {!! getMandatory() !!}</label>
                                                <input type="text" id="user_qr_comm" name="user_qr_comm" onkeypress="return isNumberKey(event)"
                                                    class="form-control" placeholder="QR Rate (%)"
                                                    value="{{ $data['user']->user_qr_comm }}">
                                            </div>
                                        </div>
                                    </div> --}}

                                    <hr>
                                    <h4>Login Credentails</h4>
                                    <br>
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label class="form-control-label" for="user_username">Username
                                                    {!! getMandatory() !!}</label>
                                                <input type="text" id="user_username" name="user_username"
                                                    class="form-control" placeholder="Username"
                                                    value="{{ $data['user']->user_username }}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label class="form-control-label" for="user_password">Password
                                                    {!! getMandatory() !!}</label>
                                                <input type="password" id="user_password" name="user_password"
                                                    class="form-control" placeholder="Password" value="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label class="form-control-label" for="user_confirm_password">Confirm
                                                    Password {!! getMandatory() !!}</label>
                                                <input type="password" id="user_confirm_password"
                                                    name="user_confirm_password" class="form-control"
                                                    placeholder="Confirm Password" value="">
                                            </div>
                                        </div>
                                    </div>


                                    @csrf
                                    <input type="hidden" name='id' id='id' value="{{ Crypt::encryptString($data['user']->id)}}" />
                                    <button class="btn btn-primary" type="submit" id='submit_btn'>Submit</button>
                                </div>

                            </form>



                        </div>
                    </div>
                </div>

            </div>

            <!-- Footer -->
            @include('pages.footer')
        </div>
    </div>


    <!-- Argon Scripts -->
    <!-- Core -->
    <script src="../assets/vendor/jquery/dist/jquery.min.js"></script>
    <script src="../assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <script src="../assets/vendor/js-cookie/js.cookie.js"></script>
    <script src="../assets/vendor/jquery.scrollbar/jquery.scrollbar.min.js"></script>
    <script src="../assets/vendor/jquery-scroll-lock/dist/jquery-scrollLock.min.js"></script>
    <script src="../assets/vendor/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../assets/vendor/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
    <script src="../assets/vendor/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
    <script src="../assets/vendor/select2/dist/js/select2.min.js"></script>
    <script src="../assets/vendor/sweetalert2/dist/sweetalert2.min.js"></script>
    <script src="../js/tmsjs/jqueryvalidation/jquery.validate.1.8.js"></script>
    <script src="../js/tmsjs/loadingoverlay/loadingoverlay.min.js"></script>
    <!-- Argon JS -->
    <script src="../assets/js/argon.js?v=1.2.0"></script>

    <script>
        $(document).ready(function() {
            $('#user_group_id').select2();
            $('#user_parent').select2();
            $("#userform_id").validate({
                rules: {
                    user_name: {
                        required: true
                    },
                    user_email: {
                        email: true
                    },
                    user_group_id: {
                        required: true
                    },
                    user_mobile: {
                        required: true
                    },
                    user_username: {
                        required: true,
                        remote: {
                            url: "/user/validateusername/",
                            type: "GET",
                            data: {
                                user_username: function() {
                                    return $("#user_username").val();
                                },
                                id: function() {
                                    return $("#id").val();
                                },
                                _token: function() {
                                    return "{{ csrf_token() }}";
                                },
                            }
                        }
                    },
                    user_password: {
                        required: @json($data['edit']) ? false : true,
                        minlength: 5
                    },
                    user_confirm_password: {
                        required: @json($data['edit']) ? false : true,
                        minlength: 5,
                        equalTo: "#user_password"
                    },
                },
                showErrors: function(errorMap, errorList) {
                    this.defaultShowErrors();

                },
                submitHandler: function(form) {
                    //  console.log(form)
                    //  alert('123');return false;

                    $.ajax({
                        url: '/user/create',
                        type: 'POST',
                        data: $('#userform_id').serialize() + "&_token={{ csrf_token() }}",
                        cache: false,
                        beforeSend: function() {
                            $('#submit_btn').text("loading...");
                            $('#submit_btn').attr("disabled", true);
                            $.LoadingOverlay("show");
                        },
                        error: function(xhr) {
                            Swal({
                                type: 'error',
                                title: "Duplicated user email.",
                                showConfirmButton: false,
                                allowOutsideClick: false,
                                timer: 1500
                            })
                            $('#submit_btn').attr("disabled", false);
                            $('#submit_btn').text("Submit");
                            $.LoadingOverlay("hide");
                        },
                        success: function(jsonObj) {
                            $('#submit_btn').attr("disabled", false);
                            $('#submit_btn').text("Submit");
                            $.LoadingOverlay("hide");
                            if (jsonObj.status == 1) {
                                if (jsonObj.msg != "") {
                                    Swal({
                                        type: 'success',
                                        title: jsonObj.msg,
                                        showConfirmButton: false,
                                        allowOutsideClick: false,
                                        timer: 1500
                                    }).then(function() {
                                        window.location.href = '/user'
                                    });
                                }

                            } else {
                                Swal({
                                    type: 'error',
                                    title: jsonObj.msg,
                                    showConfirmButton: false,
                                    allowOutsideClick: false,
                                    timer: 1500
                                })
                            }
                        }
                    });
                },
            });



        });

        function isNumberKey(evt){
          var charCode = (evt.which) ? evt.which : evt.keyCode;
          if (charCode != 46 && charCode > 31 
            && (charCode < 48 || charCode > 57))
             return false;

          return true;
       }
    </script>
</body>

</html>
