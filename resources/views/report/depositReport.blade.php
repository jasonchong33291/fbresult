<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Sales Report">
    <meta name="author" content="">
    <title>Sales Report Management</title>
    <!-- Favicon -->
    <link rel="icon" href="../assets/img/brand/favicon.png" type="image/png">
    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700">
    <!-- Icons -->
    <link rel="stylesheet" href="../assets/vendor/nucleo/css/nucleo.css" type="text/css">
    <link rel="stylesheet" href="../assets/vendor/@fortawesome/fontawesome-free/css/all.min.css" type="text/css">
    <link rel="stylesheet" href="../assets/vendor/datatables.net-bs4/css/dataTables.bootstrap4.min.css" type="text/css">
    <link rel="stylesheet" href="../assets/vendor/select2/dist/css/select2.min.css" type="text/css">
    <link rel="stylesheet" href="../assets/vendor/sweetalert2/dist/sweetalert2.min.css" type="text/css">
    <!-- Argon CSS -->
    <link rel="stylesheet" href="../assets/css/argon.css?v=1.2.0" type="text/css">
    <link rel="stylesheet" href="../css/tms.css" type="text/css">

    <style>
        .select2-container--open {
            z-index: 99999999999999;
        }

        .select2-container .select2-selection--single .select2-selection__rendered {
            white-space: nowrap !important;
        }
      </style>

</head>

<body>
    <!-- Sidenav -->
    @include('layouts.navbars.sidebar')
    <!-- Main content -->
    <div class="main-content" id="panel">
        <!-- Topnav -->
        @include('layouts.navbars.topheader')
        <!-- Header -->
        <!-- Header -->
        <div class="header bg-primary pb-6">
            <div class="container-fluid">
                <div class="header-body">
                    <div class="row align-items-center py-4">
                        <div class="col-lg-6 col-7">
                            <!-- <h6 class="h2 text-white d-inline-block mb-0">TMS</h6> -->
                            <nav aria-label="breadcrumb" class="d-none d-md-inline-block ">
                                <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                                    <li class="breadcrumb-item"><a href="dashboard"><i class="fas fa-home"></i></a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="{{ route('salesreport') }}">Sales Report</a>
                                    </li>
                                </ol>
                            </nav>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <!-- Page content -->
        <div class="container-fluid mt--6">
            <div class="row">
                <div class="col">
                    <div class="card">
                        <!-- Card header -->
                        <div class="card-header border-0">
                            <h3 class="mb-0">Sales Report</h3>
                            
                        </div>
                        <form id = 'form1'>
                            <div class="row col-md-12 mb-3">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="form-control-label filter-name">Report Type</label><br>
                                        <select name="filter_report_type" class="form-control" id="filter_report_type">
                                            <option value="summary">Summary</option>
                                            <option value="detail">Detail</option>
                                            <?php
                                            if((Auth::user()->user_group_id == 1) || (Auth::user()->user_group_id == 2) || (Auth::user()->user_group_id == 7) || (Auth::user()->user_group_id == 9)){//super admin & admin
                                            ?>
                                            <option value="bank">Bank</option>
                                            <?php }?>
                                        </select>
                                    </div>
                                </div>
                                <?php
                                if((Auth::user()->user_group_id == 1) || (Auth::user()->user_group_id == 2) || (Auth::user()->user_group_id == 7) || (Auth::user()->user_group_id == 9)){//super admin & admin
                                ?>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="form-control-label filter-name">Agent</label><br>
                                        <select name="filter_agent" class="form-control" id="filter_agent">
                                            <option value="">-- Please Select --</option>
                                            <?php
                                            foreach($data['agent'] as $c){
                                            ?>
                                            <option value = '{{$c->id }}'>{{$c->user_name}}</option>
                                            <?php
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <?php
                                }
                                $allow = array(1,2,3,6,7,9);
                                if(in_array(Auth::user()->user_group_id,$allow)){//agent
                                ?>
                                <div class="col-md-3 ">
                                    <div class="form-group div_courseruns" >
                                        <label class="form-control-label filter-name">Merchant</label><br>
                                        <select name="filter_merchant" class="form-control" id="filter_merchant">
                                            <option value="">All</option>
                                            <?php
                                            foreach($data['merchant'] as $c){
                                            ?>
                                            <option value = '{{$c->merchant_id}}'>{{$c->merchant_code}}</option>
                                            <?php
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <?php  
                                }
                                ?>

                                    <div class="col-md-3 ">
                                        <div class="form-group div_courseruns" >
                                            <label class="form-control-label filter-name">Deposit Type</label><br>
                                            <select name="filter_mode" class="form-control" id="filter_mode">
                                                <option value="">All</option>
                                                <option value="p2p">P2P</option>
                                                <option value="qr">QR</option> 
                                            </select>
                                        </div>
                                    </div>


                            </div>
                            <div class="row col-md-12 mb-3">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="form-control-label filter-name">Date From</label><br>
                                        <div class="input-group input-group-alternative">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="ni ni-calendar-grid-58"></i>
                                                </span>
                                            </div>
                                            <input class="form-control datepicker" placeholder="Select date" type="text" name="filter_date_from" id="filter_date_from" value = "<?php echo $data['filter_date_from'];?>">
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="form-control-label filter-name">Date To</label><br>
                                        <div class="input-group input-group-alternative">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="ni ni-calendar-grid-58"></i>
                                                </span>
                                            </div>
                                            <input class="form-control datepicker" placeholder="Select date" type="text" name="filter_date_to" id="filter_date_to" value= "<?php echo $data['filter_date_to'];?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-1  mt-4" >
                                    <button type="button" class="btn btn-primary float-right" id = 'search_btn'>Search</button>
                                </div>
                            </div>
                        </form>

                            <div id = 'result_table'>

                            </div>

                    </div>
                </div>
            </div>

            <!-- Footer -->
            @include('pages.footer')
        </div>
    </div>
    <!-- Argon Scripts -->
    <!-- Core -->
    <script src="../assets/vendor/jquery/dist/jquery.min.js"></script>
    <script src="../assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <script src="../assets/vendor/js-cookie/js.cookie.js"></script>
    <script src="../assets/vendor/jquery.scrollbar/jquery.scrollbar.min.js"></script>
    <script src="../assets/vendor/jquery-scroll-lock/dist/jquery-scrollLock.min.js"></script>
    <script src="../assets/vendor/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../assets/vendor/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
    <script src="../assets/vendor/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
    <script src="../assets/vendor/select2/dist/js/select2.min.js"></script>
    <script src="../assets/vendor/sweetalert2/dist/sweetalert2.min.js"></script>
    <script src="../assets/vendor/moment.min.js"></script>
    <script src="../js/tmsjs/loadingoverlay/loadingoverlay.min.js"></script>
    <!-- Argon JS -->
    <script src="../assets/js/argon.js?v=1.2.0"></script>

    <script>
        const tableOptions = {
            language: {
                paginate: {
                    next: '<i class="fas fa-chevron-right"></i>',
                    previous: '<i class="fas fa-chevron-left"></i>'
                }
            },
            "iDisplayLength": 30,
            responsive: true,
            "ordering": false,
            scrollX: false,
        };

        $(document).ready(function() {
            $('#filter_report_type').select2();
            $('#filter_merchant').select2();
            $('#filter_agent').select2();
        });

        $(document).on("click","#search_btn",function(){
            var filter_report_type = $('#filter_report_type').val();

            if($('#filter_report_type').val() == "summary"){
                var url = "salessearchsummary";
            }else if($('#filter_report_type').val() == "bank"){
                var url = "salessearchbank";
            }else{
                var url = "salessearchdetail";
            }
                $.ajax({
                    url:url,
                    type:'POST',
                    data: $('#form1').serialize() + "&_token={{csrf_token()}}",
                    cache:false,
                    beforeSend: function() {
                        $.LoadingOverlay("show");
                    },
                    error: function(xhr) {
                        $.LoadingOverlay("hide");
                    },
                    success:function(jsonObj){
                        $.LoadingOverlay("hide");

                        $('#result_table').html(jsonObj);


                        
                    }
                });
        });

       
    </script>
</body>

</html>
