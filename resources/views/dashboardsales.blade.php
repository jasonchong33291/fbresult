<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Dashboard</title>
    <!-- Favicon -->
    <link rel="icon" href="../assets/img/brand/favicon.png" type="image/png">
    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700">
    <!-- Icons -->
    <link rel="stylesheet" href="../assets/vendor/nucleo/css/nucleo.css" type="text/css">
    <link rel="stylesheet" href="../assets/vendor/@fortawesome/fontawesome-free/css/all.min.css" type="text/css">
    <link rel="stylesheet" href="../assets/vendor/datatables.net-bs4/css/dataTables.bootstrap4.min.css" type="text/css">
    <link rel="stylesheet" href="../assets/vendor/select2/dist/css/select2.min.css" type="text/css">
    <!-- Argon CSS -->
    <link rel="stylesheet" href="../assets/css/argon.css?v=1.2.0" type="text/css">
    <link rel="stylesheet" href="../css/tms.css" type="text/css">
</head>

<body>
    @include('layouts.navbars.sidebar')
    <!-- Sidenav -->

    <!-- Main content -->
    <div class="main-content" id="panel">
        <!-- Topnav -->
        @include('layouts.navbars.topheader')
        @include('layouts.headers.cards') 


        <div class="container-fluid mt--7">
           
                

            <!-- Footer -->
            {{-- @include('pages.footer') --}}
        </div>
    </div>

    <script src="../assets/vendor/jquery/dist/jquery.min.js"></script>
    <script src="../assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <script src="../assets/vendor/js-cookie/js.cookie.js"></script>
    <script src="../assets/vendor/jquery.scrollbar/jquery.scrollbar.min.js"></script>
    <script src="../assets/vendor/jquery-scroll-lock/dist/jquery-scrollLock.min.js"></script>
    <script src="../assets/vendor/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../assets/vendor/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
    <!-- Argon JS -->
    <script src="../assets/js/argon.js?v=1.2.0"></script>
    <script>
        const tableOptions = {
            language: {
                paginate: {
                    next: '<i class="fas fa-chevron-right"></i>',
                    previous: '<i class="fas fa-chevron-left"></i>'
                }
            },
            "iDisplayLength": 10,
            responsive: true,
            "ordering": false,
            scrollX: false
        };

        $(document).ready(function() {
            var osTable = $("#form_table").DataTable({
                language: {
                    paginate: {
                        next: '<i class="fas fa-chevron-right"></i>',
                        previous: '<i class="fas fa-chevron-left"></i>'
                    }
                },
                "order": [
                    [1, "desc"]
                ],
                'columnDefs': [{
                        "orderable": false,
                        "targets": [10]
                    },
                    {
                        render: function(data, type, full, meta) {
                            return "<div class='break_word width_150'>" + data + "</div>";
                        },
                        targets: [3, 6]
                    },
                    {
                        render: function(data, type, full, meta) {
                            return "<div class='break_word width_250'>" + data + "</div>";
                        },
                        targets: 7
                    }
                ],
                "iDisplayLength": 50,
                responsive: true,
                scrollX: false
            });

            var tiTable = $("#ti_form_table").DataTable({
                language: {
                    paginate: {
                        next: '<i class="fas fa-chevron-right"></i>',
                        previous: '<i class="fas fa-chevron-left"></i>'
                    }
                },
                "order": [
                    [1, "desc"]
                ],
                'columnDefs': [{
                        "orderable": false,
                    },
                    {
                        render: function(data, type, full, meta) {
                            return "<div class='break_word width_150'>" + data + "</div>";
                        },
                        targets: [6]
                    },
                    {
                        render: function(data, type, full, meta) {
                            return "<div class='break_word width_250'>" + data + "</div>";
                        },
                        targets: 4
                    }
                ],
                "iDisplayLength": 50,
                responsive: true,
                scrollX: false
            });


            var tableContainer = $(".table-bottom-scrollbarbar");
            var table = $(".table-bottom-scrollbarbar table");
            var fakeContainer = $(".table-top-scrollbar");
            var fakeDiv = $(".table-top-scrollbar div");

            var tableWidth = table.width();
            fakeDiv.width(tableWidth);

            fakeContainer.scroll(function() {
                tableContainer.scrollLeft(fakeContainer.scrollLeft());
            });

        });
    </script>
</body>

</html>
