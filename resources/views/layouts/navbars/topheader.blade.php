    <!-- Topnav -->
  <nav class="navbar navbar-top navbar-expand navbar-dark  border-bottom">
      <div class="container">
        <div class="collapse navbar-collapse" id="navbarSupportedContent">

          <li class="nav-item d-lg-block ml-lg-4" style = 'display:block'>
            <a class="header-brand" href="/" style="font-size:200%;">
              <img src = "assets/images/logo.jfif" style = 'width:180px;height:auto'/>
            </a>
          </li>
          <!-- Navbar links -->
          <ul class="navbar-nav align-items-center  ml-md-auto ">
            <li class="nav-item d-xl-none">
              <!-- Sidenav toggler -->
              <div class="pr-3 sidenav-toggler sidenav-toggler-dark" data-action="sidenav-pin" data-target="#sidenav-main">
                <div class="sidenav-toggler-inner">
                  <i class="sidenav-toggler-line"></i>
                  <i class="sidenav-toggler-line"></i>
                  <i class="sidenav-toggler-line"></i>
                </div>
              </div>
            </li>
            <li class="nav-item d-sm-none">
              <a class="nav-link" href="#" data-action="search-show" data-target="#navbar-search-main">
                <i class="ni ni-zoom-split-in"></i>
              </a>
            </li>
          </ul>
          <ul class="navbar-nav align-items-center  ml-auto ml-md-0 ">
            <li class="nav-item dropdown">
              <a class="nav-link" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <div class="media align-items-center">
                  Language
                </div>
              </a>
              <div class="dropdown-menu  dropdown-menu-right ">
                <div class="dropdown-header noti-title">
                  <h6 class="text-overflow m-0">Please choose your language</h6>
                </div>
                <a href="/api/language?lang=en" class="dropdown-item">
                  <i class="ni ni-single-02"></i>
                  <span>English</span>
                </a>
                <a href="/api/language?lang=cn" class="dropdown-item">
                  <i class="ni ni-single-02"></i>
                  <span>Chinese</span>
                </a>
                <div class="dropdown-divider"></div>
                
              </div>
            </li>
          </ul>
        </div>
      </div>
    </nav>