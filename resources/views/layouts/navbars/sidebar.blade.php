<nav class="sidenav navbar navbar-vertical  fixed-left  navbar-expand-xs navbar-light bg-white" id="sidenav-main">
    <div class="scrollbar-inner">
      <!-- Brand -->
      <div class="sidenav-header  align-items-center">
        <a class="navbar-brand" href="javascript:void(0)">
          <img src="../assets/images/PNG.png" class="navbar-brand-img" alt="...">
        </a>
      </div>
      <div class="navbar-inner">
        <!-- Collapse -->
        <div class="collapse navbar-collapse" id="sidenav-collapse-main">
          <!-- Nav items -->
            <!-- Collapse -->
            <div class="collapse navbar-collapse" id="sidenav-collapse-main">
                <!-- Collapse header -->
                <div class="navbar-collapse-header d-md-none">
                    <div class="row">
                        <div class="col-6 collapse-brand">
                            <a href="{{ route('home') }}">
                                <img src="../assets/images/PNG.png">
                            </a>
                        </div>
                        <div class="col-6 collapse-close">
                            <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#sidenav-collapse-main" aria-controls="sidenav-main" aria-expanded="false" aria-label="Toggle sidenav">
                                <span></span>
                                <span></span>
                            </button>
                        </div>
                    </div>
                </div>
                  <!-- Form -->
                  <form class="mt-4 mb-3 d-md-none">
                      <div class="input-group input-group-rounded input-group-merge">
                          <input type="search" class="form-control form-control-rounded form-control-prepended" placeholder="{{ __('Search') }}" aria-label="Search">
                          <div class="input-group-prepend">
                              <div class="input-group-text">
                                  <span class="fa fa-search"></span>
                              </div>
                          </div>
                      </div>
                  </form>
                    <!-- Navigation -->
                    <ul class="navbar-nav">
                        <li class="nav-item {{ canAccess('view_dashboard') }}">
                            <a class="nav-link <?php if(Route::currentRouteName() == "home"){ echo " active";}?>" href="{{ route('home') }}">
                                <i class="ni ni-tv-2 text-blue"></i> Dashboard
                            </a>
                        </li>
                        <?php
                        if(canAccess('view_deposit')){
                        ?>
                        <li class="nav-item ">
                            <a class="nav-link <?php if(Route::currentRouteName() == "depositp2p"){ echo " active";}?>" href="{{ route('depositp2p',['deposit_mode'=>'p2p']) }}">
                                <i class="ni ni-money-coins text-blue"></i> Deposit (P2P)
                            </a>
                        </li>
                        <li class="nav-item ">
                            <a class="nav-link <?php if(Route::currentRouteName() == "depositqr"){ echo " active";}?>" href="{{ route('depositqr',['deposit_mode'=>'qr']) }}">
                                <i class="ni ni-money-coins text-blue"></i> Deposit (QR)
                            </a>
                        </li>
                        <?php }?>
                        <?php
                        if(canAccess('view_settlement')){
                        ?>
                        <li class="nav-item ">
                            <a class="nav-link <?php if(Route::currentRouteName() == "settlement"){ echo " active";}?>" href="{{ route('settlement') }}">
                                <i class="ni ni-money-coins text-blue"></i> Settlement
                            </a>
                        </li>
                        <?php }?>
                        <?php
                        if(canAccess('view_merchant')){
                        ?>
                        <li class="nav-item">
                            <a class="nav-link <?php if(Route::currentRouteName() == "merchant"){ echo " active";}?>" href="{{ route('merchant') }}">
                                <i class="fa fa-users text-blue"></i> Merchants
                            </a>
                        </li>
                        <?php }?>
                        <?php
                        $array = [4,8,10,11];
                        if(in_array(Auth::user()->user_group_id,$array)){//merchant
                        ?>
                        <?php
                       // dd(canAccess('manage_bank_merchant'));
                        if(canAccess('manage_bank_merchant')){
                            $merchant = DB::table('db_merchant')->where('merchant_id',Auth::user()->user_merchant_id)->get()->first();
                            if($merchant->merchant_isownbank == 1){
                        ?>
                        <li class="nav-item">
                            <a class="nav-link <?php if(Route::currentRouteName() == "merchant"){ echo " active";}?>" href="/merchant/manageBankMerchantForm?merchant_id={{ Crypt::encryptString(Auth::user()->user_merchant_id) }}">
                                <i class="fa fa-users text-blue"></i> Deposit Bank Accounts 
                            </a>
                        </li>
                        <?php }
                        }
                        ?>   
                        <?php
                        if(canAccess('view_settlementbank')){
                        ?>
                        <li class="nav-item">
                            <a class="nav-link <?php if(Route::currentRouteName() == "merchant"){ echo " active";}?>" href="/merchant/manageSettlementBankMerchantForm?merchant_id={{ Crypt::encryptString(Auth::user()->user_merchant_id) }}">
                                <i class="fa fa-users text-blue"></i> Settlement Bank Accounts
                            </a>
                        </li>
                        <?php }?>  
                        <?php
                        }
                        if(canAccess('view_user')){
                        ?>
                        <li class="nav-item">
                            <a class="nav-link <?php if(Route::currentRouteName() == "users"){ echo " active";}?>" href="{{ route('users') }}">
                                <i class="fa fa-user text-blue"></i> Users
                            </a>
                        </li>
                        <?php }?>
                        <?php
                        if(canAccess('view_bankmaster')){
                        ?>
                        <li class="nav-item ">
                            <a class="nav-link <?php if(Route::currentRouteName() == "bankmaster"){ echo " active";}?>" href="{{ route('bankmaster') }}">
                                <i class="ni ni-basket text-blue"></i> Bank Master
                            </a>
                        </li>
                        <?php }?>
                        <?php
                        if(canAccess('view_bankmasterqr')){
                        ?>
                        <li class="nav-item ">
                            <a class="nav-link <?php if(Route::currentRouteName() == "bankmasterqr"){ echo " active";}?>" href="{{ route('bankmasterqr') }}">
                                <i class="ni ni-basket text-blue"></i> Bank Master QR
                            </a>
                        </li>
                        <?php }?>
                        <?php
                        if(canAccess('view_bank')){
                        ?>
                        <li class="nav-item">
                            <a class="nav-link <?php if(Route::currentRouteName() == "bank"){ echo " active";}?>" href="{{ route('bank') }}">
                                <i class="fa fa-money-check text-blue"></i> Banks
                            </a>
                        </li>
                        <?php }?>
                        <?php
                        if(canAccess('view_wallet')){
                        ?>
                        <li class="nav-item">
                            <a class="nav-link <?php if(Route::currentRouteName() == "wallet"){ echo " active";}?>" href="{{ route('wallet') }}">
                                <i class="fa fa-money-check text-blue"></i> Wallets
                            </a>
                        </li>
                        <?php }?>
                        <?php
                        if(canAccess('view_sales_report')){
                        ?>
                        <li class="nav-item">
                            <a class="nav-link <?php if(Route::currentRouteName() == "salesreport"){ echo " active";}?>" href="{{ route('salesreport') }}">
                                <i class="fa fa-money-check text-blue"></i> Sales Reports
                            </a>
                        </li>
                        <?php }?>
                        <?php
                        if((Auth::user()->user_group_id == 1) || (Auth::user()->user_group_id == 2) ){
                        ?>
                        <li class="nav-item {{Auth::user()->user_group_id == 1?'':'hide'}}">
                            <a class="nav-link <?php if(Route::currentRouteName() == "permissions"){ echo " active";}?>" href="{{ route('permissions') }}">
                                <i class="fa fa-key text-blue"></i> User Group Permissions
                            </a>
                        </li>
                        <?php }?>
                        <li class="nav-item ">
                            <a class="nav-link " href="{{ route('logout') }}">
                                <i class="fa fa-key text-blue"></i> Logout
                            </a>
                        </li>
                    </ul>
              </div>
        </div>
      </div>
    </div>
  </nav>