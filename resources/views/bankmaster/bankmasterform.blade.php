
<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Bank Master Management">
  <meta name="author" content="">
  <title>Bank Master Management</title>
  <!-- Favicon -->
  <link rel="icon" href="../assets/img/brand/favicon.png" type="image/png">
  <!-- Fonts -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700">
  <!-- Icons -->
  <link rel="stylesheet" href="../assets/vendor/nucleo/css/nucleo.css" type="text/css">
  <link rel="stylesheet" href="../assets/vendor/@fortawesome/fontawesome-free/css/all.min.css" type="text/css">
  <link rel="stylesheet" href="../assets/vendor/datatables.net-bs4/css/dataTables.bootstrap4.min.css" type="text/css">
  <link rel="stylesheet" href="../assets/vendor/select2/dist/css/select2.min.css" type="text/css">
  <link rel="stylesheet" href="../assets/vendor/sweetalert2/dist/sweetalert2.min.css" type="text/css">
  <!-- Argon CSS -->
  <link rel="stylesheet" href="../assets/css/argon.css?v=1.2.0" type="text/css">
  <link rel="stylesheet" href="../css/tms.css" type="text/css">
</head>

<body>
  <!-- Sidenav -->
  @include('layouts.navbars.sidebar') 
  <!-- Main content -->
  <div class="main-content" id="panel">
    <!-- Topnav -->
    @include('layouts.navbars.topheader') 
    <!-- Header -->
    <!-- Header -->
    <div class="header bg-primary pb-6">
      <div class="container-fluid">
        <div class="header-body">
          <div class="row align-items-center py-4">
            <div class="col-lg-6 col-7">
              <!-- <h6 class="h2 text-white d-inline-block mb-0">TMS</h6> -->
              <nav aria-label="breadcrumb" class="d-none d-md-inline-block ">
                <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                  <li class="breadcrumb-item"><a href="dashboard"><i class="fas fa-home"></i></a></li>
                  <li class="breadcrumb-item"><a href="/bankmaster/listing">Bank Master Listing</a></li>
                </ol>
              </nav>
            </div>

            <div class="col-lg-6 col-5 text-right">
             
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--6">
      <div class="row">
        <div class="col">



        <div class="card">

            <div class="card-body">
              
                  <form id = 'bankform_id' >
                      <h6 class="heading-small text-muted mb-4">Bank Master Information</h6>
                      <div class="pl-lg-4">
                        <div class="row">
                          <div class="col-lg-6">
                            <div class="form-group">
                              <label class="form-control-label" for="bankmaster_bank_id">Bank {!!getMandatory()!!}</label> 
                              <select class = 'form-control select2' name = 'bankmaster_bank_id'>
                                <option value = ''>Select One</option>
                                <?php
                                $banklist = "";
                                foreach($data['bank_list'] as $c){
                                    if($c->bank_id == $data['bankmaster']->bankmaster_bank_id){
                                      $banklist .= "<option value = '{$c->bank_id}' SELECTED>{$c->bank_name}</option>";  
                                    }else{
                                      $banklist .= "<option value = '{$c->bank_id}'>{$c->bank_name}</option>";  
                                    }
                                    
                                }
                                echo $banklist;
                                ?>
                              </select>
                            </div>
                          </div>
                          <div class="col-lg-6 hide">
                            <div class="form-group">
                              <label class="form-control-label" for="bankmaster_name">Name {!!getMandatory()!!}</label> 
                              <input type="text" id="bankmaster_name" name = "bankmaster_name" class="form-control" placeholder="Name" value="{{$data['bankmaster']->bankmaster_name}}">
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-lg-6">
                            <div class="form-group">
                              <label class="form-control-label" for="bankmaster_account_holder">Account Holder {!!getMandatory()!!}</label> 
                              <input type="text" id="bankmaster_account_holder" name = "bankmaster_account_holder" class="form-control" placeholder="Account Holder" value="{{$data['bankmaster']->bankmaster_account_holder}}">
                            </div>
                          </div>
                          <div class="col-lg-6">
                            <div class="form-group">
                              <label class="form-control-label" for="bankmaster_account_number">Account Number {!!getMandatory()!!}</label> 
                              <input type="text" id="bankmaster_account_number" name = "bankmaster_account_number" class="form-control" placeholder="Account Number" value="{{$data['bankmaster']->bankmaster_account_number}}">
                            </div>
                          </div>
                        </div>
                        {{-- <div class="row">
                          <div class="col-lg-6">
                            <div class="form-group">
                              <label class="form-control-label" for="bankmaster_seq">Seq No. </label> 
                              <input type="text" id="bankmaster_seq" name = "bankmaster_seq" class="form-control" placeholder="Seq No." value="{{$data['bankmaster']->bankmaster_seq}}">
                            </div>
                          </div>
                        </div> --}}
                      <div class="row">
                        <div class="col-lg-6">
                          <div class="form-group">
                              <label class="form-control-label" for="bankmaster_status">Status</label>
                              <select name="bankmaster_status" class="form-control select2" id="bankmaster_status" class="bankmaster_status">
                                  <option value="1"
                                      {{ $data['bankmaster']->bankmaster_status == 1 ? 'selected' : '' }}>
                                      Active
                                  </option>
                                  <option value="2"
                                      {{ $data['bankmaster']->bankmaster_status == 2 ? 'selected' : '' }}>
                                      In-active
                                  </option>
                              </select>
                          </div>
                      </div>
                      </div>
                        
                        @csrf
                        <input type = "hidden" name = 'bankmaster_id' value = "{{Crypt::encryptString($data['bankmaster']->bankmaster_id)}}"/>
                        <button class="btn btn-primary" type="submit" id = 'submit_btn'  >Submit</button>
                      </div>

                  </form>


              
            </div>
          </div>
        </div>

      </div>

      <!-- Footer -->
      @include('pages.footer') 
    </div>
  </div>


  <!-- Argon Scripts -->
  <!-- Core -->
  <script src="../assets/vendor/jquery/dist/jquery.min.js"></script>
  <script src="../assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
  <script src="../assets/vendor/js-cookie/js.cookie.js"></script>
  <script src="../assets/vendor/jquery.scrollbar/jquery.scrollbar.min.js"></script>
  <script src="../assets/vendor/jquery-scroll-lock/dist/jquery-scrollLock.min.js"></script>
  <script src="../assets/vendor/datatables.net/js/jquery.dataTables.min.js"></script>
  <script src="../assets/vendor/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
  <script src="../assets/vendor/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
  <script src="../assets/vendor/select2/dist/js/select2.min.js"></script>
  <script src="../assets/vendor/sweetalert2/dist/sweetalert2.min.js"></script>
  <script src="../js/tmsjs/jqueryvalidation/jquery.validate.1.8.js"></script>
  <script src="../js/tmsjs/loadingoverlay/loadingoverlay.min.js"></script>
  <!-- Argon JS -->
  <script src="../assets/js/argon.js?v=1.2.0"></script>

  <script>
    $(document).ready( function () {
      $('.select2').select2();

      $('#bankmaster_account_number').keyup(function(){
        $('#bankmaster_account_number').val($('#bankmaster_account_number').val().replace(/\s/g, ''))

      });

      $("#bankform_id").validate({
              rules: 
              {
                  
                  bankmaster_bank_id:
                  {
                      required: true
                  },
                  bankmaster_account_holder:
                  {
                      required: true
                  },
                  bankmaster_account_number:
                  {
                      required: true
                  },
              },
            showErrors: function(errorMap, errorList) {
                this.defaultShowErrors();

             },
             submitHandler: function (form) {
              //  console.log(form)
              //  alert('123');return false;
             
                  $.ajax({
                      url:'/bankmaster/create',
                      type:'POST',
                      data:$('#bankform_id').serialize(),
                      cache:false,
                      beforeSend: function() {
                          $('#submit_btn').text("loading...");
                          $('#submit_btn').attr("disabled",true);
                          $.LoadingOverlay("show");
                      },
                      error: function(xhr) {
                          Swal({
                              type: 'error',
                              title: "Something went wrong.",
                              showConfirmButton: false,
                              allowOutsideClick:false,
                              timer: 1500
                          })
                          $('#submit_btn').attr("disabled",false);
                          $('#submit_btn').text("Submit");
                          $.LoadingOverlay("hide");
                      },
                      success:function(jsonObj){
                          $('#submit_btn').attr("disabled",false);
                          $('#submit_btn').text("Submit");
                          $.LoadingOverlay("hide");
                          if(jsonObj.status == 1){
                              if(jsonObj.msg != ""){
                                  Swal({
                                      type: 'success',
                                      title: jsonObj.msg,
                                      showConfirmButton: false,
                                      allowOutsideClick:false,
                                      timer: 1500
                                  }).then(function() {
                                      window.location.href = '/bankmaster/listing'
                                  });
                              }

                          }else{
                              Swal({
                                  type: 'error',
                                  title: jsonObj.msg,
                                  showConfirmButton: false,
                                  allowOutsideClick:false,
                                  timer: 1500
                              })
                          }
                      }
                  });
              },
      });
      

    });

  </script>
</body>

</html>