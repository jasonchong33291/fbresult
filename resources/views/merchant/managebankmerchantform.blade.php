
<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Merchant Bank Management">
  <meta name="author" content="">
  <title>Merchant Bank Management</title>
  <!-- Favicon -->
  <link rel="icon" href="../assets/img/brand/favicon.png" type="image/png">
  <!-- Fonts -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700">
  <!-- Icons -->
  <link rel="stylesheet" href="../assets/vendor/nucleo/css/nucleo.css" type="text/css">
  <link rel="stylesheet" href="../assets/vendor/@fortawesome/fontawesome-free/css/all.min.css" type="text/css">
  <link rel="stylesheet" href="../assets/vendor/datatables.net-bs4/css/dataTables.bootstrap4.min.css" type="text/css">
  <link rel="stylesheet" href="../assets/vendor/select2/dist/css/select2.min.css" type="text/css">
  <link rel="stylesheet" href="../assets/vendor/sweetalert2/dist/sweetalert2.min.css" type="text/css">
  <!-- Argon CSS -->
  <link rel="stylesheet" href="../assets/css/argon.css?v=1.2.0" type="text/css">
  <link rel="stylesheet" href="../css/tms.css" type="text/css">
</head>

<body>
  <!-- Sidenav -->
  @include('layouts.navbars.sidebar') 
  <!-- Main content -->
  <div class="main-content" id="panel">
    <!-- Topnav -->
    @include('layouts.navbars.topheader') 
    <!-- Header -->
    <!-- Header -->
    <div class="header bg-primary pb-6">
      <div class="container-fluid">
        <div class="header-body">
          <div class="row align-items-center py-4">
            <div class="col-lg-6 col-7">
              <!-- <h6 class="h2 text-white d-inline-block mb-0">TMS</h6> -->
              <nav aria-label="breadcrumb" class="d-none d-md-inline-block ">
                <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                  <li class="breadcrumb-item"><a href="dashboard"><i class="fas fa-home"></i></a></li>
                  <li class="breadcrumb-item"><a href="/merchant/listing">Merchant Listing </a></li>
                </ol>
              </nav>
            </div>

            <div class="col-lg-6 col-5 text-right">
             
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--6">
      <div class="row">
        <div class="col">



        <div class="card">

            <div class="card-body">

                  <form id = 'merchantform_id' >
                      <h6 class="heading-small text-muted mb-4">Merchant Bank Management</h6>
                      <div class="pl-lg-4">
                        <?php 
                        if((Auth::user()->user_group_id == 1) || (Auth::user()->user_group_id == 2)){  
                        ?>
                        <div class="row">
                          <div class="col-lg-6">
                            <div class="form-group">
                                <label class="form-control-label" for="merchant_isownbank">Merchant Own Bank? {!!getMandatory()!!}</label>
                                <select name="merchant_isownbank" class="form-control select2" id="merchant_isownbank" >
                                    <option value=""> Please select one</option>
                                    <option value="1" <?php if($data['merchant']->merchant_isownbank == '1'){ echo " SELECTED";}?>> Yes</option>
                                    <option value="0" <?php if($data['merchant']->merchant_isownbank == '0'){ echo " SELECTED";}?>> No</option>
                                </select>
                            </div>
                          </div>
                        </div>
                        <?php }?>
                      <div id = 'master_bank_div' class = "<?php if($data['merchant']->merchant_isownbank == '1'){ echo " hide";}?>">  
                        <hr>
                        <div style = 'display:flex;justify-content: space-between;'>
                            <div><h4>Manage Master Bank</h4></div>
                        </div>
                        <br>
                        <table class = "table">
                          <tr>
                            <th style = 'width:200px'>Bank</th>
                            <th style = 'width:200px'>Account Holder</th>
                            <th style = 'width:200px'>Account Number</th>
                            <th style = 'width:200px'>Active?</th>
                          </tr>
                          <?php
                          $merchantbankmaster = collect($data['merchantbankmaster']);
                          
                          foreach($data['bankmaster_list'] as $t){
                            $selected = "";
                           
                            //dd($merchantbankmaster->whereIn('merchantbankmaster_bankmaster_id',[$t->bankmaster_id])->count());
                              if($merchantbankmaster->whereIn('merchantbankmaster_bankmaster_id',[$t->bankmaster_id])->count() > 0){
                                $selected = " CHECKED";
                              }
                          ?>
                              <tr>
                                  <td>{{$t->bank_name}}</td>
                                  <td>{{$t->bankmaster_account_holder}}</td>
                                  <td>{{$t->bankmaster_account_number}}</td>
                                  <td><input type = 'checkbox' name = 'bankmaster_active_list[]' value = '{{$t->bankmaster_id}}' {{$selected}}/></td>
                              </tr>
                          <?php
                          }
                          ?>
                        </table>
                      </div>
                      <div id = 'own_bank_div' class = "<?php if($data['merchant']->merchant_isownbank != '1'){ echo " hide";}?>">  
                        <hr>
                        <div style = 'display:flex;justify-content: space-between;'>
                            <div><h4>{{$data['merchant']->merchant_code}} - {{$data['merchant']->merchant_name}}</h4></div>
                            <div><button type = 'button' id = 'add_new_bank' class="btn btn-primary">Add New Line</button></div>
                        </div>
                        <br>

                        <table class = "table">
                          <tr>
                            <th style = 'width:200px'>Bank</th>
                            <th style = 'width:200px'>Account Holder</th>
                            <th style = 'width:200px'>Account Number</th>
                            <th style = 'width:200px'>Account Status</th>
                            <th style = 'width:100px'></th>
                          </tr>
                          <?php
                          foreach($data['merchantbank_list'] as $t){
                            
                            $banklist = "<option value = ''>Select One</option>";
                            foreach($data['bank_list'] as $c){
                                if($c->bank_id == $t->merchantbank_bank_id){
                                  $banklist .= "<option value = '{$c->bank_id}' SELECTED>{$c->bank_name}</option>";  
                                }else{
                                  $banklist .= "<option value = '{$c->bank_id}'>{$c->bank_name}</option>";  
                                }
                                
                            }
                            $active_status = "";
                            if($t->merchantbank_status == '1'){
                              $active_status = "<option value = '1' SELECTED>Active</option><option value = '2'>Inactive</option>";
                            }else{
                              $active_status = "<option value = '1' >Active</option><option value = '2' SELECTED>Inactive</option>";
                            }
                            

                            $html = "<tr>";
                            $html .= "<td><select class = 'form-control select2' name = 'merchantbank_bank_id[]'>$banklist</select></td>";
                            $html .= "<td><input type = 'type' class = 'form-control' name = 'merchantbank_account_holder[]' placeholder = 'Account Holder' value = '".$t->merchantbank_account_holder."'/></td>";
                            $html .= "<td><input type = 'type' class = 'form-control merchantbank_account_number' name = 'merchantbank_account_number[]' placeholder = 'Account Number' value = '".$t->merchantbank_account_number."'/></td>";
                            $html .= "<td><select class = 'form-control select2' name = 'merchantbank_status[]'>$active_status</select></td>";
                            $html .= "<td><input type = 'hidden' name = 'merchantbank_id[]' value = '".$t->merchantbank_id."'/><a href = 'javascript:void(0)' title = 'Delete' class = 'btn btn-danger deletebankline' merchantbank_id = '{$t->merchantbank_id}'><i class = 'fa fa-trash' style = 'font-size:20px;'></i></a></td>";
                            $html .= "</tr>";
                            echo $html;
                          }
                          ?>
                          <tr id = 'bank_line_tr'>
                          </tr>
                        </table>
                      </div>

                        <br>
                        <br>
                        @csrf
                        <input type = "hidden" name = 'merchant_id' value = "{{ Crypt::encryptString($data['merchant']->merchant_id) }}"/>
                        <button class="btn btn-primary" type="submit" id = 'submit_btn'  >Submit</button>
                      </div>

                  </form>


              
            </div>
          </div>
        </div>

      </div>

      <!-- Footer -->
      @include('pages.footer') 
    </div>
  </div>


  <!-- Argon Scripts -->
  <!-- Core -->
  <script src="../assets/vendor/jquery/dist/jquery.min.js"></script>
  <script src="../assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
  <script src="../assets/vendor/js-cookie/js.cookie.js"></script>
  <script src="../assets/vendor/jquery.scrollbar/jquery.scrollbar.min.js"></script>
  <script src="../assets/vendor/jquery-scroll-lock/dist/jquery-scrollLock.min.js"></script>
  <script src="../assets/vendor/datatables.net/js/jquery.dataTables.min.js"></script>
  <script src="../assets/vendor/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
  <script src="../assets/vendor/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
  <script src="../assets/vendor/select2/dist/js/select2.min.js"></script>
  <script src="../assets/vendor/sweetalert2/dist/sweetalert2.min.js"></script>
  <script src="../js/tmsjs/jqueryvalidation/jquery.validate.1.8.js"></script>
  <script src="../js/tmsjs/loadingoverlay/loadingoverlay.min.js"></script>
  <!-- Argon JS -->
  <script src="../assets/js/argon.js?v=1.2.0"></script>

  <script>
    $(document).ready( function () {
      $('.select2').select2();
      $(document).on("keyup", '.merchantbank_account_number', function() {
        $(this).val($(this).val().replace(/\s/g, ''))

      });
      $(document).on("change", '#merchant_isownbank', function() {
          if($(this).val() == "1"){
              $('#own_bank_div').removeClass('hide');
              $('#master_bank_div').addClass('hide');
          }else{
              $('#own_bank_div').addClass('hide');
              $('#master_bank_div').removeClass('hide');
          }
      });

      $(document).on("click", '#add_new_bank', function() {
        addnewbankline();
      });
      $(document).on("click", '.deletebankline', function() {
          var obj = $(this);
          
          $.ajax({
            url:'/merchant/merchantbank/deletebankline',
            type:'POST',
            data:'_token={{csrf_token()}}&merchantbank_id='+obj.attr('merchantbank_id')+"&merchant_id={{ Crypt::encryptString($data['merchant']->merchant_id) }}",
            cache:false,
            beforeSend: function() {
                $.LoadingOverlay("show");
            },
            error: function(xhr) {
                Swal({
                    type: 'error',
                    title: "Something went wrong.",
                    showConfirmButton: false,
                    allowOutsideClick:false,
                    timer: 1500
                })
                $.LoadingOverlay("hide");
            },
            success:function(jsonObj){
                $.LoadingOverlay("hide");
                if(jsonObj.status == 1){
                    if(jsonObj.msg != ""){
                          Swal({
                              type: 'success',
                              title: jsonObj.msg,
                              showConfirmButton: false,
                              allowOutsideClick:false,
                              timer: 1500
                          }).then(function() {
                            obj.parent().parent().remove();
                          });
                    }

                }else{
                    Swal({
                        type: 'error',
                        title: jsonObj.msg,
                        showConfirmButton: false,
                        allowOutsideClick:false,
                        timer: 1500
                    })
                }
                
            }
          });

      });
      
      $("#merchantform_id").validate({
                rules: 
                {
                  merchant_isownbank:
                    {
                        required: true
                    },
                },
              showErrors: function(errorMap, errorList) {
                  this.defaultShowErrors();

              },
              submitHandler: function (form) {
          
                    $.ajax({
                        url:'/merchant/merchantbank/create',
                        type:'POST',
                        data:$('#merchantform_id').serialize(),
                        cache:false,
                        beforeSend: function() {
                            $('#submit_btn').text("loading...");
                            $('#submit_btn').attr("disabled",true);
                            $.LoadingOverlay("show");
                        },
                        error: function(xhr) {
                            Swal({
                                type: 'error',
                                title: "Something went wrong.",
                                showConfirmButton: false,
                                allowOutsideClick:false,
                                timer: 1500
                            })
                            $('#submit_btn').attr("disabled",false);
                            $('#submit_btn').text("Submit");
                            $.LoadingOverlay("hide");
                        },
                        success:function(jsonObj){
                            $('#submit_btn').attr("disabled",false);
                            $('#submit_btn').text("Submit");
                            $.LoadingOverlay("hide");
                            if(jsonObj.status == 1){
                                if(jsonObj.msg != ""){
                                    Swal({
                                        type: 'success',
                                        title: jsonObj.msg,
                                        showConfirmButton: false,
                                        allowOutsideClick:false,
                                        timer: 1500
                                    }).then(function() {
                                        location.reload();
                                    });
                                }

                            }else{
                                Swal({
                                    type: 'error',
                                    title: jsonObj.msg,
                                    showConfirmButton: false,
                                    allowOutsideClick:false,
                                    timer: 1500
                                })
                            }
                        }
                    });
                },
        });


    });
  function addnewbankline(){
    $.ajax({
          url:'/merchant/merchantbank/addnewbankline',
          type:'POST',
          data:$('#merchantform_id').serialize(),
          cache:false,
          beforeSend: function() {
              $('#add_new_bank').text("loading...");
              $('#add_new_bank').attr("disabled",true);
              $.LoadingOverlay("show");
          },
          error: function(xhr) {
              Swal({
                  type: 'error',
                  title: "Something went wrong.",
                  showConfirmButton: false,
                  allowOutsideClick:false,
                  timer: 1500
              })
              $('#add_new_bank').attr("disabled",false);
              $('#add_new_bank').text("Add New Line");
              $.LoadingOverlay("hide");
          },
          success:function(jsonObj){
              $('#add_new_bank').attr("disabled",false);
              $('#add_new_bank').text("Add New Line");
              $.LoadingOverlay("hide");
              $('#bank_line_tr').before(jsonObj.html);
              $('.select2').select2();
          }
      });
  }
  </script>
</body>

</html>