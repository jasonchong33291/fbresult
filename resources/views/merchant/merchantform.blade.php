
<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Merchant Management">
  <meta name="author" content="">
  <title>Merchant Management</title>
  <!-- Favicon -->
  <link rel="icon" href="../assets/img/brand/favicon.png" type="image/png">
  <!-- Fonts -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700">
  <!-- Icons -->
  <link rel="stylesheet" href="../assets/vendor/nucleo/css/nucleo.css" type="text/css">
  <link rel="stylesheet" href="../assets/vendor/@fortawesome/fontawesome-free/css/all.min.css" type="text/css">
  <link rel="stylesheet" href="../assets/vendor/datatables.net-bs4/css/dataTables.bootstrap4.min.css" type="text/css">
  <link rel="stylesheet" href="../assets/vendor/select2/dist/css/select2.min.css" type="text/css">
  <link rel="stylesheet" href="../assets/vendor/sweetalert2/dist/sweetalert2.min.css" type="text/css">
  <!-- Argon CSS -->
  <link rel="stylesheet" href="../assets/css/argon.css?v=1.2.0" type="text/css">
  <link rel="stylesheet" href="../css/tms.css" type="text/css">
</head>

<body>
  <!-- Sidenav -->
  @include('layouts.navbars.sidebar') 
  <!-- Main content -->
  <div class="main-content" id="panel">
    <!-- Topnav -->
    @include('layouts.navbars.topheader') 
    <!-- Header -->
    <!-- Header -->
    <div class="header bg-primary pb-6">
      <div class="container-fluid">
        <div class="header-body">
          <div class="row align-items-center py-4">
            <div class="col-lg-6 col-7">
              <!-- <h6 class="h2 text-white d-inline-block mb-0">TMS</h6> -->
              <nav aria-label="breadcrumb" class="d-none d-md-inline-block ">
                <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                  <li class="breadcrumb-item"><a href="dashboard"><i class="fas fa-home"></i></a></li>
                  <li class="breadcrumb-item"><a href="/merchant/listing">Merchant Listing</a></li>
                </ol>
              </nav>
            </div>

            <div class="col-lg-6 col-5 text-right">
             
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--6">
      <div class="row">
        <div class="col">



        <div class="card">

            <div class="card-body">

                  <form id = 'merchantform_id' >
                      <h6 class="heading-small text-muted mb-4">Merchant Information</h6>
                      <div class="pl-lg-4">
                        <div class="row">
                          <div class="col-lg-6">
                            <div class="form-group">
                              <label class="form-control-label" for="merchant_code">Merchant Code </label> 
                              <input type="text" READONLY id="merchant_code" name = "merchant_code" class="form-control" placeholder="System Generate" value="{{$data['merchant']->merchant_code}}">
                            </div>
                          </div>
                          <?php 
                           if((Auth::user()->user_group_id == 1) || (Auth::user()->user_group_id == 2) || (Auth::user()->user_group_id == 6)){
                          ?>
                          <div class="col-lg-6">
                            <div class="form-group">
                                <label class="form-control-label" for="merchant_agent_id">Agent {!!getMandatory()!!}</label>
                                <select name="merchant_agent_id" class="form-control select2" id="merchant_agent_id" >
                                    <option value=""> Please select one</option>
                                    <?php
                                    foreach($data['agent_list'] as $a){
                                    ?>
                                      <option value = "<?php echo $a->id;?>" {{ $data['merchant']->merchant_agent_id == $a->id ? 'selected' : '' }}><?php echo $a->user_name;?></option>
                                    <?php
                                    }
                                    ?>
                                </select>
                            </div>
                          </div>
                          <?php }?>
                        </div>
                        <div class="row">
                          <div class="col-lg-6">
                            <div class="form-group">
                              <label class="form-control-label" for="merchant_name">Merchant Name {!!getMandatory()!!}</label> 
                              <input type="text" id="merchant_name" name = "merchant_name" class="form-control" placeholder="Merchant Name" value="{{$data['merchant']->merchant_name}}">
                            </div>
                          </div>
                          <?php 
                           if((Auth::user()->user_group_id == 1) || (Auth::user()->user_group_id == 2) || (Auth::user()->user_group_id == 6)){
                          ?>
                          <div class="col-lg-6">
                            <div class="form-group">
                                <label class="form-control-label" for="merchant_payment_method">Payment Method</label>
                                <select name="merchant_payment_method" class="form-control select2" id="merchant_payment_method" >
                                    <option value="both" <?php if($data['merchant']->merchant_payment_method == 'both'){ echo " SELECTED";}?>> Both P2P & QR</option>
                                    <option value="P2P" <?php if($data['merchant']->merchant_payment_method == 'P2P'){ echo " SELECTED";}?>> P2P</option>
                                    <option value="QR" <?php if($data['merchant']->merchant_payment_method == 'QR'){ echo " SELECTED";}?>> QR</option>
                                </select>
                            </div>
                          </div>
                          <?php }?>
                          {{-- <div class="col-lg-6">
                            <div class="form-group">
                              <label class="form-control-label" for="merchant_contact_number">Contact Number {!!getMandatory()!!}</label> 
                              <input type="text" id="merchant_contact_number" name = "merchant_contact_number" class="form-control" placeholder="Contact Number" value="{{$data['merchant']->merchant_contact_number}}">
                            </div>
                          </div> --}}
                        </div>
                        <div class="row">
                          {{-- <div class="col-lg-6">
                            <div class="form-group">
                              <label class="form-control-label" for="merchant_seq">Seq No. </label> 
                              <input type="number" id="merchant_seq" name = "merchant_seq" class="form-control" placeholder="Seq No." value="{{$data['merchant']->merchant_seq}}">
                            </div>
                          </div> --}}
                          {{-- <div class="col-lg-6">
                            <div class="form-group">
                              <label class="form-control-label" for="merchant_contact_person">Contact Person {!!getMandatory()!!}</label> 
                              <input type="text" id="merchant_contact_person" name = "merchant_contact_person" class="form-control" placeholder="Contact Person" value="{{$data['merchant']->merchant_contact_person}}">
                            </div>
                          </div> --}}
                        </div>
                        <div class="row">
                          <div class="col-lg-6">
                            <div class="form-group">
                                <label class="form-control-label" for="merchant_status">Status</label>
                                <select name="merchant_status" class="form-control select2" id="merchant_status" class="merchant_status">
                                    <option value="1"
                                        {{ $data['merchant']->merchant_status == 1 ? 'selected' : '' }}>
                                        Active (Live)
                                    </option>
                                    <option value="2"
                                        {{ $data['merchant']->merchant_status == 2 ? 'selected' : '' }}>
                                        Active (Staging)
                                    </option>
                                    <option value="3"
                                        {{ $data['merchant']->merchant_status == 3 ? 'selected' : '' }}>
                                        In-active
                                    </option>
                                </select>
                            </div>
                          </div>
                          {{-- <div class="col-lg-6">
                            <div class="form-group">
                              <label class="form-control-label" for="merchant_remark">Remarks</label> 
                               <textarea id="merchant_remark" name = "merchant_remark" class="form-control" placeholder = "Internal Remarks">{{$data['merchant']->merchant_remark}}</textarea>
                            </div>
                          </div> --}}
                        </div>

                        <hr>
                        <h4>Rate Settings</h4>
                        <br>
                        <div class="row">

                            <div class="col-lg-6 <?php if($data['merchant']->merchant_payment_method == 'QR'){ echo ' hide';}?>" id = 'p2p_div'>
                                <div class="form-group">
                                    <label class="form-control-label" for="merchant_p2p_comm">P2P Rate (%)
                                        {!! getMandatory() !!}</label>
                                    <input type="text" id="merchant_p2p_comm" name="merchant_p2p_comm"
                                        class="form-control"  placeholder = '2.5' onkeypress="return isNumberKey(event)"
                                        value="{{ $data['merchant']->merchant_p2p_comm }}">
                                </div>
                            </div>

                            <div class="col-lg-6 <?php if($data['merchant']->merchant_payment_method == 'P2P'){ echo ' hide';}?>" id = 'qr_div'>
                                <div class="form-group">
                                    <label class="form-control-label" for="merchant_qr_comm">QR Rate (%)
                                        {!! getMandatory() !!}</label>
                                    <input type="text" id="merchant_qr_comm" name="merchant_qr_comm" placeholder = '2.5' onkeypress="return isNumberKey(event)"
                                        class="form-control" 
                                        value="{{ $data['merchant']->merchant_qr_comm }}">
                                </div>
                            </div>
                        </div>

                        <hr>
                        <h4>Login Credentails</h4>
                        <br>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="form-control-label" for="user_username">Username
                                        {!! getMandatory() !!}</label>
                                    <input type="text" id="user_username" name="user_username"
                                        class="form-control" placeholder="Username"
                                        value="{{ $data['user']->user_username }}">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="form-control-label" for="user_password">Password
                                        {!! getMandatory() !!}</label>
                                    <input type="password" id="user_password" name="user_password"
                                        class="form-control" placeholder="Password" value="">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="form-control-label" for="user_confirm_password">Confirm
                                        Password {!! getMandatory() !!}</label>
                                    <input type="password" id="user_confirm_password"
                                        name="user_confirm_password" class="form-control"
                                        placeholder="Confirm Password" value="">
                                </div>
                            </div>
                        </div>

                        <hr>
                        <h4>API Setting</h4>
                        <br>
                        <div class="row">
                          <div class="col-lg-6">
                            <div class="form-group">
                              <label class="form-control-label" for="merchant_api_secretkey">API Secret Key <?php if($data['merchant']->merchant_id > 0){?><button class="btn btn-sm btn-warning" type="button" id = 'copy_api_key_btn'  onclick="copyToClipboard('merchant_api_secretkey')">Copy</button><?php }?> </label> 
                              <textarea readonly id="merchant_api_secretkey" name = "merchant_api_secretkey" class="form-control" placeholder = "API Secret Key"><?php if($data['merchant']->merchant_api_secretkey == null){ echo "-- System Generate --";}else{ echo $data['merchant']->merchant_api_secretkey;}?></textarea>
                            </div>
                          </div>
                          <div class="col-lg-6">
                            <div class="form-group">
                              <label class="form-control-label" for="merchant_api_endpoint">API EndPoint</label> 
                              <textarea id="merchant_api_endpoint" name = "merchant_api_endpoint" class="form-control" placeholder = "API EndPoint">{{$data['merchant']->merchant_api_endpoint}}</textarea>
                            </div>
                          </div>
                          <div class="col-lg-6">
                            <div class="form-group">
                              <label class="form-control-label" for="merchant_api_website_url">Website URL</label> 
                              <textarea id="merchant_api_website_url" name = "merchant_api_website_url" class="form-control" placeholder = "Website URL">{{$data['merchant']->merchant_api_website_url}}</textarea>
                            </div>
                          </div>
                          <div class="col-lg-6">
                            <div class="form-group">
                              <label class="form-control-label" for="merchant_api_redirect_url">Redirect URL</label> 
                              <textarea id="merchant_api_redirect_url" name = "merchant_api_redirect_url" class="form-control" placeholder = "Redirect URL">{{$data['merchant']->merchant_api_redirect_url}}</textarea>
                            </div>
                          </div>
                          <div class="col-lg-6">
                            <div class="form-group">
                              <label class="form-control-label" for="merchant_api_whitelist">Whitelist IP Address</label> 
                              <textarea id="merchant_api_whitelist" name = "merchant_api_whitelist" class="form-control" placeholder = "Example:xxx.xxx.xxx.xx,xxx.xxx.xxx.xx">{{$data['merchant']->merchant_api_whitelist}}</textarea>
                            </div>
                          </div>
                        </div>
                        @csrf
                        <input type = "hidden" name = 'merchant_id' id = 'merchant_id' value = "{{ Crypt::encryptString($data['merchant']->merchant_id) }}"/>
                        <input type = "hidden" name = 'user_id' id = 'user_id' value = "{{ Crypt::encryptString($data['user']->id) }}"/>
                        <button class="btn btn-primary" type="submit" id = 'submit_btn'  >Submit</button>
                      </div>

                  </form>


              
            </div>
          </div>
        </div>

      </div>

      <!-- Footer -->
      @include('pages.footer') 
    </div>
  </div>


  <!-- Argon Scripts -->
  <!-- Core -->
  <script src="../assets/vendor/jquery/dist/jquery.min.js"></script>
  <script src="../assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
  <script src="../assets/vendor/js-cookie/js.cookie.js"></script>
  <script src="../assets/vendor/jquery.scrollbar/jquery.scrollbar.min.js"></script>
  <script src="../assets/vendor/jquery-scroll-lock/dist/jquery-scrollLock.min.js"></script>
  <script src="../assets/vendor/datatables.net/js/jquery.dataTables.min.js"></script>
  <script src="../assets/vendor/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
  <script src="../assets/vendor/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
  <script src="../assets/vendor/select2/dist/js/select2.min.js"></script>
  <script src="../assets/vendor/sweetalert2/dist/sweetalert2.min.js"></script>
  <script src="../js/tmsjs/jqueryvalidation/jquery.validate.1.8.js"></script>
  <script src="../js/tmsjs/loadingoverlay/loadingoverlay.min.js"></script>
  <!-- Argon JS -->
  <script src="../assets/js/argon.js?v=1.2.0"></script>

  <script>
    $(document).ready( function () {
      $('.select2').select2();
      $('#merchant_payment_method').change(function(){

          if($(this).val() == 'P2P'){
            $('#p2p_div').removeClass('hide');
            $('#qr_div').addClass('hide');
          }else if($(this).val() == 'QR'){
            $('#p2p_div').addClass('hide');
            $('#qr_div').removeClass('hide');
          }else if($(this).val() == 'both'){
            $('#p2p_div').removeClass('hide');
            $('#qr_div').removeClass('hide');
          }
      });

      $("#merchantform_id").validate({
              rules: 
              {
                  merchant_contact_number:
                  {
                      required: true
                  },
                  merchant_contact_name:
                  {
                      required: true
                  },
                  merchant_agent_id:
                  {
                      required: true
                  },
                  merchant_name:
                  {
                      required: true
                  },
                  merchant_api_endpoint: {
                        required: true,
                        // remote: {
                        //     url: "/merchant/validateapiendpoint/",
                        //     type: "GET",
                        //     data: {
                        //       merchant_api_endpoint: function() {
                        //             return $("#merchant_api_endpoint").val();
                        //         },
                        //         merchant_id: function() {
                        //             return $("#merchant_id").val();
                        //         },
                        //         _token: function() {
                        //             return "{{ csrf_token() }}";
                        //         },
                        //     }
                        // }
                    },
                    user_username: {
                        required: true,
                        remote: {
                            url: "/user/validateusername/",
                            type: "GET",
                            data: {
                                user_username: function() {
                                    return $("#user_username").val();
                                },
                                id: function() {
                                    return $("#user_id").val();
                                },
                                _token: function() {
                                    return "{{ csrf_token() }}";
                                },
                            }
                        }
                    },
                    user_password: {
                        required: <?php if($data['password_required']){ echo "true";}else{ echo "false";}?>,
                        minlength: 5
                    },
                    user_confirm_password: {
                        required: <?php if($data['password_required']){ echo "true";}else{ echo "false";}?>,
                        minlength: 5,
                        equalTo: "#user_password"
                    },
              },
            showErrors: function(errorMap, errorList) {
                this.defaultShowErrors();

             },
             submitHandler: function (form) {
         
                  $.ajax({
                      url:'/merchant/create',
                      type:'POST',
                      data:$('#merchantform_id').serialize(),
                      cache:false,
                      beforeSend: function() {
                          $('#submit_btn').text("loading...");
                          $('#submit_btn').attr("disabled",true);
                          $.LoadingOverlay("show");
                      },
                      error: function(xhr) {
                          Swal({
                              type: 'error',
                              title: "Something went wrong.",
                              showConfirmButton: false,
                              allowOutsideClick:false,
                              timer: 1500
                          })
                          $('#submit_btn').attr("disabled",false);
                          $('#submit_btn').text("Submit");
                          $.LoadingOverlay("hide");
                      },
                      success:function(jsonObj){
                          $('#submit_btn').attr("disabled",false);
                          $('#submit_btn').text("Submit");
                          $.LoadingOverlay("hide");
                          if(jsonObj.status == 1){
                              if(jsonObj.msg != ""){
                                  Swal({
                                      type: 'success',
                                      title: jsonObj.msg,
                                      showConfirmButton: false,
                                      allowOutsideClick:false,
                                      timer: 1500
                                  }).then(function() {
                                      window.location.href = '/merchant/createForm?merchant_id='+jsonObj.merchant_id;
                                  });
                              }

                          }else{
                              Swal({
                                  type: 'error',
                                  title: jsonObj.msg,
                                  showConfirmButton: false,
                                  allowOutsideClick:false,
                                  timer: 1500
                              })
                          }
                      }
                  });
              },
      });
      

    });
    function copyToClipboard(id) {
        document.getElementById(id).select();
        document.execCommand('copy');
        Swal({
            type: 'success',
            title: 'API Secret Key Copied',
            showConfirmButton: false,
            allowOutsideClick:false,
            timer: 1500
        }).then(function() {
     
        });
    }
    function isNumberKey(evt){
          var charCode = (evt.which) ? evt.which : evt.keyCode;
          if (charCode != 46 && charCode > 31 
            && (charCode < 48 || charCode > 57))
             return false;

          return true;
    }
  </script>
</body>

</html>