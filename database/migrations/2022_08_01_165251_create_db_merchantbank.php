<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDbMerchantbank extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('db_merchantbank', function (Blueprint $table) {

            $table->id('merchantbank_id')->index('merchantbank_id');
            $table->integer('merchantbank_bank_id')->default(0)->nullable()->index('merchantbank_bank_id');
            $table->integer('merchantbank_merchant_id')->default(0)->nullable()->index('merchantbank_merchant_id');
            $table->string('merchantbank_account_holder')->default(NULL)->nullable();
            $table->string('merchantbank_account_number')->default(NULL)->nullable();
            $table->integer('merchantbank_status')->default(0)->nullable();
            $table->timestamps();
            $table->integer('updateBy')->default(0);
            $table->integer('insertBy')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('db_Merchantbank');
    }
}
