<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDbSettlement extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('db_settlement', function (Blueprint $table) {
            $table->id('settlement_id')->index('settlement_id');
            $table->integer('settlement_merchant_id')->default(0)->nullable()->index('settlement_merchant_id');
            $table->integer('settlement_merchantsettlementbank_id')->default(0)->nullable()->index('settlement_merchantsettlementbank_id');
            $table->string('settlement_no')->default(NULL)->nullable();
            $table->float('settlement_from_amount',15)->default(0)->nullable();
            $table->float('settlement_to_amount',15)->default(0)->nullable();
            $table->longText('settlement_remarks')->default(null)->nullable();
            $table->timestamps();
            $table->integer('updateBy')->default(0);
            $table->integer('insertBy')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('db_settlement');
    }
}
