<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDbBank extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('db_bank', function (Blueprint $table) {
            $table->id('bank_id')->index('bank_id');
            $table->string('bank_code')->default(NULL)->nullable();
            $table->string('bank_name')->default(NULL)->nullable();
            $table->integer('bank_seq')->default(0)->nullable();
            $table->integer('bank_status')->default(0)->nullable();
            $table->timestamps();
            $table->integer('updateBy')->default(0);
            $table->integer('insertBy')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('db_bank');
    }
}
