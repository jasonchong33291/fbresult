<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldUsers1aug extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            
            $table->integer('user_parent')->default(0)->nullable()->after('user_group_id');
            $table->string('user_code')->default(NULL)->nullable()->after('user_parent');
            $table->float('user_p2p_comm',15)->default(NULL)->nullable()->after('user_code');
            $table->float('user_qr_comm',15)->default(NULL)->nullable()->after('user_p2p_comm');
            $table->string('user_email')->default(NULL)->nullable()->change();
        });

        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
