<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDbMerchantbankmaster extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('db_merchantbankmaster', function (Blueprint $table) {
            $table->id('merchantbankmaster_id')->index('merchantbankmaster_id');
            $table->integer('merchantbankmaster_merchant_id')->default(0)->nullable()->index('merchantbankmaster_merchant_id');
            $table->string('merchantbankmaster_bankmaster_id')->default(NULL)->nullable();
            $table->integer('merchantbankmaster_status')->default(0)->nullable();
            $table->timestamps();
            $table->integer('updateBy')->default(0);
            $table->integer('insertBy')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('db_merchantbankmaster');
    }
}
