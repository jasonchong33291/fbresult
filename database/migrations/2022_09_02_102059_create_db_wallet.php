<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDbWallet extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('db_wallet', function (Blueprint $table) {
            $table->id('wallet_id')->index('wallet_id');
            $table->string('wallet_code')->default(NULL)->nullable();
            $table->string('wallet_name')->default(NULL)->nullable();
            $table->integer('wallet_seq')->default(0)->nullable();
            $table->integer('wallet_status')->default(0)->nullable();
            $table->timestamps();
            $table->integer('updateBy')->default(0);
            $table->integer('insertBy')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('db_wallet');
    }
}
