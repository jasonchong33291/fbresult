<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDbMerchantsettlementbank extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('db_merchantsettlementbank', function (Blueprint $table) {
            $table->id('merchantsettlementbank_id')->index('merchantsettlementbank_id');
            $table->integer('merchantsettlementbank_bank_id')->default(0)->nullable()->index('merchantsettlementbank_bank_id');
            $table->integer('merchantsettlementbank_merchant_id')->default(0)->nullable()->index('merchantsettlementbank_merchant_id');
            $table->string('merchantsettlementbank_account_holder')->default(NULL)->nullable();
            $table->string('merchantsettlementbank_account_number')->default(NULL)->nullable();
            $table->integer('merchantsettlementbank_status')->default(0)->nullable();
            $table->timestamps();
            $table->integer('updateBy')->default(0);
            $table->integer('insertBy')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('db_merchantsettlementbank');
    }
}
