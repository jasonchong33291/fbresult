<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDbBankmaster extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('db_bankmaster', function (Blueprint $table) {
            $table->id('bankmaster_id')->index('bankmaster_id');
            $table->string('bankmaster_bank_id')->default(NULL)->nullable()->index('bankmaster_bank_id');
            $table->string('bankmaster_name')->default(NULL)->nullable();
            $table->string('bankmaster_account_holder')->default(NULL)->nullable();
            $table->string('bankmaster_account_number')->default(NULL)->nullable();
            $table->string('bankmaster_seq')->default(NULL)->nullable();
            $table->integer('bankmaster_status')->default(0)->nullable();
            $table->timestamps();
            $table->integer('updateBy')->default(0);
            $table->integer('insertBy')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('db_bankmaster');
    }
}
