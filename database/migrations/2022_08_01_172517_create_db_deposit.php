<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDbDeposit extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('db_deposit', function (Blueprint $table) {
            $table->id('deposit_id')->index('deposit_id');
            $table->integer('deposit_merchant_id')->default(0)->nullable()->index('deposit_merchant_id');
            $table->string('deposit_mode')->default(NULL)->nullable();
            $table->string('deposit_banktype')->default(NULL)->nullable();
            $table->integer('deposit_isownbank')->default(0)->nullable()->index('deposit_isownbank');
            $table->integer('deposit_bankfrom_id')->default(NULL)->nullable()->index('deposit_bankfrom_id');
            $table->integer('deposit_bankto_id')->default(NULL)->nullable()->index('deposit_bankto_id');
            $table->string('deposit_bankto_account_holder')->default(NULL)->nullable();
            $table->string('deposit_bankto_account_number')->default(NULL)->nullable();
            $table->longText('deposit_merchant_refno1')->default(NULL)->nullable();
            $table->longText('deposit_merchant_refno2')->default(NULL)->nullable();
            $table->string('deposit_username')->default(NULL)->nullable();
            $table->string('deposit_password')->default(NULL)->nullable();
            $table->string('deposit_no')->default(NULL)->nullable();
            $table->dateTime('deposit_date')->default(NULL)->nullable();
            $table->dateTime('deposit_enddate')->default(NULL)->nullable();
            $table->float('deposit_amount',15)->default(NULL)->nullable();
            $table->float('deposit_merchant_fee',15)->default(NULL)->nullable();
            $table->float('deposit_nett_amount',15)->default(NULL)->nullable();
            $table->string('deposit_currency')->default(NULL)->nullable();
            $table->string('deposit_country')->default(NULL)->nullable();
            $table->string('deposit_tac')->default(NULL)->nullable();
            $table->string('deposit_tac2')->default(NULL)->nullable();
            $table->integer('deposit_slot')->default(0)->nullable();
            $table->string('deposit_api_id')->default(NULL)->nullable();
            $table->string('deposit_status')->default(NULL)->nullable();
            $table->string('deposit_internalstatus')->default(NULL)->nullable();
            $table->integer('deposit_internalstatuscode')->default(0)->nullable();
            $table->longText('deposit_response')->default(NULL)->nullable();
            $table->longText('deposit_bankresponse')->default(NULL)->nullable();
            $table->longText('deposit_postdata')->default(NULL)->nullable();
            $table->longText('deposit_postdataresponse')->default(NULL)->nullable();
            $table->integer('deposit_postdata_count')->default(0)->nullable();
            $table->integer('deposit_postdata_status')->default(0)->nullable();
            $table->longText('deposit_updatestatus_json')->default(NULL)->nullable();
            $table->timestamps();
            $table->integer('updateBy')->default(0);
            $table->integer('insertBy')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('db_deposit');
    }
}
