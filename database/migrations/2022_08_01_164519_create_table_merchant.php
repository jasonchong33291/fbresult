<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableMerchant extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('db_merchant', function (Blueprint $table) {
            $table->id('merchant_id')->index('ordersubmission_id');
            $table->integer('merchant_agent_id')->default(0)->nullable();
            $table->integer('merchant_user_id')->default(0)->nullable();
            $table->string('merchant_code')->default(NULL)->nullable();
            $table->string('merchant_name')->default(NULL)->nullable();
            $table->string('merchant_contact_number')->default(NULL)->nullable();
            $table->string('merchant_contact_person')->default(NULL)->nullable();
            $table->float('merchant_p2p_comm',15)->default(NULL)->nullable();
            $table->float('merchant_qr_comm',15)->default(NULL)->nullable();
            $table->integer('merchant_isownbank')->default(0)->nullable();
            $table->longText('merchant_api_endpoint')->default(NULL)->nullable();
            $table->longText('merchant_api_secretkey')->default(NULL)->nullable();
            $table->longText('merchant_api_website_url')->default(NULL)->nullable();
            $table->longText('merchant_api_redirect_url')->default(NULL)->nullable();
            $table->longText('merchant_api_whitelist')->default(NULL)->nullable();

            $table->longText('merchant_remark')->default(NULL)->nullable();
            $table->integer('merchant_status')->default(0)->nullable();
            $table->integer('merchant_seq')->default(0)->nullable();
            $table->timestamps();
            $table->integer('updateBy')->default(0);
            $table->integer('insertBy')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('db_merchant');
    }
}
