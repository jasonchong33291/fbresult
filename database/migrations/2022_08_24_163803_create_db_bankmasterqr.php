<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDbBankmasterqr extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('db_bankmasterqr', function (Blueprint $table) {
            $table->id('bankmasterqr_id')->index('bankmasterqr_id');
            $table->string('bankmasterqr_bank_id')->default(NULL)->nullable()->index('bankmasterqr_bank_id');
            $table->string('bankmasterqr_name')->default(NULL)->nullable();
            $table->longText('bankmasterqr_file')->default(NULL)->nullable();
            $table->integer('bankmasterqr_status')->default(0)->nullable();
            $table->timestamps();
            $table->integer('updateBy')->default(0);
            $table->integer('insertBy')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('db_bankmasterqr');
    }
}
