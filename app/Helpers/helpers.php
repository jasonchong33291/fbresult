<?php // Code within app\Helpers\Helper.php
namespace App\Helpers;
use Config;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Session;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon; 
use App\Helpers\HttpClient;

use Kreait\Laravel\Firebase\Facades\Firebase;

use function Psy\debug;

class Helper
{
    public static function applClasses()
    {
        // default data value
        $dataDefault = [
            'mainLayoutType' => 'vertical-modern-menu',
            'pageHeader' => false,
            'bodyCustomClass' => '',
            'navbarLarge' => false,
            'navbarBgColor' => '',
            'isNavbarDark' => null,
            'isNavbarFixed' => false,
            'activeMenuColor' => '',
            'isMenuDark' => true,
            'isMenuCollapsed' => false,
            'activeMenuType' => '',
            'isFooterDark' => null,
            'isFooterFixed' => false,
            'templateTitle' => 'ASAP',
            'isCustomizer' => true,
            'defaultLanguage'=>'en',
            'largeScreenLogo' => 'images/logo/favicon.svg',
            'smallScreenLogo' => 'images/logo/asap-logo-white.svg',
            'isFabButton'=>false,
            'direction' => env('MIX_CONTENT_DIRECTION', 'ltr'),
        ];
        // if any key missing of array from custom.php file it will be merge and set a default value from dataDefault array and store in data variable
        $data = array_merge($dataDefault, config('custom.custom'));

        // all available option of materialize template
        $allOptions = [
            'mainLayoutType' => array('vertical-modern-menu', 'vertical-menu-nav-dark', 'vertical-gradient-menu', 'vertical-dark-menu', 'horizontal-menu'),
            'pageHeader' => array(true, false),
            'navbarLarge' => array(true, false),
            'isNavbarDark' => array(null, true, false),
            'isNavbarFixed' => array(true, false),
            'isMenuDark' => array(null, true, false),
            'isMenuCollapsed' => array(true, false),
            'activeMenuType' => array('sidenav-active-square'=>'sidenav-active-square', 'sidenav-active-rounded'=>'sidenav-active-rounded', 'sidenav-active-fullwidth'=>'sidenav-active-fullwidth'),
            'isFooterDark' => array(null, true, false),
            'isFooterFixed' => array(false, true),
            'isCustomizer' => array(true, false),
            'isFabButton'=> array(false, true),
            'defaultLanguage'=>array('en'=>'en','fr'=>'fr','de'=>'de','pt'=>'pt'),
            'direction' => array('ltr'=>'ltr', 'rtl'=>'rtl'),
        ];
        //if any options value empty or wrong in custom.php config file then set a default value
        foreach ($allOptions as $key => $value) {
            if (gettype($data[$key]) === gettype($dataDefault[$key])) {
                if (is_string($data[$key])) {
                    $result = array_search($data[$key], $value);
                    if (empty($result)) {
                        $data[$key] = $dataDefault[$key];
                    }
                }
            } else {
                if (is_string($dataDefault[$key])) {
                    $data[$key] = $dataDefault[$key];
                } elseif (is_bool($dataDefault[$key])) {
                    $data[$key] = $dataDefault[$key];
                } elseif (is_null($dataDefault[$key])) {
                    is_string($data[$key]) ? $data[$key] = $dataDefault[$key] : '';
                }
            }
        }
        // if any of template logo is not set or empty is set to default logo
        if (empty($data['largeScreenLogo'])) {
            $data['largeScreenLogo'] = $dataDefault['largeScreenLogo'];
        }
        if (empty($data['smallScreenLogo'])) {
            $data['smallScreenLogo'] = $dataDefault['smallScreenLogo'];
        }
        //mainLayoutTypeClass array contain default class of body element
        $mainLayoutTypeClass = [
            'vertical-modern-menu' => 'vertical-layout vertical-menu-collapsible page-header-dark vertical-modern-menu 2-columns',
            'vertical-menu-nav-dark' => 'vertical-layout page-header-light vertical-menu-collapsible vertical-menu-nav-dark 2-columns',
            'vertical-gradient-menu' => 'vertical-layout page-header-light vertical-menu-collapsible vertical-gradient-menu 2-columns',
            'vertical-dark-menu' => 'vertical-layout page-header-light vertical-menu-collapsible vertical-dark-menu 2-columns',
            'horizontal-menu' => 'horizontal-layout page-header-light horizontal-menu 2-columns',
        ];
        //sidenavMain array contain default class of sidenav
        $sidenavMain = [
            'vertical-modern-menu' => 'sidenav-main nav-expanded nav-lock nav-collapsible',
            'vertical-menu-nav-dark' => 'sidenav-main nav-expanded nav-lock nav-collapsible navbar-full',
            'vertical-gradient-menu' => 'sidenav-main nav-expanded nav-lock nav-collapsible gradient-45deg-deep-purple-blue sidenav-gradient ',
            'vertical-dark-menu' => 'sidenav-main nav-expanded nav-lock nav-collapsible',
            'horizontal-menu' => 'sidenav-main nav-expanded nav-lock nav-collapsible sidenav-fixed hide-on-large-only',
        ];
        //sidenavMainColor array contain sidenav menu's color class according to layout types
        $sidenavMainColor = [
            'vertical-modern-menu' => 'sidenav-light',
            'vertical-menu-nav-dark' => 'sidenav-light',
            'vertical-gradient-menu' => 'sidenav-dark',
            'vertical-dark-menu' => 'sidenav-dark',
            'horizontal-menu' => '',
        ];
        //activeMenuTypeClass array contain active menu class of sidenav according to layout types
        $activeMenuTypeClass = [
            'vertical-modern-menu' => 'sidenav-active-square',
            'vertical-menu-nav-dark' => 'sidenav-active-rounded',
            'vertical-gradient-menu' => 'sidenav-active-rounded',
            'vertical-dark-menu' => 'sidenav-active-rounded',
            'horizontal-menu' => '',
        ];
        //navbarMainClass array contain navbar's default classes
        $navbarMainClass = [
            'vertical-modern-menu' => 'navbar-main navbar-color nav-collapsible no-shadow nav-expanded sideNav-lock',
            'vertical-menu-nav-dark' => 'navbar-main navbar-color nav-collapsible sideNav-lock gradient-shadow',
            'vertical-gradient-menu' => 'navbar-main navbar-color nav-collapsible sideNav-lock',
            'vertical-dark-menu' => 'navbar-main navbar-color nav-collapsible sideNav-lock',
            'horizontal-menu' => 'navbar-main navbar-color nav-collapsible sideNav-lock',
        ];
        //navbarMainColor array contain navabar's color classes according to layout types
        $navbarMainColor = [
            'vertical-modern-menu' => 'navbar-light',
            // 'vertical-modern-menu' => 'navbar-dark gradient-45deg-indigo-purple',
            'vertical-menu-nav-dark' => 'navbar-dark gradient-45deg-purple-deep-orange',
            'vertical-gradient-menu' => 'navbar-light',
            'vertical-dark-menu' => 'navbar-light',
            'horizontal-menu' => 'navbar-dark gradient-45deg-light-blue-cyan',
        ];
        //navbarLargeColor array contain navbarlarge's default color classes
        $navbarLargeColor = [
            'vertical-modern-menu' => '',
            // 'vertical-modern-menu' => 'gradient-45deg-indigo-purple',
            'vertical-menu-nav-dark' => 'blue-grey lighten-5',
            'vertical-gradient-menu' => 'blue-grey lighten-5',
            'vertical-dark-menu' => 'blue-grey lighten-5',
            'horizontal-menu' => 'blue-grey lighten-5',
        ];
        //mainFooterClass array contain Footer's default classes
        $mainFooterClass = [
            'vertical-modern-menu' => 'page-footer footer gradient-shadow',
            'vertical-menu-nav-dark' => 'page-footer footer gradient-shadow',
            'vertical-gradient-menu' => 'page-footer footer',
            'vertical-dark-menu' => 'page-footer footer',
            'horizontal-menu' => 'page-footer footer gradient-shadow',
        ];
        //mainFooterColor array contain footer's color classes
        $mainFooterColor = [
            'vertical-modern-menu' => 'footer-light',
            // 'vertical-modern-menu' => 'footer-dark gradient-45deg-indigo-purple',
            'vertical-menu-nav-dark' => 'footer-dark gradient-45deg-purple-deep-orange',
            'vertical-gradient-menu' => 'footer-light',
            'vertical-dark-menu' => 'footer-light',
            'horizontal-menu' => 'footer-dark gradient-45deg-light-blue-cyan',
        ];
        //  above arrary override through dynamic data
        $layoutClasses = [
            'mainLayoutType' => $data['mainLayoutType'],
            'mainLayoutTypeClass' => $mainLayoutTypeClass[$data['mainLayoutType']],
            'sidenavMain' => $sidenavMain[$data['mainLayoutType']],
            'navbarMainClass' => $navbarMainClass[$data['mainLayoutType']],
            'navbarMainColor' => $navbarMainColor[$data['mainLayoutType']],
            'pageHeader' => $data['pageHeader'],
            'bodyCustomClass' => $data['bodyCustomClass'],
            'navbarLarge' => $data['navbarLarge'],
            'navbarLargeColor' => $navbarLargeColor[$data['mainLayoutType']],
            'navbarBgColor' => $data['navbarBgColor'],
            'isNavbarDark' => $data['isNavbarDark'],
            'isNavbarFixed' => $data['isNavbarFixed'],
            'activeMenuColor' => $data['activeMenuColor'],
            'isMenuDark' => $data['isMenuDark'],
            'sidenavMainColor' => $sidenavMainColor[$data['mainLayoutType']],
            'isMenuCollapsed' => $data['isMenuCollapsed'],
            'activeMenuType' => $data['activeMenuType'],
            'activeMenuTypeClass' => $activeMenuTypeClass[$data['mainLayoutType']],
            'isFooterDark' => $data['isFooterDark'],
            'isFooterFixed' => $data['isFooterFixed'],
            'templateTitle' => $data['templateTitle'],
            'isCustomizer' => $data['isCustomizer'],
            'largeScreenLogo' => $data['largeScreenLogo'],
            'smallScreenLogo' => $data['smallScreenLogo'],
            'defaultLanguage'=>$allOptions['defaultLanguage'][$data['defaultLanguage']],
            'mainFooterClass' => $mainFooterClass[$data['mainLayoutType']],
            'mainFooterColor' => $mainFooterColor[$data['mainLayoutType']],
            'isFabButton'=>$data['isFabButton'],
            'direction' => $data['direction'],
        ];

         // set default language if session hasn't locale value the set default language
         if(!session()->has('locale')){
            app()->setLocale($layoutClasses['defaultLanguage']);
        }
        return $layoutClasses;
    }
    // updatesPageConfig function override all configuration of custom.php file as page requirements.
    public static function updatePageConfig($pageConfigs)
    {
        $demo = 'custom';
        $custom = 'custom';
        if (isset($pageConfigs)) {
            if (count($pageConfigs) > 0) {
                foreach ($pageConfigs as $config => $val) {
                    Config::set($demo . '.' . $custom . '.' . $config, $val);
                }
            }
        }
    }

    // riiko: get API data
    public static function get($url)
    {
        // $api_url   = config('app.url').'api/';
        // $user_info = Session::get('user_login_info');

        // $body  = [
        //     'headers' => [
        //         'Content-Type'  => 'application/json',
        //         'Authorization' => 'Bearer ' . $user_info->access_token,
        //     ],
        //     'http_errors' => false,
        // ];
        // $response = (new Client())->get($api_url.$url, $body);

        // dd($response);
        $response = HttpClient::get($url);
        $data = json_decode(json_encode($response), true);
        // dd($data);

        if(array_key_exists('data', $data)){
            return $data['data'];
        }else{
            return $data;
        }
    }

    public static function oneMapGetAddress($postal_code){

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://developers.onemap.sg/commonapi/search?returnGeom=Y&getAddrDetails=Y&pageNum=1&searchVal=".$postal_code,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
			CURLOPT_SSL_VERIFYHOST => false,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);
        $info = json_decode($response, true);

        return $info;
    }

    public static function getOneMapToken() {
        if(!empty(Cache::get('onemap_token'))) {
            return Cache::get('onemap_token');
        }

        // update 20201230 one map to get displacement instead of distance
        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => "https://developers.onemap.sg/privateapi/auth/post/getToken",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => "{\r\n  \"email\": \"weisiang@firstcom.com.sg\",\r\n  \"password\": \"Firstcomhuat88\"\r\n}",
        CURLOPT_HTTPHEADER => array(
            "cache-control: no-cache",
            "content-type: application/json",
        ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);

        if ($err) {
            $token = false;
        } else {
            // Log::debug($response);
            $response = json_decode($response);
            $token = $response->access_token;
            Cache::put('onemap_token', $token, now()->addMinutes(60));
            return $token;
        }

        // return $token;
	}

    public static function getRouteDistance($owner_lat,$owner_lng,$client_lat,$client_lng) {
		set_time_limit(0);
		$curl = curl_init();

		$token = static::getOneMapToken();

		$start = $owner_lat . ',' . $owner_lng;
		$end = $client_lat . ',' . $client_lng;

		curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://developers.onemap.sg/privateapi/routingsvc/route?start=$start&end=$end&routeType=drive&token=" . $token,
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 3,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "GET",
		//   CURLOPT_HTTPHEADER => array(
		// 	"cache-control: no-cache",
		//   ),
		));

		$response = curl_exec($curl);
        Log::debug("https://developers.onemap.sg/privateapi/routingsvc/route?start=$start&end=$end&routeType=drive&token=" . $token);
        Log::debug('Response: ' . $response);
		$err = curl_error($curl);
        Log::debug('Err: ' . $err);

		curl_close($curl);
        $response = json_decode($response);

        if(isset($response->route_summary->total_distance) && $response->route_summary->total_distance > 0) {
            return $response->route_summary->total_distance;
        }
        else {
            // fallback calculation of distance
            return static::haversineGreatCircleDistance($owner_lat,$owner_lng,$client_lat,$client_lng);
        }
	}

    public static function haversineGreatCircleDistance($latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo, $earthRadius = 6371000){
        // convert from degrees to radians
        $latFrom = deg2rad($latitudeFrom);
        $lonFrom = deg2rad($longitudeFrom);
        $latTo = deg2rad($latitudeTo);
        $lonTo = deg2rad($longitudeTo);

        $latDelta = $latTo - $latFrom;
        $lonDelta = $lonTo - $lonFrom;

        $angle = 2 * asin(sqrt(pow(sin($latDelta / 2), 2) + cos($latFrom)  * cos($latTo) * pow(sin($lonDelta / 2), 2)));
        $distance_m = $angle * $earthRadius;
        // $distance_k = round($distance_m/1000,2);
        return $distance_m;
    }

    public static function formatPrice($price) {
        return '$'.number_format((float)$price, 2, '.', '');
    }

    public static function formatDate($date) {
        return Carbon::parse($date)->format('j M Y');
    }

    public function formatTime($time) {
        return Carbon::parse($time)->format('h:i A');
    }

    public static function getAdminMenu() {
        $user_info = Session::get('user_login_info');
        return $user_info->admin_menu;
        // return NULL;

    }

    public static function clean($text) {
        // Strip HTML Tags
        $clear = strip_tags($text);
        // Clean up things like &amp;
        $clear = html_entity_decode($clear);
        // Strip out any url-encoded stuff
        $clear = urldecode($clear);
        // Replace non-AlNum characters with space
        // $clear = preg_replace('/[^A-Za-z0-9]/', ' ', $clear);
        // Replace Multiple spaces with single space
        $clear = preg_replace('/ +/', ' ', $clear);
        // Trim the string of leading/trailing space
        $clear = trim($clear);

        return $clear;
    }

    /***
        // USAGE
        whenever engine side need to ask POS to pull updates
        whenever engine side need to send something to POS

        // SAMPLE
        Helper::firebase('order',['order_no' => 'A12345']);
        Helper::firebase('order_updates',1);

        // test
        // $merchant_url = "http://www.test-direct.com/";
        // $merchant_url = "http://testdirect.com/";
        // $merchant_url = "http://testdirect.com.sg/";
        // $merchant_url = "http://test_direct.com.sg/";
        // $merchant_url = "localhost";
    */


    public static function firebase($module = 0,$value = 0) {

        $merchant_url = env('APP_URL');
        $merchant_url = preg_replace("(^https?://)", "", $merchant_url );
        $merchant_url = str_replace(".","*",$merchant_url);
        $merchant_url = str_replace("/","",$merchant_url);

        if ($module && $value) {
            if (strpos($merchant_url, 'localhost') === false && strpos($merchant_url, '127.0.0.1') === false) {
                $data[$module] = $value;

                $database = app('firebase.database');
                $reference = $database->getReference($merchant_url)->update($data);
            }
        }
    }
}
