<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Session;

class HttpClient
{

    public static function get($url, $format = '')
    {
        $user_login_info = Session::get('user_login_info');
      
        $response = [];

        if($user_login_info){
            $response = Http::withToken($user_login_info->access_token)->baseUrl(config('app.url').'api/')->get($url);
            // dd($response);
            if($response->failed()){
                if($response->getStatusCode() == '422'){
                    if($format == 'json'){
                        return $response->json();
                    }else{
                        return $response->object();
                    }
                }else{
                    // return [];
                    abort($response->getStatusCode());
                }
            }else{
                if($format == 'json'){
                    return $response->json();
                }else{
                    return $response->object();
                }
                
            }
        }else{
            return route('admin.login');
        }

        
    }


    public static function post($url,$body) {

        $user_login_info = Session::get('user_login_info');
        
        $response = [];

        if($user_login_info){
            $response = Http::withToken($user_login_info->access_token)->baseUrl(config('app.url').'api/')->post($url, $body);
            
            // dd($response);

            if($response->failed()){
                if($response->getStatusCode() == '422'){
                    return $response->object();
                }else{
                    return [];
                    // abort($response->getStatusCode());
                }
            }else{
                return $response->object();
            }
        }else{
            return route('admin.login');
        }
    }

    public static function attach($image) {

        $user_login_info = Session::get('user_login_info');

        $response = [];
        
        if($user_login_info){
            $response = Http::withToken($user_login_info->access_token)->baseUrl(config('app.url').'api/')
                        ->attach('images[]', file_get_contents($image), $image->getClientOriginalName())
                        ->post('imageUpload');
            
            if($response->failed()){
                if($response->getStatusCode() == '422'){
                    return $response->object();
                }else{
                    return [];
                    // abort($response->getStatusCode());
                }
            }else{
                return $response->object();
            }
        }else{
            return route('admin.login');
        }
    }

    public static function attachFile($file) {

        $user_login_info = Session::get('user_login_info');

        $response = [];
        
        if($user_login_info){
            $response = Http::withToken($user_login_info->access_token)->baseUrl(config('app.url').'api/')
                        ->attach('file_import', file_get_contents($file), $file->getClientOriginalName())
                        ->post('product/import');
            
            if($response->failed()){
                if($response->getStatusCode() == '422'){
                    return $response->object();
                }else{
                    return [];
                    // abort($response->getStatusCode());
                }
            }else{
                return $response->object();
            }
        }else{
            return route('admin.login');
        }
    }

    public static function delete($url)
    {

        $user_login_info = Session::get('user_login_info');

        $response = [];

        if($user_login_info){
            $response = Http::withToken($user_login_info->access_token)->baseUrl(config('app.url').'api/')->delete($url);

            if($response->failed()){
                if($response->getStatusCode() == '422'){
                    return $response->object();
                }else{
                    return [];
                    // abort($response->getStatusCode());
                }
            }else{
                return $response->object();
            }
        }else{
            return route('admin.login');
        }
    }
}
