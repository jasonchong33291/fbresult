<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Crypt;
use App\Models\Bank;
use Carbon\Carbon;

class BankController extends Controller
{
    public function createForm(Request $request){
        if(!canAccess('create_bank')){
            return response()->json([
                'status' => '0',
                'msg' => 'Permission Denied, if you think this is an error please contact our support.',
            ]);
        }
        if($request['bank_id']){
            $bank_id = Crypt::decryptString($request['bank_id']);
        }else{
            $bank_id = 0;
        }
        if ($bank_id > 0) {
            $data['bank'] = DB::table("db_bank")->where("bank_id", $bank_id)->get()->first();
        } else {
            $data['bank'] = new Bank();
        }

        return view('bank.bankform', ['data' => $data]);
    }
    public function create(Request $request){
        if(!canAccess('create_bank')){
            return response()->json([
                'status' => '0',
                'msg' => 'Permission Denied, if you think this is an error please contact our support.',
            ]);
        }
        if ($request->isMethod('post')) {

            if($request['bank_id']){
                $bank_id = Crypt::decryptString($request['bank_id']);
            }else{
                $bank_id = 0;
            }
            if ($bank_id > 0) {
                $data['bank'] = Bank::find($bank_id);
                $data['bank']->updated_at = Carbon::now();
                $data['bank']->updateBy = Auth::user()->id;
            } else {
                $data['bank'] = new Bank();
                $data['bank']->created_at = Carbon::now();
                $data['bank']->insertBy = Auth::user()->id;
            }


            $data['bank']->bank_code = $request->input('bank_code');
            $data['bank']->bank_name = $request->input('bank_name');
            $data['bank']->bank_seq = $request->input('bank_seq');
            $data['bank']->bank_status = $request->input('bank_status');
            
            $data['bank']->updateBy = Auth::user()->id;
            $data['bank']->insertBy = Auth::user()->id;


            $data['bank']->save();

            if ($bank_id > 0) {
                return response()->json([
                    'status' => '1',
                    'msg' => 'Updated Successfully.'
                ]);
            } else {
                return response()->json([
                    'status' => '1',
                    'msg' => 'Created Successfully.'
                ]);
            }
        } else {
            return response()->json([
                'status' => '0',
                'msg' => 'Somethig went wrong, please try again.',
            ]);
        }
    }
    public function listing(Request $request){

//         $cc = DB::connection('mysql2')->select("SELECT * FROM mst_duitnow_reference_log WHERE  transaction_status = 'NEW'");
//        // header('Content-type:image/png');
// //         echo "<img src = 'data:image/jpeg;base64,".base64_encode($cc[0]->transaction_image)."'/>";
// // die;
        if(!canAccess('view_bank')){
            return response()->json([
                'status' => '0',
                'msg' => 'Permission Denied, if you think this is an error please contact our support.',
            ]);
        }
        $data['bank'] = DB::table("db_bank")->whereIn('bank_status',[1,2])->get();
        $data['failled_msg'] = $request['failled_msg'];
        $data['success_msg'] = $request['success_msg'];
        return view('bank.banklisting', ['data' => $data]);
    }

    public function delete(Request $request){
        if(!canAccess('delete_bank')){
            return response()->json([
                'status' => '0',
                'msg' => 'Permission Denied, if you think this is an error please contact our support.',
            ]);
        }
        if($request['bank_id']){
            $bank_id = Crypt::decryptString($request['bank_id']);
        }else{
            $bank_id = 0;
        }
        if($bank_id > 0){
            $bank = Bank::find($bank_id);
            $bank->bank_status = 0;
            $bank->save();
            return redirect()->route('bank', ['success_msg' => 'Deleted Successfully.']);
        }else{
            return response()->json([
                'status' => '0',
                'msg' => 'Permission Denied, if you think this is an error please contact our support.',
            ]);  
        }
    }
    public function updatebankstatus(Request $request){
        if(!canAccess('edit_bank')){
            return response()->json([
                'status' => '0',
                'msg' => 'Permission Denied, if you think this is an error please contact our support.',
            ]);
        }
        if($request['bank_id']){
            $bank_id = Crypt::decryptString($request['bank_id']);
        }else{
            $bank_id = 0;
        }

        if($bank_id > 0){
            $bank = Bank::find($bank_id);
            if($request['updatetype'] == "bank_depositto_status_class"){
                $bank->bank_depositto_status = $request->bank_depositto_status;
            }else if($request['updatetype'] == "bank_status_class"){
                $bank->bank_status = $request->bank_status;
            }else if($request['updatetype'] == "bank_settlementstatus_class"){
                $bank->bank_settlement_status = $request->bank_settlement_status;
            }
            
            $bank->save();
            return response()->json([
                'status' => '1',
                'msg' => 'Updated Successfully.',
            ]); 
        }else{
            return response()->json([
                'status' => '0',
                'msg' => 'Record not found, if you think this is an error please contact our support.',
            ]);  
        }
    }
}
