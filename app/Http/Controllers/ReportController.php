<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Deposit;
use App\Models\Merchant;
use Carbon\Carbon;

class ReportController extends Controller
{
    public function sales(){

        $data['filter_date_from'] = Carbon::now()->format('01/m/Y');
        $data['filter_date_to'] = Carbon::now()->format('t/m/Y');

        if(Auth::user()->user_group_id == 3){//agent
            $data['merchant'] = DB::table("db_merchant")->where('merchant_agent_id',Auth::user()->id)->get();
            $data['agent'] = [];
        }else if(Auth::user()->user_group_id == 6){//senior
            $data['merchant'] = DB::table("db_merchant")
                                    ->join('users','users.id','=','db_merchant.merchant_agent_id')
                                    ->where('user_parent',Auth::user()->id)
                                    ->get();
            
            $data['agent'] = [];
        }else if((Auth::user()->user_group_id == 1) || (Auth::user()->user_group_id == 2) || (Auth::user()->user_group_id == 7) || (Auth::user()->user_group_id == 9)){//super admin & admin & Finance
            $data['merchant'] = Merchant::all();
            $data['agent'] = User::where('user_group_id','3')->get();
        }else{
            $data['merchant'] = [];
            $data['agent'] = [];
        }


        return view('report.depositReport', ['data' => $data]);
    }
    public function salessearchsummary(Request $request){

        $filter_date_from = $from = $request['filter_date_from'] ? Carbon::createFromFormat('d/m/Y', $request['filter_date_from'])->format('Y-m-d') : null; 
        $filter_date_to = $from = $request['filter_date_to'] ? Carbon::createFromFormat('d/m/Y', $request['filter_date_to'])->format('Y-m-d') : null; 
        $filter_merchant = $request['filter_merchant'];
        $filter_agent = $request['filter_agent'];
        $filter_mode = $request['filter_mode'];
        if(!$filter_date_from){
            $filter_date_from = "0000-00-00";
        }

        if(!$filter_date_to){
            $filter_date_to = "2040-12-31";
        }

        if(!$filter_merchant){
            $filter_merchant = 0;
        }
        if(!$filter_agent){
            $filter_agent = 0;
        }
        
        $format_date = format_date($filter_date_from) . " ~ " . format_date($filter_date_to);
       

        if(Auth::user()->user_group_id == 3){//agent
            $wherestring = " AND m.merchant_agent_id = '".Auth::user()->id."' ";
            if($filter_merchant > 0){
                $wherestring .= " AND m.merchant_id ='$filter_merchant'";
            }
        }else if(Auth::user()->user_group_id == 6){//senior
            $wherestring = " AND u.user_parent = '".Auth::user()->id."' ";
            if($filter_merchant > 0){
                $wherestring .= " AND m.merchant_id ='$filter_merchant'";
            }
        }else if((Auth::user()->user_group_id == 1) || (Auth::user()->user_group_id == 2) || (Auth::user()->user_group_id == 7) || (Auth::user()->user_group_id == 9)){//super admin & admin
            $wherestring = "";
            if($filter_merchant > 0){
                $wherestring .= " AND m.merchant_id ='$filter_merchant'";
            }
            if($filter_agent > 0){
                $wherestring .= " AND m.merchant_agent_id ='$filter_agent'";
            }
        }else if((Auth::user()->user_group_id == 4) || (Auth::user()->user_group_id == 8) || (Auth::user()->user_group_id == 10) || (Auth::user()->user_group_id == 11)){//merchant
            $wherestring = " AND m.merchant_id ='".Auth::user()->user_merchant_id."'";
        }else{
            $wherestring = " AND u.merchant_id ='0' ";
        }

        $data['deposit'] = DB::select("
        SELECT 'p2p' as deposit_mode ,m.merchant_name,m.merchant_code as merchant_code,
        COALESCE((SELECT SUM(deposit_amount) FROM db_deposit WHERE deposit_merchant_id = m.merchant_id 
        AND deposit_internalstatuscode = '80001' AND deposit_mode = 'p2p' AND LEFT(deposit_date,10) BETWEEN '$filter_date_from' AND '$filter_date_to'),0) as total_amount
        FROM db_merchant m
        INNER JOIN users u ON u.id = m.merchant_agent_id
        WHERE m.merchant_agent_id > 0 $wherestring

        UNION ALL

        SELECT 'qr' as deposit_mode ,m.merchant_name,m.merchant_code as merchant_code,
        COALESCE((SELECT SUM(deposit_amount) FROM db_deposit WHERE deposit_merchant_id = m.merchant_id 
        AND deposit_internalstatuscode = '80001' AND deposit_mode = 'qr' AND LEFT(deposit_date,10) BETWEEN '$filter_date_from' AND '$filter_date_to'),0) as total_amount
        FROM db_merchant m
        INNER JOIN users u ON u.id = m.merchant_agent_id
        WHERE m.merchant_agent_id > 0 $wherestring
        
        ORDER BY merchant_code,deposit_mode
        ");

        $html = "<table style = 'border-collapse: collapse;width:100%' cellpadding='10' border='1'>";
        $html .= " <tr>
                        <td style = 'font-weight:700; text-align:center;background-color:#88e0d0;color:black;' colspan = '6'>SALES SUMMARY REPORT<br>$format_date</td>
                    </tr>";
        $html .= "<tr>
                    <th style = 'text-align:center;background-color:#6bba62;color:white;font-weight:700'>MERCHANT CODE</th>
                    <th style = 'text-align:center;background-color:#6bba62;color:white;font-weight:700'>MERCHANT NAME</th>
                    <th style = 'text-align:center;background-color:#6bba62;color:white;font-weight:700'>TRANSACTION MODE</th>
                    <th style = 'text-align:center;background-color:#6bba62;color:white;font-weight:700'>TOTAL DEPOSIT</th>
                 </tr>
                ";
        $total_amount = 0;
        foreach($data['deposit'] as $k){

            if($filter_mode == "p2p"){
                if($k->deposit_mode == 'qr'){
                    continue;
                }
            }else if($filter_mode == "qr"){
                if($k->deposit_mode == 'p2p'){
                    continue;
                }
            }

            $total_amount = $total_amount + $k->total_amount;
            $html .= "<tr>
                            <td>".strtoupper($k->merchant_code)."</td>
                            <td>".strtoupper($k->merchant_name)."</td>
                            <td>".strtoupper($k->deposit_mode)."</td>
                            <td style = 'text-align:right;'>".num_format($k->total_amount)."</td>
                      </tr>
                     ";
        }
        $html .= "<tr>
                        <td colspan = '3' style = 'text-align:right;font-weight:700;color:red'>TOTAL : </td>
                        <td style = 'text-align:right;font-weight:700;color:red'>".num_format($total_amount)."</td>
                 </tr>
                ";

        $html .= "<table>";
        return response($html);

    }

    public function salessearchdetail(Request $request){

        $filter_date_from = $from = $request['filter_date_from'] ? Carbon::createFromFormat('d/m/Y', $request['filter_date_from'])->format('Y-m-d') : null; 
        $filter_date_to = $from = $request['filter_date_to'] ? Carbon::createFromFormat('d/m/Y', $request['filter_date_to'])->format('Y-m-d') : null; 
        $filter_merchant = $request['filter_merchant'];
        $filter_agent = $request['filter_agent'];
        $filter_mode = $request['filter_mode'];
        if(!$filter_date_from){
            $filter_date_from = "0000-00-00";
        }

        if(!$filter_date_to){
            $filter_date_to = "2040-12-31";
        }

        if(!$filter_merchant){
            $filter_merchant = 0;
        }
        if(!$filter_agent){
            $filter_agent = 0;
        }

        $format_date = format_date($filter_date_from) . " ~ " . format_date($filter_date_to);

        if(Auth::user()->user_group_id == 3){//agent
            $wherestring = " AND m.merchant_agent_id = '".Auth::user()->id."' ";
            if($filter_merchant > 0){
                $wherestring .= " AND m.merchant_id ='$filter_merchant'";
            }
        }else if(Auth::user()->user_group_id == 6){//senior
            $wherestring = " AND u.user_parent = '".Auth::user()->id."' ";
            if($filter_merchant > 0){
                $wherestring .= " AND m.merchant_id ='$filter_merchant'";
            }
        }else if((Auth::user()->user_group_id == 1) || (Auth::user()->user_group_id == 2) || (Auth::user()->user_group_id == 7) || (Auth::user()->user_group_id == 9)){//super admin & admin
            $wherestring = "";
            if($filter_merchant > 0){
                $wherestring .= " AND m.merchant_id ='$filter_merchant'";
            }
            if($filter_agent > 0){
                $wherestring .= " AND m.merchant_agent_id ='$filter_agent'";
            }
        }else if((Auth::user()->user_group_id == 4) || (Auth::user()->user_group_id == 8) || (Auth::user()->user_group_id == 10) || (Auth::user()->user_group_id == 11)){//merchant
            $wherestring = " AND m.merchant_id ='".Auth::user()->user_merchant_id."'";
        }else{
            $wherestring = " AND m.merchant_id ='0' ";
        }






        $data['deposit'] = DB::select("
        SELECT 'p2p' as deposit_mode ,m.merchant_name,m.merchant_code as merchant_code,m.merchant_p2p_comm,m.merchant_qr_comm,
        d.deposit_merchant_percent, SUM(deposit_amount) as total_amount,SUM(deposit_merchant_fee) as total_merchant_fee
        FROM db_merchant m
        LEFT JOIN db_deposit d ON d.deposit_merchant_id = m.merchant_id
        LEFT JOIN users u ON u.id = m.merchant_agent_id
        WHERE m.merchant_agent_id > 0 AND deposit_mode = 'p2p' AND d.deposit_internalstatuscode = '80001' 
        AND LEFT(deposit_date,10) BETWEEN '$filter_date_from' AND '$filter_date_to' $wherestring
        GROUP BY m.merchant_id,d.deposit_merchant_percent

        UNION ALL

        SELECT 'qr' as deposit_mode ,m.merchant_name,m.merchant_code as merchant_code,m.merchant_p2p_comm,m.merchant_qr_comm,
        d.deposit_merchant_percent, SUM(deposit_amount) as total_amount,SUM(deposit_merchant_fee) as total_merchant_fee
        FROM db_merchant m
        LEFT JOIN db_deposit d ON d.deposit_merchant_id = m.merchant_id
        LEFT JOIN users u ON u.id = m.merchant_agent_id
        WHERE m.merchant_agent_id > 0 AND deposit_mode = 'qr' AND d.deposit_internalstatuscode = '80001' 
        AND LEFT(deposit_date,10) BETWEEN '$filter_date_from' AND '$filter_date_to' $wherestring
        GROUP BY m.merchant_id,d.deposit_merchant_percent
        
        
        ORDER BY merchant_code,deposit_mode
        ");

        $html = "<table style = 'border-collapse: collapse;width:100%' cellpadding='10' border='1'>";
        $html .= " <tr>
                        <td style = 'font-weight:700; text-align:center;background-color:#88e0d0;color:black;' colspan = '6'>SALES DETAIL REPORT<br>$format_date</td>
                    </tr>";
        $html .= "<tr>
                    <th style = 'text-align:center;background-color:#6bba62;color:white;font-weight:700'>MERCHANT CODE</th>
                    <th style = 'text-align:center;background-color:#6bba62;color:white;font-weight:700'>MERCHANT NAME</th>
                    <th style = 'text-align:center;background-color:#6bba62;color:white;font-weight:700'>TRANSACTION MODE</th>
                    <th style = 'text-align:center;background-color:#6bba62;color:white;font-weight:700'>PERCENTAGE</th>
                    <th style = 'text-align:center;background-color:#6bba62;color:white;font-weight:700'>TOTAL DEPOSIT</th>
                    <th style = 'text-align:center;background-color:#6bba62;color:white;font-weight:700'>TOTAL MERCHANT FEE</th>
                 </tr>
                ";
        $total_amount = 0;
        $total_merchant_fee = 0;
        foreach($data['deposit'] as $k){

            if($filter_mode == "p2p"){
                if($k->deposit_mode == 'qr'){
                    continue;
                }
            }else if($filter_mode == "qr"){
                if($k->deposit_mode == 'p2p'){
                    continue;
                }
            }

            $total_amount = $total_amount + $k->total_amount;
            $total_merchant_fee = $total_merchant_fee + $k->total_merchant_fee;
            if($k->deposit_mode == 'p2p'){
                $comm_percent = $k->merchant_p2p_comm;
            }else{
                $comm_percent = $k->merchant_qr_comm;
            }
            $html .= "<tr>
                            <td>".strtoupper($k->merchant_code)."</td>
                            <td>".strtoupper($k->merchant_name)."</td>
                            <td>".strtoupper($k->deposit_mode)."</td>
                            <td style = 'text-align:right;'>".num_format($k->deposit_merchant_percent*100)."</td>
                            <td style = 'text-align:right;'>".num_format($k->total_amount)."</td>
                            <td style = 'text-align:right;'>".num_format($k->total_merchant_fee)."</td>
                      </tr>
                     ";
        }
        $html .= "<tr>
                        <td colspan = '4' style = 'text-align:right;font-weight:700;color:red'>TOTAL : </td>
                        <td style = 'text-align:right;font-weight:700;color:red'>".num_format($total_amount)."</td>
                        <td style = 'text-align:right;font-weight:700;color:red'>".num_format($total_merchant_fee)."</td>
                 </tr>
                ";

        $html .= "<table>";
        return response($html);

    }

    public function salessearchbank(Request $request){
        if((Auth::user()->user_group_id == 1) || (Auth::user()->user_group_id == 2) || (Auth::user()->user_group_id == 7) || (Auth::user()->user_group_id == 9)){

        }else{
            return response()->json([
                'status' => '0',
                'msg' => 'Permission Denied',
            ]);
        }
        $filter_date_from = $from = $request['filter_date_from'] ? Carbon::createFromFormat('d/m/Y', $request['filter_date_from'])->format('Y-m-d') : null; 
        $filter_date_to = $from = $request['filter_date_to'] ? Carbon::createFromFormat('d/m/Y', $request['filter_date_to'])->format('Y-m-d') : null; 
        $filter_merchant = $request['filter_merchant'];
        
        
        if(!$filter_date_from){
            $filter_date_from = "0000-00-00";
        }

        if(!$filter_date_to){
            $filter_date_to = "2040-12-31";
        }

        if(!$filter_merchant){
            $filter_merchant = 0;
        }

        $format_date = format_date($filter_date_from) . " ~ " . format_date($filter_date_to);
       
        $wherestring2 = '';
        if(Auth::user()->user_group_id == 3){//agent
            $wherestring = " AND m.merchant_agent_id = '".Auth::user()->id."' ";
            if($filter_merchant > 0){
                $wherestring2 .= " AND deposit_merchant_id ='$filter_merchant'";
            }
        }else if(Auth::user()->user_group_id == 6){//senior
            $wherestring = " AND u.user_parent = '".Auth::user()->id."' ";
            if($filter_merchant > 0){
                $wherestring2 .= " AND deposit_merchant_id ='$filter_merchant'";
            }
        }else if((Auth::user()->user_group_id == 1) || (Auth::user()->user_group_id == 2) || (Auth::user()->user_group_id == 7) || (Auth::user()->user_group_id == 9)){//super admin & admin
            $wherestring = "";
            if($filter_merchant > 0){
                $wherestring2 .= " AND deposit_merchant_id ='$filter_merchant'";
            }
        }else if((Auth::user()->user_group_id == 4) || (Auth::user()->user_group_id == 8) || (Auth::user()->user_group_id == 10) || (Auth::user()->user_group_id == 11)){//merchant
            $wherestring = " AND m.merchant_id ='".Auth::user()->user_merchant_id."'";
        }else{
            $wherestring = " AND bm.bankmaster_id ='0' ";
        }

        $data['deposit'] = DB::select("

        SELECT b.bank_name,b.bank_code,bm.bankmaster_account_holder,bm.bankmaster_account_number,bm.bankmaster_status,
        COALESCE((SELECT SUM(deposit_amount) FROM db_deposit WHERE deposit_bankmaster_id = bm.bankmaster_id AND deposit_internalstatuscode = '80001' 
        AND deposit_mode = 'p2p' AND LEFT(deposit_date,10) BETWEEN '$filter_date_from' AND '$filter_date_to'  $wherestring2),0) as total_p2pamount,
        COALESCE((SELECT SUM(deposit_amount) FROM db_deposit WHERE deposit_bankmaster_id = bm.bankmaster_id AND deposit_internalstatuscode = '80001' 
        AND deposit_mode = 'qr' AND LEFT(deposit_date,10) BETWEEN '$filter_date_from' AND '$filter_date_to' $wherestring2),0) as total_qramount
        FROM db_bankmaster bm
        LEFT JOIN db_bank b ON b.bank_id = bm.bankmaster_bank_id
        WHERE bm.bankmaster_id > 0 $wherestring
        ORDER BY bm.bankmaster_seq,bm.bankmaster_name
        ");

        // $data['deposit'] = DB::select("
        // SELECT 'p2p' as deposit_mode ,b.bank_name,b.bank_code,d.deposit_bankto_account_holder,d.deposit_bankto_account_number,
        // SUM(deposit_amount) as total_p2pamount,0 as total_qramount
        // FROM db_deposit d 
        // INNER JOIN db_merchant m ON d.deposit_merchant_id = m.merchant_id
        // LEFT JOIN db_bank b ON b.bank_id = d.deposit_bankto_id
        // LEFT JOIN users u ON u.id = m.merchant_agent_id
        // WHERE d.deposit_mode = 'p2p' AND d.deposit_internalstatuscode = '80001' 
        // AND LEFT(deposit_date,10) BETWEEN '$filter_date_from' AND '$filter_date_to' $wherestring
        // GROUP BY d.deposit_bankto_account_number

        // UNION ALL

        // SELECT 'qr' as deposit_mode ,b.bank_name,b.bank_code,d.deposit_bankto_account_holder,d.deposit_bankto_account_number,
        // 0 as total_p2pamount,SUM(deposit_amount) as total_qramount
        // FROM db_deposit d 
        // INNER JOIN db_merchant m ON d.deposit_merchant_id = m.merchant_id
        // LEFT JOIN db_bank b ON b.bank_id = d.deposit_bankto_id
        // LEFT JOIN users u ON u.id = m.merchant_agent_id
        // WHERE d.deposit_mode = 'qr' AND d.deposit_internalstatuscode = '80001' 
        // AND LEFT(deposit_date,10) BETWEEN '$filter_date_from' AND '$filter_date_to' $wherestring
        // GROUP BY d.deposit_bankto_account_number
        
        
        // ORDER BY bank_code
        // ");

        $html = "<table style = 'border-collapse: collapse;width:100%' cellpadding='10' border='1'>";
        $html .= " <tr>
                        <td style = 'font-weight:700; text-align:center;background-color:#88e0d0;color:black;' colspan = '6'>SALES SUMMARY BY BANK REPORT<br>$format_date</td>
                    </tr>";
        $html .= "<tr>
                    <th style = 'text-align:center;background-color:#6bba62;color:white;font-weight:700'>BANK CODE</th>
                    <th style = 'text-align:center;background-color:#6bba62;color:white;font-weight:700'>BANK NAME</th>
                    <th style = 'text-align:center;background-color:#6bba62;color:white;font-weight:700'>BANK HOLDER</th>
                    <th style = 'text-align:center;background-color:#6bba62;color:white;font-weight:700'>BANK ACCOUNT</th>
                    <th style = 'text-align:center;background-color:#6bba62;color:white;font-weight:700'>P2P</th>
                    <th style = 'text-align:center;background-color:#6bba62;color:white;font-weight:700'>QR</th>
                 </tr>
                ";
        $total_p2pamount = 0;
        $total_qramount = 0;
        foreach($data['deposit'] as $k){

            if($k->bankmaster_status == 0){
                if(($k->total_p2pamount == 0) && ($k->total_qramount == 0)){
                    continue;
                }
            }
            $total_p2pamount = $total_p2pamount + $k->total_p2pamount;
            $total_qramount = $total_qramount + $k->total_qramount;
            
            $html .= "<tr>
                            <td>".strtoupper($k->bank_code)."</td>
                            <td>".strtoupper($k->bank_name)."</td>
                            <td>".strtoupper($k->bankmaster_account_holder)."</td>
                            <td>".strtoupper($k->bankmaster_account_number)."</td>
                            <td style = 'text-align:right;'>".num_format($k->total_p2pamount)."</td>
                            <td style = 'text-align:right;'>".num_format($k->total_qramount)."</td>
                      </tr>
                     ";
        }
        $html .= "<tr>
                        <td colspan = '4' style = 'text-align:right;font-weight:700;color:red'>TOTAL : </td>
                        <td style = 'text-align:right;font-weight:700;color:red'>".num_format($total_p2pamount)."</td>
                        <td style = 'text-align:right;font-weight:700;color:red'>".num_format($total_qramount)."</td>
                 </tr>
                ";

        $html .= "<table>";
        return response($html);

    }
}
