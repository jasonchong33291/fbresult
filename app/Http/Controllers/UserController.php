<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Http\Requests\UserRequest;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Crypt;

use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Models\Permission;
use App\Models\Role;
use Carbon\Carbon;

class UserController extends Controller
{
    /**
     * Display a listing of the users
     *
     * @param  \App\Models\User  $model
     * @return \Illuminate\View\View
     */
    public function userlisting(User $model){
        if(!canAccess('view_user')){
            return response()->json([
                'status' => '0',
                'msg' => 'Permission Denied, if you think this is an error please contact our support.',
            ]);
        }
        $data['users'] = DB::table("users")
            ->select('*', DB::raw('users.id as user_id'))
            ->leftjoin('roles', 'roles.id', '=', 'users.user_group_id')
            ->whereIn('user_status',[1,2])
            ->where('users.id','!=','1');
            ;
        if((Auth::user()->user_group_id == 1) || (Auth::user()->user_group_id == 2)){//super admin & admin
            $data['users'] = $data['users']->whereIn('users.user_group_id',[2,7,3,9])->get();
        }else if(Auth::user()->user_group_id == 6){//senior
            $data['users'] = $data['users']->whereIn('users.user_group_id',[3])->where('user_parent',Auth::user()->id)->get();
        }else if(Auth::user()->user_group_id == 4){//merchant
            $data['users'] = $data['users']->whereIn('users.user_group_id',[10,11,8])->where('user_parent',Auth::user()->id)->get();
        }else{
            $data['users'] = [];
        }
        
        return view('users.index', ['data' => $data]);
    }

    public function create(Request $request){

        if ($request->isMethod('post')) {

            $id = Crypt::decryptString($request['id']);
            if ($id > 0) {
                if(!canAccess('edit_user')){
                    return response()->json([
                        'status' => '0',
                        'msg' => 'Permission Denied, if you think this is an error please contact our support.',
                    ]);
                }
                $data['user'] = User::find($id);
                if ($request->filled('user_password')) {
                    $data['user']->password = Hash::make($request->input('user_password'));
                }
            } else {
                if(!canAccess('create_user')){
                    return response()->json([
                        'status' => '0',
                        'msg' => 'Permission Denied, if you think this is an error please contact our support.',
                    ]);
                }
                $data['user'] = new User();
                $data['user']->password = Hash::make($request->input('user_password'));
            }
            $logged_user = Auth::user();



            $data['user']->user_name = $request->input('user_name');
            $data['user']->user_email = $request->input('user_email');
            $data['user']->user_mobile = $request->input('user_mobile');
            if((Auth::user()->user_group_id == 1) || (Auth::user()->user_group_id == 2)){//super admin & admin
                $data['user']->user_parent = $request->input('user_parent');
                $data['user']->user_group_id = $request->input('user_group_id');
                $data['user']->user_p2p_comm = $request->input('user_p2p_comm');
                $data['user']->user_qr_comm = $request->input('user_qr_comm');
            }else if(Auth::user()->user_group_id == 6){//senior
                $data['user']->user_parent = Auth::user()->id;
                $data['user']->user_group_id = 3;

                $self_info = User::find(Auth::user()->id);
                if($self_info){
                    if($self_info->user_p2p_comm >= $request->input('user_p2p_comm')){
                        $data['user']->user_p2p_comm = $request->input('user_p2p_comm');
                    }else{
                        $data['user']->user_p2p_comm = $self_info->user_p2p_comm;
                    }

                    
                    if($self_info->user_qr_comm >= $request->input('user_qr_comm')){
                        $data['user']->user_qr_comm = $request->input('user_qr_comm');
                    }else{
                        $data['user']->user_qr_comm = $self_info->user_qr_comm;
                    }
                }
            }else if(Auth::user()->user_group_id == 4){//merchant
                $data['user']->user_parent = Auth::user()->id;
                if(($request->input('user_group_id') == 10) || ($request->input('user_group_id') == 11) || ($request->input('user_group_id') == 8)){
                    $data['user']->user_group_id = $request->input('user_group_id');
                }else{
                    $data['user']->user_group_id = 10;
                }
                $data['user']->user_merchant_id = Auth::user()->user_merchant_id;
                
                $self_info = User::find(Auth::user()->id);
                if($self_info){
                    if($self_info->user_p2p_comm >= $request->input('user_p2p_comm')){
                        $data['user']->user_p2p_comm = $request->input('user_p2p_comm');
                    }else{
                        $data['user']->user_p2p_comm = $self_info->user_p2p_comm;
                    }

                    
                    if($self_info->user_qr_comm >= $request->input('user_qr_comm')){
                        $data['user']->user_qr_comm = $request->input('user_qr_comm');
                    }else{
                        $data['user']->user_qr_comm = $self_info->user_qr_comm;
                    }
                }
            }

            $data['user']->user_username = $request->input('user_username');
            $data['user']->user_status = $request->input('user_status');
            
            $data['user']->updateBy = $logged_user->id;
            $data['user']->insertBy = $logged_user->id;

            if($data['user']->user_group_id == 2){
                $data['user']->user_code = getRefno('Uadmin',TRUE, Carbon::now()->format('Y-m-d'));
            }else if($data['user']->user_group_id == 3){
                $data['user']->user_code = getRefno('Uagent',TRUE, Carbon::now()->format('Y-m-d'));     
            }else if($data['user']->user_group_id == 4){
                $data['user']->user_code = getRefno('Umerchant',TRUE, Carbon::now()->format('Y-m-d'));
            }else if($data['user']->user_group_id == 5){
                $data['user']->user_code = getRefno('Usenior',TRUE, Carbon::now()->format('Y-m-d'));
            }
           // echo $data['user']->user_code;die;

            $data['user']->save();

            $role = Role::find($request->input('user_group_id'));
            // assign role to user
            $data['user']->assignRole($role);

            if ($id > 0) {
                return response()->json([
                    'status' => '1',
                    'msg' => 'Updated Successfully.'
                ]);
            } else {
                return response()->json([
                    'status' => '1',
                    'msg' => 'Created Successfully.'
                ]);
            }
        } else {
            return response()->json([
                'status' => '0',
                'msg' => 'Somethig went wrong, please try again.',
            ]);
        }
    }
    public function validateusername(Request $request){

        $user = User::where('user_username', '=', $request->input('user_username'))->where('id','!=',Crypt::decryptString($request['id']))->first();
       
        if ($user === null) {
            echo "true";
        } else {
            echo 'false';
        }
    }
    public function createForm(Request $request){

        if($request['id']){
            $user_id = Crypt::decryptString($request['id']);
        }else{
            $user_id = 0;
        }
        
        if ($user_id > 0) {
            $data['user'] = DB::table("users")->where("id", $user_id)->get()->first();
            $data['edit'] = true;
        } else {
            $data['user'] = new User();
            $data['edit'] = false;
            $data['user']['user_p2p_comm'] = num_format(0);
            $data['user']['user_qr_comm'] = num_format(0);
        }
        if((Auth::user()->user_group_id == 1) || (Auth::user()->user_group_id == 2)){//super admin & admin
            $data['roles'] = DB::table("roles")->whereIn('id',[2,7,3,9])->get();
            $data['managers'] = DB::table("users")->where("user_status", 1)->where('user_group_id','!=','1')->get();
        }else if(Auth::user()->user_group_id == 4){//merchant
            $data['roles'] = DB::table("roles")->whereIn('id',[10,11])->get();
            $data['managers'] = DB::table("users")->where("user_status", 1)->where('user_group_id','!=','1')->get();
        }else{
            $data['roles'] = [];
            $data['managers'] = [];
        }

       
        

        return view('users.userform', ['data' => $data]);
    }
    public function changepasswordForm(Request $request){

        $logged_user = Auth::user();
       
        if($logged_user){
            $data['user'] = DB::table("users")->where("id", Auth::user()->id)->get()->first();
            $data['edit'] = true;
            return view('users.userformChangePassword', ['data' => $data]);
        }else{
            return redirect('/login');
        }
        
    }
    public function changepassword(Request $request){

        $logged_user = Auth::user();
       
        if($logged_user){
            if ($request->isMethod('post')) {

                $data['user'] = User::find(Auth::user()->id);
                if ($request->filled('user_password')) {
                    $data['user']->password = Hash::make($request->input('user_password'));
                }
                $data['user']->save();


                return response()->json([
                    'status' => '1',
                    'msg' => 'Password Changed Successfully.',
                ]);
            }else{
                return response()->json([
                    'status' => '0',
                    'msg' => 'Something went wrong , method not allow.',
                ]);
            }
        }else{
            return response()->json([
                'status' => '0',
                'msg' => 'Please refresh your page login again.',
            ]);
        }
        
    }
}
