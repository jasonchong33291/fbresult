<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;
use App\Models\Merchant;
use App\Models\User;
use App\Models\Bank;
use App\Models\Bankmaster;
use App\Models\Merchantbank;
use App\Models\Merchantbankmaster;
use App\Models\Merchantsettlementbank;
use Carbon\Carbon;

class MerchantController extends Controller
{
    public function createForm(Request $request){

        if(!canAccess('create_merchant')){
            return response()->json([
                'status' => '0',
                'msg' => 'Permission Denied, if you think this is an error please contact our support.',
            ]);
        }
        if($request['merchant_id']){
            $merchant_id = Crypt::decryptString($request['merchant_id']);
        }else{
            $merchant_id = 0;
        }
        $data['agent_list'] = [];
        $data['user'] = new User();
        if ($merchant_id > 0) { 
            $data['password_required'] = false;
            if(Auth::user()->user_group_id == 3){//agent
                $data['merchant'] = DB::table("db_merchant")->where("merchant_id", $merchant_id)
                                    ->where('merchant_agent_id',Auth::user()->id)->get()->first();
                $data['user'] = DB::table("db_merchant")
                                    ->join('users','users.id','=','db_merchant.merchant_user_id')
                                    ->where("db_merchant.merchant_id","=",$merchant_id)
                                    ->where('merchant_agent_id',Auth::user()->id)
                                    ->get()
                                    ->first();
            }else if(Auth::user()->user_group_id == 6){//senior
                $data['merchant'] = DB::table("db_merchant")
                                    ->join('users','users.id','=','db_merchant.merchant_agent_id')
                                    ->where("merchant_id", $merchant_id)
                                    ->where('user_parent',Auth::user()->id)
                                    ->get()
                                    ->first();
                $data['user'] = DB::table("db_merchant")
                                    ->join('users','users.id','=','db_merchant.merchant_agent_id')
                                    ->join('users as u2','u2.id','=','db_merchant.merchant_user_id')
                                    ->where("merchant_id", $merchant_id)
                                    ->where('users.user_parent',Auth::user()->id)
                                    ->get()
                                    ->first();
                $data['agent_list'] = DB::table("users")->where("user_parent", Auth::user()->id)->where("user_group_id",3)->get();
                
            }else if((Auth::user()->user_group_id == 1) || (Auth::user()->user_group_id == 2) || (Auth::user()->user_group_id == 7) || (Auth::user()->user_group_id == 9)){//super admin & admin & Finance
                $data['merchant'] = DB::table("db_merchant")->where("merchant_id", $merchant_id)->get()->first();
                $data['user'] = DB::table("db_merchant")
                                    ->join('users as u2','u2.id','=','db_merchant.merchant_user_id')
                                    ->where("merchant_id", $merchant_id)
                                    ->get()
                                    ->first();  
                $data['agent_list'] = DB::table("users")->where("user_group_id",3)->get();
            }else{
                $data['merchant'] = null;
            }

            if(!$data['merchant']){
                return response()->json([
                    'status' => '0',
                    'msg' => 'Permission Denied, if you think this is an error please contact our support.',
                ]);
            }
        } else {
            $data['password_required'] = true;
            $data['merchant'] = new Merchant();
            if(Auth::user()->user_group_id == 6){//senior
                $data['agent_list'] = DB::table("users")->where("user_parent", Auth::user()->id)->where("user_group_id",3)->get();
            }else if((Auth::user()->user_group_id == 1) || (Auth::user()->user_group_id == 2) || (Auth::user()->user_group_id == 7) || (Auth::user()->user_group_id == 9)){//super admin & admin & Finance
                $data['agent_list'] = DB::table("users")->where("user_group_id",3)->get();
            }
        }

        return view('merchant.merchantform', ['data' => $data]);
    }
    public function create(Request $request){

        if ($request->isMethod('post')) {

            if($request['merchant_id']){
                $merchant_id = Crypt::decryptString($request['merchant_id']);
            }else{
                $merchant_id = 0;
            }
            if ($merchant_id > 0) {
                if(!canAccess('edit_merchant')){
                    return response()->json([
                        'status' => '0',
                        'msg' => 'Permission Denied, if you think this is an error please contact our support.',
                    ]);
                }
                if(Auth::user()->user_group_id == 3){//agent
                    $merchant_data = DB::table("db_merchant")->where("merchant_id", $merchant_id)->where('merchant_agent_id',Auth::user()->id)->get()->first();
                    
                }else if(Auth::user()->user_group_id == 6){//senior
                    $merchant_data = DB::table("db_merchant")
                                        ->join('users','users.id','=','db_merchant.merchant_agent_id')
                                        ->where("merchant_id", $merchant_id)->where('user_parent',Auth::user()->id)->get()->first();
                }else if((Auth::user()->user_group_id == 1) || (Auth::user()->user_group_id == 2) || (Auth::user()->user_group_id == 7) || (Auth::user()->user_group_id == 9)){//super admin & admin & Finance
                    $merchant_data = DB::table("db_merchant")->where("merchant_id", $merchant_id)->get()->first();
                }else{
                    $merchant_data = false;
                }
                if($merchant_data){
                    $data['merchant'] = Merchant::find($merchant_data->merchant_id);
                    $data['merchant']->updated_at = Carbon::now();
                    $data['merchant']->updateBy = Auth::user()->id;

                }else{
                    return response()->json([
                        'status' => '0',
                        'msg' => 'Permission Denied, if you think this is an error please contact our support.',
                    ]);
                }
            } else {
                if(!canAccess('create_merchant')){
                    return response()->json([
                        'status' => '0',
                        'msg' => 'Permission Denied, if you think this is an error please contact our support.',
                    ]);
                }
                $data['merchant'] = new Merchant();
                $data['merchant']->merchant_code = getRefno('merchant',true,date('Y-m-d'));
                $data['merchant']->created_at = Carbon::now();
                $data['merchant']->insertBy = Auth::user()->id;
                $data['merchant']->merchant_api_secretkey = Hash::make($data['merchant']->merchant_code);
            }
           
            if(Auth::user()->user_group_id == 3){//agent
                $data['merchant']->merchant_agent_id = Auth::user()->id;
            }else if((Auth::user()->user_group_id == 1) || (Auth::user()->user_group_id == 2) || (Auth::user()->user_group_id == 7)  || (Auth::user()->user_group_id == 9)){//senior super admin & admin & Finance
                $data['merchant']->merchant_agent_id = $request->input('merchant_agent_id');
            }
            
            $data['merchant']->merchant_name = $request->input('merchant_name');
            
            $data['merchant']->merchant_contact_number = $request->input('merchant_contact_number');
            $data['merchant']->merchant_contact_person = $request->input('merchant_contact_person');
            $data['merchant']->merchant_api_endpoint = $request->input('merchant_api_endpoint');
            $data['merchant']->merchant_payment_method = $request->input('merchant_payment_method');
            
            $data['merchant']->merchant_api_website_url = $request->input('merchant_api_website_url');
            $data['merchant']->merchant_api_redirect_url = $request->input('merchant_api_redirect_url');
            $data['merchant']->merchant_remark = $request->input('merchant_remark');
            $data['merchant']->merchant_status = $request->input('merchant_status');
            $data['merchant']->merchant_seq = $request->input('merchant_seq');
            $data['merchant']->merchant_api_whitelist = $request->input('merchant_api_whitelist');


            // $agent_info = User::find($data['merchant']->merchant_agent_id);
            
            // if($agent_info){
            //     if($agent_info->user_p2p_comm >= $request->input('merchant_p2p_comm')){
            //         $data['merchant']->merchant_p2p_comm = $request->input('merchant_p2p_comm');
            //     }else{
            //         $data['merchant']->merchant_p2p_comm = $agent_info->user_p2p_comm;
            //     }

                
            //     if($agent_info->user_qr_comm >= $request->input('merchant_qr_comm')){
            //         $data['merchant']->merchant_qr_comm = $request->input('merchant_qr_comm');
            //     }else{
            //         $data['merchant']->merchant_qr_comm = $agent_info->user_qr_comm;
            //     }
            // }else{
            //     $data['merchant']->merchant_p2p_comm = 0;
            //     $data['merchant']->merchant_qr_comm = 0;
            // }

            $data['merchant']->merchant_p2p_comm = $request->input('merchant_p2p_comm');
            $data['merchant']->merchant_qr_comm = $request->input('merchant_qr_comm');
            
            $data['merchant']->updateBy = Auth::user()->id;
            $data['merchant']->insertBy = Auth::user()->id;


            $data['merchant']->save();

            if ($merchant_id > 0) {
                $users = User::find($data['merchant']->merchant_user_id);
                if($users){
                    $users->user_username = $request->input('user_username');
                    if($request->input('user_password') != ""){
                        $users->password = Hash::make($request->input('user_password'));
                    }
                    $users->save();
                }
                return response()->json([
                    'status' => '1',
                    'msg' => 'Updated Successfully.',
                    'merchant_id' => Crypt::encryptString($merchant_id)
                ]);
            } else {
                $users = new User();
                $users->user_mobile = $request->input('merchant_contact_number');
                $users->user_name = $request->input('merchant_name');
                $users->user_username = $request->input('user_username');
                $users->password = Hash::make($request->input('user_password'));
                $users->user_p2p_comm = $data['merchant']->merchant_p2p_comm;
                $users->user_qr_comm = $data['merchant']->merchant_qr_comm;
                $users->user_group_id = 4;
                $users->user_parent = $data['merchant']->merchant_agent_id;
                $users->save();
                $data['merchant'] = Merchant::find($data['merchant']->merchant_id);
                $data['merchant']->merchant_user_id = $users->id;
                $data['merchant']->save();

                $users = User::find($users->id);
                $users->user_merchant_id = $data['merchant']->merchant_id;
                $users->save();

                return response()->json([
                    'status' => '1',
                    'msg' => 'Created Successfully.',
                    'merchant_id' => Crypt::encryptString($data['merchant']->merchant_id)
                ]);
            }
        } else {
            return response()->json([
                'status' => '0',
                'msg' => 'Somethig went wrong, please try again.',
            ]);
        }
    }
    public function listing(Request $request){
        if(!canAccess('view_merchant')){
            return response()->json([
                'status' => '0',
                'msg' => 'Permission Denied, if you think this is an error please contact our support.',
            ]);
        }
        if(Auth::user()->user_group_id == 3){//agent
            $data['merchant'] = DB::table("db_merchant")
                                ->leftjoin('users','users.id','=','db_merchant.merchant_agent_id')
                                ->where('merchant_agent_id',Auth::user()->id)
                                ->whereIn('merchant_status',[1,2,3])->get();
        }else if(Auth::user()->user_group_id == 6){//senior
            $data['merchant'] = DB::table("db_merchant")
                                ->join('users','users.id','=','db_merchant.merchant_agent_id')
                                ->where('user_parent',Auth::user()->id)
                                ->whereIn('merchant_status',[1,2,3])->get();
        }else if((Auth::user()->user_group_id == 1) || (Auth::user()->user_group_id == 2) || (Auth::user()->user_group_id == 7) || (Auth::user()->user_group_id == 9)){//super admin & admin & Finance
            $data['merchant'] = DB::table("db_merchant")
                                ->leftjoin('users','users.id','=','db_merchant.merchant_agent_id')
                                ->whereIn('merchant_status',[1,2,3])
                                ->get();
        }else{
            $data['merchant'] = null;
        }

        $data['failled_msg'] = $request['failled_msg'];
        $data['success_msg'] = $request['success_msg'];
        return view('merchant.merchantlisting', ['data' => $data]);
    }
    public function delete(Request $request){
        if(!canAccess('delete_merchant')){
            return response()->json([
                'status' => '0',
                'msg' => 'Permission Denied, if you think this is an error please contact our support.',
            ]);
        }
        if($request['merchant_id']){
            $merchant_id = Crypt::decryptString($request['merchant_id']);
        }else{
            $merchant_id = 0;
        }
        if($merchant_id > 0){
            $merchant = Merchant::find($merchant_id);
            $merchant->merchant_status = 0;
            $merchant->save();
            return redirect()->route('merchant', ['success_msg' => 'Deleted Successfully.']);
        }else{
            return response()->json([
                'status' => '0',
                'msg' => 'Something went wrong, if you think this is an error please contact our support.',
            ]);  
        }
    }
    public function manageBankMerchantForm(Request $request){
        
        if(!canAccess('manage_bank_merchant')){
            return response()->json([
                'status' => '0',
                'msg' => 'Permission Denied, if you think this is an error please contact our support.',
            ]);
        }
        if($request['merchant_id']){
            $merchant_id = Crypt::decryptString($request['merchant_id']);
        }else{
            $merchant_id = 0;
        }
        if($merchant_id > 0){
            $data['merchant'] = Merchant::find($merchant_id);
            $data['merchantbank_list'] = Merchantbank::where('merchantbank_merchant_id',$merchant_id)->whereIn('merchantbank_status',[1,2])->get();
            
            $data['bank_list'] = Bank::where('bank_status',1)->orderBy('bank_seq','ASC')->get();
            $data['bankmaster_list'] = DB::table("db_bankmaster")->leftjoin('db_bank','db_bank.bank_id','=','db_bankmaster.bankmaster_bank_id')->whereIn('db_bankmaster.bankmaster_status',[1,2])->get();
            $data['merchantbankmaster'] = Merchantbankmaster::where('merchantbankmaster_merchant_id',$merchant_id)->get();
            

            return view('merchant.managebankmerchantform', ['data' => $data]);
        }else{
            return response()->json([
                'status' => '0',
                'msg' => 'Something went wrong, if you think this is an error please contact our support.',
            ]);
        }
    }
    public function addnewbankline(Request $request){

        $bank = Bank::where('bank_status',1)->orderBy('bank_seq','ASC')->get();
    
        $banklist = "<option value = ''>Select One</option>";
        foreach($bank as $c){
            $banklist .= "<option value = '{$c->bank_id}'>{$c->bank_name}</option>";  
        }

        $html = "<tr>";
        $html .= "<td><select class = 'form-control select2' name = 'merchantbank_bank_id[]'>$banklist</select></td>";
        $html .= "<td><input type = 'type' class = 'form-control' name = 'merchantbank_account_holder[]' placeholder = 'Account Holder'/></td>";
        $html .= "<td><input type = 'type' class = 'form-control merchantbank_account_number' name = 'merchantbank_account_number[]' placeholder = 'Account Number'/></td>";
        $html .= "<td><select class = 'form-control select2' name = 'merchantbank_status[]'><option value = '1' SELECTED>Active</option><option value = '2'>Inactive</option></select></td>";
        $html .= "<td><input type = 'hidden' name = 'merchantbank_id[]' value = ''/><a href = 'javascript:void(0)' title = 'Delete' class = 'btn btn-danger deletebankline' ><i class = 'fa fa-trash' style = 'font-size:20px;'></i></a></td>";
        $html .= "</tr>";
        return response()->json([
            'status' => '1',
            'html' => $html,
        ]);
    }
    public function deletebankline(Request $request){
        if(!canAccess('delete_bank_merchant')){
            return response()->json([
                'status' => '0',
                'msg' => 'Permission Denied, if you think this is an error please contact our support.',
            ]);
        }

        if($request['merchant_id']){
            $merchant_id = Crypt::decryptString($request['merchant_id']);
        }else{
            $merchant_id = 0;
        }
        if ($merchant_id > 0) {
            $update_status = DB::table('db_merchantbank')
                                ->where('merchantbank_id', $request['merchantbank_id'])
                                ->where('merchantbank_merchant_id', $merchant_id)
                                ->update(['merchantbank_status' => 0]);
            if($update_status){
                return response()->json([
                    'status' => '1',
                    'msg' => 'Deleted Successfully.',
                ]);
            }else{
                return response()->json([
                    'status' => '0',
                    'msg' => 'Deleted Failed.',
                ]);
            }


        }else{
            return response()->json([
                'status' => '0',
                'msg' => 'Something went wrong, if you think this is an error please contact our support.',
            ]);
        }
    }
    public function merchantbankCreate(Request $request){
        if(!canAccess('create_bank_merchant')){
            return response()->json([
                'status' => '0',
                'msg' => 'Permission Denied, if you think this is an error please contact our support.',
            ]);
        }

        if($request['merchant_id']){
            $merchant_id = Crypt::decryptString($request['merchant_id']);
        }else{
            $merchant_id = 0;
        }
        if ($merchant_id > 0) {
            
            $data['merchant'] = Merchant::find($merchant_id);
            if((Auth::user()->user_group_id == 1) || (Auth::user()->user_group_id == 2) || (Auth::user()->user_group_id == 7) || (Auth::user()->user_group_id == 9)){//super admin & admin & Finance
                $data['merchant']->merchant_isownbank = $request['merchant_isownbank'];
                $data['merchant']->save();
            }
            if($data['merchant']->merchant_isownbank == 1){
                if(isset($request['merchantbank_bank_id']) && sizeof($request['merchantbank_bank_id']) > 0){
                    foreach($request['merchantbank_bank_id'] as $i => $bank_id){

                        DB::table('db_merchantbank')
                                ->updateOrInsert(
                                    ['merchantbank_bank_id' => $bank_id, 'merchantbank_merchant_id' => $merchant_id,'merchantbank_id' => $request['merchantbank_id'][$i]],
                                    ['merchantbank_account_holder' => $request['merchantbank_account_holder'][$i],
                                    'merchantbank_account_number' => $request['merchantbank_account_number'][$i],
                                    'merchantbank_status' => $request['merchantbank_status'][$i],
                                    'created_at' => Carbon::now(),
                                    'updated_at' => Carbon::now()]
                                );
                    }
                }
            }else{

                Merchantbankmaster::where('merchantbankmaster_merchant_id',$merchant_id)->delete();
                foreach($request['bankmaster_active_list'] as $i => $merchantbankmaster_bankmaster_id){
                    $merchantbankmaster = new Merchantbankmaster();
                    $merchantbankmaster->merchantbankmaster_merchant_id = $merchant_id;
                    $merchantbankmaster->merchantbankmaster_bankmaster_id = $merchantbankmaster_bankmaster_id;
                    $merchantbankmaster->merchantbankmaster_status = 1;
                    $merchantbankmaster->save();
                }
            }
            return response()->json([
                'status' => '1',
                'msg' => 'Updated Successfully.',
            ]);

        }else{
            return response()->json([
                'status' => '0',
                'msg' => 'Something went wrong, if you think this is an error please contact our support.',
            ]);
        }

    }


    public function manageSettlementBankMerchantForm(Request $request){
        
        if(!canAccess('view_settlementbank')){
            return response()->json([
                'status' => '0',
                'msg' => 'Permission Denied, if you think this is an error please contact our support.',
            ]);
        }
        if($request['merchant_id']){
            $merchant_id = Crypt::decryptString($request['merchant_id']);
        }else{
            $merchant_id = 0;
        }
        if($merchant_id > 0){
            $data['merchant'] = Merchant::find($merchant_id);
            $data['merchantbank_list'] = Merchantsettlementbank::where('merchantsettlementbank_merchant_id',$merchant_id)->whereIn('merchantsettlementbank_status',[1,2])->get();
            
            $data['bank_list'] = Bank::where('bank_settlement_status',1)->orderBy('bank_name','ASC')->get();
            

            return view('merchant.managesettlementbankmerchantform', ['data' => $data]);
        }else{
            return response()->json([
                'status' => '0',
                'msg' => 'Something went wrong, if you think this is an error please contact our support.',
            ]);
        }
    }
    public function addnewsettlementbankline(Request $request){

        $bank = Bank::where('bank_status',1)->orderBy('bank_seq','ASC')->get();
    
        $banklist = "<option value = ''>Select One</option>";
        foreach($bank as $c){
            $banklist .= "<option value = '{$c->bank_id}'>{$c->bank_name}</option>";  
        }

        $html = "<tr>";
        $html .= "<td><select class = 'form-control select2' name = 'merchantsettlementbank_bank_id[]'>$banklist</select></td>";
        $html .= "<td><input type = 'type' class = 'form-control' name = 'merchantsettlementbank_account_holder[]' placeholder = 'Account Holder'/></td>";
        $html .= "<td><input type = 'type' class = 'form-control merchantsettlementbank_account_number' name = 'merchantsettlementbank_account_number[]' placeholder = 'Account Number'/></td>";
        $html .= "<td><select class = 'form-control select2' name = 'merchantsettlementbank_status[]'><option value = '1' SELECTED>Active</option><option value = '2'>Inactive</option></select></td>";
        $html .= "<td><input type = 'hidden' name = 'merchantsettlementbank_id[]' value = ''/><a href = 'javascript:void(0)' title = 'Delete' class = 'btn btn-danger deletebankline' ><i class = 'fa fa-trash' style = 'font-size:20px;'></i></a></td>";
        $html .= "</tr>";
        return response()->json([
            'status' => '1',
            'html' => $html,
        ]);
    }
    public function deletesettlementbankline(Request $request){
        if(!canAccess('delete_settlementbank')){
            return response()->json([
                'status' => '0',
                'msg' => 'Permission Denied, if you think this is an error please contact our support.',
            ]);
        }

        if($request['merchant_id']){
            $merchant_id = Crypt::decryptString($request['merchant_id']);
        }else{
            $merchant_id = 0;
        }
        if ($merchant_id > 0) {
            $update_status = DB::table('db_merchantsettlementbank')
                                ->where('merchantsettlementbank_id', $request['merchantsettlementbank_id'])
                                ->where('merchantsettlementbank_merchant_id', $merchant_id)
                                ->update(['merchantsettlementbank_status' => 0]);
            if($update_status){
                return response()->json([
                    'status' => '1',
                    'msg' => 'Deleted Successfully.',
                ]);
            }else{
                return response()->json([
                    'status' => '0',
                    'msg' => 'Deleted Failed.',
                ]);
            }


        }else{
            return response()->json([
                'status' => '0',
                'msg' => 'Something went wrong, if you think this is an error please contact our support.',
            ]);
        }
    }

    public function merchantsettlementbankCreate(Request $request){
        if(!canAccess('create_settlementbank')){
            return response()->json([
                'status' => '0',
                'msg' => 'Permission Denied, if you think this is an error please contact our support.',
            ]);
        }

        if($request['merchant_id']){
            $merchant_id = Crypt::decryptString($request['merchant_id']);
        }else{
            $merchant_id = 0;
        }
        if ($merchant_id > 0) {
            if(isset($request['merchantsettlementbank_bank_id']) && sizeof($request['merchantsettlementbank_bank_id']) > 0){
                foreach($request['merchantsettlementbank_bank_id'] as $i => $bank_id){

                    DB::table('db_merchantsettlementbank')
                            ->updateOrInsert(
                                ['merchantsettlementbank_bank_id' => $bank_id, 'merchantsettlementbank_merchant_id' => $merchant_id,'merchantsettlementbank_id' => $request['merchantsettlementbank_id'][$i]],
                                ['merchantsettlementbank_account_holder' => $request['merchantsettlementbank_account_holder'][$i],
                                'merchantsettlementbank_account_number' => $request['merchantsettlementbank_account_number'][$i],
                                'merchantsettlementbank_status' => $request['merchantsettlementbank_status'][$i],
                                'created_at' => Carbon::now(),
                                'updated_at' => Carbon::now()]
                            );
                }
            }
            return response()->json([
                'status' => '1',
                'msg' => 'Updated Successfully.',
            ]);

        }else{
            return response()->json([
                'status' => '0',
                'msg' => 'Something went wrong, if you think this is an error please contact our support.',
            ]);
        }

    }
    public function validateapiendpoint(Request $request){

        $user = Merchant::where('merchant_api_endpoint', '=', $request->input('merchant_api_endpoint'))->where('merchant_id','!=',Crypt::decryptString($request['merchant_id']))->first();
       
        if ($user === null) {
            echo "true";
        } else {
            echo 'false';
        }
    }
}
