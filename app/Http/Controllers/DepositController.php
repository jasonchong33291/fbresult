<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Carbon\Carbon;
use App\Models\Merchant;
use App\Models\Deposit;
use App\Models\Bank;
use App\Models\BankMasterQR;
use App\Models\BankMaster;
use App\Models\Wallet;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;

class DepositController extends Controller
{
    public function listing(Request $request){

        $data['deposit'] = DB::table("db_deposit")->join('db_merchant as m','m.merchant_id','=','db_deposit.deposit_merchant_id');
        $data['bank'] = Bank::where('bank_backend_status',1)->get();
        $data['bankMasterQR'] = BankMasterQR::all();
        $data['walletlist'] = Wallet::all();
        // $data['deposit'] = $data['deposit']->get();
        // foreach($data['deposit'] as $d){
        //     $bb = BankMaster::where('bankmaster_account_number',$d->deposit_bankto_account_number)->get()->first();
        //     if($bb){
        //     Deposit::where('deposit_id',$d->deposit_id)->update(['deposit_bankmaster_id'=>$bb->bankmaster_id]);    
        //     }
            
        // }
        // dd($bb);
        if(Auth::user()->user_group_id == 3){//agent
            $data['deposit'] = $data['deposit']->where('m.merchant_agent_id','=',Auth::user()->id);
            $data['deposit'] = $data['deposit']->get();
            $data['merchant'] = DB::table("db_merchant")->where('merchant_agent_id',Auth::user()->id)->where('merchant_status',1)->get();

        }else if(Auth::user()->user_group_id == 6){//senior
            $data['deposit'] = $data['deposit']
            ->join('users as u','u.users.id','=','m.merchant_agent_id')
            ->where('u.user_parent','=',Auth::user()->id);
            $data['deposit'] = $data['deposit']->get();
            $data['merchant'] = DB::table("db_merchant")
                                    ->join('users','users.id','=','db_merchant.merchant_agent_id')
                                    ->where('merchant_status',1)
                                    ->where('user_parent',Auth::user()->id)
                                    ->get();

        }else if((Auth::user()->user_group_id == 1) || (Auth::user()->user_group_id == 2) || (Auth::user()->user_group_id == 7) || (Auth::user()->user_group_id == 9)){//super admin & admin
            $data['deposit'] = $data['deposit']->get();
            $data['merchant'] = Merchant::whereIN('merchant_status',[1,2])->get();
        }else{
            $data['deposit'] = [];
            $data['merchant'] = [];
        }
        
        $data['deposit_mode'] = $request['deposit_mode'];
        $data['filter_date_from'] = $request->get("filter_date_from");
        $data['filter_date_to'] = $request->get("filter_date_to");
        
        return view('deposit.depositlisting', ['data' => $data]);
    }
    public function view(Request $request){
        if($request['deposit_id']){
            $deposit_id = Crypt::decryptString($request['deposit_id']);
        }else{
            $deposit_id = 0;
        }

        if($deposit_id > 0) { 
            $data['deposit'] = Deposit::
            select('b1.bank_name as from_bank','b2.bank_name as to_bank','bqr.bankmasterqr_name','db_merchant.merchant_code','db_deposit.*')
            ->leftjoin('db_merchant','db_merchant.merchant_id','=','db_deposit.deposit_merchant_id')
            ->leftjoin('db_bank as b1','b1.bank_id','=','db_deposit.deposit_bankfrom_id')
            ->leftjoin('db_bank as b2','b2.bank_id','=','db_deposit.deposit_bankto_id')
            ->leftjoin('db_bankmasterqr as bqr','bqr.bankmasterqr_id','=','db_deposit.deposit_qrto')
            ->where('db_deposit.deposit_id',$deposit_id)	
            ->get()->first();
            $data['screenshots'] = [];
            if($data['deposit']){
                if($data['deposit']->deposit_ftp != null){
                    $data['date'] = date('Ymd',strtotime($data['deposit']->deposit_date));
                    $data['screenshots'] = Storage::disk($data['deposit']->deposit_ftp)
						->allFiles($data['date'] . '/' . $data['deposit']->deposit_api_id);
					//dd($data['screenshots']);
                }
            }

            return view('deposit.depositdetail', ['data' => $data]);
        }else{
            return response()->json([
                'status' => '0',
                'msg' => 'Record not found, if you think this is an error please contact our support.',
            ]);
        }
    }

    public function filter(Request $request){
        // dd($request);

        ## Read value
        $draw = $request->get('draw');
        $start = $request->get("start");
        $rowperpage = $request->get("length"); // Rows display per page

        $columnIndex_arr = $request->get('order');
        $columnName_arr = $request->get('columns');
        $order_arr = $request->get('order');
        $search_arr = $request->get('filter_text');

        $columnIndex = $columnIndex_arr[0]['column']; // Column index
        $columnName = $columnName_arr[$columnIndex]['data']; // Column name
        $columnSortOrder = $order_arr[0]['dir']; // asc or desc
        $searchValue = $search_arr; // Search value

     
        $from = $request['from'] ? Carbon::createFromFormat('d/m/Y', $request['from'])->format('Y-m-d') : null;
        $to = $request['to'] ? Carbon::createFromFormat('d/m/Y', $request['to'])->format('Y-m-d') : null;
        $salesperson = $request['sales'];
        $filter_status = $request['filter_status'];
        $deposit_mode = $request['deposit_mode'];
        $filter_merchant = $request['filter_merchant'];
        $filter_bank_from = $request['filter_bank_from'];
        $filter_bank_to = $request['filter_bank_to'];
        $filter_banktype = $request['filter_banktype'];
        $filter_qrto = $request['filter_qrto'];
        
        if($request['route'] == "depositp2p"){
            $columns = array(
                0 => 'deposit_date',
                1 => 'deposit_no',
                2 => 'merchant_code',
                3 => 'deposit_merchant_refno1',
                4 => 'deposit_merchant_refno2',
                5 => 'from_bank',
                6 => 'to_bank',
                7 => 'deposit_amount',
                8 => 'deposit_merchant_fee',
                9 => 'deposit_nett_amount',
                10 => 'deposit_status',
                11 => 'action'
            );
        }else{
            $columns = array(
                0 => 'deposit_date',
                1 => 'deposit_no',
                2 => 'merchant_code',
                3 => 'deposit_merchant_refno1',
                4 => 'deposit_merchant_refno2',
                5 => 'to_bank',
                6 => 'deposit_amount',
                7 => 'deposit_merchant_fee',
                8 => 'deposit_nett_amount',
                9 => 'deposit_status',
                10 => 'action'
            );
        }


        $dir = '';

        if ($columnIndex_arr) {
            $order_by = $columns[$columnIndex_arr[0]['column']];
            $dir = $columnSortOrder;
        }

        if($from == ''){
            $from = "0000-00-00";
        }

        if($to == ''){
            $to = "2040-12-31";
        }

        $wherestring = " AND deposit_mode = '$deposit_mode'";

        $wherestring .= " AND LEFT(deposit_date,10) BETWEEN '$from' AND '$to'";

        if($filter_status != ""){
            $wherestring .= " AND deposit_status = '$filter_status'";
        }
        if($filter_merchant > 0){
            $wherestring .= " AND m.merchant_id = '$filter_merchant'";
        }
        if($filter_bank_from > 0){
            $wherestring .= " AND deposit_bankfrom_id = '$filter_bank_from'";
        }
        if($filter_bank_to > 0){
            $wherestring .= " AND deposit_bankto_id = '$filter_bank_to'";
        }
        if($filter_qrto > 0){
            $wherestring .= " AND deposit_qrto = '$filter_qrto'";
        }
        if($filter_banktype != ""){
            $wherestring .= " AND deposit_banktype = '$filter_banktype'";
        }
        
        $order = DB::table('db_deposit as d')
            ->select('d.deposit_id','d.deposit_date','d.deposit_no','d.deposit_mode','d.deposit_merchant_refno1','d.deposit_merchant_refno2',
            'd.deposit_currency','d.deposit_amount','d.deposit_merchant_fee','d.deposit_nett_amount','d.deposit_status','deposit_bankto_account_number',
            'b.bank_name as from_bank','bb.bank_name as to_bank','m.merchant_code','d.deposit_api_id','wa.wallet_name','bqr.bankmasterqr_name',
            'd.deposit_internalstatuscode'
            )
            ->leftjoin('db_merchant as m','m.merchant_id','=','d.deposit_merchant_id')
            ->leftjoin('db_bank as b','b.bank_id','=','d.deposit_bankfrom_id')
            ->leftjoin('db_bank as bb','bb.bank_id','=','d.deposit_bankto_id')
            ->leftjoin('db_bankmasterqr as bqr','bqr.bankmasterqr_id','=','d.deposit_qrto')
            ->leftjoin('db_wallet as wa','wa.wallet_code','=','d.deposit_banktype')
            ->whereRaw("d.deposit_id > 0 $wherestring");



        if ($searchValue !== NULL) {
            $order = $order->where(function ($query) use ($searchValue) {
                $query->where('d.deposit_no', 'LIKE', '%' . $searchValue . '%')
                    ->orWhere('d.deposit_merchant_refno1', 'like', '%' . $searchValue  . '%')
                    ->orWhere('d.deposit_merchant_refno2', 'like', '%' . $searchValue  . '%')
                    ->orWhere('d.deposit_date', 'like', '%' . $searchValue  . '%')
                    ->orWhere('d.deposit_amount', 'like', '%' . $searchValue  . '%')
                    ->orWhere('d.deposit_merchant_fee', 'like', '%' . $searchValue  . '%')
                    ->orWhere('d.deposit_bankto_account_number', 'like', '%' . $searchValue  . '%');
            });
        }

        if(Auth::user()->user_group_id == 3){//agent
            $order = $order->where('m.merchant_agent_id','=',Auth::user()->id);

        }else if(Auth::user()->user_group_id == 6){//senior
            $order = $order
            ->join('users as u','u.users.id','=','m.merchant_agent_id')
            ->where('u.user_parent','=',Auth::user()->id);

        }else if((Auth::user()->user_group_id == 1) || (Auth::user()->user_group_id == 2) || (Auth::user()->user_group_id == 7) || (Auth::user()->user_group_id == 9)){//super admin & admin & Finance

        }else if((Auth::user()->user_group_id == 4) || (Auth::user()->user_group_id == 8) || (Auth::user()->user_group_id == 10) || (Auth::user()->user_group_id == 11)){//merchant & merchant support
            $order = $order->where('d.deposit_merchant_id','=',Auth::user()->user_merchant_id);
        }else{
            $order = $order->where('deposit_id','=','0');
        }

        $totalRecords = $order->count();

        $order_bottom_data = $order->get();
        $order = $order->take($rowperpage)->skip($start);


        
        if ($order_by && $dir) {
            $order = $order->orderBy($order_by, $dir)->get();
        } else {
            $order = $order->orderBy('d.deposit_id', 'desc')->get();
        }

        $i = 0;
        $output = array();

        if($request['route'] == "depositp2p"){
            $order_column = array(
                'deposit_date',
                'deposit_no',
                'merchant_code',
                'deposit_merchant_refno1',
                'deposit_merchant_refno2',
                'from_bank',
                'to_bank',
                'deposit_amount',
                'deposit_merchant_fee',
                'deposit_nett_amount',
                'deposit_status',
                'action'
            );
        }else{
            $order_column = array(
                'deposit_date',
                'deposit_no',
                'merchant_code',
                'deposit_merchant_refno1',
                'deposit_merchant_refno2',
                'to_bank',
                'deposit_amount',
                'deposit_merchant_fee',
                'deposit_nett_amount',
                'deposit_status',
                'action'
            );
        }


        foreach ($order as $c) {

            foreach ($order_column as $key => $column) {
                if(canAccess('action_deposit')){
                    $action = ' <div style = "display:flex">';
                    if(($c->deposit_status == 'Failled') || ($c->deposit_status == 'Processing')) {

                        $action .= ' <a href="javascript:void(0)" class="btn btn-sm btn-success set_action_class" pid = "success" line= "'.$i.'" deposit_id="'.Crypt::encryptString($c->deposit_id).'">Set Success</a>';
                    }else if(($c->deposit_status == 'Success') || ($c->deposit_status == 'Processing')) {     
                        $action .= ' <a href="javascript:void(0)" class="btn btn-sm btn-danger set_action_class" pid = "fail" line= "'.$i.'" deposit_id="'.Crypt::encryptString($c->deposit_id).'">Set Fail</a>';
                        
                        $action .= '</div>';
                    }
                }else{
                    $action = ""; 
                }
                if ($column == 'deposit_date') {
                    $d_slit = explode(' ',$c->deposit_date);
                    $output[$i][] = $d_slit[0] . "<br>" . $d_slit[1] . "<div  deposit_id = '".Crypt::encryptString($c->deposit_id)."'></div>";
                } elseif ($column == 'deposit_no') {
                    $output[$i][] = $c->deposit_no;
                } elseif ($column == 'deposit_mode') {
                    $output[$i][] = $c->deposit_mode;
                } elseif ($column == 'merchant_code') {
                    $output[$i][] = $c->merchant_code;
                } elseif ($column == 'deposit_merchant_refno1') {
                    $output[$i][] = $c->deposit_merchant_refno1;
                } elseif ($column == 'deposit_merchant_refno2') {
                    $output[$i][] = $c->deposit_merchant_refno2;
                } elseif ($column == 'from_bank') {
                    $output[$i][] = $c->from_bank;
                } elseif ($column == 'to_bank') {
                    if($c->deposit_mode == 'qr'){
                        if((Auth::user()->user_group_id == 1) || (Auth::user()->user_group_id == 2) || (Auth::user()->user_group_id == 7) || (Auth::user()->user_group_id == 9)){
                            $output[$i][] = $c->wallet_name . "<br><br><span style = 'color:red'>QR NAME : " . $c->bankmasterqr_name . "</span>";
                        }else{
                            $output[$i][] = '';
                        }
                    }else{
                        $output[$i][] = $c->to_bank . "<br><br>" . $c->deposit_bankto_account_number;
                    }
                    

                } elseif ($column == 'deposit_bankto_account_number') {
                    $output[$i][] = $c->deposit_bankto_account_number;
                } elseif ($column == 'deposit_currency') {
                    $output[$i][] = $c->deposit_currency;
                } elseif ($column == 'deposit_amount') {
                    $output[$i][] = "RM" . num_format($c->deposit_amount);
                } elseif ($column == 'deposit_merchant_fee') {
                    $output[$i][] = "RM" . num_format($c->deposit_merchant_fee);
                } elseif ($column == 'deposit_nett_amount') {
                    $output[$i][] = "RM" . num_format($c->deposit_nett_amount);
                } elseif ($column == 'action') {
                    $output[$i][] = $action;
                } elseif ($column == 'deposit_status') {
                    if ($c->deposit_status == 'Success') {
                        $output[$i][] =  '<span class="badge badge-success action_status'.$i.'">Success</span>';
                    } elseif ($c->deposit_status == 'Registered') {
                        $output[$i][] =  '<span class="badge badge-warning action_status'.$i.'">Registered</span>';
                    } elseif (($c->deposit_status == 'Failled') || ($c->deposit_status == 'Cancelled')) {
                        $output[$i][] =  '<span class="badge badge-danger action_status'.$i.'">'.$c->deposit_status.'</span>';
                    } elseif ($c->deposit_status == 'Processing') {
                        $output[$i][] =  '<span class="badge badge-violet action_status'.$i.'">Processing</span>';
                    } else {
                        $output[$i][] =  '<span class="badge badge-black action_status'.$i.'">Unknown</span>';
                    }
                }
            }
            $i++;
        }

        $order_collection = collect($order_bottom_data);

        $response = array(
            'draw' => intval(isset($draw) ? $draw : 0),
            'recordsTotal' => intval(isset($totalRecords) ? $totalRecords : 0),
            'recordsFiltered' => intval(isset($totalRecords) ? $totalRecords : 0),
            'data' => $output,
            'total_success_count' => num_format($order_collection->where('deposit_internalstatuscode','80001')->count()),
            'total_deposit_amount' => num_format($order_collection->where('deposit_internalstatuscode','80001')->sum('deposit_amount')),
            'deposit_merchant_fee' => num_format($order_collection->where('deposit_internalstatuscode','80001')->sum('deposit_merchant_fee')),
            'deposit_nett_amount' => num_format($order_collection->where('deposit_internalstatuscode','80001')->sum('deposit_nett_amount')),
        );

        return $response;
    }
    public function export(Request $request){
        $filter_date_from = $from = $request['filter_date_from']; 
        $filter_date_to = $request['filter_date_to'] ; 
        $filter_bank_from = $request['filter_bank_from'] ; 
        $filter_bank_to = $request['filter_bank_to']; 
        $filter_status = $request['filter_status']; 
        $filter_deposit_mode = $request['filter_deposit_mode']; 

        if(($filter_date_from == "") || ($filter_date_from == null)){
            $filter_date_from = "0000-00-00";
        }

        if(($filter_date_to == "") || ($filter_date_to == null)){
            $filter_date_to = "2040-12-31";
        } 

        if($filter_date_from != null){
            $wherestring = " AND LEFT(d.deposit_date,10) BETWEEN '$filter_date_from' AND '$filter_date_to' ";
        }else{
            $wherestring = "";
        }
       
        if($filter_bank_from > 0){
            $wherestring .= " AND d.deposit_bankfrom_id = '$filter_bank_from'";
        }

        if($filter_bank_to > 0){
            $wherestring .= " AND d.deposit_bankto_id = '$filter_bank_to'";
        }

        if($filter_status != ''){
            $wherestring .= " AND d.deposit_status = '$filter_status'";
        }

        $wherestring .= " AND d.deposit_mode = '$filter_deposit_mode'";

            $data = DB::select("

                select d.*,m.merchant_code,m.merchant_name,bf.bank_name as bank_name_from,bt.bank_name as bank_name_to

                FROM db_deposit d 
                LEFT JOIN db_bank bf on bf.bank_id = d.deposit_bankfrom_id
                LEFT JOIN db_bank bt on bt.bank_id = d.deposit_bankto_id
                LEFT JOIN db_merchant m on m.merchant_id = d.deposit_merchant_id
                where d.deposit_id > 0  $wherestring 

                ORDER BY d.deposit_no
            ");

            $list = array();
            foreach($data as $i => $result){
                $list[] = array(
                    'DEPOSIT DATE/TIME (START)'            => $result->deposit_date,
                    'DEPOSIT DATE/TIME (END)'            => $result->deposit_enddate,
                    'DEPOSIT ID'            => $result->deposit_no,
                    'MERCHANT CODE'            => $result->merchant_code,
                    'CUSTOMER REF NO.'  => $result->deposit_merchant_refno1,
                    'CUSTOMER ID/NAME'            => $result->deposit_merchant_refno2,
                    'FROM BANK'              =>$result->bank_name_from,
                    'TO BANK'  => $result->bank_name_to,
                    'TO ACCOUNT HOLDER'  => $result->deposit_bankto_account_holder . " (" . $result->deposit_bankto_account_number . ")",
                    'DEPOSIT AMOUNT'         => $result->deposit_amount,
                    'MERCHANT FEE'          => $result->deposit_merchant_fee,
                    'NET AMOUNT'         => $result->deposit_nett_amount,
                    'STATUS'         => $result->deposit_status
                );
            }
         
            $filename =  "deposit_" . Carbon::now().".xls";
            header("Content-Type: application/vnd.ms-excel");
            header("Content-Disposition: attachment; filename=\"$filename\"");

            $this->ExportFile($list);
    }
    function ExportFile($records){
        $heading = false;
        if (!empty($records))
            foreach ($records as $row) {
                if (!$heading) {
                    // display field/column names as a first row
                    echo implode("\t", array_keys($row)) . "\n";
                    $heading = true;
                }
                echo implode("\t", array_values($row)) . "\n";
            }
        exit;
    }

    public function action(Request $request){

        if($request['deposit_id']){
            $deposit_id = Crypt::decryptString($request['deposit_id']);
        }else{
            $deposit_id = 0;
        }
        if($deposit_id > 0) { 

            if(!canAccess('action_deposit')){
                return response()->json([
                    'status' => '0',
                    'msg' => 'Permission Denied, if you think this is an error please contact our support.',
                ]);
            }
            date_default_timezone_set("Asia/Singapore");
           
            if($request['action'] == 'success'){
                $deposit = Deposit::find($deposit_id);
                $deposit->deposit_status = 'Success';
                $deposit->deposit_internalstatus = 'Success';
                $deposit->deposit_internalstatuscode = '80001';

                $data = array('updatedby'=>Auth::user()->id,'updatedbyname'=>Auth::user()->user_name,'updateddatetime'=>date("Y-m-d h:i:s"));

                $deposit->deposit_bankresponse = json_encode($data);
                $deposit->save();
            }else{
                $deposit = Deposit::find($deposit_id);
                $deposit->deposit_status = 'Failled';
                $deposit->deposit_internalstatus = 'Failled';
                $deposit->deposit_internalstatuscode = '80002';

                $data = array('updatedby'=>Auth::user()->id,'updateddatetime'=>date("Y-m-d h:i:s"));

                $deposit->deposit_bankresponse = json_encode($data);
                $deposit->save();
            }

            if($request['ispostback'] == 1){//require postback

                $merchant = Merchant::find($deposit->deposit_merchant_id);
                
                if($merchant->merchant_api_endpoint != null){

                    $from_bank = Bank::find($deposit->deposit_bankfrom_id);
                    if($from_bank){
                        $from_bank_name = $from_bank->bank_code;
                    }else{
                        $from_bank_name = "";
                    }

                    $to_bank = Bank::find($deposit->deposit_bankto_id);
                    if($to_bank){
                        $to_bank_name = $to_bank->bank_code;
                    }else{
                        $to_bank_name = "";
                    }

                    if($deposit->deposit_internalstatuscode == '80001'){
                        $deposit_internalstatuscode = '80001';
                    }else{
                        $deposit_internalstatuscode = '80002';
                    }

                    $data['merchant_code'] = $merchant->merchant_code;
                    $data['deposit_merchant_refno1'] = $deposit->deposit_merchant_refno1;
                    $data['deposit_merchant_refno2'] = $deposit->deposit_merchant_refno2;
                    $data['deposit_amount'] = $deposit->deposit_amount;
                    $data['deposit_currency'] = $deposit->deposit_currency;
                    $data['deposit_country'] = $deposit->deposit_country;
                    $data['deposit_from_bank'] = $from_bank_name;
                    $data['deposit_to_bank'] = $to_bank_name;
                    $data['deposit_status'] = $deposit_internalstatuscode;
                    $data['deposit_encoded_string'] = $deposit->deposit_encoded_string;
                    
                    $curl = curl_init();

                    curl_setopt_array($curl, array(
                        CURLOPT_URL => $merchant->merchant_api_endpoint,
                        CURLOPT_RETURNTRANSFER => true,
                        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                        CURLOPT_CUSTOMREQUEST => "POST",
                        CURLOPT_POSTFIELDS => json_encode($data),
                        CURLOPT_HTTPHEADER => array(
                            'Content-Type: application/json'
                          ),
                    ));
            
                    $response = curl_exec($curl);
                    $err = curl_error($curl);
            
                    curl_close($curl);
                    $info = json_decode($response, true);
                }
        
                
            }

            return response()->json([
                'status' => '1',
                'msg' => 'Updated Successfully.',
            ]);
        }else{
            return response()->json([
                'status' => '0',
                'msg' => 'Record not found, if you think this is an error please contact our support.',
            ]);
        }
    }
}
