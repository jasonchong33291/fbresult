<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\Deposit;
use App\Models\Settlement;
use App\Models\League;
use App\Models\Leaguetable;
use App\Models\Team;
use App\Models\TeamSquad;
use App\Models\TeamMember;
use App\Models\Upcomingevent;
use Log;

class ApiController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->token = '147559-CsmUOG0HqV3FzS';
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\View\View
     */
    public function cronresetdeposit(Request $request){
       

        $null_deposit = Deposit::whereNull('deposit_enddate')->where('deposit_status','Processing')->get();

        foreach($null_deposit as $c){
            $deposit_date = Carbon::parse($c->deposit_date)->addMinutes(3);
            $now = Carbon::now();

            $json['api_status'] = '80002';
            $json['requestId'] = '80002';
            $json['msg'] = 'Processing time exceeded';
            $json['amount'] = $c->deposit_amount;
            $json['reference'] = $c->deposit_api_id;
            $json['status'] = 'Failled';

            if(strtotime($now) > strtotime($deposit_date)){
                Deposit::where('deposit_id',$c->deposit_id)->update(['deposit_enddate'=>$now,
                'deposit_status'=>'Failled',
                'deposit_internalstatus'=>'Failled',
                'deposit_internalstatuscode'=>'80002',
                'deposit_updatestatus_json'=>json_encode($json)]);
            }
        }
        
    }

    public function getLeague(Request $request){
        // $cc = "hk";
        //'hk','kr','mo','my','sg','us'
        for($i=0;$i<30;$i++){
            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => 'https://api.b365api.com/v1/league?token='.$this->token.'&sport_id=1&page='.$i,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);
            $info = json_decode($response, true);
            if($info['results']){
                foreach($info['results'] as $d){ 
                    if($d['has_leaguetable'] == 0){
                        continue;
                    }
                    $league_count = League::where('league_api_id',$d['id'])->count();
                    
                    if($league_count > 0){
                        $league = League::where('league_api_id',$d['id'])->get()->first();
                        $league->league_api_id = $d['id'];
                        $league->league_api_name = $d['name'];
                        $league->league_api_country = $d['cc'];
                        $league->save();
                    }else{
                        $league = new League();
                        $league->league_api_id = $d['id'];
                        $league->league_api_name = $d['name'];
                        $league->league_api_country = $d['cc'];
                        $league->save();
                    }
                }
            }
        }
    }

    public function getLeagueTable(Request $request){

        $LNG_ID = $request['LNG_ID'];
        if($LNG_ID == ""){
            $LNG_ID = 1;
        }
        $league = League::all();

        foreach($league as $l){
            if($l->league_id == '756'){
                continue;
            }
            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => 'https://api.b365api.com/v2/league/table?token='.$this->token.'&league_id='.$l->league_api_id."&LNG_ID=".$LNG_ID,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
            ));
    
            $response = curl_exec($curl);
            $err = curl_error($curl);
    
            curl_close($curl);
            $info = json_decode($response, true);

            
            if($info['results']){
                
                $leaguetable_count = Leaguetable::where('leaguetable_league_id',$l->league_id)->where('leaguetable_lang',$LNG_ID)->count();
                
                if($leaguetable_count > 0){
                    $Leaguetable = Leaguetable::where('leaguetable_league_id',$l->league_id)->where('leaguetable_lang',$LNG_ID)->get()->first();
                    $Leaguetable->leaguetable_season = json_encode($info['results']['season']);
                    $Leaguetable->leaguetable_overall = json_encode($info['results']['overall']);
                    $Leaguetable->leaguetable_home = json_encode($info['results']['home']);
                    $Leaguetable->leaguetable_away = json_encode($info['results']['away']);
                    $Leaguetable->save();
                }else{
                    
                    $Leaguetable = new Leaguetable();
                    $Leaguetable->leaguetable_season = json_encode($info['results']['season']);
                    $Leaguetable->leaguetable_overall = json_encode($info['results']['overall']);
                    $Leaguetable->leaguetable_home = json_encode($info['results']['home']);
                    $Leaguetable->leaguetable_away = json_encode($info['results']['away']);
                    $Leaguetable->leaguetable_league_id = $l->league_id;
                    $Leaguetable->leaguetable_lang = $LNG_ID;
                    $Leaguetable->save();
                    
                }
                
            }
        }

    }

    public function getTeam(Request $request){

        for($i=0;$i<30;$i++){
            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => 'https://api.b365api.com/v1/team?token='.$this->token.'&sport_id=1&page='.$i,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);
            $info = json_decode($response, true);
            // dd($info);
            if($info['results']){
                foreach($info['results'] as $d){ 
                    if(isset($d['has_squad']) && ($d['has_squad'] == 1)){
                        
                        $team_count = Team::where('team_api_id',$d['id'])->count();
                        
                        if($team_count > 0){
                            $team = Team::where('team_api_id',$d['id'])->get()->first();
                            $team->team_api_id = $d['id'];
                            $team->team_api_name = $d['name'];
                            $team->team_api_country = $d['cc'];
                            $team->team_api_image = $d['image_id'];
                            $team->save();
                        }else{
                            $team = new Team();
                            $team->team_api_id = $d['id'];
                            $team->team_api_name = $d['name'];
                            $team->team_api_country = $d['cc'];
                            $team->team_api_image = $d['image_id'];
                            $team->save();
                        }
                    }
                }
            }
        }
    }

    public function getTeamSquad(Request $request){

        
        $Upcomingevent = Upcomingevent::all();
       
        foreach($Upcomingevent as $l){ 
            // dd($l['leaguetable_overall']);
            $dd = json_decode($l['upcomingevent_api_home'],true);
            // $dd = json_decode($l['upcomingevent_api_away'],true);
            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => 'https://api.b365api.com/v1/team/squad?token='.$this->token.'&team_id='.$dd['id'] . "&LNG_ID=". $l['upcomingevent_api_lang'],
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
            ));
    
            $response = curl_exec($curl);
            $err = curl_error($curl);
    
            curl_close($curl);
            $info = json_decode($response, true);
            // dd($info);
            
            if($info && $info['results']){
                foreach($info['results'] as $d){ 
                    $teamsquad_count = Teamsquad::where('teamsquad_api_id',$l->team_api_id)->count();
                    
                    if($teamsquad_count > 0){
                        $Teamsquad = Teamsquad::where('teamsquad_api_id',$l->team_api_id)->get()->first();
                        $Teamsquad->teamsquad_api_name = $d['name'];
                        $Teamsquad->teamsquad_api_country = $d['cc'];
                        $Teamsquad->teamsquad_api_birthday = $d['birthdate'];
                        $Teamsquad->teamsquad_api_position = $d['position'];
                        $Teamsquad->teamsquad_api_height = $d['height'];
                        $Teamsquad->teamsquad_api_weight = $d['weight'];
                        $Teamsquad->teamsquad_api_foot = $d['foot'];
                        $Teamsquad->teamsquad_api_shirtnumber = $d['shirtnumber'];
                        $Teamsquad->teamsquad_api_marketvalue = $d['marketvalue'];
                        $Teamsquad->teamsquad_api_membersince = $d['membersince'];
                        $Teamsquad->teamsquad_api_lang  = $l['upcomingevent_api_lang'];
                        $Teamsquad->save();
                    }else{
                        
                        $Teamsquad = new Teamsquad();
                        $Teamsquad->teamsquad_api_id = $d['id'];
                        $Teamsquad->teamsquad_api_name = $d['name'];
                        $Teamsquad->teamsquad_api_country = $d['cc'];
                        $Teamsquad->teamsquad_api_birthday = $d['birthdate'];
                        $Teamsquad->teamsquad_api_position = $d['position'];
                        $Teamsquad->teamsquad_api_height = $d['height'];
                        $Teamsquad->teamsquad_api_weight = $d['weight'];
                        $Teamsquad->teamsquad_api_foot = $d['foot'];
                        $Teamsquad->teamsquad_api_shirtnumber = $d['shirtnumber'];
                        $Teamsquad->teamsquad_api_marketvalue = $d['marketvalue'];
                        $Teamsquad->teamsquad_api_membersince = $d['membersince'];
                        $Teamsquad->teamsquad_team_id = $dd['id'];
                        $Teamsquad->teamsquad_upcomingevent_id  = $l['upcomingevent_id'];
                        $Teamsquad->teamsquad_api_lang  = $l['upcomingevent_api_lang'];
                        $Teamsquad->save();
                        
                    }
                }
                
            }
               
           
        }

    }

    public function getTeammember(Request $request){
        // Upcomingevent::where('upcomingevent_api_date','<=','2023-01-13 05:00:00')->update(['upcomingevent_api_ended'=>1]);
        
        $team = Team::all();

        foreach($team as $l){
            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => 'https://api.b365api.com/v1/team/members?token='.$this->token.'&team_id='.$l->team_api_id,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
            ));
    
            $response = curl_exec($curl);
            $err = curl_error($curl);
    
            curl_close($curl);
            $info = json_decode($response, true);
             
            
            if($info['results']){
                foreach($info['results'] as $d){ 
                    $teamsquad_count = Teamsquad::where('teammember_api_id',$l->team_api_id)->count();
                    
                    if($teamsquad_count > 0){
                        $Teamsquad = Teamsquad::where('teammember_api_id',$l->team_api_id)->get()->first();
                        $Teamsquad->teammember_api_name = $d['name'];
                        $Teamsquad->teammember_api_image = $d['cc'];
                        $Teamsquad->teammember_api_country = $d['birthdate'];
                        $Teamsquad->save();
                    }else{
                        
                        $Teamsquad = new Teamsquad();
                        $Teamsquad->teammember_api_id = $d['id'];
                        $Teamsquad->teammember_api_name = $d['name'];
                        $Teamsquad->teammember_api_image = $d['cc'];
                        $Teamsquad->teammember_api_country = $d['birthdate'];
                        $Teamsquad->teammember_team_id = $l->team_id;
                        $Teamsquad->save();
                        
                    }
                }
                
            }
        }

    } 

    public function getUpcomingEvent(Request $request){

        $LNG_ID = $request['LNG_ID'];
        if($LNG_ID == ""){
            $LNG_ID = 1;
        }
        $league = League::all();
        // $league = League::where('league_id',742)->get();

        foreach($league as $l){
            
            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => 'https://api.b365api.com/v3/events/upcoming?token='.$this->token.'&sport_id=1&league_id='.$l->league_api_id."&LNG_ID=".$LNG_ID,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
            ));
    
            $response = curl_exec($curl);
            $err = curl_error($curl);
    
            curl_close($curl);
            $info = json_decode($response, true);
            // date('Y-m-d',$info['results'][0]['time'])
            
            if($info['results']){
                foreach($info['results'] as $d){
                    if(!isset($d['round'])){
                        $d['round'] = 0;
                    }
                
                    $Upcomingevent_count = Upcomingevent::where('upcomingevent_api_id',$d['id'])->where('upcomingevent_api_lang',$LNG_ID)->count();
                    

                    if($Upcomingevent_count > 0){
                        $Upcomingevent = Upcomingevent::where('upcomingevent_api_id',$d['id'])->where('upcomingevent_api_lang',$LNG_ID)->get()->first();
                        $Upcomingevent->upcomingevent_api_date = date('Y-m-d H:i:s',$d['time']);
                        $Upcomingevent->upcomingevent_api_league = json_encode($d['league']);
                        $Upcomingevent->upcomingevent_api_home = json_encode($d['home']);
                        $Upcomingevent->upcomingevent_api_away = json_encode($d['away']);
                        $Upcomingevent->upcomingevent_api_round = $d['round'];
                        $Upcomingevent->upcomingevent_api_score = '0 - 0';
                        $Upcomingevent->save();
                    }else{
                        
                        $Upcomingevent = new Upcomingevent();
                        $Upcomingevent->upcomingevent_api_id = $d['id'];
                        $Upcomingevent->upcomingevent_api_date = date('Y-m-d H:i:s',$d['time']);
                        $Upcomingevent->upcomingevent_api_league = json_encode($d['league']);
                        $Upcomingevent->upcomingevent_api_home = json_encode($d['home']);
                        $Upcomingevent->upcomingevent_api_away = json_encode($d['away']);
                        $Upcomingevent->upcomingevent_api_round = $d['round'];
                        $Upcomingevent->upcomingevent_api_score = '0 - 0';
                        $Upcomingevent->upcomingevent_league_id = $l->league_id;
                        $Upcomingevent->upcomingevent_api_lang = $LNG_ID;
                        $Upcomingevent->save();
                        
                    }
                }
                
            }
        }

    }

    public function getInplayEvent(Request $request){

        $LNG_ID = $request['LNG_ID'];
        if($LNG_ID == ""){
            $LNG_ID = 1;
        }
        $league = League::all();

        foreach($league as $l){
            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => 'https://api.b365api.com/v3/events/inplay?token='.$this->token.'&sport_id=1&LNG_ID='.$LNG_ID . "&league_id=".$l->league_api_id,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
            ));
    
            $response = curl_exec($curl);
            $err = curl_error($curl);
    
            curl_close($curl);
            $info = json_decode($response, true);
            // date('Y-m-d',$info['results'][0]['time'])
            $count = 0;
            if($info){
                if(isset($info['results'])){
                    if($info['results']){
                        $count++;
                        foreach($info['results'] as $d){
                            Upcomingevent::where('upcomingevent_api_id',$d['id'])->update(['upcomingevent_api_inplay'=>1]);
                        }
                    }
                }
                
            }
        }
        echo 'done : ' . $count++;
    }
    public function getEndedEvent(Request $request){

        $LNG_ID = $request['LNG_ID'];
        if($LNG_ID == ""){
            $LNG_ID = 1;
        }
        $league = League::all();
        $count = 0;
        foreach($league as $l){
            $curl = curl_init();
            
            curl_setopt_array($curl, array(
                CURLOPT_URL => "https://api.b365api.com/v3/events/ended?token=".$this->token."&sport_id=1&LNG_ID=".$LNG_ID . "&league_id=".$l->league_api_id . "&day=" . date('Ymd'),
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
            ));
    
            $response = curl_exec($curl);
            $err = curl_error($curl);
    
            curl_close($curl);
            $info = json_decode($response, true);
            // date('Y-m-d',$info['results'][0]['time'])
            //  dd($info);
            if($info){
                if(isset($info['results'])){
                    if($info['results']){
                        $count++;
                        foreach($info['results'] as $d){
                            Upcomingevent::where('upcomingevent_api_id',$d['id'])->update(['upcomingevent_api_ended'=>1,'upcomingevent_api_inplay'=>0]);
                        }
                    }
                }
                
            }
        }
        echo 'done : ' . $count++;

    }
    public function getEventView(Request $request){

        Log::debug('IN ' . date("Y-m-d H:i:s")); 


        $LNG_ID = $request['LNG_ID'];
        if($LNG_ID == ""){
            $LNG_ID = 1;
        }
		
        $upcomingevent = Upcomingevent::where('upcomingevent_api_lang',$LNG_ID)->where('upcomingevent_api_inplay','1')
        ->get();
        // dd($upcomingevent);
		
        // $upcomingevent = Upcomingevent::where('upcomingevent_league_id','742')
        // ->where('upcomingevent_api_lang',$LNG_ID)
        // ->get();

        $event_id = "";
        foreach($upcomingevent as $l){

            // $event_id .= $l->upcomingevent_api_id . ",";
            $multiple_event_id = $l->upcomingevent_api_id;
                
            // $multiple_event_id = rtrim($event_id, ',');
            // $multiple_event_id = 5906453 . "," . 600578;
            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => 'https://api.b365api.com/v1/event/view?token='.$this->token.'&event_id=' . $multiple_event_id . '&LNG_ID='.$LNG_ID,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);
            $info = json_decode($response, true);

            if($info){
                if(isset($info['results'])){
                    foreach($info['results'] as $d){
                        if(!$d['ss']){
                            $d['ss'] = "0 - 0";
                        }else{
                            $ee = explode('-',$d['ss']);
                            if($ee){
                                $d['ss'] = $ee[0] . " - " . $ee['1'];
                            }else{
                                $d['ss'] = "0 - 0";
                            }
                            
                        }
                        $Upcomingevent = Upcomingevent::where('upcomingevent_api_id',$d['id'])->get()->first();
                        if(str_replace(' ','',$Upcomingevent['upcomingevent_api_score']) != $d['ss']){
                            $upcomingevent_api_alert = 1;
                        }else{
                            $upcomingevent_api_alert = 0;
                        }
                        
                        
                        if($d['time_status'] == 3){//end
                            $upcomingevent_api_ended = 1;
                            $upcomingevent_api_inplay = 0;
                        }else if($d['time_status'] == 1){//inplay
                            $upcomingevent_api_ended = 0;
                            $upcomingevent_api_inplay = 1;
                        }else{
                            $upcomingevent_api_ended = 0;
                            $upcomingevent_api_inplay = 0;  
                        }

                        Upcomingevent::where('upcomingevent_api_id',$d['id'])
                        ->update([
                            'upcomingevent_api_json'=>json_encode($d),'upcomingevent_api_score'=>$d['ss'],
                            'upcomingevent_api_alert'=>$upcomingevent_api_alert,
                            'upcomingevent_api_ended'=>$upcomingevent_api_ended,
                            'upcomingevent_api_inplay'=>$upcomingevent_api_inplay
                            ]
                        );
                    }
                }
            }
            echo "2<br>";
        }
        echo "done";die;
    }
    public function getEventView2(Request $request){


        $LNG_ID = $request['LNG_ID'];
        if($LNG_ID == ""){
            $LNG_ID = 1;
        }
		
        $upcomingevent = Upcomingevent::where('upcomingevent_api_lang',$LNG_ID)->where('upcomingevent_api_inplay','1')
        ->get();


        $event_id = "";
        foreach($upcomingevent as $l){
                $multiple_event_id = $l->upcomingevent_api_id;
                $curl = curl_init();

                curl_setopt_array($curl, array(
                    CURLOPT_URL => 'https://api.b365api.com/v1/event/view?token='.$this->token.'&event_id=' . $multiple_event_id . '&LNG_ID='.$LNG_ID,
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => "GET",
                ));

                $response = curl_exec($curl);
                $err = curl_error($curl);

                curl_close($curl);
                $info = json_decode($response, true);

                if($info['results']){
                    foreach($info['results'] as $d){
                        if(!$d['ss']){
                            $d['ss'] = "0 - 0";
                        }else{
                            $ee = explode('-',$d['ss']);
                            if($ee){
                                $d['ss'] = $ee[0] . " - " . $ee['1'];
                            }else{
                                $d['ss'] = "0 - 0";
                            }
                            
                        }
                        $Upcomingevent = Upcomingevent::where('upcomingevent_api_id',$d['id'])->where('upcomingevent_api_lang',$LNG_ID)->get()->first();
                        if(str_replace(' ','',$Upcomingevent['upcomingevent_api_score']) != $d['ss']){
                            $upcomingevent_api_alert = 1;
                        }else{
                            $upcomingevent_api_alert = 0;
                        }
                            

                        Upcomingevent::where('upcomingevent_api_id',$d['id'])->where('upcomingevent_api_lang',$LNG_ID)->update(['upcomingevent_api_json'=>json_encode($d),'upcomingevent_api_score'=>$d['ss'],'upcomingevent_api_alert'=>$upcomingevent_api_alert]);
                    }
                    
                }
        }
        echo "done";die;
    }
    public function updateGroup(){
        $Leaguetable = Leaguetable::where('leaguetable_lang',2)->get()->first();

        $json = json_decode($Leaguetable->leaguetable_overall,true);
        
        foreach($json['tables'] as $d){
            $name = $d['name'];

            foreach($d['rows'] as $rr){

                Upcomingevent::where('upcomingevent_api_home','like','%'.$rr['team']['id'].'%')->where('upcomingevent_api_lang',2)->update(['upcomingevent_api_group'=>$name]);
                
            }
           
        }
    }
    public function getFTLeague(){





        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://v3.football.api-sports.io/leagues?season=2022',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'GET',
        CURLOPT_HTTPHEADER => array(
            'x-rapidapi-key: 79e8513329310a647d0930dc4cabe0e8',
            'x-rapidapi-host: v3.football.api-sports.io'
        ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        echo $response;
    }
    public function getFTTeamsInformation(){





        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://v3.football.api-sports.io/teams?league=40&season=2022',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'GET',
        CURLOPT_HTTPHEADER => array(
            'x-rapidapi-key: 79e8513329310a647d0930dc4cabe0e8',
            'x-rapidapi-host: v3.football.api-sports.io'
        ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        echo $response;
    }
}
