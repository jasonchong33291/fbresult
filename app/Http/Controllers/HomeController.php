<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\Deposit;
use App\Models\Settlement;
use App\Models\Upcomingevent;
use App\Models\Leaguetable;
use App\Models\Teamsquad;
use Illuminate\Support\Facades\Storage;
use File;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request){
        
        if(session('lang') == null){
            $lang = 1;
        }else{
            $lang = session('lang');
        }
        $data['lang'] = $lang;
        $data['Upcomingevent'] = Upcomingevent::where('upcomingevent_api_lang',$lang)
        ->where('upcomingevent_api_inplay','=',1)
        ->orderBy('upcomingevent_api_date','ASC')->get();

        $data['Upcomingevent2'] = Upcomingevent::where('upcomingevent_api_lang',$lang)
        ->whereNull('upcomingevent_api_ended')
        ->orWhereIn('upcomingevent_api_ended',[0])
        ->orderBy('upcomingevent_api_date','ASC')
        ->get();
        

        $data['league'] = Leaguetable::
        join('db_league','db_league.league_id','=','db_leaguetable.leaguetable_league_id')
        ->where('leaguetable_lang',$lang)
        ->where('league_api_status',1)
        ->get();

        $data['league2'] = Leaguetable::
        join('db_league','db_league.league_id','=','db_leaguetable.leaguetable_league_id')
        ->where('leaguetable_lang',$lang)
        ->get();
        
        $data['Endedevent'] = Upcomingevent::
        where('upcomingevent_api_lang',$lang)
		->where('upcomingevent_api_ended','=',1)
        ->orderBy('upcomingevent_api_date','DESC')->get();


        return view('pages.homem', ['data' => $data]);
        // return view('pages.home', ['data' => $data]);
    }
    public function getGroups(Request $request){
        if(session('lang') == null){
            $lang = 1;
        }else{
            $lang = session('lang');
        }
        $data['lang'] = $lang;
        $data['Upcomingevent'] = Upcomingevent::whereBetween('upcomingevent_api_date',[date('Y-m-d H:i:s'),date('Y-m-d 23:59:59',strtotime("+21 day"))])
        ->where('upcomingevent_api_lang',$lang)
        ->where('upcomingevent_league_id','742')
        ->orderBy('upcomingevent_api_date','ASC')->get();

        $data['Leaguetable'] = Leaguetable::where('leaguetable_league_id',742)
        ->where('leaguetable_lang',$lang)->get()->first();
        
       
        $dd = json_decode($data['Leaguetable']->leaguetable_overall,true);
        $dd = collect($dd['tables']);
        $dd = $dd->sortBy('groupname');
        
        $data['d'] = [];
        foreach($dd as $i => $d){    
           
            
            $data['d'][$i]['group'] = $d;
           
            foreach($d['rows'] as $ii => $k){
               
                $Upcomingeventhome = Upcomingevent::where('upcomingevent_api_home', 'like', '%'.$k['team']['id'].'%')
                ->where('upcomingevent_api_lang',$lang)->where('upcomingevent_api_lang',$lang)->get();
                $Upcomingeventaway = Upcomingevent::where('upcomingevent_api_away', 'like', '%'.$k['team']['id'].'%')
                ->where('upcomingevent_api_lang',$lang)->where('upcomingevent_api_lang',$lang)->get();
                

                $data['d'][$i]['group']['home'][] = $Upcomingeventhome;
                $data['d'][$i]['group']['away'][] = $Upcomingeventaway;
                
            }
        }
        $data['data'] = [];
        $i = 0;
        foreach($data['d'] as $u){
            $k = [];
            $p = 0;
            $y = [];
            foreach($u['group']['home'] as $h){
                foreach($h as $t){
                    $y[] = $t;
                }
                
                // $k[$p] = $y;
                $p++;
            }
            
            $data['data'][$i] = $u['group'];
            $data['data'][$i]['hh'] = $y;
            $i++;
        }
        // dd($data['data']);
        
        $data['Leaguetable'] = Leaguetable::where('leaguetable_league_id',742)
        ->where('leaguetable_lang',$lang)->get();

        return view('pages.groups', ['data' => $data]);
    }
    public function getHomeResult(Request $request){

        $LNG_ID = $request['LNG_ID'];
        if($LNG_ID == ""){
            $LNG_ID = 1;
        }

        $event = Upcomingevent::where('upcomingevent_api_lang',1)->where('upcomingevent_api_inplay','1')
        ->orderBy('upcomingevent_api_date','ASC')->get();
       
        $data = [];
        foreach($event as $e){

            $e['date'] = date('Y-m-d',strtotime($e->upcomingevent_api_date));
            $e['time'] = date('H:i',strtotime($e->upcomingevent_api_date));
            $e['display_date'] = date('d/m/Y',strtotime($e->upcomingevent_api_date)) . " " . date('l',strtotime($e->upcomingevent_api_date));
            $data[] = $e;
            if($e->upcomingevent_api_alert == 1){
                Upcomingevent::where('upcomingevent_id',$e->upcomingevent_id)->update(['upcomingevent_api_alert'=>0]);
            }
        }

        // dd($data);
  
       
        File::put(public_path('/uploads/json/sources_en.json'),json_encode($data));



        $event = Upcomingevent::where('upcomingevent_api_lang',2)->where('upcomingevent_api_inplay','1')
        ->orderBy('upcomingevent_api_date','ASC')->get();
       
        $data = [];
        foreach($event as $e){

            $e['date'] = date('Y-m-d',strtotime($e->upcomingevent_api_date));
            $e['time'] = date('h:i',strtotime($e->upcomingevent_api_date));
            $e['display_date'] = date('d/m/Y',strtotime($e->upcomingevent_api_date)) . " " . date('l',strtotime($e->upcomingevent_api_date));
            $data[] = $e;
            if($e->upcomingevent_api_alert == 1){
                Upcomingevent::where('upcomingevent_id',$e->upcomingevent_id)->update(['upcomingevent_api_alert'=>0]);
            }
        }
  

        File::put(public_path('/uploads/json/sources_cn.json'),json_encode($data));
        echo "Done";
    }
    public function changeLanguage(Request $request){
        
        if($request['lang'] == 'cn'){
            session(['lang' => '2']);
        }else{
            session(['lang' => '1']);
        }
        return redirect()->back();

    }
    public function detail(Request $request){
        if(session('lang') == null){
            $lang = 1;
        }else{
            $lang = session('lang');
        }
        $data['lang'] = $lang;
        $data['Teamsquad'] = Teamsquad::where('teamsquad_upcomingevent_id',$request['q'])->where('teamsquad_api_lang',$lang)->get();
        $data['Upcomingevent'] = Upcomingevent::find($request['q']);
        // dd($data['Teamsquad']);
        if($data['Upcomingevent']){
            $data['Leaguetable'] = Leaguetable::where('leaguetable_league_id',$data['Upcomingevent']->upcomingevent_league_id)->where('leaguetable_lang',$lang)->get();
        }else{
            $data['Leaguetable'] = [];
        }
        
        

        return view('pages.detailm', ['data' => $data]);
        // return view('pages.detail', ['data' => $data]);
    }
    public function isstart(Request $request){

        
        $data['Upcomingevent'] = Upcomingevent::find($request['upcomingevent_id']);
        if($data['Upcomingevent']){
            $upcomingevent_api_json = json_decode($data['Upcomingevent']['upcomingevent_api_json'],true);
           
            if(strtotime(date("Y-m-d H:i:s")) >= strtotime($data['Upcomingevent']->upcomingevent_api_date)){
                
                if($upcomingevent_api_json['time_status'] == 3){
                    return response()->json([
                        'status' => '0',
                        'msg' => '',
                        'upcomingevent_id' => $data['Upcomingevent']->upcomingevent_id,
                    ]); 
                }
                return response()->json([
                    'status' => '1',
                    'msg' => '',
                ]); 
            }else{
                return response()->json([
                    'status' => '0',
                    'msg' => '',
                ]); 
            }

        }else{
            return response()->json([
                'status' => '0',
                'msg' => '',
            ]); 
        }

    }
    public function isstart2(Request $request){

        
        $data['Upcomingevent'] = Upcomingevent::all();
        if($data['Upcomingevent']){
            foreach($data['Upcomingevent'] as $h){
                $upcomingevent_api_json = json_decode($h['upcomingevent_api_json'],true);
                if(count($upcomingevent_api_json) > 0){
                     if(strtotime(date("Y-m-d H:i:s")) >= strtotime($h->upcomingevent_api_date)){
                         if($upcomingevent_api_json['time_status'] == 1){
                             return response()->json([
                                 'status' => '1',
                                 'msg' => '',
                                 'upcomingevent_id' => $h->upcomingevent_id,
                             ]); 
                         }
                     }
                 }
            }

            return response()->json([
                'status' => '0',
                'msg' => '',
            ]); 
        }else{
            return response()->json([
                'status' => '0',
                'msg' => '',
            ]); 
        }

    }
}
