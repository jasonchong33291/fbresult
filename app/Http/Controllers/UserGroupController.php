<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Models\Permission;
use App\Models\Role;

class UserGroupController extends Controller
{
    /**
     * Display a listing of the users
     *
     * @param  \App\Models\UserGroupController  $model
     * @return \Illuminate\View\View
     */
    public function listing()
    {
        // $checkAuthRole = Auth::user()->roles()->first()->name;

        // if ($checkAuthRole == 'Super Admin') {
        $role = Role::all();
        // } else {
        //     $role = Role::with('permissions')->get();
        // }
        // dd($role);


        return view('users.userGroups', ['role' => $role]);
    }

    public function create(Request $request)
    {

        if ($request->isMethod('post')) {

            $id = $request['role_id'];

            if ($id > 0) {
                $role = Role::find($id);
            } else {
                $validator = Validator::make($request->all(), [
                    'name' => ['required', Rule::unique('roles')],
                ]);

                if ($validator->fails()) {
                    return response()->json([
                        'status' => '0',
                        'msg' => 'Invalid User Group Name.',
                        'id' => $id
                    ]);
                }
                // $guard_name = strtolower(str_replace(' ', '_', $request->name));
                $role = Role::create(['guard_name' => 'api', 'name' => $request->name]);
            }

            $permissions = $request->permission;
            $role->revokePermissionTo($role->getAllPermissions());
            $role->givePermissionTo($permissions);
            // $role->givePermissionTo(collect($permissions)->pluck('name')->all());

            if ($id > 0) {
                return response()->json([
                    'status' => '1',
                    'msg' => 'Updated Successfully.',
                    'id' => $role->id
                ]);
            } else {
                return response()->json([
                    'status' => '1',
                    'msg' => 'Created Successfully.',
                    'id' => $role->id
                ]);
            }
        } else {
            return response()->json([
                'status' => '0',
                'msg' => 'Somethig went wrong, please try again.',
            ]);
        }
    }

    public function createForm(Request $request)
    {
        $role_id = $request['id'];
        $gn = $request['gn']; //guard name

        $user_permissions_name = array();
        // dd($role);

        if ($role_id) {
            $data['role_permission'] = DB::table("role_has_permissions")->where("role_id", $role_id)->get();
            $data['edit'] = true;

            $data['role_info'] = Role::find($role_id);
            $user_permissions = $data['role_info']->getAllPermissions();
            foreach ($user_permissions as $user_permission) {
                $user_permissions_name[] = $user_permission->name;
            }
        } else {
            $data['role_info'] = new Role();
            $data['role_permission'] = new User();
            $data['edit'] = false;
        }

        $permissions = Permission::all();
        $data['all_permissions'] = [];

        foreach ($permissions as $key => $p) {
            if (in_array($p->name, $user_permissions_name)) {
                $permissions[$key]->checked = true;
            } else {
                $permissions[$key]->checked = false;
            }

            $data['all_permissions'][$p->group][] = $p;
        }

        $data['roles'] = Role::all();

        // dd($data['all_permissions']);

        return view('users.permissionsForm', ['data' => $data]);
    }
}
