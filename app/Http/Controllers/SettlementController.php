<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Carbon\Carbon;
use App\Models\Merchant;
use App\Models\Deposit;
use App\Models\Settlement;
use App\Models\Merchantsettlementbank;

class SettlementController extends Controller
{
    public function listing(Request $request){

        $data['deposit'] = DB::table("db_deposit")->join('db_merchant as m','m.merchant_id','=','db_deposit.deposit_merchant_id');

        if(Auth::user()->user_group_id == 3){//agent
            $data['merchant'] = DB::table("db_merchant")->where('merchant_agent_id',Auth::user()->id)->get();

        }else if(Auth::user()->user_group_id == 6){//senior
            $data['merchant'] = DB::table("db_merchant")
                                    ->join('users','users.id','=','db_merchant.merchant_agent_id')
                                    ->where('user_parent',Auth::user()->id)
                                    ->get();

        }else if((Auth::user()->user_group_id == 1) || (Auth::user()->user_group_id == 2) || (Auth::user()->user_group_id == 7) || (Auth::user()->user_group_id == 9)){//super admin & admin
            $data['merchant'] = Merchant::all();
        }else{
            $data['merchant'] = [];
        }
        
        $data['deposit_mode'] = $request['deposit_mode'];
        $data['filter_date_from'] = $request->get("filter_date_from");
        $data['filter_date_to'] = $request->get("filter_date_to");
        
        return view('settlement.settlementlisting', ['data' => $data]);
    }
    public function createForm(Request $request){
        if(!canAccess('create_settlement')){
            return response()->json([
                'status' => '0',
                'msg' => 'Permission Denied, if you think this is an error please contact our support.',
            ]);
        }
        if($request['settlement_id']){
            $settlement_id = Crypt::decryptString($request['settlement_id']);
        }else{
            $settlement_id = 0;
        }
        
        $data['merchant'] = [];
        $data['available_balance'] = 0;
        $data['settlement'] = new Settlement();
        $data['merchantsettlement_bank'] = '';

        if(Auth::user()->user_group_id == 3){//agent
            $data['merchant'] = DB::table("db_merchant")
                                ->where('merchant_agent_id',Auth::user()->id)->get()->first();
            if($settlement_id > 0){
                $data['settlement'] = DB::table("db_settlement")
                                        ->join("db_merchant",'db_merchant.merchant_id','=','db_settlement.settlement_merchant_id')
                                        ->where('db_settlement.settlement_id',$settlement_id)
                                        ->where('db_merchant.merchant_agent_id',Auth::user()->id)
                                        ->get()->first();
            }
        }else if(Auth::user()->user_group_id == 6){//senior
            $data['merchant'] = DB::table("db_merchant")
                                ->join('users','users.id','=','db_merchant.merchant_agent_id')
                                ->where('users.user_parent',Auth::user()->id)
                                ->get()
                                ->first();
            if($settlement_id > 0){
                $data['settlement'] = DB::table("db_settlement")
                                    ->join("db_merchant",'db_merchant.merchant_id','=','db_settlement.settlement_merchant_id')
                                    ->join('users','users.id','=','m.merchant_agent_id')
                                    ->where('db_settlement.settlement_id',$settlement_id)
                                    ->where('users.user_parent',Auth::user()->id)
                                    ->get()->first();
            }
            
        }else if((Auth::user()->user_group_id == 1) || (Auth::user()->user_group_id == 2) || (Auth::user()->user_group_id == 7) || (Auth::user()->user_group_id == 9)){//super admin & admin
            $data['merchant'] = Merchant::All();
            if($settlement_id > 0){
                $data['settlement'] = Settlement::join("db_merchant",'db_merchant.merchant_id','=','db_settlement.settlement_merchant_id')  
                ->leftjoin('users','users.id','=','db_settlement.settlement_actionby')
                ->where('db_settlement.settlement_id',$settlement_id)
                ->get()->first();

                $merchantsettlement_bank = Merchantsettlementbank::
                join('db_bank','db_bank.bank_id','=','db_merchantsettlementbank.merchantsettlementbank_bank_id')
                ->where('merchantsettlementbank_merchant_id',$data['settlement']->settlement_merchant_id)->get();
    
                $data['merchantsettlement_bank'] = "<option value = ''>Please select one</option>";
   
                foreach($merchantsettlement_bank as $c){

                    if($data['settlement']->settlement_merchantsettlementbank_id == $c->merchantsettlementbank_id){
                        $selected = " SELECTED";
                    }else{
                        $selected = " ";
                    }
                    $data['merchantsettlement_bank'] .= "<option value = '".Crypt::encryptString($c->merchantsettlementbank_id)."' $selected>".$c->bank_name . " - (" . $c->merchantsettlementbank_account_holder . ", ".  $c->merchantsettlementbank_account_number .")</option>";
                }

                $data['available_balance'] = Deposit::where('deposit_merchant_id',$data['settlement']->settlement_merchant_id)
                ->where('deposit_internalstatuscode','80001')
                ->sum('deposit_nett_amount');

                $settlement_amount = Settlement::where('settlement_status','Approved')
                ->where('settlement_merchant_id',$data['settlement']->settlement_merchant_id)
                ->sum('settlement_to_amount');

                $data['available_balance'] = ROUND($data['available_balance'] - $settlement_amount,2);
                
            }
        }else if((Auth::user()->user_group_id == 4) || (Auth::user()->user_group_id == 8) || (Auth::user()->user_group_id == 10) || (Auth::user()->user_group_id == 11)){//merchant & merchant support
            $data['merchant'] = [];
            $data['available_balance'] = Deposit::where('deposit_merchant_id',Auth::user()->user_merchant_id)
                                        ->where('deposit_internalstatuscode','80001')
                                        ->sum('deposit_nett_amount');

            $settlement_amount = Settlement::where('settlement_status','Approved')
            ->where('settlement_merchant_id',Auth::user()->user_merchant_id)
            ->sum('settlement_to_amount');

            $data['available_balance'] = ROUND($data['available_balance'] - $settlement_amount,2);                    
           
            if($settlement_id > 0){
                $data['settlement'] = Settlement::join("db_merchant",'db_merchant.merchant_id','=','db_settlement.settlement_merchant_id')  
                ->leftjoin('users','users.id','=','db_settlement.settlement_actionby')
                ->where('db_settlement.settlement_id',$settlement_id)
                ->get()->first();

            }                         
            $merchantsettlement_bank = Merchantsettlementbank::
            join('db_bank','db_bank.bank_id','=','db_merchantsettlementbank.merchantsettlementbank_bank_id')
            ->where('merchantsettlementbank_merchant_id',Auth::user()->user_merchant_id)
            ->where('db_merchantsettlementbank.merchantsettlementbank_status',1)->get();

            $data['merchantsettlement_bank'] = "<option value = ''>Please select one</option>";

            foreach($merchantsettlement_bank as $c){

                if($data['settlement']->settlement_merchantsettlementbank_id == $c->merchantsettlementbank_id){
                    $selected = " SELECTED";
                }else{
                    $selected = " ";
                }
                $data['merchantsettlement_bank'] .= "<option value = '".Crypt::encryptString($c->merchantsettlementbank_id)."' $selected>".$c->bank_name . " - (" . $c->merchantsettlementbank_account_holder . ", ".  $c->merchantsettlementbank_account_number .")</option>";
            }       
            

        }else{
            $data['merchant'] = null;
            if($settlement_id > 0){
                $data['settlement'] = null;
            }else{
                $data['settlement'] = new Settlement();
            }
            
        }
        
        if($settlement_id > 0){
            if(!$data['settlement']){
                return response()->json([
                    'status' => '0',
                    'msg' => 'Permission Denied, if you think this is an error please contact our support.',
                ]);
            }
        }



        return view('settlement.settlementform', ['data' => $data]);
    }
    public function create(Request $request){

        if ($request->isMethod('post')) {

            if($request['settlement_id']){
                $settlement_id = Crypt::decryptString($request['settlement_id']);
            }else{
                $settlement_id = 0;
            }
            if ($settlement_id > 0) {
                if(!canAccess('edit_settlement')){
                    return response()->json([
                        'status' => '0',
                        'msg' => 'Permission Denied, if you think this is an error please contact our support.',
                    ]);
                }
                if(Auth::user()->user_group_id == 3){//agent
                    $data['settlement'] = Settlement::join("db_merchant",'db_merchant.merchant_id','=','db_settlement.settlement_merchant_id')
                                            ->where('db_settlement.settlement_id',$settlement_id)
                                            ->where('m.merchant_agent_id',Auth::user()->id)
                                            ->get()->first();
                }else if(Auth::user()->user_group_id == 6){//senior
                    $data['settlement'] = Settlement::join("db_merchant",'db_merchant.merchant_id','=','db_settlement.settlement_merchant_id')
                                        ->join('users','users.id','=','m.merchant_agent_id')
                                        ->where('db_settlement.settlement_id',$settlement_id)
                                        ->where('users.user_parent',Auth::user()->id)
                                        ->get()->first();
                    
                }else if((Auth::user()->user_group_id == 1) || (Auth::user()->user_group_id == 2) || (Auth::user()->user_group_id == 4) || (Auth::user()->user_group_id == 7) || (Auth::user()->user_group_id == 9)){//super admin & admin
                    $data['settlement'] = Settlement::join("db_merchant",'db_merchant.merchant_id','=','db_settlement.settlement_merchant_id')
                                        ->where('db_settlement.settlement_id',$settlement_id)
                                        ->get()->first();
                }else{
                    $data['settlement'] = null;
                }

                if(!$data['settlement']){
                    return response()->json([
                        'status' => '0',
                        'msg' => 'Permission Denied, record not found , if you think this is an error please contact our support.',
                    ]);
                }
            } else {
                if(!canAccess('create_settlement')){
                    return response()->json([
                        'status' => '0',
                        'msg' => 'Permission Denied, if you think this is an error please contact our support.',
                    ]);
                }
                $data['settlement'] = new Settlement();
                $data['settlement']->settlement_no = getRefno('Withdraw',true,date('Y-m-d'));
            }

            if((Auth::user()->user_group_id == 1) || (Auth::user()->user_group_id == 2) || (Auth::user()->user_group_id == 7) || (Auth::user()->user_group_id == 9)){//super admin & admin
                if($request['settlement_merchant_id']){
                    $merchant_id = Crypt::decryptString($request['settlement_merchant_id']);
                }else{
                    $merchant_id = 0;
                }
                if($merchant_id <=0){
                    return response()->json([
                        'status' => '0',
                        'msg' => 'Merchant record not found.',
                    ]);
                }
            }else if((Auth::user()->user_group_id == 4) || (Auth::user()->user_group_id == 8) || (Auth::user()->user_group_id == 10) || (Auth::user()->user_group_id == 11)){//merchant & merchant support
                $merchant_id = Merchant::where('merchant_id',Auth::user()->user_merchant_id)->get()->first()->merchant_id;
                
            }
            
            $data['settlement']->settlement_merchant_id = $merchant_id;
            $data['settlement']->settlement_ref = $request->input('settlement_ref');
            
            if($request['settlement_merchantsettlementbank_id']){
                $settlement_merchantsettlementbank_id = Crypt::decryptString($request['settlement_merchantsettlementbank_id']);
            }else{
                $settlement_merchantsettlementbank_id = 0;
            }
           
            $data['settlement']->settlement_merchantsettlementbank_id = $settlement_merchantsettlementbank_id;
            $data['settlement']->settlement_from_amount = str_replace(',','',$request->input('settlement_from_amount'));
            $data['settlement']->settlement_requested_amount = str_replace(',','',$request->input('settlement_requested_amount'));
            $data['settlement']->settlement_remarks = $request->input('settlement_remarks');
            $data['settlement']->settlement_bank_account_holder = $request->input('settlement_bank_account_holder');
            $data['settlement']->settlement_bank_account = $request->input('settlement_bank_account');


            
            $data['settlement']->updateBy = Auth::user()->id;
            $data['settlement']->insertBy = Auth::user()->id;


            //double check amount
            $available_balance = Deposit::where('deposit_merchant_id',$merchant_id)
            ->where('deposit_internalstatuscode','80001')
            ->sum('deposit_nett_amount');

            $settlement_amount = Settlement::where('settlement_status','Approved')
            ->where('settlement_merchant_id',$merchant_id)
            ->sum('settlement_to_amount');

            $available_balance = $available_balance - ($settlement_amount + $data['settlement']->settlement_requested_amount);
            if($available_balance <=0){
                return response()->json([
                    'status' => '0',
                    'msg' => "Insufficient Balance",
                ]);
            }

            if($request['submit_button_status'] == "confirm"){

                $data['settlement']->settlement_to_amount = 0;
                $data['settlement']->settlement_remarks = '';
                $data['settlement']->settlement_status = "Pending";
                $data['settlement']->settlement_submission = Carbon::now();
                $data['settlement']->save();
            }else{
                $data['settlement']->settlement_status = 'Draft';
                $data['settlement']->save();
                
            }

            if ($settlement_id > 0) {
                return response()->json([
                    'status' => '1',
                    'msg' => 'Updated Successfully.',
                    'settlement_id' => Crypt::encryptString($data['settlement']->settlement_id)
                ]);
            } else {
                return response()->json([
                    'status' => '1',
                    'msg' => 'Created Successfully.',
                    'settlement_id' => Crypt::encryptString($data['settlement']->settlement_id)
                ]);
            }
        } else {
            return response()->json([
                'status' => '0',
                'msg' => 'Somethig went wrong, please try again.',
            ]);
        }
    }
    public function filter(Request $request){
        // dd($request);

        ## Read value
        $draw = $request->get('draw');
        $start = $request->get("start");
        $rowperpage = $request->get("length"); // Rows display per page

        $columnIndex_arr = $request->get('order');
        $columnName_arr = $request->get('columns');
        $order_arr = $request->get('order');
        $search_arr = $request->get('search');

        $columnIndex = $columnIndex_arr[0]['column']; // Column index
        $columnName = $columnName_arr[$columnIndex]['data']; // Column name
        $columnSortOrder = $order_arr[0]['dir']; // asc or desc
        $searchValue = $search_arr['value']; // Search value

     
        $from = $request['from'] ? Carbon::createFromFormat('d/m/Y', $request['from'])->format('Y-m-d') : null;
        $to = $request['to'] ? Carbon::createFromFormat('d/m/Y', $request['to'])->format('Y-m-d') : null;
        $salesperson = $request['sales'];
        $filter_status = $request['filter_status'];
        $deposit_mode = $request['deposit_mode'];
        $filter_merchant = $request['filter_merchant'];

        $columns = array(
            0 => 'settlement_submission',
            1 => 'settlement_actiondatetime',
            2 => 'settlement_no',
            3 => 'merchant_code',
            4 => 'settlement_ref',
            5 => 'from_bank',
            6 => 'merchantsettlementbank_account_holder',
            7 => 'merchantsettlementbank_account_number',
            8 => 'settlement_requested_amount',
            9 => 'settlement_to_amount',
            10 => 'settlement_status',
            11 => 'settlement_remarks',
            12 => 'action'
        );


        $dir = '';

        if ($columnIndex_arr) {
            $order_by = $columns[$columnIndex_arr[0]['column']];
            $dir = $columnSortOrder;
        }

        if($from == ''){
            $from = "0000-00-00";
        }

        if($to == ''){
            $to = "2040-12-31";
        }

        $wherestring = " AND LEFT(settlement_submission,10) BETWEEN '$from' AND '$to'";

        if($filter_status != ""){
            $wherestring .= " AND settlement_status = '$filter_status'";
        }
        if($filter_merchant > 0){
            $wherestring .= " AND m.merchant_id = '$filter_merchant'";
        }
        


        $order = DB::table('db_settlement as d')
            ->select('d.*','msb.merchantsettlementbank_account_holder','msb.merchantsettlementbank_account_number',
            'b.bank_name as from_bank','m.merchant_code'
            )
            ->leftjoin('db_merchant as m','m.merchant_id','=','d.settlement_merchant_id')
            ->leftjoin('db_merchantsettlementbank as msb','msb.merchantsettlementbank_id','=','d.settlement_merchantsettlementbank_id')
            ->leftjoin('db_bank as b','b.bank_id','=','msb.merchantsettlementbank_bank_id')
            ->whereRaw("d.settlement_id > 0 $wherestring");



        if ($searchValue !== NULL) {
            $order = $order->where(function ($query) use ($searchValue) {
                $query->where('d.settlement_no', 'LIKE', '%' . $searchValue . '%')
                    ->orWhere('d.settlement_remarks', 'like', '%' . $searchValue  . '%')
                    ->orWhere('msb.merchantsettlementbank_account_number', 'like', '%' . $searchValue  . '%')
                    ->orWhere('msb.merchantsettlementbank_account_holder', 'like', '%' . $searchValue  . '%');
            });
        }

        if(Auth::user()->user_group_id == 3){//agent
            $order = $order->where('m.merchant_agent_id','=',Auth::user()->id);

        }else if(Auth::user()->user_group_id == 6){//senior
            $order = $order
            ->join('users as u','u.users.id','=','m.merchant_agent_id')
            ->where('u.user_parent','=',Auth::user()->id);

        }else if((Auth::user()->user_group_id == 1) || (Auth::user()->user_group_id == 2) || (Auth::user()->user_group_id == 7) || (Auth::user()->user_group_id == 9)){//super admin & admin

        }else if((Auth::user()->user_group_id == 4) || (Auth::user()->user_group_id == 8) || (Auth::user()->user_group_id == 10) || (Auth::user()->user_group_id == 11)){//merchant & merchant support
            $order = $order->where('m.merchant_id','=',Auth::user()->user_merchant_id);
        }else{
            $order = $order->where('settlement_id','=','0');
        }
        // debugsql($order);

        $totalRecords = $order->count();
        $order_bottom_data = $order->get();
        $order = $order->take($rowperpage)->skip($start);

        //debugsql($order);
        if ($order_by && $dir) {
            $order = $order->orderBy($order_by, $dir)->get();
        } else {
            $order = $order->orderBy('d.settlement_id', 'desc')->get();
        }

        $i = 0;
        $output = array();

        $order_column = array(
            'settlement_submission',
            'settlement_actiondatetime',
            'settlement_no',
            'merchant_code',
            'settlement_ref',
            'from_bank',
            'merchantsettlementbank_account_holder',
            'merchantsettlementbank_account_number',
            'settlement_requested_amount',
            'settlement_to_amount',
            'settlement_status',
            'settlement_remarks',
            'action'
        );
        foreach ($order as $c) {

            foreach ($order_column as $key => $column) {
               // $user_name = strtoupper($c->user_name);
                $action = '<div class="dropdown">
                                <a class="btn btn-sm btn-icon-only text-light" href="#" role="button"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="fas fa-ellipsis-v"></i>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow" style="">
                                    <a class="dropdown-item " href="/settlement/createForm?settlement_id=' . Crypt::encryptString($c->settlement_id) . '" >
                                       View Detail
                                    </a>
                                </div>
                            </div>';

                if ($column == 'settlement_submission') {
                    $d_slit = explode(' ',$c->settlement_submission);
                    $output[$i][] = $d_slit[0] . "<br>" . $d_slit[1] . "<span style = 'display:none'>" . Crypt::encryptString($c->settlement_id) . "</span>";
                } elseif ($column == 'settlement_no') {
                    $output[$i][] = $c->settlement_no;
                } elseif ($column == 'merchant_code') {
                    $output[$i][] = $c->merchant_code;
                } elseif ($column == 'settlement_ref') {
                    $output[$i][] = $c->settlement_ref;
                } elseif ($column == 'merchantsettlementbank_account_holder') {
                    $output[$i][] = $c->merchantsettlementbank_account_holder;
                } elseif ($column == 'merchantsettlementbank_account_number') {
                    $output[$i][] = $c->merchantsettlementbank_account_number;
                } elseif ($column == 'from_bank') {
                    $output[$i][] = $c->from_bank;
                }  elseif ($column == 'settlement_actiondatetime') {
                    if($c->settlement_actiondatetime != null){
                        $d_slit = explode(' ',$c->settlement_actiondatetime);
                        $output[$i][] = $d_slit[0] . "<br>" . $d_slit[1];
                    }else{
                        $output[$i][] = " - ";
                    }
                } elseif ($column == 'settlement_remarks') {
                    $output[$i][] = $c->settlement_remarks;
                }  elseif ($column == 'settlement_requested_amount') {
                    $output[$i][] = "RM" . num_format($c->settlement_requested_amount);
                } elseif ($column == 'settlement_to_amount') {
                    $output[$i][] = "RM" . num_format($c->settlement_to_amount);
                } elseif ($column == 'action') {
                    $output[$i][] = $action;
                } elseif ($column == 'settlement_status') {
                    if ($c->settlement_status == 'Approved') {
                        $output[$i][] =  '<span class="badge badge-success">Approved</span>';
                    } elseif ($c->settlement_status == 'Pending') {
                        $output[$i][] =  '<span class="badge badge-warning">Pending</span>';
                    } elseif ($c->settlement_status == 'Rejected') {
                        $output[$i][] =  '<span class="badge badge-danger">Rejected</span>';
                    } elseif ($c->settlement_status == 'Draft') {
                        $output[$i][] =  '<span class="badge badge-violet">Draft</span>';
                    } else {
                        $output[$i][] =  '<span class="badge badge-black">Unknown</span>';
                    }
                }
            }
            $i++;
        }
        $order_collection = collect($order_bottom_data);

        $response = array(
            'draw' => intval(isset($draw) ? $draw : 0),
            'recordsTotal' => intval(isset($totalRecords) ? $totalRecords : 0),
            'recordsFiltered' => intval(isset($totalRecords) ? $totalRecords : 0),
            'settlement_nett_amount' => num_format($order_collection->where('settlement_status','Approved')->sum('settlement_to_amount')),
            'settlement_pending_amount' => num_format($order_collection->where('settlement_status','Pending')->sum('settlement_requested_amount')),
            'settlement_success_count' => num_format($order_collection->where('settlement_status','Approved')->count()),
            'data' => $output,
        );

        return $response;
    }
    public function getSettlementBank(Request $request){

        if($request->isMethod('post')) {
            if($request['merchant_id']){
                $merchant_id = Crypt::decryptString($request['merchant_id']);
            }else{
                $merchant_id = 0;
            }

            if($merchant_id <=0 ){
                return response()->json([
                    'status' => '0',
                    'msg' => 'Merchant record not found , please try again .',
                ]);
            }
            $merchantsettlement_bank = Merchantsettlementbank::
            join('db_bank','db_bank.bank_id','=','db_merchantsettlementbank.merchantsettlementbank_bank_id')
            ->where('merchantsettlementbank_merchant_id',$merchant_id)
            ->where('db_merchantsettlementbank.merchantsettlementbank_status',1)->get();

            $html = "<option value = ''>Please select one</option>";

            foreach($merchantsettlement_bank as $c){
                $html .= "<option value = '".Crypt::encryptString($c->merchantsettlementbank_id)."'>".$c->bank_name . " - (" . $c->merchantsettlementbank_account_holder . ", ".  $c->merchantsettlementbank_account_number .")</option>";
            }

            $available_balance = Deposit::where('deposit_merchant_id',$merchant_id)
            ->where('deposit_internalstatuscode','80001')
            ->sum('deposit_nett_amount');

            $settlement_amount = Settlement::where('settlement_status','Approved')
            ->where('settlement_merchant_id',$merchant_id)
            ->sum('settlement_to_amount');

            $available_balance = $available_balance - $settlement_amount;

            return response()->json([
                'status' => '1',
                'html' => $html,
                'available_balance' => round($available_balance,2),
            ]);
        }else{
            return response()->json([
                'status' => '0',
                'msg' => 'Somethig went wrong, please try again.',
            ]);
        }
    }
    public function action(Request $request){

        if(!canAccess('approval_settlement')){
            return response()->json([
                'status' => '0',
                'msg' => 'Permission Denied, if you think this is an error please contact our support.',
            ]);
        }

        if($request['settlement_id']){
            $settlement_id = Crypt::decryptString($request['settlement_id']);
        }else{
            $settlement_id = 0;
        }

        if ($settlement_id > 0) {
            if($request['action'] == 'Approve?'){
                $settlement = Settlement::find($settlement_id);
                $settlement->settlement_status = 'Approved';
                $settlement->settlement_remarks = $request['settlement_remarks'];
                $settlement->settlement_to_amount = $request['settlement_to_amount'];
                $settlement->settlement_actionby = Auth::user()->id;
                $settlement->settlement_actiondatetime = Carbon::now();
                $settlement->save();
            }else{
                $settlement = Settlement::find($settlement_id);
                $settlement->settlement_status = 'Rejected';
                $settlement->settlement_remarks = $request['settlement_remarks'];
                $settlement->settlement_to_amount = $request['settlement_to_amount'];
                $settlement->settlement_actionby =  Auth::user()->id;
                $settlement->settlement_actiondatetime = Carbon::now();
                $settlement->save();
            }

            return response()->json([
                'status' => '1',
                'msg' => 'Updated Successfully.',
            ]);
        }else{
            return response()->json([
                'status' => '0',
                'msg' => 'Somethig went wrong, record not found',
            ]);
        }
    }
    public function setaction(Request $request){
        if(!canAccess('setaction_settlement')){
            return response()->json([
                'status' => '0',
                'msg' => 'Permission Denied, if you think this is an error please contact our support.',
            ]);
        }

        if($request['settlement_id']){
            $settlement_id = Crypt::decryptString($request['settlement_id']);
        }else{
            $settlement_id = 0;
        }
        if($settlement_id > 0) { 
            if($request['action'] == 'approve'){
                $settlement = Settlement::find($settlement_id);
                $settlement->settlement_status = 'Approved';
                $settlement->settlement_actionby =  Auth::user()->id;
                $settlement->settlement_actiondatetime = Carbon::now();
                $settlement->save();
            }else if($request['action'] == 'reject'){
                $settlement = Settlement::find($settlement_id);
                $settlement->settlement_status = 'Rejected';
                $settlement->settlement_actionby =  Auth::user()->id;
                $settlement->settlement_actiondatetime = Carbon::now();
                $settlement->save();
            }else{
                $settlement = Settlement::find($settlement_id);
                $settlement->settlement_status = 'Cancelled';
                $settlement->settlement_actionby =  Auth::user()->id;
                $settlement->settlement_actiondatetime = Carbon::now();
                $settlement->save();
            }
            return response()->json([
                'status' => '1',
                'msg' => 'Settlement status set successfully.',
            ]);
        }else{
            return response()->json([
                'status' => '0',
                'msg' => 'Record not found, if you think this is an error please contact our support.',
            ]);
        }

    }

    public function getSettlementBankAccount(Request $request){
        if($request->isMethod('post')) {
            if($request['merchantsettlementbank_id']){
                $merchantsettlementbank_id = Crypt::decryptString($request['merchantsettlementbank_id']);
            }else{
                $merchantsettlementbank_id = 0;
            }

            if($merchantsettlementbank_id <=0 ){
                return response()->json([
                    'status' => '0',
                    'msg' => 'Settlement Bank record not found , please try again .',
                ]);
            }
            $merchantsettlement_bank = Merchantsettlementbank::find($merchantsettlementbank_id);

            if($merchantsettlement_bank){
                return response()->json([
                    'status' => '1',
                    'merchantsettlementbank_account_number' => $merchantsettlement_bank->merchantsettlementbank_account_number,
                    'merchantsettlementbank_account_holder' => $merchantsettlement_bank->merchantsettlementbank_account_holder,
                ]);
            }else{
                return response()->json([
                    'status' => '0',
                    'merchantsettlementbank_account_number' => 0,
                    'merchantsettlementbank_account_holder' => 0,
                ]);
            }


        }else{
            return response()->json([
                'status' => '0',
                'msg' => 'Somethig went wrong, please try again.',
            ]);
        }
    }
}
