<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Crypt;
use App\Models\BankMaster;
use App\Models\Bank;
use App\Models\Merchantbankmaster;
use App\Models\Merchant;
use App\Models\Bankapplyto;
use Carbon\Carbon;

class BankMasterController extends Controller
{
    public function createForm(Request $request){

        $data['bank_list'] = Bank::where('bank_depositto_status',1)->orderBy('bank_name','ASC')->get();

        if($request['bankmaster_id']){
            $bankmaster_id = Crypt::decryptString($request['bankmaster_id']);
        }else{
            $bankmaster_id = 0;
        }
        if ($bankmaster_id > 0) {
            if(!canAccess('edit_bankmaster')){
                return response()->json([
                    'status' => '0',
                    'msg' => 'Permission Denied, if you think this is an error please contact our support.',
                ]);
            }
            $data['bankmaster'] = DB::table("db_bankmaster")->where("bankmaster_id", $bankmaster_id)->get()->first();
        } else {
            if(!canAccess('create_bankmaster')){
                return response()->json([
                    'status' => '0',
                    'msg' => 'Permission Denied, if you think this is an error please contact our support.',
                ]);
            }
            $data['bankmaster'] = new BankMaster();
        }

        return view('bankmaster.bankmasterform', ['data' => $data]);
    }
    public function create(Request $request){

        if ($request->isMethod('post')) {

            if($request['bankmaster_id']){
                $bankmaster_id = Crypt::decryptString($request['bankmaster_id']);
            }else{
                $bankmaster_id = 0;
            }
            if ($bankmaster_id > 0) {
                if(!canAccess('create_bankmaster')){
                    return response()->json([
                        'status' => '0',
                        'msg' => 'Permission Denied, if you think this is an error please contact our support.',
                    ]);
                }

                $data['bankmaster'] = BankMaster::find($bankmaster_id);
                $data['bankmaster']->updated_at = Carbon::now();
                $data['bankmaster']->updateBy = Auth::user()->id;
            } else {
                if(!canAccess('edit_bankmaster')){
                    return response()->json([
                        'status' => '0',
                        'msg' => 'Permission Denied, if you think this is an error please contact our support.',
                    ]);
                }

                $data['bankmaster'] = new BankMaster();
                $data['bankmaster']->created_at = Carbon::now();
                $data['bankmaster']->insertBy = Auth::user()->id;
            }

            
            $data['bankmaster']->bankmaster_name  = $request->input('bankmaster_name');
            $data['bankmaster']->bankmaster_bank_id  = $request->input('bankmaster_bank_id');
            $data['bankmaster']->bankmaster_account_holder = $request->input('bankmaster_account_holder');
            $data['bankmaster']->bankmaster_account_number = $request->input('bankmaster_account_number');
            $data['bankmaster']->bankmaster_seq = $request->input('bankmaster_seq');
            $data['bankmaster']->bankmaster_status = $request->input('bankmaster_status');
            


            $data['bankmaster']->save();

            if ($bankmaster_id > 0) {
                return response()->json([
                    'status' => '1',
                    'msg' => 'Updated Successfully.'
                ]);
            } else {
                if($data['bankmaster']->bankmaster_id > 0){
                    $merchants = Merchant::where('merchant_isownbank','0')->get();
                    foreach($merchants as $m){
                        $merchantbankmaster = new Merchantbankmaster();
                        $merchantbankmaster->merchantbankmaster_merchant_id = $m->merchant_id;
                        $merchantbankmaster->merchantbankmaster_bankmaster_id = $data['bankmaster']->bankmaster_id;
                        $merchantbankmaster->merchantbankmaster_status = 1;
                        $merchantbankmaster->created_at = Carbon::now();
                        $merchantbankmaster->insertBy = Auth::user()->id;
                        $merchantbankmaster->save();
                    }

                    $bank = Bank::all();
                    foreach($bank as $b){
                        $bankapplyto = new Bankapplyto();
                        $bankapplyto->bankapplyto_bankmaster_id = $data['bankmaster']->bankmaster_id;
                        $bankapplyto->bankapplyto_bank_id = $b->bank_id;
                        $bankapplyto->save();
                    }
                }
                return response()->json([
                    'status' => '1',
                    'msg' => 'Created Successfully.'
                ]);
            }
        } else {
            return response()->json([
                'status' => '0',
                'msg' => 'Somethig went wrong, please try again.',
            ]);
        }
    }
    public function listing(Request $request){
        if(!canAccess('view_bankmaster')){
            return response()->json([
                'status' => '0',
                'msg' => 'Permission Denied, if you think this is an error please contact our support.',
            ]);
        }
        $data['bankmaster'] = DB::table("db_bankmaster")
                                ->leftjoin('db_bank','db_bank.bank_id','=','db_bankmaster.bankmaster_bank_id')
                                ->whereIn('db_bankmaster.bankmaster_status',[1,2])
                                ->get();
        
        $data['bank_list2'] = Bank::where('bank_backend_status',1)->orderBy('bank_name','ASC')->get();                        
        $data['failled_msg'] = $request['failled_msg'];
        $data['success_msg'] = $request['success_msg'];
        return view('bankmaster.bankmasterlisting', ['data' => $data]);
    }
    public function delete(Request $request){
        if(!canAccess('delete_bankmaster')){
            return response()->json([
                'status' => '0',
                'msg' => 'Permission Denied, if you think this is an error please contact our support.',
            ]);
        }
        if($request['bankmaster_id']){
            $bankmaster_id = Crypt::decryptString($request['bankmaster_id']);
        }else{
            $bankmaster_id = 0;
        }
        if($bankmaster_id > 0){
            $bankmaster = BankMaster::find($bankmaster_id);
            $bankmaster->bankmaster_status = 0;
            $bankmaster->save();
            return redirect()->route('bankmaster', ['success_msg' => 'Deleted Successfully.']);
        }else{
            return response()->json([
                'status' => '0',
                'msg' => 'Permission Denied, if you think this is an error please contact our support.',
            ]);  
        }
    }
    public function updatebankMasterstatus(Request $request){
        if(!canAccess('edit_bankmaster')){
            return response()->json([
                'status' => '0',
                'msg' => 'Permission Denied, if you think this is an error please contact our support.',
            ]);
        }
        if($request['bankmaster_id']){
            $bankmaster_id = Crypt::decryptString($request['bankmaster_id']);
        }else{
            $bankmaster_id = 0;
        }

        if($bankmaster_id > 0){
            $bankmaster = BankMaster::find($bankmaster_id);
            $bankmaster->bankmaster_status = $request->bankmaster_status;
            $bankmaster->save();
            return response()->json([
                'status' => '1',
                'msg' => 'Updated Successfully.',
            ]); 
        }else{
            return response()->json([
                'status' => '0',
                'msg' => 'Record not found, if you think this is an error please contact our support.',
            ]);  
        }
    }

    public function updatebankmasterapplyto(Request $request){
        if(!canAccess('applyto_bankmaster')){
            return response()->json([
                'status' => '0',
                'msg' => 'Permission Denied, if you think this is an error please contact our support.',
            ]);
        }
        if($request['bankmaster_id']){
            $bankmaster_id = Crypt::decryptString($request['bankmaster_id']);
        }else{
            $bankmaster_id = 0;
        }
        if($request['bank_id']){
            $bank_id = Crypt::decryptString($request['bank_id']);
        }else{
            $bank_id = 0;
        }

        $ischeck = $request['ischeck'];

        if(($bankmaster_id > 0) && ($bank_id > 0)){
            $bankapplytocount = Bankapplyto::where('bankapplyto_bankmaster_id',$bankmaster_id)->where('bankapplyto_bank_id',$bank_id)->count();

            if($bankapplytocount > 0){
                Bankapplyto::where('bankapplyto_bankmaster_id',$bankmaster_id)->where('bankapplyto_bank_id',$bank_id)->delete();

            }else{
                $bankapplyto = new Bankapplyto();
                $bankapplyto->bankapplyto_bankmaster_id = $bankmaster_id;
                $bankapplyto->bankapplyto_bank_id = $bank_id;
                $bankapplyto->save();
            }
            

            return response()->json([
                'status' => '1',
                'msg' => 'Updated Successfully.',
            ]); 
        }else{
            return response()->json([
                'status' => '0',
                'msg' => 'Record not found, if you think this is an error please contact our support.',
            ]);  
        }
    }
}
