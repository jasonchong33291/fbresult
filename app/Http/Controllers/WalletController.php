<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Crypt;
use App\Models\Wallet;
use Carbon\Carbon;

class WalletController extends Controller
{
    public function createForm(Request $request){
        if(!canAccess('create_wallet')){
            return response()->json([
                'status' => '0',
                'msg' => 'Permission Denied, if you think this is an error please contact our support.',
            ]);
        }
        if($request['wallet_id']){
            $wallet_id = Crypt::decryptString($request['wallet_id']);
        }else{
            $wallet_id = 0;
        }
        if ($wallet_id > 0) {
            $data['wallet'] = DB::table("db_wallet")->where("wallet_id", $wallet_id)->get()->first();
        } else {
            $data['wallet'] = new Wallet();
        }

        return view('wallet.walletform', ['data' => $data]);
    }
    public function create(Request $request){
        if(!canAccess('create_wallet')){
            return response()->json([
                'status' => '0',
                'msg' => 'Permission Denied, if you think this is an error please contact our support.',
            ]);
        }
        if ($request->isMethod('post')) {

            if($request['wallet_id']){
                $wallet_id = Crypt::decryptString($request['wallet_id']);
            }else{
                $wallet_id = 0;
            }
            if ($wallet_id > 0) {
                $data['wallet'] = Wallet::find($wallet_id);
                $data['wallet']->updated_at = Carbon::now();
                $data['wallet']->updateBy = Auth::user()->id;
            } else {
                $data['wallet'] = new Wallet();
                $data['wallet']->created_at = Carbon::now();
                $data['wallet']->insertBy = Auth::user()->id;
            }


            $data['wallet']->wallet_code = $request->input('wallet_code');
            $data['wallet']->wallet_name = $request->input('wallet_name');
            $data['wallet']->wallet_seq = $request->input('wallet_seq');
            $data['wallet']->wallet_status = $request->input('wallet_status');
            
            $data['wallet']->updateBy = Auth::user()->id;
            $data['wallet']->insertBy = Auth::user()->id;


            $data['wallet']->save();

            if ($wallet_id > 0) {
                return response()->json([
                    'status' => '1',
                    'msg' => 'Updated Successfully.'
                ]);
            } else {
                return response()->json([
                    'status' => '1',
                    'msg' => 'Created Successfully.'
                ]);
            }
        } else {
            return response()->json([
                'status' => '0',
                'msg' => 'Somethig went wrong, please try again.',
            ]);
        }
    }
    public function listing(Request $request){
        if(!canAccess('view_wallet')){
            return response()->json([
                'status' => '0',
                'msg' => 'Permission Denied, if you think this is an error please contact our support.',
            ]);
        }
        $data['wallet'] = DB::table("db_wallet")->whereIn('wallet_status',[1,2])->get();
        $data['failled_msg'] = $request['failled_msg'];
        $data['success_msg'] = $request['success_msg'];
        return view('wallet.walletlisting', ['data' => $data]);
    }

    public function delete(Request $request){
        if(!canAccess('delete_wallet')){
            return response()->json([
                'status' => '0',
                'msg' => 'Permission Denied, if you think this is an error please contact our support.',
            ]);
        }
        if($request['wallet_id']){
            $wallet_id = Crypt::decryptString($request['wallet_id']);
        }else{
            $wallet_id = 0;
        }
        if($wallet_id > 0){
            $wallet = Wallet::find($wallet_id);
            $wallet->wallet_status = 0;
            $wallet->save();
            return redirect()->route('wallet', ['success_msg' => 'Deleted Successfully.']);
        }else{
            return response()->json([
                'status' => '0',
                'msg' => 'Permission Denied, if you think this is an error please contact our support.',
            ]);  
        }
    }
    public function updatewalletstatus(Request $request){
        if(!canAccess('edit_wallet')){
            return response()->json([
                'status' => '0',
                'msg' => 'Permission Denied, if you think this is an error please contact our support.',
            ]);
        }
        if($request['wallet_id']){
            $wallet_id = Crypt::decryptString($request['wallet_id']);
        }else{
            $wallet_id = 0;
        }

        if($wallet_id > 0){
            $wallet = Wallet::find($wallet_id);
            $wallet->wallet_status = $request->wallet_status;
            $wallet->save();
            return response()->json([
                'status' => '1',
                'msg' => 'Updated Successfully.',
            ]); 
        }else{
            return response()->json([
                'status' => '0',
                'msg' => 'Record not found, if you think this is an error please contact our support.',
            ]);  
        }
    }
}
