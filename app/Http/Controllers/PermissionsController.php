<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\HttpClient;
use Illuminate\Support\Facades\Http;
use App\Models\Role;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class PermissionsController extends Controller
{
    public function __construct()
    {

    }

    private function getUserGroupPermissions(){
        $checkAuthRole = auth()->user()->roles()->first()->name;

        if ($checkAuthRole == 'Super-Admin') {
            $user_groups = Role::all();
        } else {
            $user_groups = Role::with('permissions')->where('hidden', 0)->get();
        }
        return $this->successResponse($user_groups);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    private function showInfo($user_group_id)
    {
        $checkAuthRole = auth()->user()->roles()->first()->name;

        $role = Role::find($user_group_id);
        
        if ($checkAuthRole == 'Super-Admin') {
            if ($role) {
                $role = Role::find($user_group_id);
                $role->permissions = $role->getAllPermissions();

                return $this->successResponse($role);
            }
        } else {
            // check if user group hidden field is 0, if yes then return user group data
            if ($role->hidden == 0) {
                $role = Role::find($user_group_id);
                $role->permissions = $role->getAllPermissions();

                return $this->successResponse($role);
            } else {
                return $this->errorResponse('User Group not found', 404);
            }
        }

        return $this->errorResponse('User group not found', 404);
    }



    public function permissionslisting()
    {
        $logged_user = Auth::user();
        dd($logged_user);
        $user_groups = $this->getUserGroupPermissions();
       
        $data['user_groups'] = isset($user_groups->data) ? $user_groups->data : array();
     
        return view('users.userGroups', [
            'data'        => $data,
        ]);
    }

    public function create()
    {
        $data['edit'] = false;

        $user_groups = $this->getUserGroupPermissions();

        $response_body = [
            "user_group" => [
                "user_group_id" => 0,
                "name" => '',
            ]
        ];

        $data['user_group'] = (object)$response_body["user_group"];

        // user group permission
        $this->permissions = HttpClient::get('permissions');
   
        $all_permissions = [];
      
        foreach ($this->permissions->data->permission as $key => $permissions_data) {
            $all_permissions[$permissions_data->group][] = $permissions_data;
        }

        $data['all_permissions'] = $all_permissions;

        $data['action'] = route('admin.user_groups.store');

        return view('admin_argon.users.userGroupsForm', [
            'data'        => $data,
        ]);
    }

    public function store(Request $request)
    {   
        $user_group_data = array(
            'name'          => $request->name,
            'permission'    => $request->permission,
        );
        
        $response =  HttpClient::post('userGroup/add', $user_group_data);
     
        if ($response->status == "Error") {
            return response()->json(array('status'=>-1,'msg'=> $response->message ));
        } else {
            return response()->json(array('status'=>1,'msg'=>'User Group created successfully!'));
        }
    }


    public function edit($id)
    {

        $user_group = null; 

        $response = $this->showInfo($id);
       
        $user_group_info = $response;
        $user_permissions = $response->data->permissions;

        $response_body = [
            "user_group_id"       => $user_group_info->data->id,
            "name"      => $user_group_info->data->name,
            "permissions"    => $user_group_info->data->permissions,
        ];
        // dd($response_body);
        $data['user_group'] = (object)$response_body;

        $user_permissions_name = array();

        foreach($user_permissions as $user_permission){
            $user_permissions_name[] = $user_permission->name;
        }

        // user group permission
        $this->all_permissions = HttpClient::get('permissions');
        $all_permissions = [];

        foreach ($this->all_permissions->data->permission as $key => $permissions_data) {
            if(in_array($permissions_data->name ,$user_permissions_name)){
                $this->all_permissions->data->permission[$key]->checked = true;
            }else{
                $this->all_permissions->data->permission[$key]->checked = false;
            }

            $all_permissions[$permissions_data->group][] = $permissions_data;
        }

        $data['all_permissions'] = $all_permissions;
        $data['edit'] = true;
        $data['action'] = route('admin.user_groups.update', ['user_group_id' => $id]);

        return view('admin_argon.users.userGroupsForm', [
            'data'    => $data,
        ]);
    }

    public function update(Request $request, $id)
    {
        $user_group = null;

        $post_data = array(
            'name'      => $request->name,
            'permission' => $request->permission
        );

        // dd($post_data);

        $response_body =  HttpClient::post("userGroup/edit/{$id}", $post_data);
        
        // dd($response_body);

        if ($response_body->status == "Error") {
            return response()->json(array('status'=>0,'msg'=> $response_body->message ));
        }
        return response()->json(array('status'=>1,'msg'=>'You have modified the user group successfully!'));
    }

    public function destroy($id)
    {
        $response = Http::withToken($this->user_login_info->access_token)->baseUrl(config('app.url'))
        ->delete("api/userGroup/{$id}");
 
        $response_body = $response->object();
  
        if ($response_body->status == "Error") {
            return redirect()->route('admin.user_groups', ['user_group_id' => $id])->with('error', 'Failed to delete user group!');
        }

        return $response_body;
    }
    public function bulk_destroy(Request $request)
    {
        if(!isset($request->selected)){
            return response()->json(array('status'=>0,'msg'=> 'Error: Please select at least one user group!' ));
        }

        $selected_user_group_array = array();
        foreach ($request->selected as $key => $user_group) {
            $selected_user_group_array['user_group_id'][$key] = $user_group; 
        }

        $response = HttpClient::post("userGroup/massDelete", $selected_user_group_array );

        if ($response->status == "Error") {
            return response()->json(array('status'=>-1,'msg'=> $response->message ));
        } else {
            return response()->json(array('status'=>1,'msg'=>'User Group deleted successfully!'));
        }
    }
}
