<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Crypt;
use App\Models\BankMasterQR;
use App\Models\Bank;
use App\Models\Merchant;
use App\Models\BankMasterQRMerchant;
use Carbon\Carbon;
use URL;

class BankMasterQRController extends Controller
{
    public function createForm(Request $request){

        $data['bank_list'] = Bank::where('bank_status',1)->orderBy('bank_seq','ASC')->get();
        $data['merchant'] = Merchant::whereIn('merchant_status',[1,2])->orderBy('merchant_name','ASC')->get();
        $data['bankmasterqrmerchant'] = [];
         
        if($request['bankmasterqr_id']){
            $bankmasterqr_id = Crypt::decryptString($request['bankmasterqr_id']);
        }else{
            $bankmasterqr_id = 0;
        }
        
        if ($bankmasterqr_id > 0) {
            if(!canAccess('edit_bankmasterqr')){
                return response()->json([
                    'status' => '0',
                    'msg' => 'Permission Denied, if you think this is an error please contact our support.',
                ]);
            }
            $data['bankmasterqrmerchant'] = BankMasterQRMerchant::where('bankmasterqrmerchant_bankmasterqr_id',$bankmasterqr_id)->get();
            $data['bankmasterqr'] = DB::table("db_bankmasterqr")->where("bankmasterqr_id", $bankmasterqr_id)->get()->first();
            // $bankmasterqrmerchant = collect($data['bankmasterqrmerchant']);
            // dd($bankmasterqrmerchant->where('bankmasterqrmerchant_merchant_id',24)->count());
            $data['edit'] = true;
        } else {
            if(!canAccess('create_bankmasterqr')){
                return response()->json([
                    'status' => '0',
                    'msg' => 'Permission Denied, if you think this is an error please contact our support.',
                ]);
            }
            $data['bankmasterqr'] = new BankMasterQR();
            $data['edit'] = false;
        }

        return view('bankmasterqr.bankmasterqrform', ['data' => $data]);
    }
    public function create(Request $request){

        if ($request->isMethod('post')) {

            if($request['bankmasterqr_id']){
                $bankmasterqr_id = Crypt::decryptString($request['bankmasterqr_id']);
            }else{
                $bankmasterqr_id = 0;
            }
            if ($bankmasterqr_id > 0) {
                if(!canAccess('create_bankmasterqr')){
                    return response()->json([
                        'status' => '0',
                        'msg' => 'Permission Denied, if you think this is an error please contact our support.',
                    ]);
                }

                $data['bankmasterqr'] = BankMasterQR::find($bankmasterqr_id);
                $data['bankmasterqr']->updated_at = Carbon::now();
                $data['bankmasterqr']->updateBy = Auth::user()->id;
            } else {
                if(!canAccess('edit_bankmaster')){
                    return response()->json([
                        'status' => '0',
                        'msg' => 'Permission Denied, if you think this is an error please contact our support.',
                    ]);
                }

                $data['bankmasterqr'] = new BankMasterQR();
                $data['bankmasterqr']->created_at = Carbon::now();
                $data['bankmasterqr']->insertBy = Auth::user()->id;
            }

            
            $data['bankmasterqr']->bankmasterqr_name  = $request->input('bankmasterqr_name');
            $data['bankmasterqr']->bankmasterqr_bank_id  = $request->input('bankmasterqr_bank_id');
            $data['bankmasterqr']->bankmasterqr_name = $request->input('bankmasterqr_name');
            $data['bankmasterqr']->bankmasterqr_status = $request->input('bankmasterqr_status');
            $data['bankmasterqr']->bankmasterqr_username = $request->input('bankmasterqr_username');
            $data['bankmasterqr']->bankmasterqr_password = $request->input('bankmasterqr_password');
            
            $data['bankmasterqr']->save();

            $update_status = DB::table('db_bankmasterqrmerchant')
                                ->where('bankmasterqrmerchant_bankmasterqr_id', $data['bankmasterqr']->bankmasterqr_id)
                                ->update(['bankmasterqrmerchant_status' => 0]);

            if(isset($request['bankmasterqr_merchant']) && sizeof($request['bankmasterqr_merchant']) > 0){
                foreach($request['bankmasterqr_merchant'] as $i => $merchant_id){
                    $bankmasterqrmerchant = new BankMasterQRMerchant();
                    $bankmasterqrmerchant->bankmasterqrmerchant_merchant_id = Crypt::decryptString($merchant_id);
                    $bankmasterqrmerchant->bankmasterqrmerchant_bankmasterqr_id = $data['bankmasterqr']->bankmasterqr_id;
                    $bankmasterqrmerchant->bankmasterqrmerchant_status = 1;
                    $bankmasterqrmerchant->created_at = Carbon::now();
                    $bankmasterqrmerchant->insertBy = Auth::user()->id;
                    $bankmasterqrmerchant->save();
                }
            }

            if ($bankmasterqr_id > 0) {

                $this->uploadFile($bankmasterqr_id,$request);

                return response()->json([
                    'status' => '1',
                    'msg' => 'Updated Successfully.'
                ]);
            } else {

                $this->uploadFile($data['bankmasterqr']->bankmasterqr_id,$request);

                return response()->json([
                    'status' => '1',
                    'msg' => 'Created Successfully.'
                ]);
            }
        } else {
            return response()->json([
                'status' => '0',
                'msg' => 'Somethig went wrong, please try again.',
            ]);
        }
    }
    public function uploadFile($bankmasterqr_id = 0,$request){
        if($bankmasterqr_id > 0){
            $file = $request->file('bankmasterqr_file');
           
            if($file){
                
                $filename = explode('.',$file->hashName());
                
                $actual_name = $filename[0] . $bankmasterqr_id . "." . $filename[1];

                $path = URL::asset('uploads/qr/' . $actual_name);

                $bankmasterqr = BankMasterQR::find($bankmasterqr_id);
                $bankmasterqr->bankmasterqr_file = $path;
                $bankmasterqr->save();

                $file->move("uploads/qr",$actual_name);
            }
        }
    }
    public function listing(Request $request){
        if(!canAccess('view_bankmasterqr')){
            return response()->json([
                'status' => '0',
                'msg' => 'Permission Denied, if you think this is an error please contact our support.',
            ]);
        }
        $data['bankmasterqr'] = DB::table("db_bankmasterqr")
        ->leftjoin('db_bank','db_bank.bank_id','=','db_bankmasterqr.bankmasterqr_bank_id')
        ->whereIn('db_bankmasterqr.bankmasterqr_status',[1,2])
        ->get();
        $data['failled_msg'] = $request['failled_msg'];
        $data['success_msg'] = $request['success_msg'];
        return view('bankmasterqr.bankmasterqrlisting', ['data' => $data]);
    }
    public function delete(Request $request){
        if(!canAccess('delete_bankmasterqr')){
            return response()->json([
                'status' => '0',
                'msg' => 'Permission Denied, if you think this is an error please contact our support.',
            ]);
        }
        if($request['bankmasterqr_id']){
            $bankmasterqr_id = Crypt::decryptString($request['bankmasterqr_id']);
        }else{
            $bankmasterqr_id = 0;
        }
        if($bankmasterqr_id > 0){
            $bankmasterqr = BankMasterQR::find($bankmasterqr_id);
            $bankmasterqr->bankmasterqr_status = 0;
            $bankmasterqr->save();
            return redirect()->route('bankmasterqr', ['success_msg' => 'Deleted Successfully.']);
        }else{
            return response()->json([
                'status' => '0',
                'msg' => 'Permission Denied, if you think this is an error please contact our support.',
            ]);  
        }
    }
    public function updatebankmasterqrstatus(Request $request){
        if(!canAccess('edit_bankmasterqr')){
            return response()->json([
                'status' => '0',
                'msg' => 'Permission Denied, if you think this is an error please contact our support.',
            ]);
        }
        if($request['bankmasterqr_id']){
            $bankmasterqr_id = Crypt::decryptString($request['bankmasterqr_id']);
        }else{
            $bankmasterqr_id = 0;
        }

        if($bankmasterqr_id > 0){
            $bankmasterqr = BankMasterQR::find($bankmasterqr_id);
            $bankmasterqr->bankmasterqr_status = $request->bankmasterqr_status;
            $bankmasterqr->save();
            return response()->json([
                'status' => '1',
                'msg' => 'Updated Successfully.',
            ]); 
        }else{
            return response()->json([
                'status' => '0',
                'msg' => 'Record not found, if you think this is an error please contact our support.',
            ]);  
        }
    }
}
