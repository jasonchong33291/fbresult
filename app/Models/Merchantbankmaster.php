<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Merchantbankmaster extends Model
{
    protected $table = 'db_merchantbankmaster';
    protected $primaryKey = 'merchantbankmaster_id';
    use HasFactory;
}
