<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Wallet extends Model
{
    protected $table = 'db_wallet';
    protected $primaryKey = 'wallet_id';
    use HasFactory;
}
