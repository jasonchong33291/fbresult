<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BankMasterQR extends Model
{
    protected $table = 'db_bankmasterqr';
    protected $primaryKey = 'bankmasterqr_id';
    use HasFactory;
}
