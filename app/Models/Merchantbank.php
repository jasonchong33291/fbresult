<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Merchantbank extends Model
{
    protected $table = 'db_merchantbank';
    protected $primaryKey = 'merchantbank_id';
    use HasFactory;
}
