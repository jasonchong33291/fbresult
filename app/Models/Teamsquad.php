<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Teamsquad extends Model
{
    protected $table = 'db_teamsquad';
    protected $primaryKey = 'teamsquad_id';
    use HasFactory;
}
