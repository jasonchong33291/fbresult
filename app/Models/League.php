<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class League extends Model
{
    protected $table = 'db_league';
    protected $primaryKey = 'league_id';
    use HasFactory;
}
