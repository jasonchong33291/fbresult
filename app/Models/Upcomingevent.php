<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Upcomingevent extends Model
{
    protected $table = 'db_upcomingevent';
    protected $primaryKey = 'upcomingevent_id';
    use HasFactory;
}
