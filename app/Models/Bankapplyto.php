<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Bankapplyto extends Model
{
    protected $table = 'db_bankapplyto';
    protected $primaryKey = 'bankapplyto_id';
    use HasFactory;
}
