<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Merchantsettlementbank extends Model
{
    protected $table = 'db_merchantsettlementbank';
    protected $primaryKey = 'merchantsettlementbank_id';
    use HasFactory;
}
