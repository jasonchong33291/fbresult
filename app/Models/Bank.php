<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Bank extends Model
{
    protected $table = 'db_bank';
    protected $primaryKey = 'bank_id';
    use HasFactory;
}
