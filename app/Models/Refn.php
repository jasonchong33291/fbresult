<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Refn extends Model
{
    protected $table = 'refn';
    protected $primaryKey = 'refn_id';
    use HasFactory;
}
