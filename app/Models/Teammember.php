<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Teammember extends Model
{
    protected $table = 'db_teammember';
    protected $primaryKey = 'teammember_id';
    use HasFactory;
}
