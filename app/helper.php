<?php

use App\Models\Refn;
use Illuminate\Support\Facades\Auth;
use App\Models\Role;
use Illuminate\Http\Request;
use PHPMailer\PHPMailer\PHPMailer;

function num_format($sIn)
{
    return number_format($sIn, 2, ".", ",");
}
function format_date($datetime, $separator = "/")
{
    if ((strcasecmp($datetime, "0000-00-00") == 0) || (strcasecmp($datetime, "0000-00-00 00:00:00") == 0) || $datetime == "") {
        return "";
    } else {

        if (substr_count($datetime, "-") >= 2) {
            $timestamp = get_timestamp($datetime);
        } else {
            $timestamp = $datetime;
        }
        return date("d" . $separator . "m" . $separator . "Y", $timestamp);
    }
}
function format_date_database($datetime)
{
    $timestamp = strtotime(str_replace("/", "-", $datetime));
    $new_date_format = date('Y-m-d', $timestamp);
    if (($new_date_format == '1970-01-01') || ($new_date_format == '')) {
        return null;
    } else {
        return $new_date_format;
    }
}
function get_timestamp($datetime)
{
    $arr_datetime = explode(" ", $datetime);
    if (sizeof($arr_datetime) >= 2) {
        $arr_time = explode(":", $arr_datetime[1]);
        if (sizeof($arr_time) >= 3) {
            $hour = $arr_time[0];
            $minute = $arr_time[1];
            $second = $arr_time[2];
        } else {
            $hour = 0;
            $minute = 0;
            $second = 0;
        }
        $arr_date = explode("-", $arr_datetime[0]);
    } else {
        $hour = 0;
        $minute = 0;
        $second = 0;
        $arr_date = explode("-", $datetime);
    }
    $timestamp = mktime($hour, $minute, $second, $arr_date[1], $arr_date[2], $arr_date[0]);
    return $timestamp;
}
function getMandatory()
{
    return "<font color = 'red'>*</font>";
}
function getRefno($refn_name, $use = false, $document_date)
{
    $refn = Refn::where('refn_name', $refn_name)->get()->first();

    $return_value = $refn->refn_value;
    $current_value = $refn->refn_value;
    $refn_length = $refn->refn_length;
    $refn_prefix = $refn->refn_prefix;


    $new_value = intval(intval($current_value) + 1);

    if ($refn_name == 'Register') {
        $year = date("y", strtotime($document_date));
        $month = date("m", strtotime($document_date));
        $return_value = $refn_prefix . $year . $month . str_pad($return_value, $refn_length, "0", STR_PAD_LEFT);
    } else {
        $return_value = $refn_prefix . date("y", strtotime($document_date)) . str_pad($return_value, $refn_length, "0", STR_PAD_LEFT);
    }
    if ($use) {
        $refn->refn_value = $new_value;
        $refn->save();
    }
    return $return_value;
}

function canAccess($module_name)
{
    $permissions = Role::find(Auth::user()->user_group_id)->getPermissionNames();
    if (in_array($module_name, json_decode($permissions)) || Auth::user()->user_group_id == 1) {
        return true;
    } else {
        return false;
    }
}

function debugsql($sql){

    $query = str_replace(array('?'), array('\'%s\''), $sql->toSql());
    $query = vsprintf($query, $sql->getBindings());       
   
    dd($query);
}
function validEmail($str) {
    return (!preg_match("/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix", $str)) ? FALSE : TRUE;
}